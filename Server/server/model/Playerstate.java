package server.model;

public enum Playerstate{
	 DO_NOTHING("do nothing"),
	 ROLL_DICE("roll dice"),
	 USE_SECRET("use secret passage"),
	 MOVE("move"),
	 SUSPECT("suspect"),
	 DISPROVE("disprove"),
	 ACCUSE("accuse"),
	 END_TURN("end turn"),
	 TELEPORT("teleport"),
	;

   private final String text;

   private Playerstate(final String text) {
       this.text = text;
   }
   
   public boolean equal(String s){
   	if(s==null)
   		return false;
   	return text.equals(s);
   }

   @Override
   public String toString() {
       return text;
   }
   
   public static Playerstate getPlayerstate(String s){
	   for(Playerstate state : Playerstate.values()){
		   if(state.equal(s)){
			   return state;
		   }
	   }
	   return null;
   }
}
	