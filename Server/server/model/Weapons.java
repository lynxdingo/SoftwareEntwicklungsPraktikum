package server.model;

public enum Weapons implements Card{
	DAGGER("dagger", 2, new Key(3, 2)),
	CANDLESTICK("candlestick", 5, new Key(20, 2)),
	REVOLVER("revolver", 0, new Key(11, 20)),
	ROPE("rope", 1, new Key(3, 21)),
	PIPE("pipe", 7, new Key(11, 4)),
	SPANNER("spanner", 4, new Key(21, 21)),

	;

  
	private final String text;
    private final int pos;
    private final Key field;

    private Weapons(final String text, int pos, Key field) {
        this.text = text;
        this.pos = pos;
        this.field = field;
    }
    
    public boolean equal(String s){
    	if(s==null)
    		return false;
    	return text.equals(s);
    }

    @Override
    public String toString() {
        return text;
	    }
    public int getPosition(){
    	return pos;
    }
    public Key getStart(){
 	   return field;
    }
    public static Weapons getWeapon(String s){
		   Weapons weapon = null;
			for(Weapons w : Weapons.values()){
				if(w.equal(s)){
					weapon = w;
					break;
				}
			}
			return weapon;
			
	   	}
    public String getText() {
  		return text;
  	}
}
