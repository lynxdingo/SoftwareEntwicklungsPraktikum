package server.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Door extends Field {
	char direction;
	private Room room;
	public Door(int x, int y, char direction, Room room) {
		super(x, y);
		this.direction = direction;
		this.room = room;
	}
	
	@Override
	public void checkNeighbour(Map<Key, Field> fields) {
		// Only one Neighbor the one in which the direction goes
		int yDelta = 0;
		int xDelta = 0;
		switch(direction){
		case 's': 	yDelta++;
					break;
		case 'w': 	xDelta--;
					break;
		case 'n': 	yDelta--;
					break;
		case 'o': 	xDelta++;
					break;
			
		}
		neighbours.add(fields.get(new Key(x+xDelta, y+yDelta)));
	}
	
	@Override
	public List<Field> getFields(int roll){
		List<Field> ret = new ArrayList<Field>();
		ret.add(this);
		return ret;
	}
	
	public List<Field> getFields(int roll, boolean start){
		if(!start){
			return getFields(roll);
		}
		return neighbours.get(0).getFields(roll-1);
	}
	@Override
	public Room getRoom(){
		return room;
	}
	public char getDirection(){
		return direction;
	}
	 
}
