package server.model;

public enum Gamestate {
	NOT_STARTED("not started"),
	STARTED("started"),
	ENDED("ended")
	;

    private final String text;

    private Gamestate(final String text) {
        this.text = text;
    }
    
    public boolean equal(String s){
    	if(s==null)
    		return false;
    	return text.equals(s);
    }

    @Override
    public String toString() {
        return text;
	    }
    
    public static Gamestate getGamestate(String s){
    	for(Gamestate state : Gamestate.values()){
    		if(state.equal(s)){
    			return state;
    		}
    	}
    	return null;
    }
}
