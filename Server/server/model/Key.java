package server.model;

import org.json.JSONObject;

public class Key {
	private final int x, y;
	
	public Key(int x, int y){
		this.x = x;
		this.y = y;
	}
	public Key(double d, double e) {
		this.x = (int) d;
		this.y = (int) e;
	}
	@Override
	public boolean equals(Object o){
		if(this == o) 
			return true;
		if(!(o instanceof Key)) 
			return false;
		Key key = (Key) o;
		return x == key.x && y == key.y;
	}
	
	@Override
	public int hashCode() {
		return 23 * x + y;
	}

	public JSONObject getPosition(){
		JSONObject ret = new JSONObject();
		ret.put("x", x);
		ret.put("y", y);
		return ret;
	}
	public int getX(){
		return x;
	}
	public int getY(){
		return y;
	}
}
