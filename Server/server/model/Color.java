package server.model;

public enum Color implements Card{
	 RED("red", new Key(16, 0), 2),
	 YELLOW("yellow", new Key(23, 7), 0),
	 WHITE("white", new Key(14, 24), 4),
	 GREEN("green", new Key(9, 24), 3),
	 BLUE("blue", new Key(0, 18), 1),
	 PURPLE("purple", new Key(0, 5), 5)
	;

   private final String text;
   private final Key field;
   private final int position;

   
   private Color(final String text, Key startPosition, int position) {
       this.text = text;
       this.field = startPosition;
       this.position = position;
   }
   


public boolean equal(String s){
   	if(s==null)
   		return false;
   	return text.equals(s);
   }

   @Override
   public String toString() {
       return text;
   }
   public int getPosition(){
	   return position;
   }
   
   public static Color getColor(String s){
	   Color color = null;
		for(Color c : Color.values()){
			if(c.equal(s)){
				color = c;
				break;
			}
		}
		return color;
		
   	}
   public Key getStart(){
	   return field;
   }

}