package server.model;

public interface Card {
	 public boolean equal(String s);
	 public String toString();
	 public static Card getCard(String s){
		if(Color.getColor(s)!=null){
			return Color.getColor(s);
		}else if(Room.getRoom(s)!=null){
			return Room.getRoom(s);
		}else {
			return Weapons.getWeapon(s);
		}
	}
   public int getPosition();
}
