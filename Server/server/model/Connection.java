package server.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.json.JSONObject;

import server.code.ConnectionHandler;

public class Connection {
	private ConnectionHandler thread;
	private boolean inGame = false;
	private boolean connected = true;
	private String nick = "";
	private String ip;
	private int port;
	private List<String> compatibleExtensions = new ArrayList<String>();
	//private List<String> jsonObjects = new ArrayList<String>();
	private ObservableList<String> jsonObjects = FXCollections.observableList(new ArrayList<String>());
	//private List<Game> games = new ArrayList<Game>();
	private ObservableList<Game> games = FXCollections.observableList(new ArrayList<Game>());
	
	public ObservableList<Game> getGames(){
		return games;
	}
	public Connection(ConnectionHandler thread){
		jsonObjects = FXCollections.observableArrayList();
		this.thread = thread;
	}
	
	public ConnectionHandler getConnectionHandler(){
		return thread;
	}

	public boolean isConnected() {
		return connected;
	}

	public void setConnected(boolean connected) {
		this.connected = connected;
	}

	public boolean isInGame() {
		return inGame;
	}

	public void setInGame(boolean inGame) {
		this.inGame = inGame;
	}
	public List<String> getCompatibleExtensions(){
		return compatibleExtensions;
	}
	public ObservableList<String> getJsonObjects(){
		return jsonObjects;
	}
	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}
	public void addSendJSON(JSONObject obj){
		jsonObjects.add("Send: "+obj.toString());
	}
	public void addReceivedJSON(JSONObject obj){
		jsonObjects.add("Received: " + obj.toString());
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	

	
	
}
