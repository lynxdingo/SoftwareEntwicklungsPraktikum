package server.model;

import org.json.JSONObject;

public class Person {
	private Color color;
	private Key field;
	private Player player;
	private boolean isPlayer = false;
	public Person(Color color){
		isPlayer = false;
		this.color = color;
		field = color.getStart();
		
	}
	public Person(Player player){
		this.player = player;
		if(player != null){
			isPlayer = true;
			color = player.getColor();
			field = color.getStart();
		}
	}
	public JSONObject getPositon() {
		JSONObject ret = new JSONObject();
		ret.put("person", color.toString());
		ret.put("field", field.getPosition());
		return ret;
	}
	public Player getPlayer(){
		return player;
	}
	public boolean isPlayer(){
		return isPlayer;
	}
	public Color getColor(){
		return color;
	}
	public void setField(Key field){
		this.field = field;
	}
	public Key getField(){
		return field;
	}
}
