package server.model.gui;

import java.util.Comparator;

import server.model.Game;

public class GameStateSort implements Comparator<Game>{
	
	public int compare(Game o1, Game o2) {
		return o1.getGameState().toString().compareTo(o2.getGameState().toString()) ;
	}


}
