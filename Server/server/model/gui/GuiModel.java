package server.model.gui;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONObject;

import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.hardware.Memory;
import oshi.hardware.PowerSource;
import oshi.hardware.Processor;
import oshi.software.os.OSFileStore;
import oshi.software.os.OperatingSystem;
import oshi.software.os.OperatingSystemVersion;
import oshi.util.FormatUtil;
import oshi.util.Util;
import server.model.Model;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class GuiModel {
	private String host = "";
	private String ip = "";
	private String up_since = "";
	private String lastSent = "";
	private String lastReceived = "";
	private InetAddress iAddress;
	private String operatingSystem = "";
	private String osVersion = "";
	private SystemInfo si = new SystemInfo();
	private HardwareAbstractionLayer hal = si.getHardware();
	private Memory memory = hal.getMemory();
	private long memTotal;
	private long memAvailable;
	private OSFileStore[] filePartitions;
	private Processor[] cpus;
	private PowerSource[] psource;
	private Message message;
	private Page page;
	private GameStateSort stateCompare;
	private ConnectionSort nickCompare;
	private GameSort idCompare;
	private Model model;
	
	 private final LongProperty bytesSent =
	            new SimpleLongProperty();
	 
	 public final long getBytesSent(){
	        return bytesSent.get();
	    }
	 
	  public final LongProperty BytesSentProperty(){
	        return bytesSent;
	    }

	    public final void setBytesSent(long sent){
	    	BytesSentProperty().set( getBytesSent() + sent);
	    }
	    
	    private final LongProperty bytesReceived =
	            new SimpleLongProperty();
	 
	 public final long getBytesReceived(){
	        return bytesReceived.get();
	    }
	 
	  public final LongProperty BytesReceivedProperty(){
	        return bytesReceived;
	    }

	    public final void setBytesReceived(long received){
	    	BytesReceivedProperty().set( getBytesReceived() + received);
	    }
	    
	    public GuiModel(Model model)
		{
			up_since = getTime();
			activateListeners();
			this.model = model;
			message = new Message();
			page = new Page(this.model);
			stateCompare = new GameStateSort();
			nickCompare = new ConnectionSort();
			idCompare = new GameSort();
			
		}
	    
	    public void activateListeners(){
	    	this.BytesSentProperty().addListener( 
	    			(observable,oldvalue,newvalue) -> 
	    				this.lastSent = getTime()
	    			);
	    	this.BytesReceivedProperty().addListener(
	    			(observable,oldvalue,newvalue) -> 
    				this.lastReceived = getTime()
    			);
	    }
	    
	    public String getLastSent() {
	    	return lastSent;
	    }
	    public String getLastReceived() {
	    	return lastReceived;
	    }
		
		public String getTime()
		{
			return LocalDateTime.now().toString();
		}
	    
		public void setFileSystem()
		{
			filePartitions = hal.getFileStores();
		}
		public OSFileStore[] getFileSystem()
		{
			return filePartitions ;
		}
		
		
		public void setCpus()
		{
			cpus = hal.getProcessors() ;
		}
		
		public Processor[] getCpus()
		{
			return cpus ;
		}

		public boolean hasBatteries(){
			return (hal.getPowerSources().length > 0);
		}
		
		public void setPowerSource(){
				psource = hal.getPowerSources();
		}
		
		public PowerSource[] getPowerSource()
		{
			return psource ;
		}
		

		public void setMemTotal()
		{
			
			memTotal = memory.getTotal();
		}
		
		public long getMemTotal() {
			return memTotal;
		}
		
		public void setmemAvailable() {
			memAvailable = memory.getAvailable();
		}
		
		public long getmemAvailable() {
			return memAvailable ;
		}
		
		
		public void setOS()
		{
			OperatingSystem os = si.getOperatingSystem();
			OperatingSystemVersion version = os.getVersion();
			operatingSystem = os.toString();
			osVersion = version.toString();
		}
		public String getOs()
		{
			return operatingSystem;
		}
		public String getOsVersion()
		{
			return osVersion;
		}
		
		
		
		public String getHostName()
		{
	        	return host;
		}
		
		public void setHostName()
		{
			 host = iAddress.getHostName();
		}
		
		public String getIp()
		{
			 return ip;
		}
		public InetAddress getInetAddress()
		{
			 return iAddress;
		}
		
		public void setIpAddress()
		{
			try {
				iAddress = InetAddress.getLocalHost() ;
				ip = iAddress.getHostAddress();
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public String getUp(){
			return up_since;
		}

		public void countBytesSent(JSONObject jobj) {
			long sent = jobj.toString().getBytes().length;
			setBytesSent(sent);
			
		}
		
		public void countBytesReceived(JSONObject jobj) {
			long received = jobj.toString().getBytes().length;
			setBytesReceived(received);
			
		}
		
		public Message getMessage(){
			return message;
		}
		public Page getPage(){
			return page;
		}
		public GameStateSort getStateCompare() {
			return stateCompare;
		}
		public ConnectionSort getNickCompare() {
			return nickCompare;
		}
		public GameSort getIdCompare() {
			return idCompare;
		}

}
