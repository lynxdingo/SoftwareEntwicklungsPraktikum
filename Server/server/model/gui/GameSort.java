package server.model.gui;

import java.util.Comparator;

import server.model.Connection;
import server.model.Game;

public class GameSort  implements Comparator<Game> {

	@Override
	public int compare(Game o1, Game o2) {
		return ((Integer) o1.getGameID()).compareTo((Integer) o2.getGameID() ); 
	}


}
