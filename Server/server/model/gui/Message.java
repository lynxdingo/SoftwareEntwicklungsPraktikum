package server.model.gui;


import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;


public class Message extends MyPageCreator{
		private String begSnt = "Send: ";
		private String begRcv = "Received: ";
		private String begSpan = "<span class=\"highlight\">";
		private String endSpan = "</span>";
		
		
			/**
			 * bound to webView
			 */
		    private final StringProperty messages =
		            new SimpleStringProperty(this, "text_from_jsonObjects_or_results", null);
		    
		    
		    public final StringProperty MessageProperty(){
		        return messages;
		    }

		    public final String getMessages(){
		        return messages.get();
		    }
		    

		    
		    public String getSubString(String pattern,String lowercaseText,String text)
		    {
		    	StringBuilder sb = new StringBuilder();
		    	int start = -1;
				int end  = -1;

				 for(int i = 0, n = lowercaseText.length() ; i < n ; i++) 
			    	{ 
			    	    char c = lowercaseText.charAt(i); 
			    	    if (c == pattern.charAt(0) )
			    	    {
			    	    	start = i;
			    	    	end = i + pattern.length() ;
			    	    	if (lowercaseText.substring(start, end).equals(pattern)  )
			    	    	{
			    	    		String old = text.substring(start, end);
			    	    		String newString = text.replace(old,createHTMLUnit(begSpan,old,endSpan));
			    	    	
			    	    		 int endnewString = getEndofSubstring(endSpan,newString);
			    	    		 if (endnewString > 0)
			    	    			{
			    	    			 	sb.append(newString, 0, endnewString);
			    	    			 	break;
			    	    			}
			    	    		}
			    	    }
			    	}
			    return sb.toString();
		    }
		    
		    public String getHighlightedMessage(String s,String searchText) {
		    	StringBuilder text = new StringBuilder(s);
		    	StringBuilder lowercaseText  = new StringBuilder(s.toLowerCase());
		    	StringBuilder sb = new StringBuilder();
		    	boolean found = false;
		    	while (lowercaseText.toString().contains(searchText)) { 
					found = true;
					sb.append(getSubString(searchText,lowercaseText.toString(),text.toString()));
				    int index = lowercaseText.indexOf(searchText); 
					int end = index + searchText.length();
					if (end > 0)
					{
						lowercaseText.delete(0, end);
						text.delete(0, end);
					}
				}
		    	if (found) {
		    		return sb.append(text).toString();
		    	}
		    	else {
		    		return sb.toString();
		    	}
		    	
		    }
		    
		    private String setClassOfRow(String s) {
		    	if (s.contains(begRcv)) {
		    		return getPStartRcv();
		    	} else {
		    		return getPStartSnt();
		    	}
		    }
		    
		    private String createBody(ObservableList<String> observableList)
			{
				StringBuilder sb = new StringBuilder();
				for (String s : observableList )
				{
					s = convertHTMLtoText( s);
						sb.append(createHTMLUnit(setClassOfRow(s), s,getPEnd()));
				}
				return sb.toString();
			}
		    
		    private String createBody(ObservableList<String> observableList,String searchText)
		    {
				StringBuilder sb = new StringBuilder();
				for (String s : observableList )
				{
					s = s.trim();
					s = convertHTMLtoText( s);
					
					searchText = convertHTMLtoText( searchText);
					
					String newMsg = getHighlightedMessage(s,searchText);
					if (!newMsg.isEmpty()) {
						sb.append(createHTMLUnit(setClassOfRow(s), newMsg,getPEnd()));
					}
				}
				return sb.toString();
			}
		    
		    private String getOutput(List<String> list)
			{
				StringBuilder sb = new StringBuilder();
				for (String s : list )
				{	
					sb.append(createHTMLUnit(getPStart(), s,getPEnd()));
				}
				
				return sb.toString();
			}
		    
		   /**
		    * display messages sent to and received by server 
		 * @param headline 
		 * @param searchText 
		    * @param list: ObservableList<String>
		    */
		    public final void setMessages(ObservableList<String> list, String headline, String searchText) {
		    	StringBuilder page = new StringBuilder(createBeginning(headline) );
		    	int searchLength = searchText.length();
	    		String allMessages = list.toString().trim().toLowerCase();
		    		if (allMessages.contains(searchText)) {
		    			page.append(createBody(list,searchText));
		    		}
		    		else {
		    			String error = "Your search for " +searchText+ " did not yield any result.";
		    			page.append(createError(error));
		    		}
	    		page.append(createEnd());
			    MessageProperty().set(page.toString());
	    		
	    }
		    
		    public final void setMessages(ObservableList<String> list, String headline) {
		    	
		    	StringBuilder page = new StringBuilder(createBeginning(headline) );
		    	page.append(createBody(list));
	    		page.append(createEnd());
			    MessageProperty().set(page.toString());
	    		
	    }

		public final void setMessages(List<String> outputList, String headline) {
			StringBuilder page = new StringBuilder(createBeginning(headline) );
    		page.append(getOutput(outputList));
    		page.append(createEnd());
		    MessageProperty().set(page.toString());
			
		}
}
