package server.model.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import oshi.hardware.PowerSource;
import oshi.hardware.Processor;
import oshi.software.os.OSFileStore;
import oshi.util.FormatUtil;
import server.model.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

public class Page extends MyPageCreator{
	private String trBegEven = "<tr class =\"even\">";
	private String trColor = "<tr class=\"mycolorgame";
	private Model model;
	
	/**
	 * bound to webView
	 */
    private final StringProperty htmlPage=
            new SimpleStringProperty(this, "text_to_create_tables", null);
    
    
    public Page(Model model) {
		this.model = model;
	}

	public final StringProperty PageProperty(){
        return htmlPage;
    }

    public final String getPage(){
        return htmlPage.get();
    }
    
	public String getColor(Game g,String nick) {
		String color = "gray";
		if (!g.getWatchers().isEmpty() && g.getWatchers().contains(nick) )
		{
			return color;
		}
		if (!g.getPlayers().isEmpty() ) {
			for (Player p: g.getPlayers())
			{
				if (p.getNick().equals(nick) ) {
					return p.getColor().toString();
					
				}
			}
		}
		return color;
	}
    
    private String setRowEvenOrOdd(int i){
    	if (i % 2 == 0) {
			 return trBegEven;
		}
    	else {
			return getTrBeg();
		}
    }
    
    private String createRowGamesList(String tr,int gameid,Gamestate state ){
    	StringBuilder row = new StringBuilder();
    	row.append(tr);
    	row.append(createHTMLUnit(getTdBeg(),gameid ,getTdEnd()));
    	row.append(createHTMLUnit(getTdBeg(),state ,getTdEnd()));
    	row.append(getTrEnd());
    	return row.toString();
    }
    
    private String createBodyOfConnectionList(ObservableList<Connection> list,String[] columnArr, String headline) {
    	int numberColumns = columnArr.length;
    	int numberRows = list.size();
    	
    	StringBuilder body = new StringBuilder( );
		for (int i = 0; i < numberRows; i++)
		{	
			body.append(setRowEvenOrOdd(i));
			for (int g = 0; g < numberColumns; g++ )
			{
				body.append(createHTMLUnit(getTdBeg(),list.get( i).getNick(),getTdEnd()));
				++i;
			}
			--i;
			body.append(getTrEnd());
			
		}
		return body.toString();
	}

    private Object createBodyOfConnectionList(ObservableList<Connection> list,
    		String[] columnArr, String headline, String searchText) {
    		int numberColumns = columnArr.length;
        	int numberRows = list.size();
        	int index = 0;
        	
        	StringBuilder body = new StringBuilder( );
    		for (int i = 0; i < numberRows; i++)
    		{	
    			String nick = list.get( i).getNick().trim().toLowerCase();
    			if (nick.contains(searchText) && nick.length() > 2 ) {
    			
    				body.append(setRowEvenOrOdd(index));
    				for (int g = 0; g < numberColumns; g++ )
    					{
    					body.append(createHTMLUnit(getTdBeg(),list.get( i).getNick(),getTdEnd()));
    					++i;
    					}
    				--i;
    				body.append(getTrEnd());
    				++index;
    				}
    			
    		}
    		if (index == 0) {
    			String error = "Your search for " +searchText+ " did not yield any result.";
    			body.append(createError(error,columnArr.length));
    		}
    		return body.toString();
    	}
  
	private String createBodyOfGamesList(String[] columns,	String headline, String searchText) {
		StringBuilder body = new StringBuilder();
		Map<Integer, Game> collect = model.getGames().entrySet()
		        .parallelStream()
		        .filter(e -> e.getValue().getGameState().toString() == searchText)
		        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		if (!collect.isEmpty() ) {
			int index = 0;
			for (Map.Entry<Integer, Game> entry : collect.entrySet())
			{
				body.append(createRowGamesList(setRowEvenOrOdd(index),entry.getKey(),entry.getValue().getGameState()));
				++index;
			}
		}
		else {
			String error = "Your search for " +searchText+ " did not yield any result.";
			body.append(createError(error,columns.length));
		}
		return body.toString();
	}
	
	private String createBodyOfGamesList(String[] columns,	String headline, int parseInt) {
		StringBuilder body = new StringBuilder();
		Game game = model.getGames().get(parseInt);
		
		if (game != null ) {
			body.append(createRowGamesList(setRowEvenOrOdd(0),parseInt,game.getGameState()));
		}
		else {
			String error = "Your search for " +parseInt+ " did not yield any result.";
			body.append(createError(error,columns.length));
		}
		return body.toString();
	}
	
	private String createBodyOfUserList(String[] columns,String headline, Connection conn,int parseInt) {
		StringBuilder body = new StringBuilder();
		Game game = model.getGames().get(parseInt);
		
		if (game != null ) {
			String color = getColor(game,conn.getNick() );
			String rowStart = trColor + color + getCloseTag2();
			body.append(createRowGamesList(rowStart,parseInt,game.getGameState()));
		}
		else {
			String error = "Your search for " +parseInt+ " did not yield any result.";
			body.append(createError(error,columns.length));
		}
		return body.toString();
	}
	
	private String createBodyOfUserList(String[] columns, String headline,Connection conn,String searchText) {
		List<Game> list = conn.getGames();
		int numberRows = list.size();
		StringBuilder body = new StringBuilder( );
		boolean found = false;
		for (int i = 0; i < numberRows; i++)
		{		
			    Game game = model.getGames().get(list.get(i).getGameID());
			    if (game.getGameState().toString().equals(searchText)) 
			    {
			    	String color = getColor(game,conn.getNick() );
					int gameid = list.get( i).getGameID();
					Gamestate state = list.get( i).getGameState();
					String rowStart = trColor + color + getCloseTag2();
					body.append(createRowGamesList(rowStart,gameid,state));
					found = true;
			    }
		}
		if (!found) {
			String error = "Your search for " +searchText+ " did not yield any result.";
			body.append(createError(error,columns.length));
		}
		return body.toString();
	}
	
	  private String createBodyOfUserList(String[] columns, String headline,Connection conn) {
		  	List<Game> userGames = conn.getGames();
			int numberRows = userGames.size();
			StringBuilder body = new StringBuilder();
			for (int i = 0; i < numberRows; i++)
			{		
				    Game game = model.getGames().get(userGames.get(i).getGameID());
					String color = getColor(game,conn.getNick() );
					int gameid = userGames.get( i).getGameID();
					Gamestate state = userGames.get( i).getGameState();
					String rowStart = trColor + color + getCloseTag2();
					body.append(createRowGamesList(rowStart,gameid,state));
			}
			return body.toString();
			
		}
	  
	
	private String createError(String error, int columns) {
		StringBuilder errorMessage = new StringBuilder(trColor);
		errorMessage.append(getTrBeg());
		errorMessage.append("red");
		errorMessage.append(getCloseTag2());
		String tdBeg = "<td colspan=\"" + columns + getCloseTag2();
		errorMessage.append(createHTMLUnit(tdBeg,error,getTdEnd()));
		errorMessage.append(getTrEnd());
		return errorMessage.toString();
	}

	public final void setPage(String[] header, ArrayList<String> row, String headline) {
		int numberColumns = header.length;
		StringBuilder page = new StringBuilder(createBeginning(header,headline) );
		page.append(getTrBeg());
		for (String s : row)
		{
			page.append(createHTMLUnit(getTdBeg(), s,getTdEnd()));
		}
		page.append(getTrEnd());
		page.append(createEndOfTable());
		PageProperty().set(page.toString());
		
	}

	public final void setPage(String[] header, OSFileStore[] fileSystem,String headline) {
    	int numberRows = fileSystem.length;
    	StringBuilder page = new StringBuilder(createBeginning(header,headline) );
    	
    	for (int i = 0; i < numberRows; i++)
		{	
    			page.append(setRowEvenOrOdd(i));
				long usable = fileSystem[i].getUsableSpace();
				long total = fileSystem[i].getTotalSpace();
				
				if (fileSystem[i].getDescription().isEmpty()) {
					page.append(createHTMLUnit(getTdBeg(),"file system",getTdEnd()));
				}
				else{
					page.append(createHTMLUnit(getTdBeg(),fileSystem[i].getDescription(),getTdEnd()));
				}
				page.append(createHTMLUnit(getTdBeg(),FormatUtil.formatBytes(usable),getTdEnd()));
				page.append(createHTMLUnit(getTdBeg(),FormatUtil.formatBytes(total),getTdEnd()));
				page.append(createHTMLUnit(getTdBeg(),String.format("%.1f%%", 100d * usable / total),getTdEnd()));
				page.append(getTrEnd());
			}
			page.append(createEndOfTable());
			PageProperty().set(page.toString());
			
		}

	public final void setPage(String[] header, Processor[] cpus, String headline) {
		int numberRows = cpus.length;
		StringBuilder page = new StringBuilder(createBeginning(header,headline) );
		for (int i = 0; i < numberRows; i++)
		{	
				page.append(setRowEvenOrOdd(i));
				page.append(createHTMLUnit(getTdBeg(),cpus[i].getIdentifier(),getTdEnd()));
				page.append(createHTMLUnit(getTdBeg(),cpus[i],getTdEnd()));
				page.append(createHTMLUnit(getTdBeg(),String.format(" %.1f%%",cpus[i].getProcessorCpuLoadBetweenTicks() * 100),getTdEnd()));
				page.append(getTrEnd());
			}
			
			page.append(createEndOfTable());
			PageProperty().set(page.toString());
	}

	public final void setPage(String[] header, PowerSource[] powerSource,	String headline) {
		int numberRows = powerSource.length;
		StringBuilder page = new StringBuilder(createBeginning(header,headline) );
		//{"Name","Remaining Capacity", "Remaining Time"}
		
		for (int i = 0; i < numberRows; i++)
		{	
			double timeRemaining = powerSource[i].getTimeRemaining();
				page.append(setRowEvenOrOdd(i));
				page.append(createHTMLUnit(getTdBeg(),powerSource[i].getName(),getTdEnd()));
				page.append(createHTMLUnit(getTdBeg(),String.format("%.1f%%",powerSource[i].getRemainingCapacity() * 100d),getTdEnd()));
				page.append(createHTMLUnit(getTdBeg(),String.format("%d:%02d remaining",(int) (timeRemaining / 3600),(int) (timeRemaining / 60) % 60),getTdEnd()));
				page.append(getTrEnd());
			}
			
			page.append(createEndOfTable());
			PageProperty().set(page.toString());
	}
	
	 public final void setPageOfConnectionList(String [] columnArr, ObservableList<Connection> list ,String headline, String searchText)
	    {
	    	StringBuilder page = new StringBuilder(createBeginning(columnArr,headline) );
	    	page.append(createBodyOfConnectionList(list,columnArr, headline,searchText));
			page.append(createEndOfTable());
			PageProperty().set(page.toString());
	    }
	    
	    public final void setPageOfConnectionList(String [] columnArr, ObservableList<Connection> list ,String headline)
	    {
	    	int numberColumns = columnArr.length;
	    	int numberRows = list.size();
	    	
	    	StringBuilder page = new StringBuilder(createBeginning(columnArr,headline) );
	    	page.append(createBodyOfConnectionList(list,columnArr, headline));
			page.append(createEndOfTable());
			PageProperty().set(page.toString());
	}
	
	public final void setPageOfGamesList(String[] columns, String headline, String searchText) {
    	StringBuilder page = new StringBuilder(createBeginning(columns,headline) );
    		if (searchText.equals("started") || searchText.equals("not started")) {
    			page.append(createBodyOfGamesList(columns, headline,searchText));
    		}
    		else if (isNumber(searchText)) {
    			page.append(createBodyOfGamesList(columns, headline,Integer.parseInt(searchText) ));
    		}
    		else {
    			String error = "Your search for " +searchText+ " did not yield any result.";
    			page.append(createError(error,columns.length));
    		}
		page.append(createEndOfTable());
		PageProperty().set(page.toString());
	}
	
	public final void setPageOfUserList(String[] columns,	String headline, Connection conn) {
		StringBuilder page = new StringBuilder(createBeginning(columns,headline) );
		page.append(createBodyOfUserList(columns, headline, conn));
		page.append(createEndOfTable());
		PageProperty().set(page.toString());
	}

	public final void setPageOfUserList(String[] columns,	String headline, Connection conn, String searchText) {
		StringBuilder page = new StringBuilder(createBeginning(columns,headline) );
		
		if (searchText.equals("started") || searchText.equals("not started")) {
			page.append(createBodyOfUserList(columns, headline,conn,searchText));
			page.append(createEndOfTable());
		}
		else if (isNumber(searchText)) {
			page.append(createBodyOfUserList(columns, headline,conn,Integer.parseInt(searchText) ));
		}
		else {
			String error = "Your search for " +searchText+ " did not yield any result.";
			page.append(createError(error,columns.length));
		}
		
		PageProperty().set(page.toString());
		
	}
}
