package server.model.gui;



public class MyPageCreator {
	private String beginning = "<head><body>";
	private String wrapperStart = "<div class=\"wrapper\">";
	private String wrapperEnd = "</div>";
	private String tableBeg = "<table>";
	private String tableEnd = "</table>";
	private String endBody = "</body>";
	private String endHead = "</head>";
	private String tHeadBeg = "<thead>";
	private String tHeadEnd = "</thead>";
	private String tBodyBeg = "<tbody>";
	private String tBodyEnd = "</tbody>";
	private String thBeg = "<th";   
	private String closeTag2 = "\">";
	private String thEnd = "</th>";
	private String trBeg = "<tr>";
	
	private String trEnd = "</tr>";
	private String hBeg = "<h1>";
	private String hEnd = "</h1>";
	
	private String tdBeg = "<td>";
	private String tdEnd = "</td>";
	
	private String openTag = "<";
	private String closedTag = ">";
	private String lt = "&lt";
	private String gt = "&gt";
	private String paragraphStart = "<p>";
	private String paragraphEndTag ="</p>";
	private String paragraphStartRcv = "<p class=\"rcv\">";
	private String paragraphStartSnt = "<p class=\"snt\">";
	
	
	public String createBeginning(String header) {
    	StringBuilder sb = new StringBuilder();
    	sb.append(beginning);
    	sb.append(hBeg);
    	sb.append(header);
    	sb.append(hEnd);
		sb.append(tBodyBeg);
		return sb.toString();
    }
	
    public String createBeginning(String [] columnArr, String header) {
    	StringBuilder sb = new StringBuilder();
    	sb.append(beginning);
    	sb.append(hBeg);
    	sb.append(header);
    	sb.append(hEnd);
    	sb.append(tableBeg);
    	sb.append(tHeadBeg);
    	sb.append(getTrBeg());
		for (String columnHeader : columnArr )
		{
			sb.append(thBeg);
			sb.append(closedTag);
			sb.append(columnHeader);
			sb.append(thEnd);
		}
		sb.append(getTrEnd());
		sb.append(tHeadEnd);
		
		sb.append(tBodyBeg);
		return sb.toString();
    }
    
    public String createEndOfTable() {
    	StringBuilder sb = new StringBuilder();
    	sb.append(tBodyEnd);
    	sb.append(tableEnd);
    	sb.append(endBody);
    	sb.append(endHead);
    	return sb.toString();
    }
    
    public String createEnd() {
    	StringBuilder sb = new StringBuilder();
    	sb.append(endBody);
    	sb.append(endHead);
    	return sb.toString();
    }
    
    public String getPathToStricpt() {  
    StringBuilder script = new StringBuilder();  
    script.append("<script type=\"text/javascript\" src=\"/resources/scroll-to-end.js\">");  
    script.append(" </script>");  
    return script.toString();
    
}
    
    /**
     * replace all occurrences of '<' with '&lt'
     * and '>' with '&gt'
     * @param s: String
     * @return a String
     */
    public String convertHTMLtoText(String s) {
    	String newString = s.replaceAll(openTag,lt);
    	newString = newString.replaceAll(closedTag,gt);
    	return newString;
    }
    
    /**
     * Count the number of displayed messages in webView
     * @return an integer 
     */
    public int countLines(String pattern,String text)
	{
		int start = -1;
		 int end  = -1;
		 int lines = 0;
		 
	    	for(int i = 0, n = text.length() ; i < n ; i++) 
	    	{ 
	    	    char c = text.charAt(i); 
	    	    if (c == pattern.charAt(0) )
	    	    {
	    	    	start = i;
	    	    	end = i + pattern.length() ;
	    	    	if (text.substring(start, end).equals(pattern)  )
	    	    	{
	    	    		lines++;
	    	    	}
	    	    	else 
	    	    	{
	    	    		i = end - 1;
	    	    		
	    	    	}
	    	    	
	    	    }
	    	}
	    	return lines;
	}
    
    /**
     * Get the end of the first message
     * @return an index
     */
    public int getEndofSubstring(String pattern,String text)
    {
    	int start = -1;
		 int end  = -1;
		 
	    	for(int i = 0, n = text.length() ; i < n ; i++) 
	    	{ 
	    	    char c = text.charAt(i); 
	    	    if (c == pattern.charAt(0) )
	    	    {
	    	    	start = i;
	    	    	end = i + pattern.length() ;
	    	    	if (text.substring(start, end).equals(pattern)  )
	    	    	{
	    	    		//System.out.println("Found end tag " +  s.substring(start, end) + " at " + start);
		    	    	break;
	    	    	}
	    	    	else
	    	    	{
	    	    		start = -1;
	    	    		end = -1;
	    	    	}
	    	    }
	    	}
	    	return end;
    }
    public String createHTMLUnit(String tag1,Object text,String tag2)
    {
    	StringBuilder sb = new StringBuilder(tag1);
    	sb.append(text);
    	sb.append(tag2);
    	return sb.toString();
    }
    
    public boolean isNumber(String searchText)  
    {  
      try  
      {  
        int i = Integer.parseInt(searchText);  
      }  
      catch(NumberFormatException ex)  
      {  
        return false;  
      }  
      return true;  
    }
	
	public String writeToPage(String body) {
		
		return "";
	}
	
	 public String createError(String m)
		{
			StringBuilder sb = new StringBuilder();
			sb.append(createHTMLUnit(paragraphStartSnt, m,getPEnd()));
			return sb.toString();
		}

	public String getTrBeg() {
		return trBeg;
	}

	public void setTrBeg(String trBeg) {
		this.trBeg = trBeg;
	}

	public String getTdBeg() {
		return tdBeg;
	}

	public void setTdBeg(String tdBeg) {
		this.tdBeg = tdBeg;
	}

	public String getTdEnd() {
		return tdEnd;
	}

	public void setTdEnd(String tdEnd) {
		this.tdEnd = tdEnd;
	}

	public String getTrEnd() {
		return trEnd;
	}

	public void setTrEnd(String trEnd) {
		this.trEnd = trEnd;
	}
	public String getCloseTag2() {
		return closeTag2;
	}

	public String getCloseTag() {
		return closedTag;
	}
	public String getPStart() {
		return paragraphStart;
	}
	public String getPEnd() {
		return paragraphEndTag;
	}
	public String getPStartRcv() {
		return paragraphStartRcv;
	}
	public String getPStartSnt() {
		return paragraphStartSnt;
	}

	public void setCloseTag(String closeTag2) {
		this.closedTag = closeTag2;
	}
}
