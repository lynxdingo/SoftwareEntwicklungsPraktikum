package server.model.gui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.List;

public class MyFileCreator {
	private String t = "\t";
	
	public void writeToFile(String file, List<String> data) {
		this.getClass().getResourceAsStream(file);
		File emptyFile = new File(file);
		emptyFile.setWritable(true);
		
		
		PrintWriter out;
		
		try {
			
			FileWriter fWriter = new FileWriter(emptyFile, false);
			out = new PrintWriter(new BufferedWriter(fWriter));
			
				String line = "";
				
				for ( int i = 0; i < data.size() - 1; i = i+2) {
					line = data.get(i) +t+ data.get(i+1);
					out.println( line);
				}
				
				out.close();
				
//				if (emptyFile.exists()) {
//					System.out.println("File exists");
//					System.out.println("Pfad von data.tsv: " + emptyFile.getAbsolutePath());
//				}
//				if (!emptyFile.exists()) {
//					System.out.println("File not created");
//				}
				emptyFile.deleteOnExit();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
