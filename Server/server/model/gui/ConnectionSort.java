package server.model.gui;
import java.util.Comparator;

import server.code.ConnectionHandler;
import server.model.Connection;


	
	public class ConnectionSort implements Comparator<Connection> {

		public int compare(Connection o1, Connection o2) {
			return o1.getNick().compareTo(o2.getNick()) ;
		}
		
	}
	
