package server.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

public class Player {
	private Color color;
	private String pstate = ":: do nothing";
	private List<Playerstate> state = new ArrayList<Playerstate>();
	//private ObservableList<Playerstate> state = FXCollections.observableArrayList();
	private int gameID;
	private Connection conn;
	private List<Card> cards = new ArrayList<Card>();
	private int lastThrow;
	boolean out = false;
	boolean powerActive = true;	//SuperPower-Extension
	private String nick = "";
	
	public boolean isOut() {
		return out;
	}
	public void setOut() {
		this.out = true;
	}
	public void usePower() {
		powerActive = false;
		if(this.color == Color.YELLOW) {
			powerActive = true;
		}
		if(this.color == Color.PURPLE) {
			powerActive = true;
		}
		
	}
	public boolean getPowerActive() {
		return this.powerActive;
	}
	public Player(Connection conn, int gameID, Color color){
		state.clear();
		state.add(Playerstate.DO_NOTHING);
		this.conn = conn;
		this.gameID = gameID;
		this.color = color;
		this.nick = conn.getNick();
		this.powerActive = true;
		
	}
	public Color getColor(){
		return color;
	}
	public void resetPlayerstate(){
		state.clear();
		pstate = ":: ";
	}
	public JSONObject getPlayerInfo(){
		JSONObject ret = new JSONObject();
		ret.put("nick", conn.getNick());
		ret.put("color", color.toString());
		ret.put("playerstate", state.toString());
		return ret;
	}
	public Connection getConnection(){
		return conn;
	}
	public boolean isColor(Color color){
		return this.color == color;
	}
	public void addCard(Card card){
		cards.add(card);
	}
	public List<Card> getCards(){
		return cards;
	}
	public void addPlayerState(Playerstate state){
		this.state.add(state);
		PlayerStatetoString();
	}
//	public ObservableList<Playerstate> getPlayerState(){
//		return state;
//	}
	public List<Playerstate> getPlayerState(){
		return state;
	}
	public void setThrow(int result){
		lastThrow = result;
	}
	public int getLastThrow(){
		return lastThrow;
	}
	public int getGameID(){
		return gameID;
	}
	public String getNick(){
		return nick;
	}
	
	/**
	 * @codeauthor Angela
	 * convert all playerstates to string.
	 * this method is needed for tableview so that
	 * a list is displayed in a table cell
	 */
	public String getPstate(){
		return pstate;
	}
	
	public void PlayerStatetoString()
	 {
		 for (int i = 0; i < state.size(); i++)
		 {
			 if (! pstate.contains(state.get(i).toString())) {
					 pstate += state.get(i).toString() + " :: "; 
				 }
			 
		 }
	 }
}
