package server.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

public class Field {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Field.class);

	protected int x, y;
	private int evenRoll = 0;
	private int oddRoll = 0;
	private boolean inaccessible = false;
	protected List<Field> neighbours = new ArrayList<Field>();
	public Field(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	public JSONObject getFieldPositon(){
		JSONObject ret = new JSONObject();
		ret.put("x", x);
		ret.put("y", y);
		return ret;
	}
	public void checkNeighbour(Map<Key, Field> fields) {
		Key key = new Key(x, y-1);
		if(fields.containsKey(key)){
			Field field = fields.get(key);
			if(field.getRoom()==null){
				neighbours.add(fields.get(key));
			}else{
				Door door = (Door) field;
				if(door.getDirection() == 's')
					neighbours.add(door);
			}
		}
		key = new Key(x, y+1);
		if(fields.containsKey(key)){
			Field field = fields.get(key);
			if(field.getRoom()==null){
				neighbours.add(fields.get(key));
			}else{
				Door door = (Door) field;
				if(door.getDirection() == 'n')
					neighbours.add(door);
			}
		}
		key = new Key(x-1, y);
		if(fields.containsKey(key)){
			Field field = fields.get(key);
			if(field.getRoom()==null){
				neighbours.add(fields.get(key));
			}else{
				Door door = (Door) field;
				if(door.getDirection() == 'o')
					neighbours.add(door);
			}
		}
		key = new Key(x+1, y);
		if(fields.containsKey(key)){
			Field field = fields.get(key);
			if(field.getRoom()==null){
				neighbours.add(fields.get(key));
			}else{
				Door door = (Door) field;
				if(door.getDirection() == 'w')
					neighbours.add(door);
			}
		}
		
	}
	
	public List<Field> getFields(int roll){
		List<Field> ret = new ArrayList<Field>();
		if(inaccessible){
			if (logger.isDebugEnabled()) {
				logger.debug("getFields(int) - Inaccesible"); //$NON-NLS-1$
			}
			return ret; 
		}
		if(roll == 1){
			return neighbours;
		}
		if(roll%2 == 0){
			if(roll <= evenRoll){
				ret.add(this);
				return ret;
			}
			evenRoll = roll;
		}else{
			if(roll <= oddRoll){
				return ret;
			}
			oddRoll = roll;
		}
		for(Field field : neighbours){
			ret.addAll(field.getFields(roll-1));
		}
		return ret;
			
	}
	public void resetRoll(){
		evenRoll = 0;
		oddRoll = 0;
		inaccessible = false;
	}
	public Room getRoom(){
		return null;
	}
	public int getX(){
		return x;
	}
	public int getY(){
		return y;
	}
	public Key getKey(){
		return new Key(x, y);
	}
	public void setInaccessible() {
		inaccessible = true;
	}
	
}
