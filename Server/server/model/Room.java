package server.model;

public enum Room implements Card{
	 HALL("hall", 0),
	 LOUNGE("lounge", 4),
	 DININGROOM("diningroom", 2),
	 KITCHEN("kitchen", 6),
	 BALLROOM("ballroom", 7),
	 CONSERVATORY("conservatory", 3),
	 BILLIARD("billiard", 8),
	 LIBRARY("library", 1),
	 STUDY("study", 5),
	 POOL("pool", -1)
	;
	 private final String text;
	 private final int position;
	   private Room(final String text, int position) {
	       this.text = text;
	       this.position = position;
	   }
	   
	   public boolean equal(String s){
	   	if(s==null)
	   		return false;
	   	return text.equals(s);
	   }

	   @Override
	   public String toString() {
	       return text;
		    }
	   public int getPosition(){
		   return position;
	   }
	   
	   public static Room getRoom(String s){
		   Room room = null;
			for(Room r : Room.values()){
				if(r.equal(s)){
					room = r;
					break;
				}
			}
			return room;
			
	   	}

}
