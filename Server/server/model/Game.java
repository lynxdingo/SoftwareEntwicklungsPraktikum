package server.model;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class Game {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Game.class);

	private int gameID;
	private Gamestate gameState;
	//private List<Player> players = new ArrayList<Player>();
	private String timeCreated = "";
	private String timeStarted = "";
	//private List<Connection> watchers = new ArrayList<Connection>();
	private ObservableList<Connection> watchers = FXCollections.observableArrayList();
	private List<Weapon> weapons = new ArrayList<Weapon>();
	private Map<Color, Person> persons = new HashMap<Color, Person>();
	private List<Card> solution = new ArrayList<Card>();
	private List<Card> pool = new ArrayList<Card>(); 
	private int playerCount;
	private List<Color> order;
	private int activePlayer;
	private int disprovePlayer = 0;
	private Suspicion suspicion;

	public Game(int gameID){
		this.gameID = gameID;
		gameState = Gamestate.NOT_STARTED;
		this.setTimeCreated(LocalDateTime.now().toString());
		
	}
	public ObservableList<Connection> getWatchers(){
		return watchers;
	}
	public JSONObject getGameInfo(){
		JSONObject ret = new JSONObject();
		ret.put("gameID", gameID);
		ret.put("gamestate", gameState);
		ret.put("players", getPlayerInfo());
		ret.put("watchers", getWatchersInfo());
		ret.put("person positions", getPersonPositions());
		ret.put("weapon positions", getWeaponPositions());
		
		return ret;
	}
	
	private JSONArray getPlayerInfo(){
		JSONArray ret = new JSONArray();
		for(Map.Entry<Color, Person> entry : persons.entrySet()){
			if(entry.getValue().isPlayer())
				ret.put(entry.getValue().getPlayer().getPlayerInfo());
		}
		return ret;
	}
	
	private JSONArray getWatchersInfo(){
		JSONArray ret = new JSONArray();
		for(Connection watcher : watchers){
			ret.put(watcher.getNick());
		}
		return ret;
	}
	
	private JSONArray getPersonPositions(){
		JSONArray ret = new JSONArray();
		for(Map.Entry<Color, Person> entry : persons.entrySet()){
			ret.put(entry.getValue().getPositon());
		}
		return ret;
	}
	
	private JSONArray getWeaponPositions(){
		JSONArray ret = new JSONArray();
		for(Weapon weapon : weapons){
			ret.put(weapon.getPositon());
		}
		return ret;
	}
	
	public boolean isGameID(int gameID){
		return this.gameID == gameID;
	}
	
	public Map<Color, Person> getPersons(){
		return persons;
	}
	
	public void addPerson(Person person){
		persons.put(person.getColor(), person);
	}
	public int getGameID(){
		return gameID;
	}
	public Gamestate getGameState(){
		return gameState;
	}
	public void setGameState(Gamestate gameState){
		this.gameState = gameState;
	}
	
	public ObservableList<Player> getPlayers(){
		//List<Player> ret = new ArrayList<Player>();
		ObservableList<Player> ret = FXCollections.observableArrayList();
		for(Map.Entry<Color, Person> entry : persons.entrySet()){
			if(entry.getValue().isPlayer()){
				ret.add(entry.getValue().getPlayer());
			}
		}
		return ret;
	}
	public boolean isPlayer(Connection conn){
		for(Player player : getPlayers()){
			if(player.getConnection()==conn){
				return true;
			}
		}
		return false;
	}
	public void addToSolution(Card card){
		solution.add(card);
		//TODO delete
		if (logger.isDebugEnabled()) {
			logger.debug("addToSolution(Card) - Karte: " + card.toString()); //$NON-NLS-1$
		}
	}
	public void setPool(List<Card> cards){
		this.pool = cards;
	}
	public void setCount(){
		playerCount = persons.size();
	}
	public int getCount(){
		return playerCount;
	}
	public void setOrder(List<Color> order){
		this.order = order;
		activePlayer = 0;
	}
	public Person getNextPerson(){
		activePlayer++;
		if(activePlayer >= order.size()){
			activePlayer = 0;
		}
		if(getActivePerson().getPlayer().isOut())
			return getNextPerson();
		else
			return getActivePerson();
		
		
	}
	public Person getActivePerson(){
		return persons.get(order.get(activePlayer));
	}
	public List<Color> getOrder(){
		return order;
	}
	public List<Connection> getAllConnections(){
		List<Connection> connections = new ArrayList<Connection>();
		for(Player player : getPlayers()){
			connections.add(player.getConnection());
		}
		connections.addAll(watchers);
		return connections;
	}
	public boolean isColorTaken(Color color){
		for(Player player : getPlayers()){
			if(player.isColor(color))
				return true;
		}
		return false;
	}
	public Person getPerson(Connection conn){
		for(Map.Entry<Color, Person> entry : persons.entrySet()){
			if(entry.getValue().isPlayer()&&entry.getValue().getPlayer().getConnection()==conn){
				return entry.getValue();
			}
		}
		return null;
	}
	public JSONArray getPoolCards(){
		JSONArray ret = new JSONArray();
		for(Card card : pool){
			ret.put(card.toString());
		}
		return ret;
	}
	public Suspicion getSuspicion() {
		return suspicion;
	}
	public void setSuspicion(Suspicion suspicion) {
		this.suspicion = suspicion;
	}
	public Player getNextDisprove(){
		disprovePlayer++;
		int player = activePlayer + disprovePlayer;
		if(player >= order.size()){
			player = player - order.size();
		}
		return persons.get(order.get(player)).getPlayer();
	}
		
	public Player getActiveDisprove(){
		int player = activePlayer + disprovePlayer;
		if(player >= order.size()){
			player = player - order.size();
		}
		return persons.get(order.get(player)).getPlayer();
	
	}
	public boolean isSolution(Suspicion sus){
		if(!solution.contains(sus.getRoom()))
			return false;
		if(!solution.contains(sus.getWeapon()))
			return false;
		if(!solution.contains(sus.getColor()))
			return false;
		
		return true;
	}
	public List<Card> getPool(){
		return pool;
	}
	public void disproveEnd(){
		disprovePlayer = 0;
	}
	
	public List<Connection> getAllConnectionsExcept(Connection conn){
		List<Connection> ret = new ArrayList<Connection>();
		ret = getAllConnections();
		
		ret.remove(conn);
		return ret;
	}
	public List<Key> getPositions() {
		List<Key> ret = new ArrayList<Key>();
		for(Entry<Color, Person> entry : persons.entrySet()){
			ret.add(entry.getValue().getField());
		}
		return ret;
	}
	public void fillGame() {
		for(Color c : Color.values()){
			if(!persons.containsKey(c)){
				persons.put(c, new Person(c));
			}
		}
		
	}
	public JSONObject getSolution(){
		JSONObject ret = new JSONObject();
		for(Card c : solution){
			if(c instanceof Room){
				ret.put("room", c.toString());
			}else if(c instanceof Weapons){
				ret.put("weapon", c.toString());
			}else if(c instanceof Color){
				ret.put("person", c.toString());
			}
		}
		return ret;
	}
	
	public List <Weapon> getWeapons() {
		return weapons;
	}
	
	/**
	 * @codeauthor Angela
	 * record time when a new game has been created
	 * or started
	 */
	public void setTimeCreated(String t) {
		
		timeCreated = t;
	}
	public String getTimeCreated() {
		return timeCreated;
	}
	public void setTimeStarted(String t) {
		timeStarted = t;
	}
	public String getTimeStarted() {
		return timeStarted;
	}
}