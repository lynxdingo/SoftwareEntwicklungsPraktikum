package server.model;

import org.json.JSONObject;

public class Suspicion {
	private Room room;
	private Weapons weapon;
	private Color color;
	private boolean illegal = false;
	public Suspicion(Room room, Weapons weapon, Color color){
		this.room = room;
		this.color = color;
		this.weapon = weapon;
	}
	public Suspicion(String room, String weapon, String color){
		this.room = Room.getRoom(room);
		this.color = Color.getColor(color);
		this.weapon = Weapons.getWeapon(weapon);
		if(room == null ||color == null ||weapon == null ){
			illegal = true;
		}
	}
	public JSONObject getJSON(){
		JSONObject ret = new JSONObject();
		ret.put("person", color.toString());
		ret.put("weapon", weapon.toString());
		ret.put("room", room.toString());
		return ret;
	}
	public boolean isIllegal(){
		return illegal;
	}
	public Room getRoom() {
		return room;
	}
	public Weapons getWeapon() {
		return weapon;
	}
	public Color getColor() {
		return color;
	}
	public boolean contains(Card card){
		if(card == room||card == weapon||card == color)
			return true;
		return false;
	}
	@Override
	public boolean equals(Object o){
		if(this == o) 
			return true;
		if(!(o instanceof Suspicion)) 
			return false;
		Suspicion sus = (Suspicion) o;
		return sus.getColor() == color && sus.getRoom() == room && sus.getWeapon() == weapon;
	}
}
