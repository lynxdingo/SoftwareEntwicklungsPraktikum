package server.model;

import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import model.GameField;
import server.code.GameHandler;
import server.model.gui.GuiModel;

public class Model {
	//private List<Connection> connections = new ArrayList<Connection>();
	private ObservableList<Connection> connections;	
	private GameHandler gameHandler;
	private String[] extensions = {"Chat", "Superpower"};
	//private Map<Integer, Game> games = new HashMap<Integer, Game>();
	private ObservableMap<Integer, Game> games = FXCollections.observableMap(new HashMap<Integer, Game>());
	private int gameID = 0;
	private Random random;
	private GameField field; 
	private GuiModel guiModel;
	
	//SP-Extension nur einmal pro SPiel pro Person
//	private boolean[] superpowerUsed = {false, false, false, false, false, false};
	
	//Inititalize all Modeldata
	public Model(){
		gameHandler = new GameHandler(this);
		random = new Random();
		connections = FXCollections.observableArrayList();	
		field = new GameField();
		guiModel = new GuiModel(this);
	
	}
	
	public int getRandom(int low, int high){
		return random.nextInt(high-low)+low;
	}
	
	public GameHandler getGameHandler() {
		return gameHandler;
	}
	public ObservableList<Connection> getConnections(){
		return connections;
	}

	public boolean hasExtension(String extension){
		for(String ex : extensions){
			if(extension.equalsIgnoreCase(ex))
				return true;
		}
		return false;
	}
	
	public ObservableMap<Integer, Game> getGames(){
		return games;
	}
	
	
	public List<Connection> getAllClientsWithout(Connection conn){
		List<Connection> ret = connections.stream().filter(c -> c.isConnected()).collect(Collectors.toList());
		ret.remove(conn);
		return ret;
		
	}
	public List<Connection> getAllClientsWithout(List<Connection> connection){
		List<Connection> ret = connections.stream().filter(c -> c.isConnected()).collect(Collectors.toList());
		ret.removeAll(connection);
		return ret;
	}
	
	public GameField getGameField(){
		return field;
	}
	

	
	public int getGameID(){
		gameID++;
		return gameID;
	}
	
	public GuiModel getGuiModel() {
		return guiModel;
	}
	
//	public boolean[] getSuperpowerUsed() {
//		return superpowerUsed;
//	}
}
