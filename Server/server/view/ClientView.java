package server.view;

import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import server.model.Connection;
import server.model.Model;

public class ClientView {
	 	private Model model;
	    private TableView <Connection> table;
	    private ObservableList<Connection> data;
	    private TableColumn<Connection, String> colIp;
	    private TableColumn<Connection, Integer> colPort;
	    private TableColumn<Connection, String> colNick;
	 
	 public ClientView (Model mod)
	 {
		 model = mod;
		 data = model.getConnections();
	 }
	 
	 public TableView <Connection> setUpClientList() 
	 {
		table =  new TableView <Connection>(data);
	    colIp = new TableColumn <Connection, String>("IP-Address");
	    colIp.setMinWidth(300);
	    colIp.setSortable(true);
	    colIp.setCellValueFactory(
        new PropertyValueFactory<Connection, String>("ip"));
     
	    colPort = new TableColumn <Connection, Integer> ("port");
	    colPort.setMinWidth(100);
	    colPort.setSortable(true);
	    colPort.setCellValueFactory(
        new PropertyValueFactory<Connection, Integer>("port"));

	    colNick = new TableColumn <Connection, String>("Nick");
	    colNick.setMinWidth(300);
	    colNick.setSortable(true);
	    colNick.setCellValueFactory(
        new PropertyValueFactory<Connection, String>("nick"));
   
		table.getColumns().addAll(colNick,colIp, colPort);
		table.setItems(data);
		return table;
	 }
	 
	 public ObservableList<Connection> getData() {
		 return data;
	 }
	 public TableView<Connection> getTable() {
		 return table;
	 }
	 
	 public TableColumn<Connection, String> getColNick() {
		 return colNick;
	 }
	 
	 
}
