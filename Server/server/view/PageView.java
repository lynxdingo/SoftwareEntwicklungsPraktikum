package server.view;

import static model.Constants.CSS_ANGELA;
import javafx.scene.control.ScrollPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import server.model.Model;

public class PageView {
	private Model model;
	private WebView htmlarea = new WebView();
	private WebEngine wEngine = htmlarea.getEngine();
	private ScrollPane window = new ScrollPane();
	private String errorMessage = "";
 
 
 public PageView (String s)
 {
	 errorMessage = s;
 }
 	
 public ScrollPane setUpHtmlView() {
	 
	 //Font.loadFont(ServerScreen.class.getResource(FONT_TEXT).toExternalForm(), 10);
	 wEngine.setUserStyleSheetLocation(getClass().getResource(CSS_ANGELA).toExternalForm());
	 wEngine.setJavaScriptEnabled(true);
	 wEngine.loadContent(errorMessage);
	 window.setContent(htmlarea);
	 window.setFitToHeight(true);
	 window.setFitToWidth(true);
	 
	 return window;
	 
 }
 
public ScrollPane setUpHtmlView(String file) {
	 
	 //Font.loadFont(ServerScreen.class.getResource(FONT_TEXT).toExternalForm(), 10);
	 wEngine.setUserStyleSheetLocation(getClass().getResource(CSS_ANGELA).toExternalForm());
	 wEngine.load(getClass().getResource(file).toExternalForm());
	 wEngine.setJavaScriptEnabled(true);
	 //wEngine.loadContent(errorMessage);
	 window.setContent(htmlarea);
	 window.setFitToHeight(true);
	 window.setFitToWidth(true);
	 
	 return window;
	 
 }
 
 public WebEngine getEngine(){
	 return wEngine;
 }
 
}
