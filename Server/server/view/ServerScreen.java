package server.view;

import static model.Constants.CSS_ANGELA;
import static model.Constants.FONT;
import static model.Constants.FONT_SERVER;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import server.model.Model;

public class ServerScreen extends Scene{
	 private Model model;
	 private ClientView cView;
	 private PageView serverSheet,messageSheet,watchersSheet,gamesSheet,infoSheet,fileSheet;
	 private String errorServer = "<html><body><b>No facts about this server available at the moment.</b></html></body>";
	 private String errorMessage = "<html><body><b>Here you'll see the messages.</b></html></body>";
	 private String errorWatchers = "<html><body><b>No watchers at the moment.</b></html></body>";
	 private String errorGames = "<html><body><b>Selected user is not in a game at the moment.</b></html></body>";
	 private String errorOutput= "<html><body><b>Nothing to ping at the moment.</b></html></body>";
	 private String errorGraph = "<html><body><b>No throuput available at the moment.</b></html></body>";
	 private GameView gView;
	 private StackPane root;
	 private GridPane grid = new GridPane();
	 private Button btnPing, btnRefresh, btnMsg,btnDisco,btnEnd,btnSearch, btnSort,btnPlayers,btnWatchers,btnGames,btnDel;
	 private Label lblNf = new Label ("Notification");
	 private Label lblDisplay = new Label ("No content available");
	 private Label lblInfoBox = new Label ("Search through or sort pages that are displayed at the bottom.");
	 private Label lblInfoSearch = new Label ("But first select one attribute you'd like to search for or sort by.");
	 private Label lblAttribute = new Label ("I. Choose one attribute");
	 private Label lblList = new Label ("II. Search the attribute in");
	 private TextField txtSearch = new TextField();
	 private TreeItem server, client, game;
	 private TreeItem top = new TreeItem("Cluedo");
	 private TreeView<String>  tree = new TreeView<String> (top);
	
	 private RadioButton rdoNick = new RadioButton("Nick");
	 private RadioButton rdoID = new RadioButton("GameID");
	 private RadioButton rdoGameStateS = new RadioButton("started");
	 private RadioButton rdoGameStateN = new RadioButton("not started");
	 private RadioButton rdoTxt = new RadioButton("Text");
	
	 private RadioButton rdoMessages = new RadioButton("Messages List");
	 private RadioButton rdoGList = new RadioButton("Games List");
	 private RadioButton rdoCList = new RadioButton("Connection List");
	 private RadioButton rdoWList = new RadioButton("Watchers");
	 private RadioButton rdoGU = new RadioButton("Games of User");
	 
	 private ToggleGroup sortGroup = new ToggleGroup();
	 private ToggleGroup listGroup = new ToggleGroup();
	 private HBox paneDisplay; 
	 private HBox paneMessages;
	 
	 public ServerScreen (Model mod )	{
		 	super(new StackPane(), Color.LIGHTGRAY );
			this.root = (StackPane)this.getRoot();
			this.getStylesheets().add(CSS_ANGELA);
	   		model = mod;
	   		cView = new ClientView(model);
	   		serverSheet = new PageView(errorServer);
	   		messageSheet = new PageView(errorMessage);
	   		watchersSheet = new PageView(errorWatchers);
	   		gamesSheet = new PageView(errorGames);
	   		infoSheet = new PageView(errorOutput);
	   		fileSheet = new PageView(errorGraph);
	   		gView = new GameView(model);
			setUp();
		}
	 
	 public void setUp() {
		 top.setExpanded(true);
		 tree.setShowRoot(true);
		 btnPing = new Button("ping");
		 btnRefresh = new Button("refresh");
		 btnMsg = new Button("show messages");
		 btnDisco = new Button("disconnect client");
		 btnEnd = new Button("end game");
		 btnSearch = new Button("search");
		 btnSort = new Button("sort");
		 btnPlayers = new Button("list players");
		 btnWatchers = new Button("list watchers");
		 btnGames = new Button ("list games");
		 btnDel = new Button ("delete game");
		 
		 lblNf.setId("lblNf");
		 lblInfoBox.setWrapText(true);
		
		 btnRefresh.getStyleClass().add("btnLists");
		 btnMsg.getStyleClass().add("btnLists");
		 
		 btnPing.getStyleClass().add("btnCons");
		 btnDisco.getStyleClass().add("btnCons");
		 
		 btnPlayers.getStyleClass().add("btnGames");
		 btnWatchers.getStyleClass().add("btnGames");
		 btnEnd.getStyleClass().add("btnGames");
		 btnGames.getStyleClass().add("btnGames");
		 btnDel.getStyleClass().add("btnGames");
		
		 btnSearch.getStyleClass().add("btnMsgs");
		 btnSort.getStyleClass().add("btnMsgs");
		
		 server = setVisible("Server",top);
		 setVisible("Name",server);
		 setVisible("CPU",server);
		 setVisible("RAM",server);
		 setVisible("Disks",server);
		 setVisible("Throughput",server);
		 setVisible("Throughput Chart",server);
		 setVisible("Power",server);
		 
		 client = setVisible("Client",top);
		 setVisible("Connection List",client);
		 
		 game = setVisible("Game",top);
		 setVisible("All Games",game);
		 
		 lblAttribute.getStyleClass().add("lblFilters");
		 lblList.getStyleClass().add("lblFilters");
		 
		 
		 lblNf.setWrapText(true);
		 lblInfoSearch.setWrapText(true);
		 txtSearch.setPromptText("filter by search.");
		 
		 rdoNick.setToggleGroup(sortGroup);
		 rdoID.setToggleGroup(sortGroup);
		 rdoGameStateS.setToggleGroup(sortGroup);
		 rdoGameStateN.setToggleGroup(sortGroup);
		 rdoTxt.setToggleGroup(sortGroup);
		 
		 rdoMessages.setToggleGroup(listGroup);
		 rdoGList.setToggleGroup(listGroup);
		 rdoCList.setToggleGroup(listGroup);
		 rdoWList.setToggleGroup(listGroup);
		 rdoGU.setToggleGroup(listGroup);
		 
		 rdoGList.setDisable(true);  
		 rdoCList.setDisable(true); 
		 rdoWList.setDisable(true); 
		 rdoGU.setDisable(true); 
		 
		 Font.loadFont(ServerScreen.class.getResource(FONT).toExternalForm(), 10);
		 Font.loadFont(ServerScreen.class.getResource(FONT_SERVER).toExternalForm(), 10);
		 
		 
		 VBox paneMainButtons = new VBox(btnRefresh,btnMsg, btnPlayers, btnWatchers,btnGames,btnEnd,btnDel,btnPing, btnDisco  );
		 paneMainButtons.setSpacing(10);
		 
		 paneDisplay = new HBox();
		 paneDisplay.getChildren().add(lblDisplay);
		 //sendText.setPrefWidth(gridChat.getMaxWidth() /2);
		 
		 //HBox paneSort = new HBox(rdoNick,btnSort);
		 HBox paneSearch = new HBox(10,txtSearch,btnSearch);
		 
		 VBox paneRadioSort = new VBox(lblAttribute,rdoNick,rdoID,rdoGameStateS,rdoGameStateN,rdoTxt);
		 paneRadioSort.setSpacing(10);
		 
		 VBox paneRadioSearch = new VBox(lblList,rdoMessages ,rdoGList,rdoCList,rdoWList,rdoGU,paneSearch );
		 paneRadioSearch.setSpacing(10);
		 
		 VBox paneInfo = new VBox(btnSort);
		 paneInfo.setSpacing(10);
		 
		 VBox paneLbl = new VBox(lblInfoBox,lblInfoSearch);
		 paneInfo.setSpacing(10);

		 HBox paneSort = new HBox(10,paneRadioSort,btnSort);
		// HBox paneSearch = new HBox(10,paneRadioSearch,btnSearch);
		 
		 VBox paneTree = new VBox(tree,paneSort);
		 paneTree.setSpacing(10);
		 
		 
		 
//		 VBox paneMsgButtons = new VBox();
//		 paneMsgButtons.setSpacing(10);
		 
		 paneMessages = new HBox(10);
		 
		 grid.setPadding(new Insets(10));
		 grid.setHgap(10);
		 grid.setVgap(10);
		 grid.setGridLinesVisible(false);
		 
		 grid.add(paneTree, 0, 1);
		 grid.add(lblNf, 0, 0);
		 grid.add(paneMainButtons,3,1);
		 grid.add(paneDisplay, 1, 1);
		 grid.add(paneRadioSearch,0 , 2);
		 grid.add(paneMessages, 1, 2);
		//grid.add(paneMsgButtons, 3, 2);
		 
		// grid.setHalignment(lblInfoBox, HPos.LEFT);
		 grid.setHalignment(tree, HPos.LEFT);
		 grid.setHalignment(txtSearch, HPos.LEFT);
		 grid.setHalignment(paneDisplay, HPos.LEFT);
		 grid.setHalignment(paneMessages, HPos.CENTER);
		 
		 grid.setColumnSpan(paneMainButtons, 1);
		 grid.setColumnSpan(paneDisplay, 2);
		 grid.setColumnSpan(paneMessages, 2);
		 grid.setRowSpan(paneDisplay, 1);
		 grid.setColumnSpan(lblNf, 3);
		 
		 ColumnConstraints col1 = new ColumnConstraints();
		 col1.setPercentWidth(30);
		 ColumnConstraints col2 = new ColumnConstraints();
		 col2.setPercentWidth(20);
		 ColumnConstraints col3 = new ColumnConstraints();
		 col3.setPercentWidth(35);
		 ColumnConstraints col4 = new ColumnConstraints();
		 col4.setPercentWidth(15);
		 grid.getColumnConstraints().addAll(col1,col2,col3,col4);
		 
//		 RowConstraints row1 = new RowConstraints();
//		 row1.setPercentHeight(10);
//		 RowConstraints row2 = new RowConstraints();
//		 row2.setPercentHeight(45);
//		 RowConstraints row3 = new RowConstraints();
//		 row3.setPercentHeight(45);
//		 grid.getRowConstraints().addAll(row1,row2,row3);
		 
		 root.getChildren().add(grid);
		 
	 }
	 
	 public TreeItem<String> setVisible(String item,TreeItem<String> parent) {
		 TreeItem<String> print = new TreeItem<String>(item);
		 print.setExpanded(true);
		 parent.getChildren().add(print);
		 return print;
	 }
	 
	 public void setDisplay(Node child) {
		 //System.out.println("Klassenname: "+  child.getClass().getName());
		 
		 if (paneDisplay.getChildren().isEmpty() ){
			 	paneDisplay.getChildren().add(child);
		 }
		 else {
			 paneDisplay.getChildren().clear();
			 paneDisplay.getChildren().add(child);
		 }
		 
	 }
	 
	 public void setBottom(Node child) {
		 if (paneMessages.getChildren().isEmpty() ){
			 paneMessages.getChildren().add(child);
		 }
		 else {
			 paneMessages.getChildren().clear();
			 paneMessages.getChildren().add(child);
		 }
	 }
	 
	 public void resetDisplay() {
		 paneDisplay.getChildren().clear();
		 paneDisplay.getChildren().add(lblDisplay);
	 }
	 public Button getBtnPing() {
		 return btnPing;
	 }
	 public Button getBtnRefresh() {
		 return btnRefresh;
	 }
	 public Button getBtnMsg() {
		 return btnMsg;
	 }
	 public Button getBtnDisco() {
		 return btnDisco;
	 }
	 public Button getBtnEnd() {
		 return btnEnd;
	 }
	 public Button getBtnPlayers() {
		 return btnPlayers;
	 }
	 public Button getBtnWatchers() {
		 return btnWatchers;
	 }
	 public Button getBtnGames() {
		 return btnGames;
	 }
	 public Button getBtnDel() {
		 return btnDel;
	 }
	 public Button getBtnSort() {
		 return btnSort;
	 }
	 public Button getBtnSearch() {
		 return btnSearch;
	 }
	 public Label getLblNf() {
		 return lblNf;
	 }
	 public TextField getTxtSearch(){
		 return txtSearch; 
	 }
	 public RadioButton getRdoNick() {
		 return rdoNick;
	 }
	 public RadioButton getrdoID() {
		 return rdoID;
	 }
	 public RadioButton getRdoGameStateS() {
		 return rdoGameStateS;
	 }
	 public RadioButton getRdoGameStateN() {
		 return rdoGameStateN;
	 }
	 public RadioButton getRdoTxt() {
		 return rdoTxt;
	 }
	 public RadioButton getRdoMessages() {
		 return rdoMessages;
	 }
	 public RadioButton getRdoGList() {
		 return rdoGList;
	 }
	 public RadioButton getRdoCList() {
		 return rdoCList;
	 }
	 public RadioButton getRdoWList() {
		 return rdoWList;
	 }
	 public RadioButton getRdoGU() {
		 return rdoGU;
	 }
	 public TreeView<String> getTree() {
		 return tree;
	 }
	 public ClientView getClientView() {
		 return cView;
	 }
	 public PageView getServerSheet() {
		 return serverSheet;
	 }
	 public PageView getMessageSheet() {
		 return messageSheet;
	 }
	 public PageView getWatchersSheet() {
		 return watchersSheet;
	 }
	 public PageView getGamesSheet() {
		 return gamesSheet;
	 }
	 public PageView getInfoSheet() {
		 return infoSheet;
	 }
	 public PageView getFileSheet() {
		 return fileSheet;
	 }
	 public GameView getGameView() {
		 return gView;
	 }
}
