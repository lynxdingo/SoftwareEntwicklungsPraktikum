package server.view;

import java.util.Map;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import server.model.Game;
import server.model.Gamestate;
import server.model.Model;
import server.model.Player;

public class GameView {
	 	private Model model;
	    private TableView <Game> tableGames;
	    private TableView <Player> tablePlayers;
	    private ObservableList<Game> games ;
	    private ObservableList<Player> players ;
	    private TableColumn<Game, Integer> colGameID;
	    private TableColumn<Game, Gamestate> colGameState;
	    private TableColumn<Game, String> colWeaponPositions;
	    private TableColumn<Game, String> colTimeCreated;
	    private TableColumn<Game, String> colTimeStarted;
	    private TableColumn<Player, Integer> colID;
	    private TableColumn<Player, String> colNick;
	    private TableColumn<Player, String> colPlayerState;
	   
	 
	 
	 public GameView (Model mod)
	 {
		 model = mod;
		 games = FXCollections.observableArrayList();
		 players = FXCollections.observableArrayList();
		 
	 }
	 public void createGamesList() {
		 games.clear();
		 for ( Map.Entry<Integer, Game> entry : model.getGames().entrySet()) 
			{
			    Integer key = entry.getKey();
			    Game value = entry.getValue();
			    games.add(value);
			}
		 
	 }
	 
	 public TableView <Player> setUpPlayersList(ObservableList<Player> list) 
	 {
		players = list;
		tablePlayers =  new TableView <Player>(players);
		
		colID = new TableColumn <Player, Integer>("Game ID");
		colID.setMinWidth(100);
		colID.setSortable(true);
		colID.setCellValueFactory(
        new PropertyValueFactory<Player, Integer>("gameID"));
     
		colNick = new TableColumn <Player, String>("Nick");
		colNick.setMinWidth(100);
		colNick.setSortable(true);
		colNick.setCellValueFactory(
        new PropertyValueFactory<Player, String>("nick"));
		
		colPlayerState = new TableColumn<Player, String>("PlayerState");
		colPlayerState.setMinWidth(400);
		colPlayerState.setSortable(true);
		colPlayerState.setCellValueFactory(
		new PropertyValueFactory<Player, String>("pstate"));

		tablePlayers.getColumns().addAll(colID,colNick,colPlayerState);
		tablePlayers.setItems(players);
		return tablePlayers;

	 }
	 
	 public TableView <Game> setUpGameList() 
	 {
		createGamesList();
		tableGames =  new TableView <Game>(games);
		colGameID = new TableColumn <Game, Integer>("Game ID");
		colGameID.setMinWidth(100);
		colGameID.setSortable(true);
		colGameID.setCellValueFactory(
        new PropertyValueFactory<Game, Integer>("gameID"));
     
		colGameState = new TableColumn <Game, Gamestate> ("GameState");
		colGameState.setMinWidth(100);
		colGameState.setSortable(true);
		colGameState.setCellValueFactory(
        new PropertyValueFactory<Game, Gamestate>("gameState"));

	    colTimeCreated = new TableColumn <Game, String>("Game created on");
	    colTimeCreated.setMinWidth(300);
	    colTimeCreated.setSortable(true);
	    colTimeCreated.setCellValueFactory(
        new PropertyValueFactory<Game, String>("timeCreated"));
	    
	    colTimeStarted = new TableColumn <Game, String>("Game started on");
	    colTimeStarted.setMinWidth(300);
	    colTimeStarted.setSortable(true);
	    colTimeStarted.setCellValueFactory(
        new PropertyValueFactory<Game, String>("timeStarted"));
   
	    tableGames.getColumns().addAll(colGameID,colGameState,colTimeCreated,colTimeStarted);
	    tableGames.setItems(games);
		return tableGames;
	 }
	 
	 public ObservableList<Game> getData() {
		 return games;
	 }
	 public ObservableList<Player> getPlayers() {
		 return players;
	 }
	 
	 public TableView<Game> getTable() {
		 return tableGames;
	 }
	 public TableView<Player> getTablePlayers() {
		 return tablePlayers;
	 }
	 
	 public TableColumn<Player, String> getColNick() {
		 return colNick;
	 }
//	 public WebEngine getEngine(){  
//		 return wEngine;
//	 }
	 
		
//		colPlayerState.setCellValueFactory((p) -> {
//         return new ReadOnlyObjectWrapper<Playerstate>(makeString(p.getValue().getPlayerState()), "state");
//     });
	 
//	 colPlayerState = new TableColumn<Player, String>("PlayerState");
//		colPlayerState.setMinWidth(400);
//		colPlayerState.setCellValueFactory((p) -> {
//	         return new ReadOnlyObjectWrapper<String>(p.getValue().getState().toString() , "state");
//	     });
//		
	 
//	 colPlayerState.setCellValueFactory((p) -> {
//         return new ReadOnlyObjectWrapper<String>(p.getValue().getState().toString() , "state");
//     });
	 
	 
}
