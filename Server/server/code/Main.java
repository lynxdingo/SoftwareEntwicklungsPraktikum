package server.code;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import server.model.Field;
import server.model.Key;
import server.model.Model;
import server.view.ServerScreen;


public class Main extends Application{
	private static final Logger log = LogManager.getLogger( Main.class.getName() );
	private AcceptThread acceptThread;
	private Model model;
	
	private void initialize() throws IOException
	{
		acceptThread = new AcceptThread(model);
		Thread thread = new Thread(acceptThread);
		thread.setDaemon(true);
		thread.start();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		log.info("Server Application was started");
		model = new Model();
		
		ServerScreen gui = new ServerScreen(model);
		new PresenterGui(gui,model);
		
		initialize();
		Long one = System.currentTimeMillis();
		System.out.println("HAllo");
		Set<Field> accesibleFields = model.getGameField().getFieldsForRoll(new Key(12,6), 7, new ArrayList<Key>());
		System.out.println("Time: "+ (System.currentTimeMillis() - one));
		for(Field field : accesibleFields){
			log.info("Is Accesible: x: "+ field.getX() + " y: " + field.getY());
		}
		
		/** Fenster wird 1/4 so groß wie der Bildschirm und bewegt sich in de mitte des Bildschirms
		 * @CodeAutor: Alex */
	    Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
	    primaryStage.setWidth(primScreenBounds.getWidth()/2);
	    primaryStage.setHeight(primScreenBounds.getHeight()/2);
	    primaryStage.setX(primScreenBounds.getWidth()/2 - primaryStage.getWidth()/2);
	    primaryStage.setY(primScreenBounds.getHeight()/2 - primaryStage.getHeight()/2);
				
	     primaryStage.setScene(gui);
	     primaryStage.setTitle("Cluedo Server");
	     primaryStage.show();
	     		
	}

	static{
	    SimpleDateFormat dateFormat = new SimpleDateFormat("D-HH-mm-ss");
	    System.setProperty("logFilename", "Server_"+dateFormat.format(new Date()));
	}
  public static void main(String[] args) {
	org.apache.logging.log4j.core.LoggerContext ctx =
			    (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
			ctx.reconfigure();
    launch(args);
  }
	
}
