package server.code;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import server.model.Model;

public class AcceptThread implements Runnable{
	private static final Logger log = LogManager.getLogger( AcceptThread.class.getName() );
	private boolean running = true;
	private ServerSocket serverSocket;
	private Model model;
	private final int UDPPort = 30303;
	private final int TCPPort = 40404;
	public AcceptThread(Model model){
		this.model = model;
		
	}
	
	@Override
	public void run() {
		try{
			//BroadcastThread öffnen
			UDPHandshake udpServer = new UDPHandshake(UDPPort, TCPPort);
			Thread udp = new Thread(udpServer);
			udp.setDaemon(true);
			udp.start();
			
			//TCP Accepter öffnen
			serverSocket = new ServerSocket(TCPPort);
			while(running){
				Socket socket = serverSocket.accept();
				log.info("new Connection: IP: "+socket.getInetAddress().toString());
				ConnectionHandler r = new ConnectionHandler(socket, model);
				Thread t = new Thread(r);
				t.setDaemon(true);
				t.start();
				
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
	public void log(){
		log.log(Level.DEBUG, "Aha");
	}

}
