package server.code;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

public class UDPHandshake implements Runnable{
	private static final Logger log = LogManager.getLogger( UDPHandshake.class.getName() );
	private final int UDPPort;
	private final int TCPPort;
	private DatagramSocket udpSocket;
	private InetAddress ia;
	private JSONObject ret;
	private byte[] raw;
	public UDPHandshake(int UDPPort, int TCPPort) {
		this.UDPPort = UDPPort;
		this.TCPPort = TCPPort;
		
		ret = new JSONObject();
		ret.put("type", "udp server");
		ret.put("group", "offeneonthologen");
		ret.put("tcp port", TCPPort);
		log.info("Handshake Message: " + ret);
		
		raw = ret.toString().getBytes();
	}
	

	@Override
	public void run() {
		//Create UDPHandshakeServer
		try(DatagramSocket udpSocket = new DatagramSocket(null)){;
			udpSocket.setReuseAddress(true);
			udpSocket.bind(new InetSocketAddress(30303));
			//Send Broadcast
			ia = InetAddress.getByName("255.255.255.255");
			DatagramPacket packet = new DatagramPacket(raw, raw.length, ia, UDPPort);
			
			udpSocket.setBroadcast(true);
			udpSocket.send(packet);
			log.info("Broadcast was sended!");
			
			
			//Receive Request
			//udpSocket.setBroadcast(true);
			while(true){
				packet = new DatagramPacket(new byte[1024], 1024);
				udpSocket.receive(packet);
				
				InetAddress clientAddress = packet.getAddress();
				int port = packet.getPort();	//Redundant, should be 30303, to be secure
				byte[] data = packet.getData();
				
				JSONObject recv = new JSONObject(new String(data));
				log.info("Received UDPMessage from: " + clientAddress.toString() + " from port: " + port + ": " + recv);
				if(recv.isNull("type")||recv.isNull("group")){
					log.warn("Client send wrong message!");
					continue;
				}
				
				if(!recv.getString("type").equalsIgnoreCase("udp client")){
					log.info("Received non client packet: " + "type: " + recv.getString("type"));
					continue;
				}
				//Message is valid

				packet = new DatagramPacket(raw, raw.length, ia, UDPPort);;//Maybe you should use UDPPort (30303)
				udpSocket.send(packet);
				log.info("Send answer to:" + clientAddress + ": " + ret);
					
				
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
