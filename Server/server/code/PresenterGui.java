package server.code;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;

import oshi.util.FormatUtil;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.Toggle;
import javafx.util.Callback;
import server.model.*;
import server.model.gui.ConnectionSort;
import server.model.gui.MyFileCreator;
import server.model.Connection;
import server.view.*;


public class PresenterGui {

	private ServerScreen gui;
	private Model model;
	private String mycolor = "my";
	private GameHandler gameHandler;
	private String [] endGameKeys = new String [] {"type","gameID","nick" };
	private Object [] value = new Object[3];
	private boolean isAscendingMessage = true;
	private boolean isAscendingNick = true;
	private boolean isAscendingID = true;
	private String [] rdoSort = new String [] {"Nick","GameID","started","not started","Text"};
	private String [] rdoSearch = new String [] {"Messages List","Games List","Connection List","Watchers","Games of User"};
	private String errorMessage = "";
	
	public PresenterGui(ServerScreen g, Model m) {
		
		gui = g;
		model = m;
		gameHandler = model.getGameHandler();
		activateEvent();
		
	}
	
	public void activateEvent() {
		gui.getBtnRefresh().setOnAction(e -> refresh());
		gui.getBtnMsg().setOnAction(e -> showMessages(""));
		gui.getBtnPlayers().setOnAction(e -> showPlayers());
		gui.getBtnWatchers().setOnAction(e -> showWatchers("",""));
		gui.getTree().setOnMouseClicked(e -> refresh());
		gui.getBtnEnd().setOnAction(e -> endGame(false));
		gui.getBtnGames().setOnAction(e -> showGames("",""));
		gui.getBtnDisco().setOnAction(e -> disconnectClient());
		gui.getBtnDel().setOnAction(e -> deleteGame());
		gui.getBtnPing().setOnAction(e -> pingClient());
		gui.getBtnSort().setOnAction(e->sort());
		gui.getBtnSearch().setOnAction(e->search(gui.getTxtSearch().getText()));
		gui.getRdoNick().setOnAction(e -> setSearchRdoButtons());
		gui.getrdoID().setOnAction(e -> setSearchRdoButtons());
		gui.getRdoGameStateS().setOnAction(e -> setSearchRdoButtons());
		gui.getRdoGameStateN().setOnAction(e -> setSearchRdoButtons());
		gui.getRdoTxt().setOnAction(e -> setSearchRdoButtons());
		model.getGuiModel().BytesReceivedProperty().addListener(
				(observable,oldvalue,newvalue) -> showBytesGraph()
				);
		model.getGuiModel().BytesSentProperty().addListener(
				(observable,oldvalue,newvalue) -> showBytesGraph()
				);
		
	}
	
	public boolean IsSelectedTree()
	{
		return gui.getTree().getSelectionModel().getSelectedItem() != null;
	}
	
	public boolean IsSelectedTableGame()
	{	
		return gui.getGameView().getTable().getSelectionModel().getSelectedItem() != null;
	}
	
	public boolean IsSelectedTableConnection()
	{	
		return gui.getClientView().getTable().getSelectionModel().getSelectedItem() != null;
	}
	
	
	public void refresh()
	{	
		gui.getLblNf().setText("");
		boolean hasSelected = IsSelectedTree();
		if (! hasSelected) {
			gui.getLblNf().setText("Please select a submenu below.");
		}
		else
		{	
			gui.getLblNf().setText("");
			String treeItem = gui.getTree().getSelectionModel().getSelectedItem().getValue();
			switch (treeItem)
			{
			case "Name":
				loadServerFacts();
				break;
			case "CPU":
				showCpus();
				break;
			case "RAM":
				showRam();
				break;
			case "Disks":
				showDisks();
				break;
			case "Throughput":
				showThroughPut();
				break;
			case "Throughput Chart":
				testChart();
				break;
			case "Power":
				showPower();
				break;
			case "Connection List":
				loadClientList();
				break;
			case "All Games":
				loadGameList();
				break;
			}
		}
	}
	

	public JSONObject getJSON(String [] keys, Object [] values ) {
		JSONObject jObj = new JSONObject();
		for (int i = 0; i< keys.length; i++ ) {
			jObj.put(keys[i], values[i]);
		}
		return jObj;
		
	}
	public JSONObject getJSON(String s, Object o) {
		JSONObject jObj = new JSONObject();
		jObj.put(s, o);
		return jObj;
	}
	public void setCenter(PageView view){
		gui.setDisplay(view.setUpHtmlView() );
 		view.getEngine().loadContent(model.getGuiModel().getPage().getPage());
	}
	public void setCenter(PageView view,String hFile){
		gui.setDisplay(view.setUpHtmlView(hFile) );
	}
	public void setBottomMessages(PageView view){
		gui.setBottom(view.setUpHtmlView() );
		view.getEngine().loadContent(model.getGuiModel().getMessage().getMessages() );
	}
	public void setBottomTable(PageView view){
  		gui.setBottom(view.setUpHtmlView() );
  		view.getEngine().loadContent(model.getGuiModel().getPage().getPage());
	}
	public boolean isClientListEmpty() {
		return (gui.getClientView().getData().size() == 0);
	}
	public boolean isGamesListEmpty() {
		return model.getGames().isEmpty();
	}
	
	private String getFormattedNumber(double number) {
			 return (String.format(".%04.0f",number * 10000 ));
	}
	
	
	
	private void testChart() {
		long sent = model.getGuiModel().getBytesSent();
		long rcv = model.getGuiModel().getBytesReceived();
		double total = sent + rcv ;
		if (total == 0) {
			gui.setDisplay(gui.getFileSheet().setUpHtmlView() );
		}else {
			gui.getLblNf().setText("It can take up to 10 seconds until data for chart has been created. Then press refresh again.");
			//showBytesGraph();
			setCenter(gui.getFileSheet(),"/resources/index.html" );
		}

	}
	
	private void showBytesGraph() {
		long sent = model.getGuiModel().getBytesSent();
		long rcv = model.getGuiModel().getBytesReceived();
		double total = sent + rcv ;
		MyFileCreator myfile = new MyFileCreator();
		List<String> bytesList = new ArrayList<String>();
		bytesList.add("letter");
		bytesList.add("frequency");
		bytesList.add("sent");
		
			double sentPercentage = ( (double) sent / total) ;
			double receivedPercentage = ((double) rcv / total);
			bytesList.add(getFormattedNumber(sentPercentage));
			bytesList.add("received");
			bytesList.add(getFormattedNumber(receivedPercentage));
			myfile.writeToFile("src/resources/data.tsv",bytesList);
			
	}
	
	
	public void showPower() {
		String [] header;
		String headline;
		if (!model.getGuiModel().hasBatteries()) {
			gui.getLblNf().setText("Sorry Dude. No batteries found.");
			
		}
		else {
			model.getGuiModel().setPowerSource();
			header = new String []{"Name","Remaining Capacity", "Remaining Time"};
			headline =  "Batteries";
			model.getGuiModel().getPage().setPage(header,model.getGuiModel().getPowerSource() ,headline );
			setCenter(gui.getServerSheet());
		}
	}
	
	
	public void showCpus() {
		String [] header;
		String headline;
		model.getGuiModel().setCpus();
		
		if (model.getGuiModel().getCpus().length == 0) {
			gui.getLblNf().setText("Sorry Dude. No CPUs found.");
		}
		else {
			header = new String []{"Identifier" ,"Frequency","Load per CPU"};
			headline =  model.getGuiModel().getCpus().length + " CPU(s) found!";
			model.getGuiModel().getPage().setPage(header,model.getGuiModel().getCpus(),headline );
			setCenter(gui.getServerSheet());
	 		
		}
	}
	
	public void showDisks() {
		String [] header;
		String headline;
		model.getGuiModel().setFileSystem();
		if (model.getGuiModel().getFileSystem().length == 0) {
			gui.getLblNf().setText("Sorry Dude. No disks found.");
		}
		else {
			header = new String []{"Partition", "Available","Total","Free"};
			headline = "File System";
			
			model.getGuiModel().getPage().setPage(header,model.getGuiModel().getFileSystem(),headline );
			setCenter(gui.getServerSheet());
		}
		
	}
	
	public void showThroughPut() {
		String [] header = new String []{"sent altogether", "received altogether","last sent on","last received on"};
		String headline = "How much data did the server receive or send excluding login?";
		ArrayList<String> row = new ArrayList<String>();
		row.add(FormatUtil.formatBytes(model.getGuiModel().getBytesSent()  ));
		row.add(FormatUtil.formatBytes(model.getGuiModel().getBytesReceived() ));
		row.add(model.getGuiModel().getLastSent() );
		row.add(model.getGuiModel().getLastReceived() );
		
		model.getGuiModel().getPage().setPage(header,row,headline );
		setCenter(gui.getServerSheet());
	}
	
	public void showRam() {
		model.getGuiModel().setMemTotal();
		model.getGuiModel().setmemAvailable();
		String [] header = new String []{"Available","Total","Free"};
		String headline = "Memory";
		ArrayList<String> row = new ArrayList<String>();
		long usable = model.getGuiModel().getmemAvailable();
		long total = model.getGuiModel().getMemTotal();
		
		row.add(FormatUtil.formatBytes(usable));
		row.add(FormatUtil.formatBytes(total));
		row.add(String.format("%.1f%%", 100d * usable / total));
		
		model.getGuiModel().getPage().setPage(header,row,headline );
		setCenter(gui.getServerSheet());
	}
	
	public void loadServerFacts()
	{
			String [] header = new String []{"Host","IP-Address","OS","on since"};
			model.getGuiModel().setIpAddress();
			model.getGuiModel().setHostName();
			model.getGuiModel().setOS();
			String headline = "Server Facts";
			ArrayList<String> row = new ArrayList<String>();
			row.add(model.getGuiModel().getHostName());
			row.add(model.getGuiModel().getIp());
			row.add(model.getGuiModel().getOs());
			row.add(model.getGuiModel().getUp());
		
			model.getGuiModel().getPage().setPage(header,row,headline );
			setCenter(gui.getServerSheet());
	}
	
	/**
	 * Load the connection list in the table
	 * and display it in the displayPane of the serverScreen
	 */
	public void loadClientList()
	{
		boolean isEmpty = isClientListEmpty();
		if (isEmpty)
		 {
			gui.getLblNf().setText("Sorry Dude. There aren't any clients connected to this server at the moment.");
		 } else {
			 gui.setDisplay(gui.getClientView().setUpClientList());
		 }
	}
	
	public void loadGameList()
	{
		boolean isEmpty = isGamesListEmpty();
		if (isEmpty)
		 {
			gui.getLblNf().setText("Sorry Dude. No games are taking place.");
		 } else {
			 gui.setDisplay(gui.getGameView().setUpGameList());
		 }
	}
	
	
	public void searchGamesList(String searchText) {
		boolean isEmpty = isGamesListEmpty();
		if (isEmpty)
		 {
			gui.getLblNf().setText("Sorry Dude. No games are taking place.");
		 }else {	
	    String headline = "Search Results in Games List";
		String [] columns = new String[] {"GameID","GameState"};
		model.getGuiModel().getPage().setPageOfGamesList(columns,headline,searchText);
		setBottomTable(gui.getWatchersSheet());
		}
	 }
		
	public void searchConnectionList(String searchText) {
		boolean isEmpty = isClientListEmpty();
		if (isEmpty)
		 {
			gui.getLblNf().setText("Sorry Dude. There aren't any clients connected to this server at the moment.");
		 } else {
			String headline = "Search Results of Nicks in Connection List "; 
			 printConnectionList(model.getConnections(),headline, searchText);
		 }
	}
		
	
	public String getSearchSelection(){
		if (gui.getRdoMessages().isSelected() && !gui.getRdoMessages().isDisabled()) {
			return rdoSearch[0];
		}
		else if (gui.getRdoGList().isSelected() && !gui.getRdoGList().isDisabled() ) {
			return rdoSearch[1];
		}
		else if (gui.getRdoCList().isSelected() && !gui.getRdoCList().isDisabled() ) {
			return rdoSearch[2];
		}
		else if (gui.getRdoWList().isSelected() && !gui.getRdoWList().isDisabled()) {
			return rdoSearch[3];
		}
		else if (gui.getRdoGU().isSelected() && !gui.getRdoGU().isDisabled()) {
			return rdoSearch[4];
		}
		else {
			return "";
		}
	}
	 
	public String getSortSelection(){
		if (gui.getRdoNick().isSelected() && !gui.getRdoNick().isDisabled()) {
			return rdoSort[0];
		}
		else if (gui.getrdoID().isSelected() && !gui.getrdoID().isDisabled()) {
			return rdoSort[1];
		}
		else if (gui.getRdoGameStateS().isSelected() && !gui.getRdoGameStateS().isDisabled()) {
			return rdoSort[2];
		}
		else if (gui.getRdoGameStateN().isSelected() && !gui.getRdoGameStateN().isDisabled()) {
			return rdoSort[3];
		}
		else if (gui.getRdoTxt().isSelected() && !gui.getRdoTxt().isDisabled()) {
			return rdoSort[4];
		}
		else {
			return "";
		}
	}
	
	public void sort(){
		gui.getLblNf().setText("");
		String radio = getSortSelection();
		switch(radio){
		case "Nick":
			showWatchers("sort","");
			break;
		case "GameID":
			showGames("sortGameID","");
			break;
		case "started":
			showGames("sortStarted","");
			break;
		case "not started":
			showGames("sortNotStarted","");
			break;
		case "Text":
			gui.getLblNf().setText("The 'Text' attribute is not sortable. Please select another attribute.");
			break;
		default:
			gui.getLblNf().setText("Please select an attribute in the sort area.");
			break;
		}
	}

	public boolean prepSearch(String textToSearch)
	{	
		textToSearch = textToSearch.trim().toLowerCase();
		boolean isReady = false;
		int lengthOfText = textToSearch.length();
		String selectedSearch = getSearchSelection();
		String selectedSort = getSortSelection();
		//String [] rdoSearch = new String [] {"Messages List","Games List","Connection List","Watchers","Games of User"};
		switch(selectedSort){
		case "Nick":
			if (lengthOfText > 0 && (rdoSearch[2].equals(selectedSearch) || rdoSearch[3].equals(selectedSearch)|| rdoSearch[0].equals(selectedSearch))) {
				isReady = true;
			}
			if (lengthOfText == 0) {
				errorMessage = "Your search text is a required field.";
			}
			else {
				errorMessage = "Please select one attribute and one list to perform search.";
			}
			break;
		case "GameID":
			if (lengthOfText > 0 && (rdoSearch[1].equals(selectedSearch)|| rdoSearch[4].equals(selectedSearch) )) {
				isReady = true;
			}
			if (lengthOfText == 0) {
				errorMessage = "Your search text is a required field.";
			}
			else {
				errorMessage = "Please select one attribute and one list to perform search.";
			}
			break;
		case "started":
			if (rdoSearch[1].equals(selectedSearch) || rdoSearch[4].equals(selectedSearch) ||rdoSearch[0].equals(selectedSearch)) {
				isReady = true;
			}
			else {
				errorMessage = "Please select one attribute and one list to perform search.";
			}
			break;
		case "not started":
			if (rdoSearch[1].equals(selectedSearch) || rdoSearch[4].equals(selectedSearch) ||rdoSearch[0].equals(selectedSearch))  {
				isReady = true;
			}
			else  {
				errorMessage = "Please select one attribute and one list to perform search.";
			}
			break;
		case "Text":
			if (rdoSearch[0].equals(selectedSearch) )  {
				isReady = true;
			}
			else  {
				errorMessage = "Please select one attribute and one list to perform search.";
			}
			break;
		default:
			break;
		}
		
		return isReady;
	}
	
//	String queryStr="te*t";
//	queryStr= queryStr.replaceAll("\\*", "\\\\w*");
//queryStr= queryStr.replaceAll("\\*", ".*"); 
	public void search(String searchText){
		gui.getLblNf().setText("");
		boolean isSearchable = prepSearch(searchText);
		String selectedSearch = getSearchSelection();
		String selectedSort = getSortSelection();
		
		if ( isSearchable ) { 
			searchText = searchText.trim().toLowerCase();
			//searchText = searchText.replaceAll("\\*", "\\\\w*");
			switch(selectedSearch){
			case "Messages List":
				if (selectedSort.equals(rdoSort[4]) || selectedSort.equals(rdoSort[0]) ) {
					showMessages(searchText);
				}
				if (selectedSort.equals(rdoSort[2])) {
					showMessages(rdoSort[2]);
				}
				if (selectedSort.equals(rdoSort[3])) {
					showMessages(rdoSort[3]);
				}
				break;
			case "Games List":
				if (selectedSort.equals(rdoSort[1])) {
					searchGamesList(searchText);
				}
				if (selectedSort.equals(rdoSort[2])) {
					searchGamesList(rdoSort[2]);
				}
				if (selectedSort.equals(rdoSort[3])) {
					searchGamesList(rdoSort[3]);
				}
				break;
			case "Connection List":
				if (selectedSort.equals(rdoSort[0])) {
					searchConnectionList(searchText);
				}
				break;
			case "Watchers":
				if (selectedSort.equals(rdoSort[0])) {
					showWatchers("",searchText);
				}
				break;
			case "Games of User":
				if (selectedSort.equals(rdoSort[1])) {
					showGames("",searchText);
				}
				if (selectedSort.equals(rdoSort[2])) {
					showGames("",rdoSort[2]);
				}
				if (selectedSort.equals(rdoSort[3])) {
					showGames("",rdoSort[3]);
				}
			break;
			
			
			}
		}
		if ( !isSearchable ) { 
			gui.getLblNf().setText(errorMessage);
		}
	}
	
	
   public void setSearchRdoButtons() {
	   String radio = getSortSelection();
	   String selectedSearch = getSearchSelection();
		switch(radio){
		case "Nick":
			gui.getRdoMessages().setDisable(false);
			gui.getRdoCList().setDisable(false);
			gui.getRdoWList().setDisable(false);
			gui.getRdoGList().setDisable(true);
			gui.getRdoGU().setDisable(true);
			gui.getTxtSearch().setDisable(false);
			break;
		case "GameID":
			gui.getRdoMessages().setDisable(true);
			gui.getRdoCList().setDisable(true);
			gui.getRdoWList().setDisable(true);
			gui.getRdoGList().setDisable(false);
			gui.getRdoGU().setDisable(false);
			gui.getTxtSearch().setDisable(false);
			break;
		case "started":
			gui.getRdoMessages().setDisable(false);
			gui.getRdoCList().setDisable(true);
			gui.getRdoWList().setDisable(true);
			gui.getRdoGList().setDisable(false);
			gui.getRdoGU().setDisable(false);
			gui.getTxtSearch().setDisable(true);
			break;
		case "not started":
			gui.getRdoMessages().setDisable(false);
			gui.getRdoCList().setDisable(true);
			gui.getRdoWList().setDisable(true);
			gui.getRdoGList().setDisable(false);
			gui.getRdoGU().setDisable(false);
			gui.getTxtSearch().setDisable(true);
			break;
		case "Text":
			gui.getRdoMessages().setDisable(false);
			gui.getRdoCList().setDisable(true);
			gui.getRdoWList().setDisable(true);
			gui.getRdoGList().setDisable(true);
			gui.getRdoGU().setDisable(true);
			gui.getTxtSearch().setDisable(false);
			break;
//		default:
//			gui.getLblNf().setText("Please select a checkbox in the sort area.");
//			break;
		}
   }
			
		
		
	public Connection getConnection(){
		boolean isEmpty = isClientListEmpty();
		boolean hasSelected = false;
		if (isEmpty)
		 {
			gui.getLblNf().setText("Sorry Dude. There aren't any clients connected to this server at the moment.");
			return null;
		 }
		else 
		{
			hasSelected = IsSelectedTableConnection();
			if (!hasSelected) {
			gui.getLblNf().setText("Please select a row in the connection list first before pressing the button.");
			return null;
			}
			else {
				Connection user = gui.getClientView().getTable().getSelectionModel().getSelectedItem();
				return user;
			}
		}
		
	}
	
	public Game getGame(){
		boolean isEmpty = isGamesListEmpty();
		boolean hasSelected = false;
		if (isEmpty)
		 {
			gui.getLblNf().setText("Sorry Dude. No games are taking place.");
			return null;
		 }
		else
		{	
			hasSelected = IsSelectedTableGame();
			if (!hasSelected) {
			gui.getLblNf().setText("Please select a row in the games list first before pressing the button.");
			return null;
			}
			else {
				Game keyGame = gui.getGameView().getTable().getSelectionModel().getSelectedItem();
				Game selectedGame = model.getGames().get(keyGame.getGameID());
				return selectedGame;
			}
		}
	}
	
	public void deleteGame() {
		gui.getLblNf().setText("");
		Game game = getGame();
		if (game != null )
		{
			String state = game.getGameState().toString();
			switch(state) {
				case "not started":
					endGame(true);
					
					//gui.getLblNf().setText("Game has been deleted.");
					break;
				case "started":
					gui.getLblNf().setText("Please press the 'end game' button to end this game.");
					break;
			}
		}
	}
	
	public void removeWatchers(ObservableList<Connection> watchers){
		for (Iterator<Connection> iterator = watchers.iterator(); iterator.hasNext(); ) {
		    Connection conn = iterator.next();
		    value[2] = conn.getNick();
			 JSONObject delete =  getJSON(endGameKeys,value);
			 gameHandler.processJSON(delete, conn);
		}
	}
	
	public void endGame(boolean deleteGame) {
		gui.getLblNf().setText("");
		Game selectedGame = getGame();
		if (selectedGame != null )
		{
			List<String> outputList = new ArrayList<String>();
			String headline = "Ending Game " + selectedGame.getGameID();
			value[0] = "leave game";
			value[1] = selectedGame.getGameID();
			String state = selectedGame.getGameState().toString();
			ObservableList<Player> players = selectedGame.getPlayers();
			
			for (Iterator<Player> iterator = players.iterator(); iterator.hasNext(); ) {
			    Player player = iterator.next();
			    value[2] = player.getNick();
				  JSONObject delete =  getJSON(endGameKeys,value);
				  gameHandler.processJSON(delete, player.getConnection());
			}
			if (deleteGame) {
				model.getGames().remove(value[1]);
				headline = "Deleting Game " + value[1];
			}
			
			switch(state) {
			
			case "not started":
				if (players == null || players.isEmpty()  ) {
					outputList.add("Players were removed from game.");
				}
				if (!players.isEmpty() ) {
					outputList.add("Players were not removed from game.");
				}
				if (selectedGame.getWatchers().isEmpty()){
					outputList.add("Nobody was watching this game.");
					outputList.add("You can only end games which have already been started.");
				}
				if (!selectedGame.getWatchers().isEmpty() ) {
					outputList.add("Removing watchers. But you can only end games which were already started.");
				}
				if (model.getGames().get(value[1]) != null) {
					outputList.add("This game still exists. Check out Games List.");
				}
				if (model.getGames().get(value[1]) == null) {
					outputList.add("Game has been deleted.");
				}
				break;
			case "started":
				if (players == null  ) {
					outputList.add("Players were removed from game.");
				}
				if (!players.isEmpty() ) {
					outputList.add("Players were not removed from game.");
				}
				if (selectedGame.getWatchers() == null){
					outputList.add("Nobody was watching this game.");
				}
				if (!selectedGame.getWatchers().isEmpty() ) {
					outputList.add("Removing watchers. ");
				}
				if (model.getGames().get(value[1]) != null) {
					outputList.add("This game still exists. Check out Games List.");
				}
				if (model.getGames().get(value[1]) == null) {
					outputList.add("Game has been deleted.");
				}
				
				break;
			}
			model.getGuiModel().getMessage().setMessages(outputList,headline);
			setBottomMessages(gui.getInfoSheet());
			if (!selectedGame.getWatchers().isEmpty() ) {
				ObservableList<Connection> watchers = selectedGame.getWatchers();
				removeWatchers(watchers);
			}
		}
	}
	public void printConnectionList(ObservableList<Connection> observableList,String headline,String searchText){
		String [] columns = new String[] {"Nick"};
  		model.getGuiModel().getPage().setPageOfConnectionList(columns, observableList,headline,searchText);
  		setBottomTable(gui.getWatchersSheet());
	}
	
	public void printConnectionList(ObservableList<Connection> observableList,String headline){
		String [] columns = new String[] {"Nick"};
  		model.getGuiModel().getPage().setPageOfConnectionList(columns, observableList,headline);
  		setBottomTable(gui.getWatchersSheet());
	}
	
	public void sortWatchers(Game game) 
	{//!game.getWatchers().isEmpty()
			
			String headline = "Sorted Watchers List of Game " + game.getGameID(); 
			if ( isAscendingNick)
	        	 {	
				Collections.sort(game.getWatchers(), model.getGuiModel().getNickCompare());
				printConnectionList(game.getWatchers(),headline);
	        	 }
			if ( !isAscendingNick)
        	 {	
				Collections.sort(game.getWatchers() ,Collections.reverseOrder(model.getGuiModel().getNickCompare()));
				printConnectionList(game.getWatchers(),headline);
        	 }
			isAscendingNick = !isAscendingNick;
		}
	
	public void searchWatchers(Game game, String searchText) 
	{//!game.getWatchers().isEmpty() && 
			String headline = ""; 
				if (  searchText.length() > 0 )
 	        	 {
					headline = "Searching for Watchers of Game " + game.getGameID(); 
					printConnectionList(game.getWatchers(),headline,searchText);
 	        	 }
		}

	public void showWatchers(String s, String searchText) 
	{	
		gui.getLblNf().setText("");
		Game game = getGame();
		if (game != null )
		{		
			String headline = "All Watchers of Game " + game.getGameID(); 
			switch(s){
		case "sort":
			if (!game.getWatchers().isEmpty() )
        	 {
				sortWatchers(game);
        	 }
			if (game.getWatchers().isEmpty() )
       	 {
        		gui.getLblNf().setText("No watchers are currently involved in the game.");
       	 }
			break;
			default:
				if (!game.getWatchers().isEmpty() &&  searchText.length() == 0)
 	        	 {
					printConnectionList(game.getWatchers(),headline);
 	        	 }
				if (!game.getWatchers().isEmpty() &&   searchText.length() > 0 )
 	        	 {
					searchWatchers(game,searchText);
 	        	 }
				if (game.getWatchers().isEmpty() )
	        	 {
 	        		gui.getLblNf().setText("No watchers are currently involved in the game.");
	        	 }
			break;
		}
  	 }
	}
	
	public void showPlayers() 
	{	
		gui.getLblNf().setText("");
		Game game = getGame();
		if (game != null )
		{
  	        	 if (!game.getPlayers().isEmpty() )
  	        	 {
  	        		gui.setBottom(gui.getGameView().setUpPlayersList(game.getPlayers()));
  	        		paintNickColumn();	
  	        	 }
  	        	 if (game.getPlayers().isEmpty())
  	        	 {
	        		gui.getLblNf().setText("No players are currently involved in the game.");
  	        	 }
  	     }
	}
	
	/**
	 * After user pressed the refresh button 
	 * paint the nick of the client red if
	 * client is disconnected
	 */
	public void paintNickColumn()
	{
		gui.getGameView().getColNick().setCellFactory(new Callback<TableColumn<Player, String>, TableCell<Player, String>>() {
	        public TableCell<Player, String> call(TableColumn<Player, String> param) {
	            return new TableCell<Player, String>() {
	                public void updateItem(String item, boolean empty) {
	                    super.updateItem(item, empty);
	                    if (!isEmpty()) { 
	                       setText(item);   
	                       for (int i = 0; i < gui.getGameView().getPlayers().size(); i++) {
		          	        	 if (item.equals(gui.getGameView().getPlayers().get(i).getNick()  )  )
		          	        	 {
		          	        		String playerColor = gui.getGameView().getPlayers().get(i).getColor().toString();
		          	        		TableRow currentRow = getTableRow();
		          	        		currentRow.getStyleClass().add(mycolor + playerColor);
		          	        	 }
		          	        }
	                    } 
	                }
	            };
	        }
	    });
	}
	
	
	public void pingClient() {
		gui.getLblNf().setText("");
		Connection user = getConnection();
		if (user != null)
		{
			gui.getLblNf().setText("Trying to reach selected host.");
			String headline = "Checking Connection to User " + user.getNick();
			List<String> outputList = new ArrayList<String>();
			String ip = user.getIp();
			double duration = 0;
			int timeOut = 10000; 
			boolean status = false;
			double start = 0;
			   
			try {
			    	start = System.currentTimeMillis();
			    	outputList.add(String.format("Pinging user " + user.getNick() + " with a timeout of %,d ms." , timeOut ));
					status = InetAddress.getByName(ip).isReachable(timeOut);
					duration = System.currentTimeMillis()-start;
			} catch (UnknownHostException e) {
					outputList.add("Host may not exist.");
			} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			}
			if (status) {
				 //list.add(String.format("Ping time is %.3f seconds",duration / 1000 ));
				 outputList.add(String.format("Ping time is %.0f ms.",duration ));
			 }
			 if (!status){
				 outputList.add("Could not ping host.");
			 }
			 model.getGuiModel().getMessage().setMessages(outputList,headline);
			 setBottomMessages(gui.getInfoSheet());
  	        }
			
		}
	
	public void printGames(Connection user,String headline,String searchText,String [] columns){
  		model.getGuiModel().getPage().setPageOfUserList(columns, headline,user,searchText);
  		setBottomTable(gui.getGamesSheet());
	}
	public void printGames(Connection user,String headline,String [] columns){
  		model.getGuiModel().getPage().setPageOfUserList(columns, headline,user);
  		setBottomTable(gui.getGamesSheet());
	}
	public void sortGames(String s,Connection user) 
	{
			
				switch(s) {
					case "sortGameID":
						if (!user.getGames().isEmpty() && isAscendingID){
							//user.getGames().indexOf( );
		 				Collections.sort(user.getGames(), model.getGuiModel().getIdCompare());
						}
						if (!user.getGames().isEmpty() && !isAscendingID){
		 				Collections.sort(user.getGames(),  Collections.reverseOrder(model.getGuiModel().getIdCompare()));
						}
					break;
					case "sortStarted":
//						if (!user.getGames().isEmpty())
//		  	        	 {
		  	        		Collections.sort(user.getGames(),  Collections.reverseOrder(model.getGuiModel().getStateCompare()));
//		  	        	 }
						break;
					case "sortNotStarted":
//						if (!user.getGames().isEmpty())
//		  	        	 {
		  	        		Collections.sort(user.getGames(), model.getGuiModel().getStateCompare());
		  	        	 //}
						break;
						
		 			}
				isAscendingID = !isAscendingID;
		}
		//String [] rdoSort = new String [] {"Nick","GameID","started","not started","Text"};
	public void searchGames(String searchText, Connection user) 
	{
			String headline = "Search Results in Games of " + user.getNick() ;
			String [] columns = new String[] {"GameID","GameState"};
//							if (!user.getGames().isEmpty() )
//			  	        	 {
			  	        		printGames(user,headline,searchText,columns);
//			  	        	 }
		 			}
			 		
	
	public void showGames(String task, String searchText) 
	{	
		gui.getLblNf().setText("");
		Connection user = getConnection();
		
		if (user != null) {
			String headline = "All Games of " + user.getNick() ;
			String [] columns = new String[] {"GameID","GameState"};
			switch(searchText.length()){
			case 0:
				if (!user.getGames().isEmpty() && task.contains("sort"))
 	        	 {	
					String headlineSort = "Sorted Games of " + user.getNick() ;
					sortGames(task,user);
					printGames(user,headlineSort,columns);
 	        	 }
				if (!user.getGames().isEmpty() && task.length()==0 )
 	        	 {
					printGames(user,headline,columns);
 	        		
 	        	 }
				if (user.getGames().isEmpty()) {
					gui.getLblNf().setText("Selected user is not in a game at the moment.");
				}
				break;
			default:
				if (!user.getGames().isEmpty() )
 	        	 {
					searchGames(searchText,user);
 	        		
 	        	 }
				if (user.getGames().isEmpty()) {
					gui.getLblNf().setText("Selected user is not in a game at the moment.");
				}
				break;
			}
		 }
	}
			 		
	public void disconnectClient() {
		gui.getLblNf().setText("");
		Connection user = getConnection();
		if (user != null)
		{
			String key = "type";
			String val = "disconnect";
			JSONObject disco = getJSON(key, val );
			gameHandler.processJSON(disco,user);
		}
		
	}
	public void printMessages(Connection user,String searchText, String headline){
		model.getGuiModel().getMessage().setMessages(user.getJsonObjects(),headline,searchText);
		setBottomMessages(gui.getMessageSheet());
	}
	
	public void printMessages(Connection user,String headline){
		model.getGuiModel().getMessage().setMessages(user.getJsonObjects(),headline);
		setBottomMessages(gui.getMessageSheet());
	}
	
	public void searchMessages(Connection user,String searchText)
	{
			 ObservableList<String> observableList = user.getJsonObjects();
//  	        	if (! observableList.isEmpty())
// 	        	 {	
  	        		String headline = "Search Results in Messages Regarding User " + user.getNick();
 	        		printMessages(user,searchText,headline);
 	        	 //}
		}

	/**
	 * Load all the messages that are associated with the client in webView
	 * @param searchText 
	 * @param string 
	 * @throws IOException 
	 */
	public void showMessages(String searchText)
	{	
		gui.getLblNf().setText("");
		Connection user = getConnection();
		if (user != null)
		{	
			ObservableList<String> observableList = user.getJsonObjects();
			switch(searchText.length()){
			case 0:
				 if (! observableList.isEmpty() )
  	        	 {	
  	        		String headline = "The Messages Regarding User " + user.getNick();
  	        		printMessages(user, headline);
  	        	 }
				break;
			default:
				searchMessages(user,searchText);
				break;
  	        }
		}
	}
}