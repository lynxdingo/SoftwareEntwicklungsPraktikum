package server.code;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import server.model.Card;
import server.model.Color;
import server.model.Connection;
import server.model.Game;
import server.model.Gamestate;
import server.model.Key;
import server.model.Model;
import server.model.Person;
import server.model.Player;
import server.model.Playerstate;
import server.model.Room;
import server.model.Suspicion;
import server.model.Weapons;

public class GameHandler {
	private static final Logger log = LogManager.getLogger( GameHandler.class.getName() );
	Model model;
	
	public GameHandler(Model model){
		this.model = model;
	}
	/**
	 * Verarbeitet die gesendeten JSONObjekte der Clients.
	 * @param obj	Das gesendete JSONObject
	 * @param conn	Die Verbindung über welche gesendet wurde (Der Client)
	 */
	public void processJSON(JSONObject obj, Connection conn) {
		if(obj.has("jsonID")){
			JSONObject ok = new JSONObject();
			ok.put("type", "ok");
			ok.put("jsonID", obj.getInt("jsonID"));
		}
		if(obj.isNull("type")){
			sendError(conn, "You have to put a type Argument");
			return;
		}
		String type = obj.getString("type");
		if(!conn.isConnected()&&type!="login"){
			sendError(conn, "You have to login first!");
			return;
		}
			int k = 1;
		switch(type){
		case "login": 	handleLogin(conn, obj);
//		System.out.println(k);
//		k++;
						break;
		case "chat":	handleChat(conn, obj);
//		System.out.println(k);
//		k++;
						break;
		case "leave game":	handleLeave(conn, obj);
//		System.out.println(k);
//		k++;
							break;
		case "disconnect":	handleDisconnect(conn, obj);
//		System.out.println(k);
//		k++;
							break;
		case "create game": handleCreateGame(conn, obj);
//		System.out.println(k);
//		k++;
							break;
		case "join game":	handleJoinGame(conn, obj);
//		System.out.println(k);
//		k++;
							break;
		case "watch game":	handleWatchGame(conn, obj);
//		System.out.println(k);
//		k++;
							break;
		case "start game":	handleStartGame(conn, obj);
//		System.out.println(k);
//		k++;
							break;
		case "roll dice":	handleRollDice(conn, obj);
//		System.out.println(k);
//		k++;
							break;
		case "move":		handleMove(conn, obj);
//		System.out.println(k);
//		k++;
							break;
		case "secret passage":	handleSecretPassage(conn, obj);
//		System.out.println(k);
//		k++;
								break;
		case "suspect":		handleSuspect(conn, obj);
//		System.out.println(k);
//		k++;
							break;
		case "disprove":	handleDisprove(conn, obj);
//		System.out.println(k);
//		k++;
							break;
		case "end turn":	handleEndTurn(conn, obj);
//		System.out.println(k);
//		k++;
							break;
		case "accuse":		handleAccuse(conn, obj);
//		System.out.println(k);
//		k++;
							break;
		case "superpower":	handleSuperpower(conn, obj);
//		System.out.println(k);
//		k++;
							break;
		default:			sendError(conn, "This Type is not supported");
		
		}
		
		
		sendOK(conn);
		
	}
	
	private void handleAccuse(Connection conn, JSONObject obj) {
		//normal
		//Complete Request
		if(obj.isNull("gameID")||obj.isNull("statement")){
			sendError(conn, "The Request was not complete!");
			return;
		}
		int gameID = obj.getInt("gameID");
		//Does Game exist
		if(!model.getGames().containsKey(gameID)){
			sendError(conn, "A Game with this ID does not exist!");
			return;
		}
		Game game = model.getGames().get(gameID);
		//Is he Player in the Game
		if(!game.isPlayer(conn)){
			sendError(conn, "You have to be Player to roll the dice!");
			return;
		}
		Person person = game.getPerson(conn);
		
		//Is it his time to accuse
		if(!person.getPlayer().getPlayerState().contains(Playerstate.ACCUSE)){
			sendError(conn, "its not your turn to accuse somebody!");
			return;
		}
		if(person != game.getActivePerson()){
			sendError(conn, "Strange Error: Your not the active Player");
		}
		
		//Is statement not complete
		JSONObject sus = obj.getJSONObject("statement");
		if(sus.isNull("weapon")||sus.isNull("person")||sus.isNull("room")){
			sendError(conn, "You have to make a complete Accusation");
			return;
		}
		Suspicion suspicion = new Suspicion(sus.getString("room"), sus.getString("weapon"), sus.getString("person"));
		if(game.isSolution(suspicion)){
			//Hes right
			JSONObject end = new JSONObject();
			end.put("type", "game ended");
			end.put("gameID", gameID);
			end.put("nick", person.getPlayer().getConnection().getNick());
			end.put("statement", obj.getJSONObject("statement"));
			
			game.setGameState(Gamestate.ENDED);
			model.getGames().remove(gameID);  
			sendMessage(model.getConnections(), end);
		}
		else{
			//Wrong Accusation
			JSONObject wrong = new JSONObject();
			wrong.put("type", "wrong accusation");
			wrong.put("gameID", gameID);
			wrong.put("statement", obj.get("statement"));
			
			sendMessage(game.getAllConnections(), wrong);
			
			person.getPlayer().setOut();
			person.getPlayer().resetPlayerstate();
			person.getPlayer().addPlayerState(Playerstate.DO_NOTHING);
			sendStatusupdate(game.getAllConnections(), person.getPlayer());
			Person next = game.getNextPerson();  
			Room room =  model.getGameField().getFields().get(next.getField()).getRoom();
			next.getPlayer().resetPlayerstate();
			if(room == Room.CONSERVATORY || room == Room.STUDY || room == Room.KITCHEN || room == Room.LOUNGE){
				//He could use the secretPassage
				next.getPlayer().addPlayerState(Playerstate.USE_SECRET);
			}
			next.getPlayer().addPlayerState(Playerstate.ROLL_DICE);
			next.getPlayer().addPlayerState(Playerstate.ACCUSE);
			
			sendStatusupdate(game.getAllConnections(), next.getPlayer());
		}
		
		
	}
	private void handleEndTurn(Connection conn, JSONObject obj) {
		//normal
		//Complete Request
		if(obj.isNull("gameID")){
			sendError(conn, "The Request was not complete!");
			return;
		}
		int gameID = obj.getInt("gameID");
		//Does Game exist
		if(!model.getGames().containsKey(gameID)){
			sendError(conn, "A Game with this ID does not exist!");
			return;
		}
		Game game = model.getGames().get(gameID);
		//Is he Player in the Game
		if(!game.isPlayer(conn)){
			sendError(conn, "You habe to be Player to roll the dice!");
			return;
		}
		Person person = game.getPerson(conn);
		
		//Is it his time to end the turn
		if(!person.getPlayer().getPlayerState().contains(Playerstate.END_TURN)){
			sendError(conn, "Its not your time to end the game!");
			return;
		}
		if(person != game.getActivePerson()){
			sendError(conn, "Strange mistake: Your not the active player");
			return;
		}
		
		//Everythings ok
		person.getPlayer().resetPlayerstate();
		person.getPlayer().addPlayerState(Playerstate.DO_NOTHING);
		sendStatusupdate(game.getAllConnections(), person.getPlayer());
		Person next = game.getNextPerson();
		Room room =  model.getGameField().getFields().get(next.getField()).getRoom();
		next.getPlayer().resetPlayerstate();
		if(room == Room.CONSERVATORY || room == Room.STUDY || room == Room.KITCHEN || room == Room.LOUNGE){
			//He could use the secretPassage
			next.getPlayer().addPlayerState(Playerstate.USE_SECRET);
		}
		next.getPlayer().addPlayerState(Playerstate.ROLL_DICE);
		next.getPlayer().addPlayerState(Playerstate.ACCUSE);
		
		sendStatusupdate(game.getAllConnections(), next.getPlayer());
		
		
	}
	private void handleDisprove(Connection conn, JSONObject obj) {
		//normal
		//Complete Request
		if(obj.isNull("gameID")){
			sendError(conn, "The Request was not complete!");
			return;
		}
		int gameID = obj.getInt("gameID");
		//Does Game exist
		if(!model.getGames().containsKey(gameID)){
			sendError(conn, "A Game with this ID does not exist!");
			return;
		}
		Game game = model.getGames().get(gameID);
		//Is he Player in the Game
		if(!game.isPlayer(conn)){
			sendError(conn, "You habe to be Player to roll the dice!");
			return;
		}
		Person person = game.getPerson(conn);
		
		//Is it his time to disprove
		if(!person.getPlayer().getPlayerState().contains(Playerstate.DISPROVE)){
			sendError(conn, "Its not your time to disprove");
			return;
		}
		if(person.getPlayer() != game.getActiveDisprove()){
			sendError(conn, "Strange Error");
			return;
		}
		//Can he disprove
		if(obj.isNull("card")){
			//The next one has to disprove
			Player next = game.getNextDisprove();
			if(next == game.getActivePerson().getPlayer()){
				//One round is over
				JSONObject noDisprove = new JSONObject();
				noDisprove.put("type", "no disprove");
				noDisprove.put("gameID", gameID);
				
				game.getActivePerson().getPlayer().resetPlayerstate();
				game.getActivePerson().getPlayer().addPlayerState(Playerstate.END_TURN);
				game.getActivePerson().getPlayer().addPlayerState(Playerstate.ACCUSE);
				sendStatusupdate(game.getAllConnections(), game.getActivePerson().getPlayer());
				person.getPlayer().resetPlayerstate();
				person.getPlayer().addPlayerState(Playerstate.DO_NOTHING);
				sendStatusupdate(game.getAllConnections(), person.getPlayer());
				sendMessage(game.getAllConnections(), noDisprove);
				game.disproveEnd();
			}
			else{
				//There is a next Player
				person.getPlayer().resetPlayerstate();
				person.getPlayer().addPlayerState(Playerstate.DO_NOTHING); 
				sendStatusupdate(game.getAllConnections(), person.getPlayer());
				next.resetPlayerstate();
				next.addPlayerState(Playerstate.DISPROVE);
				sendStatusupdate(game.getAllConnections(), next);
			}
		}
		else{
			//He can disprove
			Card card = Card.getCard(obj.getString("card"));
			if(card == null){
				sendError(conn, "This Card does not exist!");
				return;
			}
			//Does he has the Card
			if(!person.getPlayer().getCards().contains(card)){
				sendError(conn, "You do not possess this card!");
				return;
			}
			//If it is not in the suspicion
			if(!game.getSuspicion().contains(card)){
				sendError(conn, "You cant disprove the Suspicion with this Card!");
				return;
			}
			person.getPlayer().resetPlayerstate();
			person.getPlayer().addPlayerState(Playerstate.DO_NOTHING);
			sendStatusupdate(game.getAllConnections(), person.getPlayer());
			
			game.disproveEnd();
			game.getActivePerson().getPlayer().resetPlayerstate();
			game.getActivePerson().getPlayer().addPlayerState(Playerstate.END_TURN);
			game.getActivePerson().getPlayer().addPlayerState(Playerstate.ACCUSE);
			sendStatusupdate(game.getAllConnections(), game.getActivePerson().getPlayer());
			
			//Send mina
			JSONObject disprove = new JSONObject();
			disprove.put("type", "disproved");
			disprove.put("gameID", gameID);
			disprove.put("nick", person.getPlayer().getConnection().getNick());
			
			sendMessage(game.getAllConnectionsExcept(game.getActivePerson().getPlayer().getConnection()),disprove);
			
			disprove.put("card", card.toString());
			
			//Send Player
			sendMessage(game.getActivePerson().getPlayer().getConnection(), disprove);
					
		}
		
	}
	private void handleSuspect(Connection conn, JSONObject obj) {
		//normal
		//Complete Request
		if(obj.isNull("gameID")||obj.isNull("statement")){
			sendError(conn, "The Request was not complete!");
			return;
		}
		int gameID = obj.getInt("gameID");
		//Does Game exist
		if(!model.getGames().containsKey(gameID)){
			sendError(conn, "A Game with this ID does not exist!");
			return;
		}
		Game game = model.getGames().get(gameID);
		//Is he Player in the Game
		if(!game.isPlayer(conn)){
			sendError(conn, "You have to be Player to roll the dice!");
			return;
		}
		Person person = game.getPerson(conn);
		
		JSONObject statement = obj.getJSONObject("statement");
		if(statement.isNull("person")||statement.isNull("weapon")){
			sendError(conn, "Your Suspicion is not complete!");
			return;
		}
		Color color = Color.getColor(statement.getString("person"));
		Weapons weapon = Weapons.getWeapon(statement.getString("weapon"));
		Room room =  model.getGameField().getFields().get(person.getField()).getRoom();
		
		System.out.println(person.getField().getPosition());
		
		if(room == null){
			sendError(conn, "Strange Error: Your not in a room");
			return;
		}
		if(statement.has("room")){
			if(room != Room.getRoom(statement.getString("room"))){
				sendError(conn, "Your not in this frickin room, moron");
				return;
			}
		}
		//Is it the pool?
		if(room == Room.POOL){
			sendError(conn, "No one can be killed inside the pool!");
			return;
		}
		//Is it his turn to suspect?
		if(!person.getPlayer().getPlayerState().contains(Playerstate.SUSPECT)){
			sendError(conn, "Its not your turn to suspect somebody");
			return;
		}
		
		
		//Save suspicion inside game
		game.setSuspicion(new Suspicion(room, weapon, color));
		
		//Move Suspected Murder to Room
		Person suspect = game.getPersons().get(color);
		suspect.setField(model.getGameField().getFieldForRoom(room));
		
		//Send Suspicion to mina
		JSONObject send = new JSONObject();
		send.put("type", "suspicion");
		send.put("gameID", gameID);
		send.put("statement", statement);
		sendMessage(game.getAllConnections(), send);
		
		Player disprove = game.getNextDisprove();
		disprove.resetPlayerstate();
		disprove.addPlayerState(Playerstate.DISPROVE);
		
		//Statusupdate for disprove
		
		sendStatusupdate(game.getAllConnections(), disprove);
		
		
		
		
		
		
	}
	private void handleSecretPassage(Connection conn, JSONObject obj) {
		//normal
		//Complete Request
		if(obj.isNull("gameID")){
			sendError(conn, "The Request was not complete!");
			return;
		}
		int gameID = obj.getInt("gameID");
		//Does Game exist
		if(!model.getGames().containsKey(gameID)){
			sendError(conn, "A Game with this ID does not exist!");
			return;
		}
		Game game = model.getGames().get(gameID);
		//Is he Player in the Game
		if(!game.isPlayer(conn)){
			sendError(conn, "You have to be Player to roll the dice!");
			return;
		}
		Person person = game.getPerson(conn);
		
		//It is his turn to move
		if(!person.getPlayer().getPlayerState().contains(Playerstate.USE_SECRET)){
			sendError(conn, "You cant use a secret passage just now!");
			return;
		}
		Room room = model.getGameField().getFields().get(person.getField()).getRoom();
		Key key = new Key(9, 12);
		switch(room){
		case STUDY:		key = 	new Key(19,18);
								break;
		case KITCHEN:	key = 	new Key(6,3);
								break;
		case LOUNGE:	key = 	new Key(4,19);
								break;
		case CONSERVATORY:key = new Key(17,5);
								break;
		default:				sendError(conn, "Strange mistake youre in the wrong room ");
								return;
		}
		person.setField(key);
		
		person.getPlayer().resetPlayerstate();
		person.getPlayer().addPlayerState(Playerstate.SUSPECT);
		person.getPlayer().addPlayerState(Playerstate.ACCUSE);
		JSONObject newPos = new JSONObject();
		newPos.put("type", "moved");
		newPos.put("gameID", gameID);
		newPos.put("person position", person.getPositon());
		
		sendMessage(game.getAllConnections(), newPos);

		sendStatusupdate(game.getAllConnections(), person.getPlayer());
		
	}
	private void handleMove(Connection conn, JSONObject obj) {
		//normal
		//Complete Request
		if(obj.isNull("gameID")||obj.isNull("field")){
			sendError(conn, "The Request was not complete!");
			return;
		}
		JSONObject field = obj.getJSONObject("field");
		//Is the field complete
		if(field.isNull("x")||field.isNull("y")){
			sendError(conn, "You havent send the full Field");
			return;
		}
		Key key = new Key(field.getInt("x"),field.getInt("y"));
		//Does the Field is accesible
		if(!model.getGameField().getFields().containsKey(key)){
			sendError(conn, "The Place you are trying to access does not exist");
			return;
		}
			
		int gameID = obj.getInt("gameID");
		//Does Game exist
		if(!model.getGames().containsKey(gameID)){
			sendError(conn, "A Game with this ID does not exist!");
			return;
		}
		Game game = model.getGames().get(gameID);
		//Is he Player in the Game
		if(!game.isPlayer(conn)){
			sendError(conn, "You habe to be Player to roll the dice!");
			return;
		}
		Person person = game.getPerson(conn);
		//Its the Players Time to Move
		
		if(!person.getPlayer().getPlayerState().contains(Playerstate.MOVE)){
			sendError(conn, "Its not your time to move your figure");
			return;
		}
		
		//Can the Player reach the Field from his old Positon
		
		if(!model.getGameField().getFieldsForRoll(person.getField(), person.getPlayer().getLastThrow(),game.getPositions()).contains(model.getGameField().getFields().get(key))){
			sendError(conn, "You cant reach this place from your position!");
			return;
		}
		
		if(model.getGameField().getFields().get(key).getRoom()==Room.POOL){
			JSONObject pool = new JSONObject();
			pool.put("type", "poolcards");
			pool.put("gameID", gameID);
			pool.put("cards", game.getPoolCards());
			sendMessage(conn, pool);
		}
		
		//Set new Position
		person.setField(key);
		
		//Set new State
		//Hes inside a room
		person.getPlayer().resetPlayerstate();
		if(model.getGameField().getFields().get(key).getRoom()!=null&&model.getGameField().getFields().get(key).getRoom()!=Room.POOL){
			person.getPlayer().addPlayerState(Playerstate.SUSPECT);
		}else{
			person.getPlayer().addPlayerState(Playerstate.END_TURN);
		}
		person.getPlayer().addPlayerState(Playerstate.ACCUSE);

		//Send new Position
		JSONObject newPos = new JSONObject();
		newPos.put("type", "moved");
		newPos.put("gameID", gameID);
		newPos.put("person position", person.getPositon());
		sendMessage(game.getAllConnections(), newPos);
		
		sendStatusupdate(game.getAllConnections(), person.getPlayer());
		
		
		
		
			
		
	}
	private void handleRollDice(Connection conn, JSONObject obj) {
		//normal
		//Complete Request
		if(obj.isNull("gameID")){
			sendError(conn, "The Request was not complete! (RollDice)");
			return;
		}
		int gameID = obj.getInt("gameID");
		//Does Game exist
		if(!model.getGames().containsKey(gameID)){
			sendError(conn, "A Game with this ID does not exist!");
			return;
		}
		Game game = model.getGames().get(gameID);
		//Is he Player in the Game
		if(!game.isPlayer(conn)){
			sendError(conn, "You habe to be Player to roll the dice!");
			return;
		}
		Person person = game.getPerson(conn);
		
		//It is his turn to roll
		if(!person.getPlayer().getPlayerState().contains(Playerstate.ROLL_DICE)){
			sendError(conn, "Its not your time to roll the dice!");
			return;
		}
		
		//Get Throw
		int die1 = model.getRandom(1, 6);
		int die2 = model.getRandom(1, 6);
		
		//Save Result in Player
		person.getPlayer().setThrow(die1+die2);
		
		//Send Result to all Players and Watchers
		
		JSONObject send = new JSONObject();
		send.put("type", "dice result");
		send.put("gameID", gameID);
		
		JSONArray arr = new JSONArray();
		arr.put(die1);
		arr.put(die2);
		
		send.put("result", arr);
		
		//Send it!
		sendMessage(game.getAllConnections(), send);
		
		//Set the new Status
		person.getPlayer().resetPlayerstate();
		person.getPlayer().addPlayerState(Playerstate.MOVE);
		person.getPlayer().addPlayerState(Playerstate.ACCUSE);
		
		//Send Statusupdate
		sendStatusupdate(game.getAllConnections(), person.getPlayer());
		
		
	}
	private void handleStartGame(Connection conn, JSONObject obj) {
		//normal
		//Complete Request
		if(obj.isNull("gameID")){
			sendError(conn, "The Request was not complete!");
			return;
		}
		int gameID = obj.getInt("gameID");
		//Does Game exist
		if(!model.getGames().containsKey(gameID)){
			sendError(conn, "A Game with this ID does not exist!");
			return;
		}
		Game game = model.getGames().get(gameID);
		
		//Is game startable
		if(game.getGameState() != Gamestate.NOT_STARTED){
			sendError(conn, "Whether its started or even ended, its pretty dumb trying to start it!");
			return;
		}
		
		//Is Player not in the game
		if(!game.isPlayer(conn)){
			sendError(conn, "You have to play inside the Game to start it!");
			return;
		}
		
		//Are 3 or more Player in the Game
		//TOREMOVE
		//TODO
		/*if(game.getPlayers().size()<3){
			sendError(conn, "There have to be 3 or more Players");
			return;
		}*/
		
		
		game.setGameState(Gamestate.STARTED);
		
		
		//Make Lists
		List<Color> colors = new ArrayList<Color>(EnumSet.allOf(Color.class));
		List<Weapons> weapons = new ArrayList<Weapons>(EnumSet.allOf(Weapons.class));
		List<Room> room = new ArrayList<Room>(EnumSet.allOf(Room.class));
		room.remove(Room.POOL);
		
		//Shuffle each
		Collections.shuffle(colors);
		Collections.shuffle(weapons);
		Collections.shuffle(room);
		
		//Make Solution
		game.addToSolution(colors.remove(0));
		game.addToSolution(weapons.remove(0));
		game.addToSolution(room.remove(0));
		
		
		
		//Make one List
		List<Card> cards = new ArrayList<Card>();
		cards.addAll(colors);
		cards.addAll(weapons);
		cards.addAll(room);
		//Shuffle
		Collections.shuffle(cards);
		
		//Give everybody cards
		while(cards.size()/game.getPlayers().size()>=1){
			for(Player player : game.getPlayers()){
				player.addCard(cards.remove(0));
			}
		}
		
		//Send everybody his cards
		for(Player player : game.getPlayers()){
			JSONObject send = new JSONObject();
			send.put("type", "player_cards");
			send.put("gameID", gameID);

			JSONArray cardArr = new JSONArray();
			for(Card card : player.getCards()){
				cardArr.put(card.toString());
			}
			send.put("cards", cardArr);
			sendMessage(player.getConnection(), send);
		}
		
		//Fill the Pool
		game.setPool(cards);
		
		//Make the Order
		List<Color> order = new ArrayList<Color>();
		order.addAll(game.getPersons().keySet());
		//Shuffle the order
		Collections.shuffle(order);
		if(order.contains(Color.RED)){
			order.set(order.indexOf(Color.RED), order.get(0));
			order.set(0, Color.RED);
		}
		
		game.setOrder(order);
		
		//Send mina the order
		JSONObject start = new JSONObject();
		start.put("type", "game started");
		start.put("gameID", gameID);
		start.put("gamestate", "started");
		JSONArray jsonOrder = new JSONArray();
		for(Color color : game.getOrder()){
			jsonOrder.put(game.getPersons().get(color).getPlayer().getConnection().getNick());
		}
		game.setTimeStarted(LocalDateTime.now().toString());
		
		game.fillGame();
		start.put("order", jsonOrder);
		sendMessage(model.getConnections(), start);
		
		//Fill in the Rest of the People
		for(Color color : Color.values()){
			if(!game.isColorTaken(color)){
				game.addPerson(new Person(color));
			}
		}
		/*//Set Starting Positions
		for(Map.Entry<Color, Person> entry: game.getPersons().entrySet()){
			entry.getValue().setField(model.getGameField().getFields().get(entry.getKey().getStart()));
		}*/
		
		//Set the Player to their Start Position
		
		Player active = game.getActivePerson().getPlayer();
		active.resetPlayerstate();
		active.addPlayerState(Playerstate.ROLL_DICE);
		active.addPlayerState(Playerstate.ACCUSE);

		sendStatusupdate(game.getAllConnections(), active);
	
	}
	private void sendStatusupdate(List<Connection> conns, Player player){
		JSONObject send = new JSONObject();
		send.put("type", "stateupdate");
		send.put("gameID", player.getGameID());
		
		JSONObject playerObj = new JSONObject();
		playerObj.put("nick", player.getConnection().getNick());
		JSONArray state = new JSONArray();
		for(Playerstate ps : player.getPlayerState()){
			state.put(ps.toString());
		}
		playerObj.put("playerstate", state);
		playerObj.put("color", player.getColor().toString());
		
		send.put("player", playerObj);
		
		sendMessage(conns, send);
	}
	private void handleWatchGame(Connection conn, JSONObject obj) {
		//normal
		//Complete Request
		if(obj.isNull("gameID")){
			sendError(conn, "The Watch Request was not complete!");
			return;
		}
		int gameID = obj.getInt("gameID");
		//Does Game exist
		if(!model.getGames().containsKey(gameID)){
			sendError(conn, "A Game with this ID does not exist!");
			return;
		}
		Game game = model.getGames().get(gameID);
		
		//Is person playing inside this game?
		if(game.isPlayer(conn)){
				sendError(conn, "You cannot watch the game and play in it at the same Time!");
				return;
		}
		
		
		//Inform Clients
		JSONObject send = new JSONObject();
		send.put("type", "watcher added");
		send.put("gameID", gameID);
		send.put("nick", conn.getNick());
		
		sendMessage(model.getConnections(), send);
		
		//Send Gameinfo to new watcher
		send = new JSONObject();
		send.put("type", "gameinfo");
		send.put("game", game.getGameInfo());
		sendMessage(conn, send);

		conn.getGames().add(game);
		//Add him to watchers
		game.getWatchers().add(conn);
		
	}
	private void handleJoinGame(Connection conn, JSONObject obj) {
		//normal
		//Complete Request
		if(obj.isNull("gameID")||obj.isNull("color")){
			sendError(conn, "The Join Request was not done properly.");
			return;
		}
		int gameID = obj.getInt("gameID");
		//Does color exist?
		Color color = Color.getColor(obj.getString("color"));
		if(color == null){
			sendError(conn, "This Color does not exist!");
			return;
		}
		//Does Game exist
		if(!model.getGames().containsKey(gameID)){
			sendError(conn, "A Game with this ID does not exist!");
			return;
		}
		Game game = model.getGames().get(gameID);
		//Is the Game Joinable
		if(game.getGameState()!=Gamestate.NOT_STARTED){
			sendError(conn, "The Game is not joinable");
			return;
		}
		
		//Is he already in the game?
		if(game.isPlayer(conn)){
			sendError(conn, "You are already inside the Game!");
			return;
		}
		
		//Is the Color taken?
		if(game.isColorTaken(color)){
			sendError(conn, "The Color has already been taken!");
			return;
		}
		//Add Player
		Player player = new Player(conn, game.getGameID(), color);
		game.addPerson(new Person(player));
		
		//Send Message
		JSONObject send = new JSONObject();
		send.put("type", "player added");
		send.put("gameID", gameID);
		send.put("player", player.getPlayerInfo());
		
		conn.getGames().add(game);
		
		sendMessage(model.getConnections(), send);
		
		
		
	}
	private void handleCreateGame(Connection conn, JSONObject obj) {
		//is in normal
		if(obj.isNull("color")){
			sendError(conn, "You have to send a color to create a game");
			return;
		}
		Color color = Color.getColor(obj.getString("color"));
		if(color == null){
			sendError(conn, "This Color does not exist!");
			return;
		}
		Game game = new Game(model.getGameID());
		Player player = new Player(conn, game.getGameID(), color);
		game.addPerson(new Person(player));
		model.getGames().put(game.getGameID(), game);
		
		JSONObject send = new JSONObject();
		send.put("type", "game created");
		send.put("gameID", game.getGameID());
		send.put("player", player.getPlayerInfo());
		
		sendMessage(model.getConnections(), send);
		
		conn.getGames().add(game);
		
	}
	private void handleDisconnect(Connection conn, JSONObject obj) {
		JSONObject disco = new JSONObject();
		disco.put("type", "disconnected");
		disco.put("message", "Bye Bye See ya soon");
		sendMessage(conn, disco);
		model.getConnections().remove(conn);
		for(Game game : conn.getGames()){
			JSONObject leave = new JSONObject();
			leave.put("gameID", game.getGameID());
			handleLeave(conn, leave);
		}
		
		JSONObject left = new JSONObject();
		left.put("type", "user left");
		left.put("nick", conn.getNick());
		sendMessage(model.getConnections(), left);
		
	}
	private void handleLeave(Connection conn, JSONObject obj) {
		//Complete?
		if(obj.isNull("gameID")){
			sendError(conn, "You have to say which game you want to leave!");
		}
		int gameID = obj.getInt("gameID");
		//Does Game exist
		if(!model.getGames().containsKey(gameID)){
			sendError(conn, "A Game with this ID does not exist!");
			return;
		}
		Game game = model.getGames().get(gameID);
		conn.getGames().remove(game);
		boolean wasPlayer = false;
		//Delete from Players
		for(Iterator<Player> iterator = game.getPlayers().iterator(); iterator.hasNext();){
			Player player = iterator.next();
			if(player.getConnection()==conn){
				wasPlayer = true;
				iterator.remove();
			}
		}
		//Delete from Watchers
		for(Iterator<Connection> iterator = game.getWatchers().iterator(); iterator.hasNext();){
			Connection watcher = iterator.next();
			if(watcher == conn){
				iterator.remove();
			}
		}
		
		//Tell Mina
		
		JSONObject send = new JSONObject();
		send.put("type", "left game");
		send.put("gameID", gameID);
		send.put("nick", conn.getNick());
		
		sendMessage(model.getConnections(), send);
		boolean del = false;
		//If he was a Player delete the frickin Game (And its running)
		if(game.getGameState()==Gamestate.STARTED&&wasPlayer){
			game.setGameState(Gamestate.ENDED);
			//Tell mina
			send = new JSONObject();
			send.put("type", "game ended");
			send.put("gameID", gameID);
			send.put("statement", game.getSolution());
			sendMessage(model.getConnections(), send);
			model.getGames().remove(gameID);
			del = true;
		}
		
		//Game is Empty
		if(del || game.getPlayers().isEmpty()){
			send = new JSONObject();
			send.put("type", "game deleted");
			send.put("gameID", gameID);
			sendMessage(model.getConnections(), send);
			model.getGames().remove(gameID);
			
		}
		
		
		
	}
	private void handleChat(Connection conn, JSONObject obj) {
		//Is Extension ok
		if(!conn.getCompatibleExtensions().contains("Chat")){
			sendError(conn, "Chat is not supported by you client!");
			return;
		}
		//Is everything in place
		if(obj.isNull("sender")||obj.isNull("message")||obj.isNull("timestamp")){
			sendError(conn, "Chat: Not all arguments where correct");
			return;
		}
		List<Connection> toSend = new ArrayList<Connection>();
		
		if(obj.has("gameID")){
			Game game = model.getGames().getOrDefault(obj.get("gameID"), null);
			if(game==null){ //No Game with ID found
				sendError(conn, "Chat: There is no game with this ID");
				return;
			}
			for(Player player : game.getPlayers()){
				toSend.add(player.getConnection());
			}
		}else if(obj.has("nick")){
			Connection nick = model.getConnections().stream().filter(c->c.getNick().equals(obj.getString("nick"))).findFirst().get();
			if(nick==null){
				sendError(conn, "There is no Player with this name!: " + obj.getString("nick"));
				return;
			}
			toSend.add(nick);
			
		}else{ //Minna!!!!
		
			toSend = model.getAllClientsWithout(conn);
		}

		toSend.remove(conn);
		if(toSend.isEmpty()){
			sendError(conn, "No ones want to receive your message :(!");
			return;
		}
		sendMessage(toSend, obj);
		
		
		
	}
	private void handleLogin(Connection conn, JSONObject obj){//If not connected need type login
		if(!obj.isNull("nick")&&!obj.isNull("group")&&!obj.isNull("version")){//Client send login
			if(obj.getString("nick").length()<3){
				sendError(conn, "Your Nickname has to be at least 3 charachters long!");
				return;
			}else{//Nickname is long enough
				for(Connection connection : model.getAllClientsWithout(conn)){
					if(obj.getString("nick").equals(connection.getNick())){
						sendError(conn, "This username is already used!");
						return;
					}
				}
				conn.setNick(obj.getString("nick"));
				if(!obj.isNull("expansions")){
					JSONArray extensions = obj.getJSONArray("expansions");
					int i = 0;
					while(!extensions.isNull(i)){
						System.out.println("Has expansion: "+ extensions.getString(i));
						if(model.hasExtension(extensions.getString(i)))
							System.out.println("PAsst doch!!!!");
							conn.getCompatibleExtensions().add(extensions.getString(i));
						i++;
					}
				}
				conn.setConnected(true);
				//Send (Login Successfull)
				JSONObject succ = new JSONObject();
				succ.put("type", "login successful");
				succ.put("nick array", getNickArray());
				succ.put("game array", getGameArray());
				sendMessage(conn, succ);
				
				
				JSONObject userAdded = new JSONObject();
				userAdded.put("type", "user added");
				userAdded.put("nick", conn.getNick());
				sendMessage(model.getConnections(), userAdded);
			}
	}
		
	}
	
	private JSONArray getGameArray() {
		JSONArray ret = new JSONArray();
		for(Entry<Integer, Game> entry : model.getGames().entrySet()){
			ret.put(entry.getValue().getGameInfo());
		}
		return ret;
	}
	private JSONArray getNickArray(){
		JSONArray ret = new JSONArray();
		for(Connection conn : model.getConnections()){
			ret.put(conn.getNick());
		}
		return ret;
	}
	
	
	private void sendMessage(Connection conn, JSONObject msg){
		conn.getConnectionHandler().send(msg);
	}
	private void sendMessage(List<Connection> connections, JSONObject msg){
		for(Connection conn : connections){
			sendMessage(conn, msg);   
		}
	}

	private void sendOK(Connection conn) {
		JSONObject send = new JSONObject();
		send.put("type", "ok");
		sendMessage(conn, send);
		
	}

	private void sendError(Connection conn, String errMsg) {

		JSONObject send = new JSONObject();
		send.put("type", "error");
		send.put("message", errMsg);
		sendMessage(conn, send);
		
	}
	
	/**Superpower**/
	private void handleSuperpower(Connection conn, JSONObject obj) {
		
		int gameID = obj.getInt("gameID");
		Game game = model.getGames().get(gameID);
		Person person = game.getPerson(conn);

		String color = obj.getString("color");
		
		//Complete Request
		if(obj.isNull("gameID")||obj.isNull("color")){
			System.out.println("TEST:" +gameID+"  "+color);
			sendError(conn, "The Request was not complete! U DUMBASSS" + gameID + "  " + color);
			return;
		}
		//Is he Player in the Game
		if(!game.isPlayer(conn)){
			sendError(conn, "You have to be a Player to use your Superpower!");
			return;
		}
		//Does Game exist
		if(!model.getGames().containsKey(gameID)){
			sendError(conn, "A Game with this ID does not exist!");
			return;
		}
		//Its the Players Time use his Superpower
		if(!person.getPlayer().getPlayerState().contains(Playerstate.END_TURN)){
			sendError(conn, "Its not your time to use your superpower");
			return;
		}
//		//Can only be used if its your turn.
//		if(person.getPlayer().getPlayerState().contains(Playerstate.DO_NOTHING)){
//			sendError(conn, "Its not your time to use your superpower, its not your turn");
//			return;
//		}
		if(!person.getPlayer().getPowerActive()) {
			sendError(conn, "Bro, you already used your Superpower!");
			return;
		}

		//Welche Superpower?
		switch (color) {
		case "red" : 	person.getPlayer().resetPlayerstate();		//2x Dice
						person.getPlayer().addPlayerState(Playerstate.ROLL_DICE);
						log.info("RedPowerused");
						break;
		case "blue":	person.getPlayer().resetPlayerstate();		//Springe zu 22,5
						person.getPlayer().addPlayerState(Playerstate.SUSPECT);
						person.getPlayer().addPlayerState(Playerstate.ACCUSE);
						JSONObject newPos = new JSONObject();
						newPos.put("type", "moved");
						newPos.put("gameID", gameID);
						person.setField(new Key(22,5));
						newPos.put("person position", person.getPositon());
						sendMessage(game.getAllConnections(), newPos);
						log.info("BluePowerused");
						break;
		case "green":	person.getPlayer().resetPlayerstate();		//2x Suspect
						person.getPlayer().addPlayerState(Playerstate.SUSPECT);
						log.info("greenPowerused");
						break;
		case "white":	person.getPlayer().resetPlayerstate();		//Zuruck zum Anfang
						person.getPlayer().addPlayerState(Playerstate.SUSPECT);
						person.getPlayer().addPlayerState(Playerstate.ACCUSE);
						JSONObject newPos2 = new JSONObject();
						newPos2.put("type", "moved");
						newPos2.put("gameID", gameID);
						person.setField(Color.WHITE.getStart());
						newPos2.put("person position", person.getPositon());
						sendMessage(game.getAllConnections(), newPos2);
						log.info("whitePowerused");
						break;
		case "purple":	JSONObject send = new JSONObject();		
						send.put("type", "superpowerPurple");
						send.put("gameID", gameID); 
						log.info("PurplePowerused");
						break;
		case "yellow":	for(Connection c : game.getAllConnections()) {
						sendError(c, randomMessage()); //zufalliger String zum nerven
						}
						break;
	
		}
		
		//Power wird aufgebraucht
		person.getPlayer().usePower();	//Power auf false setzen
		
		sendStatusupdate(game.getAllConnections(), person.getPlayer());
			
		
			
	}
	
	public String randomMessage() {
		String[] liste = {
				"Kartoffeln sind besser als Aluminium!",
				"Wo Katzen sind, sind auch Erdbeeren!",
				"Akku auf 15%, bitte schließen sie eine Mikrowelle an.",
				"Hey Listen...Hey Listen...Hey Listen...Zelda?",
				"Squid Kid Squid Kid Squid Kid SPLARATTON",
				"Sie sind der 1337 Depp, der diese Nachricht bekommt!",
				"Die Wurzel von 42 ist die Essenz des Lebens...",
				"Hallo mIr giLt seiFe dankE!",
				"SOS AFFANALARM",
				"420 blaze it MLG noscope WOOOW12 doge!21111!"
		};
		int r = (int) (Math.random()*10);
		return liste[r];
	}
}
