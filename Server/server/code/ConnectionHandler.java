package server.code;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import server.model.Connection;
import server.model.Model;

public class ConnectionHandler implements Runnable{
	private static final Logger log = LogManager.getLogger( ConnectionHandler.class.getName() );
	private Socket socket;
	private Model model;
	private OutputStreamWriter out;
	private BufferedReader in;
	private Connection conn;
	public ConnectionHandler(Socket socket, Model model) {
		this.socket = socket;
		this.model = model;
		
		conn = new Connection(this);
		conn.setIp(socket.getInetAddress().getHostAddress());
		conn.setPort(socket.getPort());
		model.getConnections().add(conn);
	}
	@Override
	public void run() {
		try {
			out = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
			in = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			
			String line;
			while((line=in.readLine())!=null)
			{
				JSONObject obj = new JSONObject(line);
				conn.addReceivedJSON(obj);
				log.log(Level.INFO, "Received from client on port: " + socket.getPort() + ": " + obj.toString());
				model.getGameHandler().processJSON(obj, conn);
				model.getGuiModel().countBytesReceived(obj);
			}
		} catch (JSONException | IOException e) {
			//Connection lost
			JSONObject disco = new JSONObject();
			disco.put("type", "disconnect");
			model.getGameHandler().processJSON(disco, conn);
		}
		
		
		
	}
	
	public void send(JSONObject obj)
	{
		try {
			if(socket.isClosed() || socket.isOutputShutdown() || !socket.isConnected() || !model.getConnections().contains(conn))
				return;
			out.write(obj.toString() + "\n");
			out.flush();
			conn.addSendJSON(obj);
			log.log(Level.INFO, "Send to client on port: " + socket.getPort() + ": " + obj.toString());
			model.getGuiModel().countBytesSent(obj);
		} catch (IOException e) {
			log.error("Client lost");
		}
	}
	

}
