package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import server.model.Card;
import server.model.Color;
import server.model.Field;
import server.model.Gamestate;
import server.model.Key;
import server.model.Playerstate;
import server.model.Suspicion;
import server.model.Weapons;
import code.AI;

/**
 * Beeinhaltet alle Relevanten Infos für ein Spiel.
 * Anders alks Gamestub enthalt dies, Würfel, sämtliche Variablen die sich das letzte Objekt merken, Suscpicion etc.
 * @Codeauthor: Marcus, An
 * @author anikiNT
 *
 */
public class Game{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Game.class);

	private GameStub stub;
	private int gameID = -1;	//No game selected
	private Gamestate state = Gamestate.NOT_STARTED;
	private List<Playerstate> myPlayerstate = new ArrayList<Playerstate>();
	private Color color = null;		//Null == Watcher
	private Map<Color, Player> players = new HashMap<Color, Player>();
	private List<String> watchers = new ArrayList<String>();
	private Map<Color, Person> persons = new HashMap<Color, Person>();
	private Map<Weapons, Weapon> weapons = new HashMap<Weapons, Weapon>();
	private List<Card> cards = new ArrayList<Card>();
	private List<Card> pool = new ArrayList<Card>();
	private int die1, die2;
	private Key movedKey;
	private Suspicion suspicion;
	private Set<Field> accesibleFields = new HashSet<Field>();
	private Color suspector = null;
	private Card lastDisprovedCard = null;
	private Color lastDisprover = null;
	private List<Color> order = new ArrayList<Color>();
	private Color activeDisprover = null;
	private Color accuser = null;
	private boolean accuseMode = false;
	private boolean aiControlled = false;
	private AI ai;
	private SimpleBooleanProperty superPower = new SimpleBooleanProperty(false);	//superpower-extension wird an checkbox im colorchosescreen geknüpft
	
	/**
	 * Inititalize the Gamevariables to let them bind (Weaopns Persons)
	 */
	public Game() {
		for(Color color : Color.values()){
			players.put(color, new Player(new Nick(""), color, new ArrayList<Playerstate>()));
			persons.put(color, new Person(color));
		}
		for(Weapons weapon : Weapons.values()){
			weapons.put(weapon, new Weapon(weapon));
		}
	}
	public List<Color> getOrder(){
		return order;
	}
	
	public boolean isAI(){
		return aiControlled;
	}
	public void setAI(boolean ai){
		this.aiControlled = ai;
	}
	public void setColor(Color c){
		this.color = c;
	}
	public Map<Color, Person> getPersons(){
		return persons;
	}
	public Map<Weapons, Weapon> getWeapons(){
		return weapons;
	}
	public void setSuspicion(Suspicion suspicion){
		this.suspicion = suspicion;
	}
	public Suspicion getSuspicion(){
		return suspicion;
	}
	public void setPlayerstate(List<Playerstate> list){
		this.myPlayerstate = list;
	}
	public void setPosition(Color color, Key key){
		persons.get(color).setPosition(key);
	}
	public List<Card> getCards(){
		return cards;
	}
	public Map<Color, Player> getPlayers(){
		return players;
	}
	public Color getColor(){
		return color;
	}
	public int getGameID(){
		return gameID;
	}
	public Gamestate getGameState() {
		return state;
	}
	public AI getAI(){
		return ai;
	}
	public void setAI(AI ai){
		this.ai = ai;
	}
	public void setLastRoll(int die1, int die2){
		this.die1 = die1;
		this.die2 = die2;
	}
	public int getLastRoll(){
		return die1 + die2;
	}
	public void addToPool(Card card){
		pool.add(card);
	}
	
	public void setGameStub(GameStub stub){
		reset();
		this.stub = stub;
		this.gameID = stub.getGameID();
		this.state = stub.getGamestate();
		for(Map.Entry<Color, Player> entry : players.entrySet()){
			if(stub.getPlayers().containsKey(entry.getKey())){		
				entry.getValue().setNick(stub.getPlayers().get(entry.getKey()).getNick());
				entry.getValue().setPlayerstate(stub.getPlayers().get(entry.getKey()).getPlayerstate());
			}
		}
		watchers.clear();
		for(Nick nick : stub.getWatchers()){
			watchers.add(nick.getNick());
		}
		
		for(Map.Entry<Color, Person> entry : persons.entrySet()){
			if(stub.getPersonPositions().containsKey(entry.getKey())){
				entry.getValue().setPosition(stub.getPersonPositions().get(entry.getKey()));
			}
		}
		
		for(Map.Entry<Weapons, Weapon> entry : weapons.entrySet()){
			if(stub.getWeaponPositions().containsKey(entry.getKey())){
				entry.getValue().setPosition(stub.getWeaponPositions().get(entry.getKey()));
			}
		}
		this.order = stub.getOrder();
		
	}
	public List<Playerstate> getPlayerstate() {
		return myPlayerstate;
	}
	public Key getActivePlayerPosition(){
		Color color = null;
		for(Player player : players.values()){
			if(player.getPlayerstate().contains(Playerstate.MOVE)){
				color = player.getColor();
			}
		}
		if(color == null)
			return null;
		return persons.get(color).getPosition();
		
	}
	public void setAccesibleFields(Set<Field> accesibleFields) {
		this.accesibleFields = accesibleFields;
	}
	public Set<Field> getAccesibleFields(){
		return accesibleFields;
	}
	public void setMovedKey(Key key) {
		movedKey = key;
	}
	public Key getMovedKey(){
		return movedKey;
	}
	public List<Card> getPool(){
		return pool;
	}
	public List<Player> getDisproveOrder(){
		List<Player> ret = new ArrayList<Player>();
		for(Entry<Color, Player> entry : players.entrySet()){
			if (logger.isDebugEnabled()) {
				logger.debug("getDisproveOrder() - Nickname: " + entry.getValue().getNick().getNick()); //$NON-NLS-1$
			}
		}
		int s = order.indexOf(suspector);
		if(s==-1)
			return new ArrayList<Player>();
		for(int i = s+1; i<order.size(); i++){
			ret.add(players.get(order.get(i)));
			if (logger.isDebugEnabled()) {
				logger.debug("getDisproveOrder() - Vor: " + players.get(order.get(i)).getNick().getNick()); //$NON-NLS-1$
			}
		}
		for(Color color : order){
			if(color == suspector)
				break;
			ret.add(players.get(color));
			if (logger.isDebugEnabled()) {
				logger.debug("getDisproveOrder() - Nach: " + players.get(color).getNick().getNick()); //$NON-NLS-1$
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getDisproveOrder() - Moinsens"); //$NON-NLS-1$
		}
		return ret;
	}
	public void setSuspecter(Color color){
		suspector = color;
	}
	public List<Card> getDisproveCards() {
		List<Card> ret = new ArrayList<Card>();
		for(Card card : cards){
			if(suspicion.contains(card)){
				ret.add(card);
			}
		}
		return ret;
	}
	public Color getColorFromNickname(String name){
		for(Entry<Color, Player> entry : players.entrySet()){
			if(name.equals(entry.getValue().getNick().getNick())){
				return entry.getKey();   
			}
		}
		return null;
	}
	public void setLastDisproveCard(Card card) {
		lastDisprovedCard = card;
	}
	public Card getLastDisproveCard(){
		return lastDisprovedCard;
	}
	public Color getLastDisprover() {
		return lastDisprover;
	}
	public void setLastDisprover(Color lastDisprover) {
		this.lastDisprover = lastDisprover;
	}
	public void setActiveDisprover(Color color) {
		activeDisprover = color;
	}
	public Color getActiveDisprover(){
		return activeDisprover;
	}
	public Color getAccuser() {
		return accuser;
	}
	public void setAccuser(Color color){
		accuser = color;
	}
	public boolean isAccuseMode() {
		return accuseMode;
	}
	public void setAccuseMode(boolean bool){
		accuseMode = bool;
	}
	public List<Key> getPositions() {
		List<Key> ret = new ArrayList<Key>();
		for(Entry<Color, Person> entry : persons.entrySet()){
			ret.add(entry.getValue().getPosition());
		}
		return ret;
	}
	/**
	 * Dämliche Methode
	 * @param suspicion
	 */
	public void removeFromOrder(Suspicion suspicion) {
		//Get Active Player
		Color color = null;
		for(Entry<Color, Player> entry : players.entrySet()){
			if(entry.getValue().getPlayerstate().contains(Playerstate.ACCUSE)){
				color = entry.getKey();
			}
		}
		if(color == null){
			logger.error("removeFromOrder(Suspicion) - I cant remove a nonexistent Player"); //$NON-NLS-1$
			return;
		}
		order.remove(color);
	}
	public void reset() {
		stub = null;
		gameID = -1;
		state = Gamestate.NOT_STARTED;
		myPlayerstate.clear();
		color = null;
		watchers.clear();
		players.clear();
		for(Color color : Color.values()){
			players.put(color, new Player(new Nick(""), color, new ArrayList<Playerstate>()));
		}
		for(Entry<Color, Person> entry : persons.entrySet()){
			entry.getValue().setPosition(entry.getKey().getStart());
		}
		for(Entry<Weapons, Weapon> entry : weapons.entrySet()){
			entry.getValue().setPosition(new Key(11,8));
		}
		cards.clear();
		pool.clear();
		die1 = 1;
		die2 = 1;
		movedKey = null;
		suspicion = null;
		accesibleFields.clear();
		suspector = null;
		lastDisprovedCard = null;
		lastDisprover = null;
		order.clear();
		activeDisprover = null;
		accuser = null;
		accuseMode = false;
		aiControlled = false;
		ai = null;
		
	}
	public Color getSuspector() {
		return suspector;
	}
	
	/**
	 * Superpower Extension
	 */
	public BooleanProperty superPowerProperty() {
		return superPower;
	}
	
	public boolean getSuperPower() {
		return superPower.get();
	}
}
