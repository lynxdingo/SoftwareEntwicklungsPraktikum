package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import view.ScreenBundler;
import code.Client;
import code.JSONHandler;
import control.Presenter;
import model.chat.*;

/**
 * Verwaltet sämtliche Dinge, die der Client braucht.
 * @Codeauthor Markus
 * @author anikiNT
 *
 */
public class ClientModel {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(ClientModel.class);

	private ObservableList<Server> servers = FXCollections.observableArrayList(); 
	private List<String> compatibleExtensions = new ArrayList<String>();
	//private Map<String, Nick> nicks = new HashMap<String, Nick>();
	private ObservableMap<String, Nick> nicks = FXCollections.observableMap(new HashMap<String, Nick>());
	private ObservableMap<Integer, GameStub> games = FXCollections.observableMap(new HashMap<Integer, GameStub>());
	private boolean connected;
	private boolean loggedIn;
	private String nick;
	private Game game;	
	private GameField field;
	private Client client;
	private String[] expansions = {"Chat", "Superpower"};
	private ScreenBundler bundle;
	private JSONHandler handler;
	private Receive receive = new Receive();
	private Send send = new Send();
	private Date date = new Date();
	
	public Date getTime(){
		return date;
	}
	
	
	public void setScreens(ScreenBundler screens){
		this.bundle = screens;
	}
	public ScreenBundler getScreens(){
		return bundle;
	}
	
	public ClientModel(){
		field = new GameField();
		game = new Game();
		
	}
	public void setClient(Client client){
		this.client = client;
	}
	public Client getClient(){
		return client;
	}
	public String[] getExpansions(){
		return expansions;
	}
	
	
	public GameField getGameField(){
		return field;
	}
	public void addServer(Server server){
		servers.add(server);
	}
	public ObservableMap<String, Nick> getNicks(){
		return nicks;
	}
	public ObservableMap<Integer, GameStub> getGames(){
		return games;
	}
	public ObservableList<Server> getServers(){
		return servers;
	}
	public boolean isConnected() {
		return connected;
	}
	public void setConnected(boolean connected) {
		this.connected = connected;
	}
	public boolean isLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	public String getNickname() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public void addCompatibleExpansions(String extension){
		compatibleExtensions.add(extension);
	}
	public boolean isCompatible(String extension){
		return compatibleExtensions.contains(extension);
	}
	public void addNick(Nick nick){
		nicks.put(nick.getNick(), nick);
	}
	public void addGameStub(GameStub game){
		games.put(game.getGameID(), game);
		if (logger.isDebugEnabled()) {
			logger.debug("addGameStub(GameStub) - Theres a new game"); //$NON-NLS-1$
		}
	}
	public Nick getNick(String nick){
		return nicks.get(nick);
	}
	public void deleteNick(String nick){
		if(nicks.get(nick)==null)
			return;
		nicks.get(nick).removeExistence();
		nicks.remove(nick);
	}
	
	
	public Game getGame(){
		return game;
	}
	
	public Receive getRCV(){
		return receive;
	}
	
	public Send getSend(){
		return send;
	}
	
	public void setPresenter(Presenter presenter) {
		handler.setPresenter(presenter);
		
	}
	public JSONHandler getHandler(){
		return handler;
	}


	public void setHandler(JSONHandler handler) {
		this.handler = handler;
		
	}

	
}
