package model;

import java.net.InetAddress;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @codeauthor marcus
 * @author anikiNT
 *
 */
public class Server {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Server.class);

	private final InetAddress serverAddress;
	private final int port;
	private final String group;
	public Server(InetAddress server, int port, String group){
		this.serverAddress = server;
		this.port = port;
		this.group = group;
	}
	public InetAddress getAddress(){
		return serverAddress;
	}
	public int getPort(){
		return port;
	}
	public String getGroup(){
		return group;
	}
}
