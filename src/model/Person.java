package model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import server.model.Color;
import server.model.Key;

/**
 * Eine Person bzw. Spielfigur die von PlaceableObject erbt.
 * @Codeauthor An, Marcus
 * @author anikiNT
 *
 */
public class Person extends PlaceableObject{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Person.class);

	private boolean powerAvailable;			//Superpower
	private Color color;
	//private Key position;
	private boolean isPlayer = false;
	public Person(Color color){
		isPlayer = false;
		this.color = color;
		setPosition(color.getStart());
		setPosition(color.getStart().getX()*1.0, color.getStart().getY()*1.0);
		
	}
	public JSONObject getPositon() {
		JSONObject ret = new JSONObject();
		ret.put("person", color.toString());
		ret.put("field", getPosition());
		return ret;
	}
	public boolean isPlayer(){
		return isPlayer;
	}
	public Color getColor(){
		return color;
	}

	//Extensions SuperPower
	public void activatePower() {
		System.out.println("lade superpower....");
		if(powerAvailable == false) return;
		switch (this.color.toString()) {
		case "red" : break;			//Eine Karte die einen anderen Spieler angezeigt wird, soll ihnen angezeigt werden.
		case "yellow" : break;		//2x Dice
		case "white" : break;		//Alle Raume sind Geheimgange
		case "green" : break;		//Suspect: Raumunabhängig
		case "blue" : break;		//Falls jmd disproven kann: Nochmal eine Karte anzeigen lassen (wird schwer vl andern)
		case "purple" : break;		//2x Suspect
		default: System.out.println("ActivatePower(): No SuperPower available"); break;
		}
	}
	
}
