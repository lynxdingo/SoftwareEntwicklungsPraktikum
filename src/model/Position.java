package model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @Codeauthor Marcus
 * @author anikiNT
 *
 * @param <T>
 */
public class Position <T>{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Position.class);

	final double x, y;
	T object;
	public Position(double x, double y){
		this.x = x;
		this.y = y;
	}
	public T getObject(){
		return object;
	}
	public boolean isFree(){
		return object==null;
	}
	public void setObject(T object){
		this.object = object;
	}
	public double getX(){
		return x;
	}
	public double getY(){
		return y;
	}
}
