package model;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import server.model.Color;
import server.model.Playerstate;

public class Player {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Player.class);

	private Nick nick;
	private Color color;
	private List<Playerstate> playerstate = new ArrayList<Playerstate>();
	public Player (Nick nick, Color color, List<Playerstate> state){
		this.nick = nick;
		this.color = color;
		this.playerstate = state;
	}
	public Color getColor(){
		return color;
	}
	public void setNick(Nick nick){
		this.nick.setName(nick.getNick());
	}
	public Nick getNick(){
		return nick;
	}
	public void setPlayerstate(List<Playerstate> state){
		this.playerstate = state;
	}
	public List<Playerstate> getPlayerstate(){
		return playerstate;
	}
}
