package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import server.model.Color;
import server.model.Gamestate;
import server.model.Key;
import server.model.Weapons;

/**
 * Enthält die essentiellen Objekte des Spiels.
 * Dieser Teil is transferierbar zwischen Server und Client.
 * @author anikiNT
 *
 */
public class GameStub {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(GameStub.class);

	protected int gameID;
	protected Gamestate gamestate;
	//protected Map<Color, Player> players = new HashMap<Color, Player>();
	protected ObservableMap<Color, Player> players = FXCollections.observableMap(new HashMap<Color, Player>());
	protected List<Nick> watchers = new ArrayList<Nick>();
	protected Map<Color, Key> persons = new HashMap<Color, Key>();
	protected Map<Weapons, Key> weapons = new HashMap<Weapons, Key>();
	private List<Color> order = new ArrayList<Color>();
	
	public Gamestate getGamestate(){
		return gamestate;
	}
	public ObservableMap<Color, Player> getPlayers(){
		return players;
	}
	public List<Nick> getWatchers(){
		return watchers;
	}
	public Map<Color, Key> getPersonPositions(){
		return persons;
	}
	public Map<Weapons, Key> getWeaponPositions(){
		return weapons;
	}
	
	public GameStub(int gameID, Gamestate gamestate){
		this.gameID = gameID;
		this.gamestate = gamestate;
	}
	public void addToOrder(Color color){
		order.add(color);
	}
	
	public int getGameID(){
		return gameID;
	}
	
	public void addPlayer(Player player){
		players.put(player.getColor(), player);
	}
	public void addWatcher(Nick nick){
		watchers.add(nick);
	}
	public void addPerson(Color color, Key position){
		persons.put(color, position);
	}
	public void addWeapon(Weapons weapon, Key position){
		weapons.put(weapon, position);
	}
	public void setGamestate(Gamestate state){
		this.gamestate = state;
	}
	public void remove(Nick nick){
		watchers.remove(nick);
		persons.remove(nick.getNick());
	}
	public List<Color> getOrder(){
		return order;
	}
	public Color getColorFromNickname(String name){
		for(Entry<Color, Player> entry : players.entrySet()){
			if(name.equals(entry.getValue().getNick().getNick())){
				return entry.getKey();
			}
		}
		return null;
	}
}
