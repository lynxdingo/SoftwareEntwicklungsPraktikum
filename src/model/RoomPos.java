package model;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import server.model.Color;
import server.model.Room;
import server.model.Weapons;

/**
 * @codeauthor marcus
 * @author anikiNT
 *
 */
public class RoomPos {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(RoomPos.class);

	private List<Position<Color>> personPositions = new ArrayList<Position<Color>>();
	private List<Position<Weapons>> weaponPositions = new ArrayList<Position<Weapons>>();
	private Room room;
	
	public  List<Position<Color>> getPersonPositions (){
		return personPositions;
	}
	public  List<Position<Weapons>> getWeaponPositions(){
		return weaponPositions;
	}
	public void setRoom(Room room){
		this.room = room;
	}
	public Room getRoom(){
		return room;
	}
	public void remove(Weapons w) {
		for(Position<Weapons> pos : weaponPositions){
			if(w.equals(pos.getObject())){
				pos.setObject(null);
			}
		}
	}
	public void remove(Color c) {
		for(Position<Color> pos : personPositions){
			if(c.equals(pos.getObject())){
				pos.setObject(null);
			}
		}
	}
}
