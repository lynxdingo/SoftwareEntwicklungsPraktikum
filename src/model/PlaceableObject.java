package model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import server.model.Key;

/**
 * Klasse für Objekte, die sich auf dem Spielfeld bewegt werden können.
 * @Codeauthor: An, Marcus
 * @author anikiNT
 *
 */
public abstract class PlaceableObject {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(PlaceableObject.class);
	
	private ObjectProperty<Key> position = new SimpleObjectProperty<Key>();			//Position des Objects
	private DoubleProperty yProperty = new SimpleDoubleProperty();					//veraltet
	private DoubleProperty xProperty = new SimpleDoubleProperty();					//veraltet
	private RoomPos currentRoom;

	/**
	 * setzt die Position des Objects auf das Feld (In form eines Keys)
	 * @param key
	 */
	public void setPosition(Key key){
		this.position.set(key);
		/*xProperty.set(key.getX());
		yProperty.set(key.getY());*/
	}
	
	public RoomPos getCurrentRoom() {
		return currentRoom;
	}
	/**
	 * veraltet wird nunr über Objectproperty gelöst
	 * @param x
	 * @param y
	 */
	public void setPosition(Double x, Double y){
		xProperty.set(x);
		yProperty.set(y);
	}

	public void setCurrentRoom(RoomPos currentRoom) {
		this.currentRoom = currentRoom;
	}

	public Key getPosition(){
		return position.get();
	}
	
	public DoubleProperty xProperty(){
		return xProperty;
	}
	
	public DoubleProperty yProperty(){
		return yProperty;
	}
	public ObjectProperty<Key> keyProperty(){
		return position;
	}
	
	
}
