package model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/** Constanten für Alles werden hier gesammelt 
 * @CodeAutor: Alex */
public class Constants {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Constants.class);
	
	private Constants () {}
	
	public static final String PUMUSIC = new String("/resources/backgroundsound.mp3");
	
	/** Stylsheetaddresse
	 * @CodeAutor: Alex */
	public static final String STANDARTCSS = new String("/resources/dev_skin.css");
	public static final String CSS_ANGELA = new String("/resources/dev_server.css");
	public static final String FONT = new String("/resources/Bebas.ttf");
	public static final String FONT_SERVER = new String("/resources/Nobile-Regular.ttf");
	public static final String FONT_TEXT = new String("/resources/accidental_presidency-webfont.ttf");
		
	/**
	 * Adressen für Texturen
	 * @CodeAutor: Alex*/
	public static final String CARDS_MURDERER = new String("/resources/dev_cards_scaliert1.png");
	public static final String CARDS_WEAPON = new String("/resources/dev_cards_scaliert2.png");
	public static final String CARDS_ROOM = new String("/resources/dev_cards_scaliert3.png");
	
	public static final String FIGURES_PlAYERS = new String("/resources/dev_figures1.png");
	public static final String FIGURES_WEAPONS = new String("/resources/dev_figures2.png");
	public static final String FIGURES_ROOMS = new String("/resources/dev_figures3.png");
	public static final String FIGURES_UNKOWN = new String("/resources/dev_figures4.png");
	
	public static final String FIELD_BACKGROUND = new String("/resources/dev_field_skaliert1.png");
	public static final String FIELD_LIGHT1 = new String("/resources/dev_field_skaliert2.png");
	public static final String FIELD_LIGHT2 = new String("/resources/dev_field_skaliert3.png");
	public static final String FIELD_WALLS = new String("/resources/dev_field_skaliert4.png");
	public static final String FIELD_HIGHLIGHT = new String("/resources/dev_field_skaliert5.png");
	
	public static final String BACKGROUND = new String("/resources/20141113_64.jpg");
		
	public static final String WUERFEL = new String("/resources/dev_wuerfel.png");
	public static final String WUERFEL_SMALL = new String("/resources/dev_wuerfel_small.png");
	public static final String ROOM_HIGHLIGHT = new String("/resources/dev_field_skaliert5.png");
	public static final String TON = new String("/resources/dev_ton.png");
	public static final String END_TURN = new String("/resources/EndTurn.png");
	
	public static final String ICON = new String("/resources/dev_icon.png");
	public static final String SOUND = new String("/resources/dev_ton.png");
	
	
	/** Constanten für das Spielfeld. Offset ist der Rand. Wiedth die Cellenbreite des Spielfeldes. 
	 * FieldRatio ist fürs Scalieren da der keine Brüche mag (Width/Height)
	 * @CodeAutor: Alex */
	//Offset ist die Breite des randes in Cellengröße mach alex macht dne vielleicht kleiner 20px/CELL_SIZE ist min wegen shadow
	public static final double FIELD_OFFSET = 0.25;
	public static final int PLAYER_OFFSET = 0;
	public static final int WIDTH = 24;
	public static final int HEIGHT = 25;
	public static final double FIELDRATIO = 0.96;
	
	/**Die Size for Die(würfel) Texture*/
	public static final int DIE_SIZE = 500;
	public static final int DIE_SIZE_SMALL = 100;
	 
	/** Constante für die View 
	 * @CodeAutor: Alex */ 
	public static final double GOLDENRATIO = 0.618;	

	/** Zellengrößes in der Feldtextur
	 * @CodeAutor: Alex */ 
	public static final int CELL_SIZE = 100;
	
	/** Höhe und Breite und Seitenverhältnis (Bruche funktionierne nicht (breite/hoehe)) des Hintergrundbildes 
	 * @CodeAutor: Alex */ 
	
	/** Höhe und Breite der Carten in der Text
	 * @CodeAutor: Alex */ 
	public static final int CARD_WIDTH = 380;
	public static final int CARD_HEIGHT = 675;

	/** Empirisch gemessene pixel grösse von text der größße 45px
	 * @CodeAutor: Alex */ 
	public static final int TEXT_PIXEL_SIZE = 66;
	
	public static final int menugapX = 100;

}
