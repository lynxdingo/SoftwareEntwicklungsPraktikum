package model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import server.model.Weapons;


/**
 * Waffe für das Spiel, erbt von PlacableObject
 * @codeauthor An, Marcus
 * @author anikiNT
 *
 */
public class Weapon extends PlaceableObject{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Weapon.class);

	private Weapons weapon;


	public Weapon(Weapons weapons) {
		this.weapon = weapons;
		setPosition(weapons.getStart());
	}
	public JSONObject getPositon() {
		JSONObject ret = new JSONObject();
		ret.put("weapon", weapon.toString());
		ret.put("field", getPositon());
		return ret;
	}

	
	
	public final Weapons getWeapon() {
		return weapon;
	}

	public final void setWeapon(Weapons weapon) {
		this.weapon = weapon;
	}


}
