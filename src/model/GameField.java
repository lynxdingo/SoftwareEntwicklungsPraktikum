package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import server.model.Door;
import server.model.Field;
import server.model.Key;
import server.model.Room;


/**
 * Das Spielfeld für Cluedo.
 * @Codeauthor: Marcus, An
 * @author anikiNT
 *
 */
public class GameField {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(GameField.class);

	private Map<Key, Field> fields = new HashMap<Key, Field>();
	
	public GameField(){
		initializeField();
	}

	/**
	 * Liest eine Textdatei ein, generiert dadruch ein Spielfeld in Form einer Hashmap.
	 */
	private void initializeField(){
		try{
			BufferedReader br = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/resources/Field.txt"),"UTF-8"));
			String line;
			char direction = '\0';
			boolean door;
			boolean field;
			Room room = null;
			while((line = br.readLine())!= null){
				door = false;
				field = false;
				
				int val[] = new int[2];
				int i = 1;
				for(char c : line.toCharArray()){
					if(c==',')
						i = 0;
					else if(Character.isDigit(c)){
						val[i] = val[i] *10 + Character.getNumericValue(c);
						field = true;
						
					}
					else if(c == 'n'||c == 'o'||c == 's'||c == 'w'){
						direction = c;
						door = true;
					}
					else{
						switch(c){
							case '!': 	room = Room.STUDY;
										break;
							case '"': 	room = Room.HALL;
										break;
							case '§': 	room = Room.LOUNGE;
										break;
							case '$': 	room = Room.LIBRARY;
										break;
							case '%': 	room = Room.POOL;
										break;
							case '&': 	room = Room.DININGROOM;
										break;
							case '/': 	room = Room.BILLIARD;
										break;
							case '(': 	room = Room.BALLROOM;
										break;
							case ')': 	room = Room.KITCHEN;
										break;
							case '=': 	room = Room.CONSERVATORY;
										break;
						}
					}
				}
				
				if(door)
					fields.put(new Key(val[0], val[1]), new Door(val[0], val[1], direction, room));
				else if(field)
					fields.put(new Key(val[0], val[1]), new Field(val[0], val[1]));
				
			}
			br.close();
			if (logger.isDebugEnabled()) {
				logger.debug("initializeField() - Anzahl Felder: " + fields.size()); //$NON-NLS-1$
			}
			
			for(Entry<Key, Field> entry : fields.entrySet()){
				entry.getValue().checkNeighbour(fields);
			}
		}catch(IOException e){
			logger.error("initializeField()", e); //$NON-NLS-1$
		}
	}

	/**
	 * Liefert die Felder zurück, die von einem Feld mit einer bestimmten Würfelanzahl
	 * erreicht werden kann.
	 * @param field
	 * @param roll
	 * @param positions
	 * @return
	 */
	public Set<Field> getFieldsForRoll(Key field, int roll, List<Key> positions){
		positions.remove(field);
		Room room = fields.get(field).getRoom();
		Set<Field> accesibleFields;
		fields.entrySet().stream().filter(e->positions.contains(e.getKey())).forEach(e->e.getValue().setInaccessible());
		if(room != null){
			List<Entry<Key, Field>> doors = fields.entrySet().stream().filter(f->f.getValue().getRoom()==room).collect(Collectors.toList());		
			List<Field> fieldList = new ArrayList<Field>();
			for(Entry<Key, Field> door : doors){
				fieldList.addAll(((Door)door.getValue()).getFields(roll, true));
			}
			accesibleFields = new HashSet<Field>(fieldList);
			for (Iterator<Field> iterator = accesibleFields.iterator(); iterator.hasNext();) {
				Field entry = (Field) iterator.next();
				if(entry.getRoom()==room){
					iterator.remove();
				}
				
			}
		}else{
			accesibleFields = new HashSet<Field>(fields.get(field).getFields(roll));
		}
		accesibleFields.removeAll(positions);
		//Reset fields
		fields.entrySet().forEach(f->f.getValue().resetRoll());
		return accesibleFields;
		
	}
	public Map<Key, Field> getFields(){
		return fields;
	}
	
	/**
	 * Liefert ein Feld das Raumes param:room.
	 * @param room
	 * @return
	 */
	public Key getFieldForRoom(Room room){
		for(Entry<Key, Field>entry : fields.entrySet()){
			if(room.equals(entry.getValue().getRoom())){
				return entry.getKey();
			}
		}
		return null;
	}
}
