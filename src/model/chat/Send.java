package model.chat;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * @codeauthor Angela
 * @author anikiNT
 *
 */
public class Send {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Send.class);
	
	
		public Send () {
		
		}
		
		public Send(String m, String t)
		{
			this.setSendMessage(m);
			this.setSendTXT(t);
		}
		
		/**
		 * associated with the textArea which contains the entered message
		 */
		private final StringProperty sendTXT =
		            new SimpleStringProperty();


		    public final StringProperty SendTXTProperty(){
		        return sendTXT;
		    }

		    public final String getSendTXT(){
		        return sendTXT.get();
		    }

		    public final void setSendTXT(String sendTXT){
		    	SendTXTProperty().set(sendTXT);
		    }
		    

		    /**
		     * associated with lblNotification
		     */
		    private final StringProperty sendMessage =
		            new SimpleStringProperty(this, "message_to_display", null);


		    public final StringProperty SendMessageProperty(){
		        return sendMessage;
		    }

		    public final String getSendMessage(){
		        return sendMessage.get();
		    }

		    public final void setSendMessage(String sendMSG){
		    	SendMessageProperty().set(sendMSG);
		    }
		    
		    

		  

}
