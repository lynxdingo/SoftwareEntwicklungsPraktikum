package model.chat;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import server.model.gui.MyPageCreator;

/**
 * @codeauthor Angela
 * @author anikiNT
 *
 */
public class Receive extends MyPageCreator {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Receive.class);

		private StringBuilder sb;
		private int numberLines;
		private String paragraphStartTag = "<p class=\"my";
		private String beginning = "<head><body>";
		private String wrapperStart = "<div id=\"wrapper\">";
		private String wrapperEnd = "</div>";
		private String breakLine = "<br/>";
		
		
		public Receive ()  {
			sb = new StringBuilder();
			numberLines = 0;
		}
			/**
			 * bound to webView
			 */
		    private final StringProperty rcvTXT =
		            new SimpleStringProperty(this, "text_to_receive", null);
		    
		    
		    public final StringProperty ReceiveProperty(){
		        return rcvTXT;
		    }

		    public final String getRcvTXT(){
		        return rcvTXT.get();
		    }
		    
		    /**
		     * this javascript makes webView automatically scroll
		     *  to the bottom of the loaded page 
		     * @return a string that contains a javascript 
		     */
		    public String getScriptTag() {  
		        StringBuilder script = new StringBuilder();  
		        script.append("   <script language=\"javascript\" type=\"text/javascript\">");  
		        script.append("       function toEnd(){");  
		        script.append("           window.scrollTo(0, document.body.scrollHeight);");  
		        script.append("       }");  
		        script.append("       window.onload = toEnd;"); 
		        script.append("   </script>");  
		        return script.toString();
		        
		    }
		    
		    /**
		     * Append the received message to older messages
		     * and create a HTML page
		     * @param string 
		     * @param s=received message
		     * @param c=color of player or watcher
		     */
		    
		    public final void setRcvTXT(String partOne, String text, String color){
		    	numberLines = countLines(getPEnd(),sb.toString());
		    	text = convertHTMLtoText(text);
		    	
		    	String paragraphBeginning = paragraphStartTag + color + getCloseTag2();
    			String middle = partOne + breakLine + text;
    			sb.append(createHTMLUnit(paragraphBeginning,middle,getPEnd()));
	    		
    			if (numberLines > 11)  {
	    			int end = getEndofSubstring(getPEnd(),sb.toString());
	    			
	    			if (end > 0)
	    			{
	    				sb.delete(0, end);
	    			}
	    		}
	    		
	    		StringBuilder page = new StringBuilder();
	    		page.append(beginning);
	    		page.append(getScriptTag());
	    		page.append(sb);
	    		page.append(createEnd());
	    		
			    ReceiveProperty().set(page.toString());
	    		
	    }
}
