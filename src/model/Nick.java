package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Speichert den Nicknamen ab bzw. kann an ein anderes Objekt gebindet werden.
 * @Codeauthor: Marcus, An
 * @author anikiNT
 *
 */
public class Nick {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Nick.class);

	private StringProperty nick = new SimpleStringProperty();
	public Nick(String nick){
		this.nick.set(nick);
	}
	public String getNick(){
		return nick.get();
	}
	public void removeExistence(){
		//TODO delete from everything (maybe not needed)
	}
	public void setName(String name){
		this.nick.set(name);
	}
	public StringProperty nickProperty(){
		return nick;
	}
}
