package model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Klasse für setViewPoint, einfach 4 Parameter um 2DReKtangle(a,b,c,d) zu erstellen, wird gespeichert in den
 * jeweiligen enum Klassen. Sollte VERALTET SEIN.
 * @author anikiNT
 *
 */
public class Four {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Four.class);

	private int a;
	private int b;
	private int c;
	private int d;
	
	public Four(int a, int b, int c, int d) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}

	public final int getA() {
		return a;
	}

	public final void setA(int a) {
		this.a = a;
	}

	public final int getB() {
		return b;
	}

	public final void setB(int b) {
		this.b = b;
	}

	public final int getC() {
		return c;
	}

	public final void setC(int c) {
		this.c = c;
	}

	public final int getD() {
		return d;
	}

	public final void setD(int d) {
		this.d = d;
	}

}
