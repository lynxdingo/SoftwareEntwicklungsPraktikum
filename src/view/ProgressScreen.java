package view;

import static model.Constants.CELL_SIZE;
import static model.Constants.FIELDRATIO;
import static model.Constants.FIELD_OFFSET;
import static model.Constants.HEIGHT;
import static model.Constants.STANDARTCSS;
import static model.Constants.WIDTH;
import static view.ImageLoader.getImageLoader;
import javafx.beans.binding.Bindings;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * pbar = Ladebalken
 * 
 * hier entsteht die Ladeansicht
 * über den Presenter soll der Ladebalken hochlaufen,
 * in dem gleichen Zeitraum, in dem das Spiel geladen wird
 * @author Tim
 *
 */

public class ProgressScreen extends Scene {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(ProgressScreen.class);

	private StackPane root;
	private GridPane grid = new GridPane();
	
	private ImageView background = new ImageView(getImageLoader().getImage("background"));

	private ProgressBar pbar = new ProgressBar(0.25);
	
	public ProgressScreen() {

		super(new StackPane());
		this.root = (StackPane)this.getRoot();
		this.getStylesheets().add(STANDARTCSS);
		
		
		root.getStyleClass().add("screen");
		
		/**
		 * Hintergrund wird erzeugt und dynamisch an Hintergrundfüllend aber
		 * Proportional sclariert
		 * 
		 * @CodeAutor: Alex
		 */
		double bgratio = background.getImage().getWidth()/background.getImage().getHeight();
		background.scaleXProperty().bind(Bindings
				.when(this.widthProperty().greaterThan(this.heightProperty().multiply(bgratio)))
				.then(this.widthProperty())
				.otherwise(this.heightProperty().multiply(bgratio))				
				.divide(background.getImage().getWidth()));
		background.scaleYProperty().bind(Bindings
				.when(this.heightProperty().greaterThan(this.widthProperty().divide(bgratio)))
				.then(this.heightProperty())
				.otherwise(this.widthProperty().divide(bgratio))				
				.divide(background.getImage().getHeight() ));	
		root.getChildren().addAll(background,grid);
		
		grid.setAlignment(Pos.BOTTOM_CENTER);
		grid.setHgap(0);
	    grid.setVgap(CELL_SIZE);
        grid.setPadding(new Insets(0, 0, 0, 0));
        
        grid.add(pbar, 0, 0);
        GridPane.setHalignment(pbar, HPos.CENTER);
        grid.translateYProperty().bind(this.heightProperty().multiply(-0.1));
        pbar.minWidthProperty().bind(this.widthProperty().multiply(0.6));
        pbar.maxWidthProperty().bind(this.widthProperty().multiply(0.6));
      	}
}
