package view;

import static model.Constants.CARD_HEIGHT;
import static model.Constants.CARD_WIDTH;
import static model.Constants.CELL_SIZE;
import static model.Constants.DIE_SIZE;
import static model.Constants.DIE_SIZE_SMALL;
import static model.Constants.FIELDRATIO;
import static model.Constants.FIELD_OFFSET;
import static model.Constants.FONT_TEXT;
import static model.Constants.HEIGHT;
import static model.Constants.STANDARTCSS;
import static model.Constants.WIDTH;
import static view.ImageLoader.getImageLoader;

import java.util.HashMap;
import java.util.Map;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Pair;
import model.ClientModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import server.model.Card;
import server.model.Color;
import server.model.Room;
import server.model.Weapons;

/**
 * Das was rechts von Gamestage ist, hier gibts es Chat, Optionen, und Nodes+Spieler+Würfel etc. in einer TabPane.
 * @author anikiNT
 * @codeauthor Tim, Alex, Angela, An
 */
public class TabMenu extends TabPane {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(TabMenu.class);
	private DoubleProperty Tabcounter = new SimpleDoubleProperty(2);
	/**TabeMenu
	 * mit drei Reitern: Notes, Chat, Options
	 * by Tim, Alex, Markus	 */
	
	/**
	 * Texturen und Elemente für Notes
	 * @CodeAutor: Alex
	 */
	private Tab notes = new Tab("Notes");
	private GridPane gridnotes = new GridPane();
	
	private ImageView PlayerIcon = new ImageView(getImageLoader().getImage("murdererCards"));
	private Text PlayerName = new Text("Player"); 
	
	private Button accuse = new Button("accuse !");
	private Button notesButton2 = new Button("nothing");
	private Button superpowerbtn = new Button("Spower");
	private ImageView die1, die2;
	private ImageView selectedRoom = new ImageView(getImageLoader().getImage("roomIcons"));
	private ImageView selectedColor = new ImageView(getImageLoader().getImage("playerIcons"));
	private ImageView selectedWeapon = new ImageView(getImageLoader().getImage("weaponIcons"));
	private boolean diceWhereVisible = false;
	private boolean midButtonWasVisible = false;
	
	/** werte für die scalierer und die größen der Karten
	 * @CodeAutor: Alex */	
	//21 + 3.5 + 1 = 25 + 0.25 + 0.25 Notes ist mit dem tapmenu genauso groß wie das spielfeld
	private double scaledheigth = 350;
	//380*0.519 ist eigentlich 197 aber 200 past super
    private double scaledwidth = 200;
	private DoubleProperty noteswidth =  new SimpleDoubleProperty(scaledwidth*8+CELL_SIZE*0.1*9);
	private DoubleProperty textwidth = new SimpleDoubleProperty(scaledwidth*7+CELL_SIZE*0.1*6);
	
	
	
	private Slider slider = new Slider();
	private Label volumeValue = new Label();
	
	
	/**
	 * Elemente des Reiters	Chat
	 * by Tim
	 */
	private WebView htmlarea = new WebView();
    private WebEngine wEngine = htmlarea.getEngine();
    private ScrollPane rcvBox = new ScrollPane();
    private GridPane gridChat = new GridPane();
	//TextArea rcvBox = new TextArea();
    private Label lblNotification = new Label();
   
	private ComboBox<String> dropDownMenu = new ComboBox<String>();
	private Map<Integer, Pair<ImageView, Text>> handCards = new HashMap<Integer, Pair<ImageView, Text>>();
	private ListView<String> chatUsers = new ListView<String>();
	private TextArea sendText = new TextArea();
	private Button btnSend = new Button("Send");
	private Label lblUser = new Label("User List");

	/**
	 * Elemente des Reiters Options
	 * by Tim
	 */
	private GridPane gridOptions = new GridPane();
	private ToggleButton btnSound = new ToggleButton();
	private Label lblSound = new Label("Sound:");
	private Label lblVolume = new Label("Volume:");
	private Button btnLeave = new Button("Leave Game");
	private ClientModel model;
	private String midBtnRec = null;
	private Rectangle2D SoundOn;
	private Rectangle2D SoundOff;
	
	public TabMenu(ClientModel model)
	{
		this.model = model;
		createTabs();
//		createTooltip();
		bindViewComponentsToModel();
	}
	
	public void createTabs()
	{
		this.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		
		/**
		 * Sorgt dafür dass die Tabpane header alle gleichmäßig groß sind und die gesammte breite ausnutzen
		 * @CodeAutor: Alex
		 */
		this.setTabMaxHeight(CELL_SIZE);
		this.setTabMinHeight(CELL_SIZE);
		//.subtract(6) just in case /ich bekomme den controllbutton nicht ganz weg, aber sehr klein)
		this.tabMaxWidthProperty().bind(super.widthProperty().divide(Tabcounter).subtract(6));
		this.tabMinWidthProperty().bind(super.widthProperty().divide(Tabcounter).subtract(6));
		this.setPadding(new Insets(0, 0, 0, 0));
				
		/**
		 * Notes im Gamemenu
		 * @CodeAutor: Alex
		 */	
		//_______________GridNotes Anfang______________________________________		
		notes.setContent(gridnotes);	
		
		gridnotes.setHgap(CELL_SIZE*0.1);
		gridnotes.setVgap(0);
		gridnotes.setPadding(new Insets(0, 0, 0, 0));     
        
		/** breite für gridpane columns und row wird festgelegt weis scaliert wird
		 * @CodeAutor: Alex */
        ColumnConstraints col1 = new ColumnConstraints((scaledwidth-CELL_SIZE*0.1)/2);
        ColumnConstraints col2 = new ColumnConstraints((scaledwidth-CELL_SIZE*0.1)/2);
        ColumnConstraints col3 = new ColumnConstraints((scaledwidth-CELL_SIZE*0.1)/2);
        ColumnConstraints col4 = new ColumnConstraints((scaledwidth-CELL_SIZE*0.1)/2);
        gridnotes.getColumnConstraints().addAll(col1, col2, col3, col4);
		for (int i = 4; i < 10; i++) {
            ColumnConstraints column = new ColumnConstraints(scaledwidth);
            gridnotes.getColumnConstraints().add(column);        }
        
        RowConstraints row1 = new RowConstraints((scaledheigth)/4);
        RowConstraints row2 = new RowConstraints((scaledheigth)/4);
        RowConstraints row3 = new RowConstraints((scaledheigth)/2);
        gridnotes.getRowConstraints().addAll(row1, row2, row3);
                        
        // Linien zum bessern erkennen der Grids sollte im Spiel ausgeschaltet sein
        //gridnotes.setGridLinesVisible(true);
        
		/** Karte die eigene farbe und namen anzeigt wird inizialisiert 
		 * @CodeAutor: Alex */
        PlayerIcon.setViewport(new Rectangle2D(CARD_WIDTH*1, 0, CARD_WIDTH, CARD_HEIGHT));
        PlayerIcon.getStyleClass().add("blue");
        PlayerIcon.setDisable(true);
        
        double cardscale = 0.519;
        PlayerIcon.setScaleX(cardscale);
        PlayerIcon.setScaleY(cardscale);
        PlayerIcon.setTranslateX((CARD_WIDTH-scaledwidth)/-2);
        
        PlayerName.getStyleClass().add("smallblack");
        
        gridnotes.add(PlayerIcon, 0, 0, 2, 3);
        gridnotes.add(PlayerName, 0, 0, 2, 3);
        GridPane.setHalignment(PlayerName, HPos.CENTER);
        GridPane.setValignment(PlayerName, VPos.BOTTOM);
        
		/**Buttons neben spielerkarte
		 * @CodeAutor: Alex */
        superpowerbtn.getStyleClass().add("blackbtn");
        superpowerbtn.setPickOnBounds(false);
        GridPane.setHalignment(superpowerbtn, HPos.CENTER);
        GridPane.setValignment(superpowerbtn, VPos.CENTER);
        gridnotes.add(superpowerbtn, 0, 1, 2, 1);
        superpowerbtn.setVisible(false);
        
        //ist midBTN 
        accuse.setPickOnBounds(false);
        GridPane.setHalignment(accuse, HPos.CENTER);
        GridPane.setValignment(accuse, VPos.CENTER);
        gridnotes.add(accuse, 2, 0, 2, 1);
        
        notesButton2.setPickOnBounds(false);
        GridPane.setHalignment(notesButton2, HPos.CENTER);
        GridPane.setValignment(notesButton2, VPos.CENTER);
        gridnotes.add(notesButton2, 2, 1, 2, 1);
        notesButton2.setVisible(false);
        
		/** Würfel unter Buttons und neben Spielerkarte sind nicht immer zusehen
		 * @CodeAutor: Alex */
		die1 = new ImageView(getImageLoader().getImage("dieSmall"));
		die1.setViewport(new Rectangle2D(0, DIE_SIZE_SMALL, DIE_SIZE_SMALL, DIE_SIZE_SMALL));
		//setDie1(1);
		die1.getStyleClass().add("shinygray");
		die1.setPickOnBounds(false);
		gridnotes.add(die1, 2, 2, 1, 1);
        GridPane.setHalignment(die1, HPos.CENTER);
        GridPane.setValignment(die1, VPos.TOP);
        die1.setVisible(false);
		
		die2 = new ImageView(getImageLoader().getImage("dieSmall"));
		die2.setViewport(new Rectangle2D(0, DIE_SIZE_SMALL, DIE_SIZE_SMALL, DIE_SIZE_SMALL));
		//setDie2(1);
		die2.getStyleClass().add("shinygray");
		die2.setPickOnBounds(false);
		gridnotes.add(die2, 3, 2, 1, 1);
        GridPane.setHalignment(die2, HPos.CENTER);
        GridPane.setValignment(die2, VPos.BOTTOM);
        die2.setVisible(false);
        
		/** sind an seleber stelle wie Würfel sind auch nicht immer zu sehen
		 * @CodeAutor: Alex */
        selectedColor.setViewport(new Rectangle2D(0, 0, CELL_SIZE, CELL_SIZE));
        selectedColor.getStyleClass().add("shadow");
        selectedColor.setDisable(true);
        selectedColor.setVisible(false);
        //selectedColor.setVisible(false);
        gridnotes.add(selectedColor, 2, 2, 1, 1);
        GridPane.setHalignment(selectedColor, HPos.CENTER);
        GridPane.setValignment(selectedColor, VPos.TOP);
        selectedColor.setTranslateX(-CELL_SIZE*0.12);
        selectedColor.setTranslateY(-CELL_SIZE*0.12);
        
        selectedWeapon.setViewport(new Rectangle2D(0, 0, CELL_SIZE, CELL_SIZE));
        selectedWeapon.getStyleClass().add("shadow");
        selectedWeapon.setDisable(true);
        selectedWeapon.setVisible(false);
        //selectedWeapon.setVisible(false);
        gridnotes.add(selectedWeapon, 2, 2, 2, 1);
        GridPane.setHalignment(selectedWeapon, HPos.CENTER);
        GridPane.setValignment(selectedWeapon, VPos.CENTER);
        
        selectedRoom.setViewport(new Rectangle2D(0, 0, CELL_SIZE, CELL_SIZE));
        selectedRoom.getStyleClass().add("shadow");
        selectedRoom.setDisable(true);
        selectedRoom.setVisible(false);
        //selectedRoom.setVisible(false);
        gridnotes.add(selectedRoom, 3, 2, 1, 1);
        GridPane.setHalignment(selectedRoom, HPos.CENTER);
        GridPane.setValignment(selectedRoom, VPos.BOTTOM);
        selectedRoom.setTranslateX(CELL_SIZE*0.12);
        selectedRoom.setTranslateY(CELL_SIZE*0.12);
                
		/**Eigene Karten werden inizialisiert
		 * @CodeAutor: Alex */
        for(Color c : Color.values()) {
        	
        	ImageView possession = new ImageView(getImageLoader().getImage("murdererCards"));
        	
        	possession.getStyleClass().add(c.toString());
        	possession.setViewport(new Rectangle2D(c.getPosition()*CARD_WIDTH, 0, CARD_WIDTH, CARD_HEIGHT));
        	possession.setDisable(true);
        	
        	possession.setScaleX(cardscale);
        	possession.setScaleY(cardscale);
        	possession.setTranslateX((CARD_WIDTH-scaledwidth)/-2);
        	
        	Text possessionname = new Text(c.toString());
        	possessionname.getStyleClass().add("smallblack");
        	handCards.put(c.getPosition(),new Pair<ImageView, Text>(possession, possessionname));
        	        	
        	gridnotes.add(possession, 4 + c.getPosition(), 0, 1, 3);
        	gridnotes.add(possessionname, 4 + c.getPosition(), 0, 1, 3); 
        	GridPane.setHalignment(possessionname, HPos.CENTER);
        	GridPane.setValignment(possessionname, VPos.BOTTOM);	}
                   
        
        
		/** Notizbuch wird gezeichnet hier die Spieler
		 * @CodeAutor: Alex */
     	for(Color c : Color.values()){
     		ImageView icon = new ImageView(getImageLoader().getImage("playerIcons"));
     		icon.setViewport(new Rectangle2D(0, c.getPosition()*CELL_SIZE, CELL_SIZE, CELL_SIZE));
     		icon.getStyleClass().add("shadow");
     		icon.setDisable(true);
     		gridnotes.add(icon, 0, 3+ c.getPosition());
   			
   			CheckBox isknown = new CheckBox();
   			isknown.setFocusTraversable(false);
   			isknown.setPickOnBounds(false);
   			GridPane.setHalignment(isknown, HPos.CENTER);
   			gridnotes.add(isknown, 1, 3+ c.getPosition());
   			
   			/** bindet die länge der linie und des Textfelde an die breite der Scene 
   			 * @CodeAutor: Alex */
   			Line textunderline = new Line(0, 0, textwidth.getValue(), 0);
   			textunderline.endXProperty().bind(Bindings
   					.when(super.widthProperty().greaterThan(noteswidth.getValue()))
   					.then(textwidth.add(super.widthProperty().subtract(noteswidth.getValue())))
   					.otherwise(textwidth) 	);
   			textunderline.setDisable(true);
   			textunderline.setTranslateY(-12);
   			textunderline.getStyleClass().add(c.toString());
   			GridPane.setValignment(textunderline, VPos.BOTTOM);
   			gridnotes.add(textunderline, 2, 3+ c.getPosition(), 8, 1);
   			
   			TextField text = new TextField(c.toString());
   			text.minWidthProperty().bind(textunderline.endXProperty());
   			text.maxWidthProperty().bind(textunderline.endXProperty());
   			text.getStyleClass().add("notestext");
   			text.setPickOnBounds(false);
   			text.setFocusTraversable(false);
   			gridnotes.add(text, 2, 3+ c.getPosition(), 8, 1); 	}

		/** Notizbuch wird gezeichnet hier die Waffen
		 * @CodeAutor: Alex */
     	for(Weapons c : Weapons.values()){
     		ImageView icon = new ImageView(getImageLoader().getImage("weaponIcons"));
     		icon.setViewport(new Rectangle2D(0, c.getPosition()*CELL_SIZE, CELL_SIZE, CELL_SIZE));
     		icon.getStyleClass().add("shadow");
     		icon.setDisable(true);
     		gridnotes.add(icon, 0, 9+ c.getPosition());
   			
   			CheckBox isknown = new CheckBox();
   			isknown.setFocusTraversable(false);
   			isknown.setPickOnBounds(false);
   			GridPane.setHalignment(isknown, HPos.CENTER);
   			gridnotes.add(isknown, 1, 9+ c.getPosition());
   			
   			/** bindet die länge der linie und des Textfelde an die breite der Scene 
   			 * @CodeAutor: Alex */
   			Line textunderline = new Line(0, 0, textwidth.getValue(), 0);
   			textunderline.endXProperty().bind(Bindings
   					.when(super.widthProperty().greaterThan(noteswidth.getValue()))
   					.then(textwidth.add(super.widthProperty().subtract(noteswidth.getValue())))
   					.otherwise(textwidth) 	);
   			textunderline.setDisable(true);
   			textunderline.setTranslateY(-12);
   			textunderline.getStyleClass().add(c.toString());
   			GridPane.setValignment(textunderline, VPos.BOTTOM);
   			gridnotes.add(textunderline, 2, 9+ c.getPosition(), 8, 1);
   			
   			TextField text = new TextField(c.toString());
   			text.getStyleClass().add("notestext");
   			text.setPickOnBounds(false);
   			text.setFocusTraversable(false);
   			gridnotes.add(text, 2, 9+ c.getPosition(), 7, 1); 	}
     	
		/** Notizbuch wird gezeichnet hier die Räume
		 * @CodeAutor: Alex */
       	for(Room c : Room.values()){
     		
     		if(c.toString()!="pool") {
     			ImageView icon = new ImageView(getImageLoader().getImage("roomIcons"));
     			icon.setViewport(new Rectangle2D(0,c.getPosition()*CELL_SIZE, CELL_SIZE, CELL_SIZE));
     			icon.getStyleClass().add("shadow");
     			icon.setDisable(true);
     			gridnotes.add(icon, 0, 17+ c.getPosition());
   			
     			CheckBox isknown = new CheckBox();
     			isknown.setFocusTraversable(false);
     			isknown.setPickOnBounds(false);
     			GridPane.setHalignment(isknown, HPos.CENTER);
     			gridnotes.add(isknown, 1, 17+ c.getPosition());
   			
       			/** bindet die länge der linie und des Textfelde an die breite der Scene 
       			 * @CodeAutor: Alex */
     			Line textunderline = new Line(0, 0, textwidth.getValue(), 0);
       			textunderline.endXProperty().bind(Bindings
       					.when(super.widthProperty().greaterThan(noteswidth.getValue()))
       					.then(textwidth.add(super.widthProperty().subtract(noteswidth.getValue())))
       					.otherwise(textwidth) 	);
     			textunderline.setDisable(true);
     			textunderline.setTranslateY(-12);
     			textunderline.getStyleClass().add(c.toString());
     			GridPane.setValignment(textunderline, VPos.BOTTOM);
     			gridnotes.add(textunderline, 2, 17+ c.getPosition(), 8, 1);
   			
     			TextField text = new TextField(c.toString());
     			text.getStyleClass().add("notestext");
     			text.setPickOnBounds(false);
       			text.setFocusTraversable(false);
     			gridnotes.add(text, 2, 17+ c.getPosition(), 7, 1); 	}	
     		}
      //_______________GridNotes Ende______________________________________
     	     	
		
     	
       	
      //_______________Chat Anfang______________________________________
		Tab chat = new Tab("Chat");
		
		sendText.setPromptText("write your message");
		sendText.setWrapText(true);
		sendText.setPrefWidth(gridChat.getMaxWidth() /2);
//		sendText.setStyle("-fx-font-size: 45px;"); 
		lblNotification.setWrapText(true);
		lblNotification.getStyleClass().add("chat");
		
		btnSend.setPickOnBounds(false);
	
		rcvBox.setFitToHeight(true);
		rcvBox.setFitToWidth(true);
		rcvBox.setPrefWidth(gridChat.getMaxWidth() /2);
		//rcvBox.setMaxWidth(450);
	    rcvBox.setContent(htmlarea);
	    Font.loadFont(TabMenu.class.getResource(FONT_TEXT).toExternalForm(), 10);
	    wEngine.setUserStyleSheetLocation(getClass().getResource(STANDARTCSS).toExternalForm());
	    wEngine.setJavaScriptEnabled(true);
	    wEngine.loadContent("<b>Here you'll receive the messages.</b>");
	    lblNotification.setText("Notification");
	    dropDownMenu.setPromptText("Select where to send.");
	    lblUser.setDisable(true);
	    dropDownMenu.setPickOnBounds(false);
	    
	    
	    HBox hbControl = new HBox();
	    hbControl.getChildren().addAll(lblUser,chatUsers);
	    hbControl.setPadding(new Insets(10));
	    
	    VBox vbControl = new VBox();
	    vbControl.getChildren().add(hbControl);
	    vbControl.setPadding(new Insets(10));
	   
	    gridChat.setPadding(new Insets(10));
	    gridChat.setHgap(10);
	    gridChat.setVgap(10);
	    
	    double rowheight = 200;
        RowConstraints r1 = new RowConstraints((rowheight)*2.5);
        RowConstraints r2 = new RowConstraints((rowheight)*1.5);
        RowConstraints r3 = new RowConstraints((rowheight));
        gridChat.getRowConstraints().addAll(r1, r2, r3);
        
        ColumnConstraints c1 = new ColumnConstraints(800);
        ColumnConstraints c2 = new ColumnConstraints(800);
        ColumnConstraints c3 = new ColumnConstraints(400);
        gridnotes.getColumnConstraints().addAll(c1, c2, c3);
       
	    gridChat.addRow(0, rcvBox);
	    gridChat.add(sendText, 0, 1, 1, 1);
	    gridChat.add(vbControl, 1, 0, 2, 1);
	    gridChat.add(btnSend, 1, 1);
	    gridChat.add(dropDownMenu, 2, 1);
	    gridChat.add(lblNotification, 1, 2);
	    GridPane.setHalignment(rcvBox, HPos.LEFT);
	    GridPane.setHalignment(sendText, HPos.LEFT);
	    GridPane.setHalignment(lblUser, HPos.RIGHT);
	    GridPane.setHalignment(dropDownMenu, HPos.RIGHT);
	    GridPane.setHalignment(chatUsers, HPos.RIGHT);
	    GridPane.setHalignment(btnSend, HPos.LEFT);
	    GridPane.setHalignment(lblNotification, HPos.LEFT);
	    GridPane.setValignment(lblNotification, VPos.BOTTOM);
	   
	    rcvBox.setMinHeight(400);
        rcvBox.setMinWidth(800);
        sendText.setMinHeight(100);
        sendText.setMinWidth(400);
        vbControl.setMinWidth(400);
//        gridChat.setGridLinesVisible(true);
		chat.setContent(gridChat);
		
	 //_______________Chat Ende______________________________________
	    	
		
		/** OPTIONS_TAB */
		Tab options = new Tab("Options");

        gridOptions.setPadding(new Insets(0, 0, 0, 0));
        gridOptions.setHgap(10);
	    gridOptions.setVgap(10);
       gridOptions.setGridLinesVisible(true);
	    
//      Sound.setViewport(new Rectangle2D(0 ,0, CARD_WIDTH*1.315, CARD_HEIGHT*0.742));
     	SoundOn = new Rectangle2D(CARD_WIDTH*1.3155, 0, CARD_WIDTH*1.42, CARD_HEIGHT*0.742);
//      Sound.setViewport(new Rectangle2D(0, CARD_HEIGHT*1.48, CARD_WIDTH*1.31, CARD_HEIGHT*0.742));
     	SoundOff = new Rectangle2D(CARD_WIDTH*1.3155, CARD_HEIGHT*1.48, CARD_WIDTH*1.42, CARD_HEIGHT*0.742);
     	
     	ImageView Sound = new ImageView(getImageLoader().getImage("sound"));

     	btnSound.setGraphic(Sound);
      
	    Sound.viewportProperty().bind(Bindings
	        .when(btnSound.selectedProperty())
	      	.then(SoundOn)
	      	.otherwise(SoundOff)
	      	    );
      	Sound.scaleXProperty().bind(btnSound.widthProperty().divide(1250));
      	Sound.scaleYProperty().bind(btnSound.heightProperty().divide(1250));
      	
        slider.setMin(0);
        slider.setMax(100);
        slider.setValue(50);
       // slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
       // slider.setMajorTickUnit(50);
        slider.setMinorTickCount(5);
        slider.setBlockIncrement(30);
        slider.valueProperty().addListener(new ChangeListener<Number>() {
            
			@Override
			public void changed(ObservableValue<? extends Number> arg0,
					Number arg1, Number arg2) {
            volumeValue.setText(String.format("%.2f", arg2));
				
			}
        });
        volumeValue.setText(String.valueOf(slider.getValue()));
        
        for (int i = 0; i < 6; i++) {
            ColumnConstraints column = new ColumnConstraints(CELL_SIZE);
            gridOptions.getColumnConstraints().add(column);}
        
        btnLeave.setPickOnBounds(false);
        btnSound.setPickOnBounds(false);
        gridOptions.add(lblSound, 0, 0, 2, 1);
        gridOptions.add(btnSound, 5, 0);
        gridOptions.add(lblVolume, 0, 1, 2, 1);
        gridOptions.add(slider, 2, 1, 2, 1);
        gridOptions.add(volumeValue, 3, 1, 2, 1);
        gridOptions.add(btnLeave, 0, 5, 3, 1);
        GridPane.setHalignment(lblSound, HPos.LEFT);
        GridPane.setHalignment(lblVolume, HPos.LEFT);
        GridPane.setHalignment(volumeValue, HPos.RIGHT);
        GridPane.setHalignment(btnSound, HPos.RIGHT);
        
        options.setContent(gridOptions);
		        	   
		this.getTabs().addAll(chat,options);
	}
	
	/** Zählt als property wie viele taps in der tappane sind
	 * @CodeAutor: Alex */
	public DoubleProperty getTabcounter() {
		return Tabcounter;
	}
	
	public void bindViewComponentsToModel()
	{
		model.getSend().SendTXTProperty().bindBidirectional(sendText.textProperty());
		model.getSend().SendMessageProperty().bindBidirectional( lblNotification.textProperty());
	}
	
	public ComboBox<String> getDropDownMenu()
	{
		return dropDownMenu;
	}
	
	public Button getBtnSend()
	{
		return btnSend;
	}
	public ListView<String> getChatUsers()
	{
		return chatUsers;
	}
	public TextArea getSendText()
	{
		return sendText;
	}

	public WebEngine getWebEngine()
	{
		return wEngine;
	}
	
	/** returned den Accuse button in notes
	 * @CodeAutor: Alex */
	public Button getAccuse()	{

		return accuse;	}
	
	/** returned das Gridpane aus notes
	 * @CodeAutor: Alex */
	public GridPane getGridnotes() {
		return gridnotes;	}
	
	/** Returned die würfel aus notes
	 * @CodeAutor: Alex */
	public ImageView getDie1() {
		return die1;	}
	
	public ImageView getDie2() {
		return die2;	}
	
	/** returned den Tap für Notes
	 * @CodeAutor: Alex */
	public Tab getNotes() {
		return notes;	}
	
	public void refreshCards() {
		int i = 0;
		PlayerIcon.setViewport(new Rectangle2D(model.getGame().getColor().getPosition()*CARD_WIDTH, 0, CARD_WIDTH, CARD_HEIGHT));
		PlayerIcon.getStyleClass().clear();
		PlayerIcon.getStyleClass().add(model.getGame().getColor().toString());
		PlayerName.setText(model.getNickname());
		for(Card c : model.getGame().getCards()){
			if(!handCards.containsKey(i))
				break;
			ImageView possession = handCards.get(i).getKey();
        	possession.getStyleClass().clear();
        	possession.getStyleClass().add(c.toString());
			if(c instanceof Weapons){
	    		possession.setImage(getImageLoader().getImage("weaponCards"));
	    	}else if(c instanceof Room){
	    		possession.setImage(getImageLoader().getImage("roomCards"));
	    	}else{
	    		possession.setImage(getImageLoader().getImage("murdererCards"));
	    	}
			possession.setViewport(new Rectangle2D(c.getPosition()*CARD_WIDTH, 0, CARD_WIDTH, CARD_HEIGHT));
			handCards.get(i).getValue().setText(c.toString());
			//klapt nicht bei gelb karte ? 
			//possession.getStyleClass().add(c.toString());
			i++;
		}
		for(int j = i; j < handCards.size(); j++){
			handCards.get(j).getKey().setVisible(false);
			handCards.get(j).getValue().setVisible(false);
		}
		
		
		//muss noch gemacht werdne
		//PlayerIcon.setViewport(new Rectangle2D(CARD_WIDTH*1, 0, CARD_WIDTH, CARD_HEIGHT));
        //PlayerIcon.getStyleClass().add("blue");
        //PlayerName.setText(arg0);
		
		
	}
	
	public Button getBtnLeave() {
		return btnLeave;
	}
	
	public Button getBtnPower() {
		return notesButton2;
	}

	public void setDice(boolean b) {
		die1.setVisible(b);
		die2.setVisible(b);
		
	}//CELL SIZE has to be DIE SIZE
	public void setDie1(int die){
		die1.setViewport(new Rectangle2D(0, (die-1)*DIE_SIZE_SMALL, DIE_SIZE_SMALL, DIE_SIZE_SMALL));
	}
	public void setDie2(int die){
		die2.setViewport(new Rectangle2D(0, (die-1)*DIE_SIZE_SMALL, DIE_SIZE_SMALL, DIE_SIZE_SMALL));
	}
	public void setAccuseMode(boolean b) {
		if(b){
			accuse.setText("abort");
			diceWhereVisible = die1.isVisible();
			setDice(false);
			midButtonWasVisible = notesButton2.isVisible();
			midBtnRec = notesButton2.getText();
			notesButton2.setVisible(true);
			notesButton2.setText("Accuse");
		}else{
			setDice(diceWhereVisible);
			notesButton2.setVisible(midButtonWasVisible);
			notesButton2.setText(midBtnRec);
			accuse.setText("accuse !");
		}
		
	}
	public void setEndTurnButton(boolean active){
		if(active)
			setDice(false);
		notesButton2.setText("EndTurn");
		notesButton2.setVisible(active);
	}
	public void setSuspectButton(boolean active){
		if(active){
			setDice(false);
		}
		
		notesButton2.setText("Suspect");
		notesButton2.setVisible(active);
		if(!active){
			selectedWeapon.setVisible(active);
			selectedRoom.setVisible(active);
			selectedColor.setVisible(active);
		}
		
	}

	public void suspect(Card suspect) {
		ImageView icon = selectedRoom;
		if(suspect instanceof Room)
			icon = selectedRoom;
		else if(suspect instanceof Weapons)
			icon = selectedWeapon;
		else if(suspect instanceof Color)
			icon = selectedColor;
		
		icon.setViewport(new Rectangle2D(0, suspect.getPosition()*CELL_SIZE, CELL_SIZE, CELL_SIZE));
		icon.setVisible(true);
		
	}

	public Button getNotesButton2() {
		return notesButton2;
	}

	public void setNotesButton2(Button notesButton2) {
		this.notesButton2 = notesButton2;
	}

	public void setAccuse(Button accuse) {
		this.accuse = accuse;
	}
	public Button getAccuse(Button accuse) {
		return this.accuse;
	}
	public void hideSuper() {
		this.notesButton2.setVisible(false);
	}
	public ImageView getPlayerIcon() {
		return this.PlayerIcon;
	}
	public Button getSuperpowerBtn() {
		return this.superpowerbtn;
	}
	
	public GridPane getgridChat() {
		return gridChat;
	}
	
}


