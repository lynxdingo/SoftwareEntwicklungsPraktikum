package view;

import static model.Constants.CARD_HEIGHT;
import static model.Constants.CARD_WIDTH;
import static model.Constants.CELL_SIZE;
import static model.Constants.FIELDRATIO;
import static model.Constants.FIELD_OFFSET;
import static model.Constants.HEIGHT;
import static model.Constants.STANDARTCSS;
import static model.Constants.WIDTH;
import static view.ImageLoader.getImageLoader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javafx.beans.binding.Bindings;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.util.Pair;
import model.GameStub;
import model.Player;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import server.model.Color;

/**
 * Lobby Interface. Auswähl der SpielerFarbe oder Zuschauer und Spielmodie
 * 
 * @CodeAutor: Alex
 */
public class ColorChoseScreen extends Scene {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(ColorChoseScreen.class);

	/**
	 * Gerüste für aufbau
	 * 
	 * @CodeAutor: Alex
	 */
	private StackPane root;
	private GridPane grid = new GridPane();

	/**
	 * Geladene Texturen
	 * 
	 * @CodeAutor: Alex
	 */
	private ImageView background = new ImageView(getImageLoader().getImage("background"));
	
	private Map<Color, Pair<ImageView, Text>> cards = new HashMap<Color, Pair<ImageView, Text>>();
	private Map<Color, StackPane> boxes = new HashMap<Color, StackPane>();
	
	/**
	 * Checkboxen fur Spielmodi auswahl und start Button
	 * 
	 * @CodeAutor: Alex
	 */
	private CheckBox EightPlayerCheck = new CheckBox("8 Players");
	private CheckBox AICheck = new CheckBox("AI  Controlled");
	private CheckBox SuperPowerCheck = new CheckBox("Superpower");
	private CheckBox HelpCheck = new CheckBox("Help");
	private Button StartButton = new Button("START");
	private JSONObject names;

	public ColorChoseScreen() {
		super(new StackPane());
		this.root = (StackPane)this.getRoot();
		this.getStylesheets().add(STANDARTCSS);

		/**
		 * Hintergrund wird erzeugt und dynamisch an Hintergrundfüllend aber
		 * Proportional sclariert
		 * 
		 * @CodeAutor: Alex
		 */
		// hintergrund wird textur geladen und 50% opacity gesetzt hintergrund
		// ist #191919
		root.getStyleClass().add("screen");
		background.setOpacity(0.5);
		double bgratio = background.getImage().getWidth()/background.getImage().getHeight();
		background.scaleXProperty().bind(Bindings
				.when(this.widthProperty().greaterThan(this.heightProperty().multiply(bgratio)))
				.then(this.widthProperty())
				.otherwise(this.heightProperty().multiply(bgratio))				
				.divide(background.getImage().getWidth()));
		background.scaleYProperty().bind(Bindings
				.when(this.heightProperty().greaterThan(this.widthProperty().divide(bgratio)))
				.then(this.heightProperty())
				.otherwise(this.widthProperty().divide(bgratio))				
				.divide(background.getImage().getHeight() ));	
		root.getChildren().addAll(background);

		/**
		 * GridPane wird initialisiert und in die StackPane Root eingefügt,
		 * damit sich die Scaliere nicht in quere kommen
		 * 
		 * @CodeAutor: Alex
		 */
		root.getChildren().add(grid);
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(CARD_WIDTH * 0.1);
		grid.setVgap(CARD_WIDTH * 0.1);
		grid.setPadding(new Insets(0, 0, 0, 0));

		// Linien zum bessern erkennen der Grids sollte im Spiel ausgeschaltet sein
		//grid.setGridLinesVisible(true);

		String json = "";
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/resources/colors.json")));
			json = br.readLine();
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("ColorChoseScreen()", e); //$NON-NLS-1$
		}
		JSONObject colors = new JSONObject(json);
		names = colors;
		for(Color c: Color.values()){
			ImageView tex = new ImageView(getImageLoader().getImage("murdererCards"));
			Text name = new Text(colors.getString(c.toString()));
			StackPane box = new StackPane();
			name.getStyleClass().add("black");
			tex.setPickOnBounds(true);
			tex.setViewport(new Rectangle2D(CARD_WIDTH * c.getPosition(), CARD_HEIGHT, CARD_WIDTH, CARD_HEIGHT));
			box.setAlignment(Pos.BOTTOM_CENTER);
			box.getChildren().addAll(tex,name);
			box.getStyleClass().add(c.toString());
			box.setPickOnBounds(false);
			grid.add(box, c.getPosition(), 0);
			cards.put(c, new Pair<ImageView, Text>(tex, name));
			boxes.put(c, box);
			
		}
		/**
		 * Focus wird für Checkboxen abgestellt
		 * 
		 * @CodeAutor: Alex
		 */
		EightPlayerCheck.setFocusTraversable(false);
		AICheck.setFocusTraversable(false);
		SuperPowerCheck.setFocusTraversable(false);
		HelpCheck.setFocusTraversable(false);

		/**
		 SuperPower-Button (war der alte history button)
		 */
		SuperPowerCheck.setSelected(false);
		GridPane.setHalignment(StartButton, HPos.RIGHT);

		/**
		 * Durchsichtigleit von Texture aktiviert kein Mouseevent
		 * 
		 * @CodeAutor: Alex
		 */
		EightPlayerCheck.setPickOnBounds(false);
		AICheck.setPickOnBounds(false);
		SuperPowerCheck.setPickOnBounds(false);
		HelpCheck.setPickOnBounds(false);
		StartButton.setPickOnBounds(false);

		/**
		 * Einfügen der Checkboxes in GridPane
		 * 
		 * @CodeAutor: Alex
		 */
		//grid.add(EightPlayerCheck, 0, 1);
		grid.add(AICheck, 0, 1);
		grid.add(SuperPowerCheck, 1, 1);
		//grid.add(HelpCheck, 3, 1);
		grid.add(StartButton, 5, 1);

		/**
		 * Dynmaisches Scaleiren wie Field proportional zur Bidlschirmgrösse damit die schriff überall gleich ist 
		 * ++ weil es 20 px breiter ist als field mit einem kleinen zusatz weniger als 4% änderung vom rest
		 * @CodeAutor: Alex
		 */
        grid.scaleXProperty().bind(
				Bindings.when(this.widthProperty().lessThan(this.heightProperty().multiply(FIELDRATIO+0.04)))
						.then(this.widthProperty())
						.otherwise(this.heightProperty().multiply(FIELDRATIO+0.04))
						.divide(CELL_SIZE*(WIDTH +FIELD_OFFSET +FIELD_OFFSET)+20+CARD_WIDTH*0.1*2));
		grid.scaleYProperty().bind(
				Bindings.when(
						this.heightProperty().lessThan(this.widthProperty().divide(FIELDRATIO+0.04)))
						.then(this.heightProperty())
						.otherwise(this.widthProperty().divide(FIELDRATIO+0.04))
						.divide(CELL_SIZE*(HEIGHT +FIELD_OFFSET +FIELD_OFFSET)));
		
					
		/*
		 * //so kann man die hue einstellen. hierlassen wird später als vorlage vielleicht 
		 * gebraucht ColorAdjust oneeighty = new ColorAdjust();
		 * oneeighty.setHue(1); oneeighty.setSaturation(0);
		 * oneeighty.setBrightness(0); GelbTex.setEffect(oneeighty);
		 */

		/*
		 * //blendmode spielerei hierlassen wird später als vorlage gebraucht
		 * Image hardlighttex = new Image(GameScreen.class.getResourceAsStream(
		 * "/resources/dev_hardlight.png")); ImageView hardlight = new
		 * ImageView(hardlighttex);
		 * hardlight.setBlendMode(BlendMode.HARD_LIGHT); //führ dazu das es
		 * außer visuell ignoriert wird. also elemente darunter bekommen
		 * mouseevents mit hardlight.setDisable(true); grid.add(hardlight, 1,
		 * 1);
		 */

	}

	/**
	 * Gibt Button und Checkboxes aus. Wird vom Presenter Für Mousevenets
	 * gebraucht
	 * 
	 * @CodeAutor: Alex
	 */
	public Map<Color, StackPane> getBoxes(){
		return boxes;
	}
	public Button getStartButton() {
		return StartButton;
	}

	public CheckBox getEightPlayerCheck() {
		return EightPlayerCheck;
	}

	public CheckBox getAICheck() {
		return AICheck;
	}

	public CheckBox getSuperPowerCheck() {
		return SuperPowerCheck;
	}

	public CheckBox getHelpCheck() {
		return HelpCheck;
	}

	/**
	 * Gibt Gridpane aus. Wird zum repostionnieren gerbaucht bei 8PlayerCheck
	 * true
	 * 
	 * @CodeAutor: Alex
	 */
	public GridPane getGrid() {
		return grid;
	}

	/**
	 * Gibt die Karten aus. Wird vom Preserenter Für Mouseevents gebraucht
	 * 
	 * @CodeAutor: Alex
	 */
	public StackPane getBox(Color c) {
		return boxes.get(c);
	}

	/**
	 * Gibt KartenTextur aus. wird von Flip im Presenter gebraucht um
	 * Kartentextur zu ändern
	 * 
	 * @CodeAutor: Alex
	 */
	public ImageView getTex(Color c) {
		return cards.get(c).getKey();
	}
	
	public void setGameStub(GameStub stub) {
		reset();
		if(stub == null)
			return;
		for(Entry<Color, Player> entry : stub.getPlayers().entrySet()){
			String name = entry.getValue().getNick().getNick();
			cards.get(entry.getKey()).getKey().setViewport(new Rectangle2D(CARD_WIDTH * entry.getKey().getPosition(), 0, CARD_WIDTH, CARD_HEIGHT));
			cards.get(entry.getKey()).getValue().setText(name);
			boxes.get(entry.getKey()).setDisable(true);
		}
		
	}

	private void reset() {
		for(Entry<Color, Pair<ImageView, Text>> entry : cards.entrySet()){
			entry.getValue().getValue().setText(names.getString(entry.getKey().toString()));
		}
	}

}