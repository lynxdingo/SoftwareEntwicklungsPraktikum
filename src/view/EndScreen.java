package view;

import static model.Constants.CARD_HEIGHT;
import static model.Constants.CARD_WIDTH;
import static model.Constants.CELL_SIZE;
import static model.Constants.FIELDRATIO;
import static model.Constants.FIELD_OFFSET;
import static model.Constants.HEIGHT;
import static model.Constants.STANDARTCSS;
import static model.Constants.WIDTH;
import static view.ImageLoader.getImageLoader;
import javafx.beans.binding.Bindings;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import server.model.Suspicion;

/**
 * Endbildschirm: Zeigt die Lösung an.
 * @author anikiNT
 *
 */
public class EndScreen extends Scene {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(EndScreen.class);

	private StackPane root;
	private GridPane grid = new GridPane();
	
	private ImageView background = new ImageView(getImageLoader().getImage("background"));
	
	private ImageView Accuser = new ImageView(getImageLoader().getImage("murdererCards"));
	private ImageView Murderer = new ImageView(getImageLoader().getImage("murdererCards"));
	private ImageView Weapon = new ImageView(getImageLoader().getImage("weaponCards"));
	private ImageView Room = new ImageView(getImageLoader().getImage("roomCards"));
	private ImageView Fragezeichen = new ImageView(getImageLoader().getImage("icon"));

	private Text txtPlayer = new Text();
	private Text txtMurderer = new Text();
	private Text txtWeapon = new Text();
	private Text txtRoom = new Text();
	private Label lblPlayer = new Label("Detective");
	private Label lblMurderer = new Label("Murderer");
	private Label lblWeapon = new Label("Weapon");
	private Label lblRoom = new Label("Location");
	
	private StackPane GelbBox = new StackPane();
	private StackPane BlauBox = new StackPane();
	private StackPane RotBox = new StackPane();
	private StackPane WeissBox = new StackPane();
	
	private Button btnBacktoMenu = new Button("To Lobby");
	
	public EndScreen() {

		super(new StackPane());
		this.root = (StackPane)this.getRoot();
		this.getStylesheets().add(STANDARTCSS);
		
		/** Hintergrund wird erzeugt und dynamisch an Hintergrundfüllend aber Proportional sclariert 
    	 * @CodeAutor: Alex */
		root.getStyleClass().add("screen");
		background.setOpacity(0.5);
		double bgratio = background.getImage().getWidth()/background.getImage().getHeight();
		background.scaleXProperty().bind(Bindings
				.when(this.widthProperty().greaterThan(this.heightProperty().multiply(bgratio)))
				.then(this.widthProperty())
				.otherwise(this.heightProperty().multiply(bgratio))				
				.divide(background.getImage().getWidth()));
		background.scaleYProperty().bind(Bindings
				.when(this.heightProperty().greaterThan(this.widthProperty().divide(bgratio)))
				.then(this.heightProperty())
				.otherwise(this.widthProperty().divide(bgratio))				
				.divide(background.getImage().getHeight() ));	
		root.getChildren().addAll(background);
		//grid.setGridLinesVisible(true);
		
		root.getChildren().add(grid);	
		grid.setAlignment(Pos.CENTER);
        grid.setHgap(CARD_WIDTH*0.1);
        grid.setVgap(CARD_WIDTH*0.1);
        grid.setPadding(new Insets(0, 0, 0, 0));
         
        //bekomme nicht hin das fragezeichen auszuschneiden
//        Fragezeichen.setViewport(new Rectangle2D(0, 0, CARD_WIDTH, CARD_HEIGHT));
        Accuser.setViewport(new Rectangle2D(0, 0, CARD_WIDTH, CARD_HEIGHT)); 
        Murderer.setViewport(new Rectangle2D(CARD_WIDTH, 0, CARD_WIDTH, CARD_HEIGHT));
        Room.setViewport(new Rectangle2D(CARD_WIDTH*2, 0, CARD_WIDTH, CARD_HEIGHT));
        Weapon.setViewport(new Rectangle2D(CARD_WIDTH*4, 0, CARD_WIDTH, CARD_HEIGHT));
        
        txtPlayer.getStyleClass().add("black");
        txtMurderer.getStyleClass().add("black");
        txtWeapon.getStyleClass().add("black");
        txtRoom.getStyleClass().add("black");
        
        GelbBox.setAlignment(Pos.BOTTOM_CENTER);
        BlauBox.setAlignment(Pos.BOTTOM_CENTER);
        RotBox.setAlignment(Pos.BOTTOM_CENTER);
        WeissBox.setAlignment(Pos.BOTTOM_CENTER);
        
        RotBox.getChildren().addAll(Accuser, txtPlayer);
        BlauBox.getChildren().addAll(Murderer, txtMurderer);
        WeissBox.getChildren().addAll(Weapon, txtWeapon);
        GelbBox.getChildren().addAll(Room, txtRoom);
                
        GelbBox.setPickOnBounds(false);
        BlauBox.setPickOnBounds(false);
        RotBox.setPickOnBounds(false);
        WeissBox.setPickOnBounds(false);
        
        GelbBox.setDisable(true);
        BlauBox.setDisable(true);
        RotBox.setDisable(true);
        WeissBox.setDisable(true);
                    
        grid.add(lblPlayer, 0, 0);
        grid.add(lblMurderer, 1, 0);
        grid.add(lblWeapon, 2, 0);
        grid.add(lblRoom, 3, 0);
        grid.add(RotBox, 0, 1);
        grid.add(BlauBox, 1 ,1);
        grid.add(WeissBox, 2 ,1);
        grid.add(GelbBox, 3 ,1);
        grid.add(btnBacktoMenu, 3, 2);
        
        GridPane.setHalignment(lblPlayer, HPos.CENTER);
        GridPane.setHalignment(lblMurderer, HPos.CENTER);
        GridPane.setHalignment(lblWeapon, HPos.CENTER);
        GridPane.setHalignment(lblRoom, HPos.CENTER);
        GridPane.setHalignment(btnBacktoMenu, HPos.RIGHT);
	    
		/**
		 * Dynmaisches Scaleiren wie Field proportional zur Bidlschirmgrösse damit die schriff überall gleich ist
		 * @CodeAutor: Alex
		 */
        grid.scaleXProperty().bind(
				Bindings.when(this.widthProperty().lessThan(this.heightProperty().multiply(FIELDRATIO)))
						.then(this.widthProperty())
						.otherwise(this.heightProperty().multiply(FIELDRATIO))
						.divide(CELL_SIZE*(WIDTH +FIELD_OFFSET +FIELD_OFFSET)));
		grid.scaleYProperty().bind(
				Bindings.when(
						this.heightProperty().lessThan(this.widthProperty().divide(FIELDRATIO)))
						.then(this.heightProperty())
						.otherwise(this.widthProperty().divide(FIELDRATIO))
						.divide(CELL_SIZE*(HEIGHT +FIELD_OFFSET +FIELD_OFFSET)));
			}
	
	public Button getBackToMenu(){
		return btnBacktoMenu;
	}
	
	//hatte hier server.model.Player nick noch drin hat aber nicht so recht funktioniert
	public void setAccuse(server.model.Color color, Suspicion suspicion) {
		RotBox.getStyleClass().clear();
		BlauBox.getStyleClass().clear();
		WeissBox.getStyleClass().clear();
		GelbBox.getStyleClass().clear();
		if(suspicion.isIllegal()){
			
		}
		Murderer.setViewport(new Rectangle2D(suspicion.getColor().getPosition()*CARD_WIDTH, 0, CARD_WIDTH, CARD_HEIGHT));
		BlauBox.getStyleClass().add(suspicion.getColor().toString());
//		txtMurderer.setText(suspicion.getColor().toString());
		Weapon.setViewport(new Rectangle2D(suspicion.getWeapon().getPosition()*CARD_WIDTH, 0, CARD_WIDTH, CARD_HEIGHT));
		WeissBox.getStyleClass().add(suspicion.getWeapon().toString());
//		txtWeapon.setText(suspicion.getWeapon().toString());
		Room.setViewport(new Rectangle2D(suspicion.getRoom().getPosition()*CARD_WIDTH, 0, CARD_WIDTH, CARD_HEIGHT));
		GelbBox.getStyleClass().add(suspicion.getRoom().toString());
//		txtRoom.setText(suspicion.getRoom().toString());
		
		if(color != null){
			Accuser.setImage(ImageLoader.getImageLoader().getImage("murdererCards"));
			RotBox.setVisible(true);
			Accuser.setViewport(new Rectangle2D(color.getPosition()*CARD_WIDTH, 0, CARD_WIDTH, CARD_HEIGHT));
			RotBox.getStyleClass().add(color.toString());
//			txtPlayer.setText(nick.getNick().toString());
		}else{
			Accuser.setImage(ImageLoader.getImageLoader().getImage("unknownCards"));
			Accuser.setViewport(new Rectangle2D(0, 0, CARD_WIDTH, CARD_HEIGHT));
		}
	}
}
