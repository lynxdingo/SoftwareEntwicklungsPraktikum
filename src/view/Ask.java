package view;

import static model.Constants.CARD_WIDTH;
import static model.Constants.CELL_SIZE;
import static view.ImageLoader.getImageLoader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import model.ClientModel;
import model.Player;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import server.model.Card;
import server.model.Color;
import server.model.Room;
import server.model.Suspicion;
import server.model.Weapons;

/**
 * Suspicion Screen der angezeigt wird, wenn eine Suspicion/Disprove/Accuse-Phase aufgerufen wird.
 * 
 * @Codeauthor: Alex(?)
 * @author anikiNT
 *
 */
public class Ask extends StackPane{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Ask.class);

	private ClientModel model;
	//private GridPane grid = new GridPane();
	
	private ImageView background = new ImageView(getImageLoader().getImage("background"));

	private VBox vbox = new VBox(CARD_WIDTH * 0.1 );
	
	private Label suspicion;
	
	private Map<Color, HBox> names = new HashMap<Color, HBox>();
	
	/** Texturen werden Geladen
	 * @CodeAutor: Alex */
	private HBox suspicionBox;
	private ImageView suspectWeapon = new ImageView(getImageLoader().getImage("weaponIcons"));
	private ImageView suspectRoom = new ImageView(getImageLoader().getImage("roomIcons"));
	private ImageView suspectColor = new ImageView(getImageLoader().getImage("playerIcons"));
	
	private HBox myBox = new HBox();
	private ImageView myWeapon = new ImageView(getImageLoader().getImage("weaponIcons"));
	private ImageView myRoom = new ImageView(getImageLoader().getImage("roomIcons"));
	private ImageView myColor = new ImageView(getImageLoader().getImage("playerIcons"));
	private Label nothingBtn = new Label("No Disprove");
	
	private Color lastDisprover;
	
	
	private Button contButton = new Button("continue");
	
	public Ask (ClientModel model) {
		this.model = model;
		//this.setBackground(new Background(new BackgroundFill(javafx.scene.paint.Color.PINK, null, null)));
		
		GaussianBlur gaussBlur = new GaussianBlur();
		gaussBlur.setRadius(50.0);
		
		background.setEffect(gaussBlur);
		background.setOpacity(0.5);
		
		this.getChildren().addAll(background, vbox);
		
		fillVBox();
     	 
     
 				               
	}
	/** Getter für Buttons
	 * @CodeAutor: Alex, Markus */
	public ImageView getWeaponButton(){
		return myWeapon;
	}
	public ImageView getColorButton(){
		return myColor;
	}
	public ImageView getRoomButton(){
		return myRoom;
	}
	public ImageView getbackground() {
		return background;	}
	
	public Button getcontButton(){
		return contButton;	}
	
	public void setSuspicion (Suspicion sus){
		fillVBox();
		List<Card> myCards = model.getGame().getDisproveCards();
		suspectRoom.setViewport(new Rectangle2D(0, sus.getRoom().getPosition() * CELL_SIZE, CELL_SIZE, CELL_SIZE));
		suspectWeapon.setViewport(new Rectangle2D(0, sus.getWeapon().getPosition() * CELL_SIZE, CELL_SIZE, CELL_SIZE));
		suspectColor.setViewport(new Rectangle2D(0, sus.getColor().getPosition() * CELL_SIZE, CELL_SIZE, CELL_SIZE));
		myBox.getChildren().clear();
		
		if(myCards.contains(sus.getRoom())){
			myRoom.setViewport(new Rectangle2D(0, sus.getRoom().getPosition() * CELL_SIZE, CELL_SIZE, CELL_SIZE));
			myRoom.setDisable(true);
			myBox.getChildren().add(myRoom);
		}
		if(myCards.contains(sus.getWeapon())){
			myWeapon.setViewport(new Rectangle2D(0, sus.getWeapon().getPosition() * CELL_SIZE, CELL_SIZE, CELL_SIZE));
			myWeapon.setDisable(true);
			myBox.getChildren().add(myWeapon);
		}
		if(myCards.contains(sus.getColor())){
			myColor.setViewport(new Rectangle2D(0, sus.getColor().getPosition() * CELL_SIZE, CELL_SIZE, CELL_SIZE));
			myColor.setDisable(true);
			myBox.getChildren().add(myColor);
		}
		if(myCards.isEmpty()){
			nothingBtn.setDisable(true);
			myBox.getChildren().add(nothingBtn);
		}
	}
	public void setDisprove(Color disprover, Card card) {
		contButton.setDisable(false);
		HBox hbox = names.get(disprover);
		if(hbox == null)
			return;
		if(card==null){
			ImageView icon = new ImageView(getImageLoader().getImage("unknownIcons"));
			icon.setViewport(new Rectangle2D(0, CELL_SIZE, CELL_SIZE, CELL_SIZE));
			hbox.getChildren().add(icon);
		}else{
			ImageView icon;
			if(card instanceof Room){
				icon = new ImageView(getImageLoader().getImage("roomIcons"));
			}else if(card instanceof Weapons){
				icon = new ImageView(getImageLoader().getImage("weaponIcons"));
			}else{
				icon = new ImageView(getImageLoader().getImage("playerIcons"));
			}
			icon.setViewport(new Rectangle2D(0, card.getPosition()*CELL_SIZE, CELL_SIZE, CELL_SIZE));
			hbox.getChildren().add(icon);
		}
		
	}
	private void fillVBox(){
		lastDisprover = null;
		if (logger.isDebugEnabled()) {
			logger.debug("fillVBox() - fillVBox"); //$NON-NLS-1$
		}
		//Add Suspicion Label
		vbox.getChildren().clear();
		suspicion = new Label("Suspicion");
		vbox.getChildren().add(suspicion);
		
		//Create suspicion Box
		suspicionBox = new HBox();
		suspicionBox.getChildren().addAll(suspectRoom, suspectColor, suspectWeapon);
		vbox.getChildren().add(suspicionBox);
		
		for(Player player : model.getGame().getDisproveOrder()){
			if(player.getColor() == model.getGame().getColor()){
				vbox.getChildren().add(myBox);
			}else{
				HBox hbox = new HBox();
				Label nickname = new Label(player.getNick().getNick());
				if (logger.isDebugEnabled()) {
					logger.debug("fillVBox() - Added: " + nickname.getText()); //$NON-NLS-1$
				}
				hbox.getChildren().add(nickname);
				names.put(player.getColor(), hbox);
				vbox.getChildren().add(hbox);
			}
		}

 		contButton.setPickOnBounds(true);
 		contButton.setDisable(true);
		vbox.getChildren().add(contButton);
	}
	public VBox getVBox() {
		return vbox;
	}
	public Label getNothingBtn(){
		return nothingBtn;
	}
	public void setMyTurn() {
		if (logger.isDebugEnabled()) {
			logger.debug("setMyTurn() - yeah its my turn"); //$NON-NLS-1$
		}
		nothingBtn.setDisable(false);
		myWeapon.setDisable(false);
		myRoom.setDisable(false);
		myColor.setDisable(false);
	}
	public void setTurn(Color activeDisprover) {
		if(lastDisprover != null){
			//TODO stop highlighting
		}
		lastDisprover = activeDisprover;
		//TODO Highlight him
	}
	public void noDisprove() {
		contButton.setDisable(false);
		
	}
	
}
