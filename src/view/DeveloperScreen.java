package view;

import static model.Constants.CELL_SIZE;
import static model.Constants.FIELDRATIO;
import static model.Constants.FIELD_OFFSET;
import static model.Constants.HEIGHT;
import static model.Constants.WIDTH;
import static view.ImageLoader.getImageLoader;
import javafx.beans.binding.Bindings;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Einfach alle Screens erreichbar.
 * @Codeauthor: An
 * @author anikiNT
 *
 */
public class DeveloperScreen extends Scene{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(DeveloperScreen.class);
	
	private static StackPane root = new StackPane();;
	private GridPane grid = new GridPane();
	
	//Buttons mit e->Screen
	private Button colorChose = new Button("ColorChoseScreen");
	private Button end = new Button("EndScreen");
	private Button error = new Button("ErrorScreen");
	private Button game = new Button("GameScreen");
	private Button lobby = new Button("LobbyScreen");
	private Button menu = new Button("MenuScreen");
	private Button options = new Button("OptionsScreen");
	private Button progress = new Button("ProgressScreen");
	
	//Texturen
	private ImageView background = new ImageView(getImageLoader().getImage("background"));
	
	//Konstruktor
	public DeveloperScreen() {
		super(root);
		
		
		//Skalieren + Skin
		root.getStyleClass().add("screen");
		background.setOpacity(0.5);
		double bgratio = background.getImage().getWidth()/background.getImage().getHeight();
		background.scaleXProperty().bind(Bindings
				.when(this.widthProperty().greaterThan(this.heightProperty().multiply(bgratio)))
				.then(this.widthProperty())
				.otherwise(this.heightProperty().multiply(bgratio))				
				.divide(background.getImage().getWidth()));
		background.scaleYProperty().bind(Bindings
				.when(this.heightProperty().greaterThan(this.widthProperty().divide(bgratio)))
				.then(this.heightProperty())
				.otherwise(this.widthProperty().divide(bgratio))				
				.divide(background.getImage().getHeight() ));	
		root.getChildren().add(background);
		root.getChildren().add(grid);
		//Ausrichtung des Grids
		grid.setAlignment(Pos.CENTER);
		
		/**
		 * Dynmaisches Scaleiren wie Field proportional zur Bidlschirmgrösse damit die schriff überall gleich ist
		 * @CodeAutor: Alex
		 */
        grid.scaleXProperty().bind(
				Bindings.when(this.widthProperty().lessThan(this.heightProperty().multiply(FIELDRATIO)))
						.then(this.widthProperty())
						.otherwise(this.heightProperty().multiply(FIELDRATIO))
						.divide(CELL_SIZE*(WIDTH +FIELD_OFFSET +FIELD_OFFSET)));
		grid.scaleYProperty().bind(
				Bindings.when(
						this.heightProperty().lessThan(this.widthProperty().divide(FIELDRATIO)))
						.then(this.heightProperty())
						.otherwise(this.widthProperty().divide(FIELDRATIO))
						.divide(CELL_SIZE*(HEIGHT +FIELD_OFFSET +FIELD_OFFSET)));
		
		
		grid.add(colorChose ,0 , 0);
		grid.add(end ,0 , 1);
		grid.add(error ,0 , 2);
		grid.add(game ,0 , 3);
		grid.add(lobby ,0 , 4);
		grid.add(menu ,0 , 5);
		grid.add(options ,0 , 6);
		grid.add(progress ,0 , 7);
		
	}
	//Getter für Buttons
	public Button getEnd() {
		return end;
	}
	public Button getError() {
		return error;
	}
	public Button getGame() {
		return game;
	}
	public Button getLobby() {
		return lobby;
	}
	public Button getMenu() {
		return menu;
	}
	public Button getOptions() {
		return options;
	}
	public Button getProgress() {
		return progress;
	}
	public Button getColorChose() {
		return colorChose;
	}

}
