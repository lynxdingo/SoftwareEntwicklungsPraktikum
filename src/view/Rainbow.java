package view;

import javafx.animation.FillTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.effect.BlendMode;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

/** ist ein Rainbow overlay das vom presenter im gamescreen aktiviert wird
 * @CodeAutor: Alex */
public class Rainbow extends Group {

	
	/** Elemente für rainbow
	 * @CodeAutor: Alex */
	private Rectangle rect = new Rectangle(100, 100);
    private int duration = 500;
    private SequentialTransition sequentialTransition = new SequentialTransition();
	
	public Rainbow () {
	    
		/** das rechteck wird zu Overlay und für maus deactiviert und standartmässig unsichtbar
		 * @CodeAutor: Alex */
		rect.setBlendMode(BlendMode.SOFT_LIGHT);
		//rect.setOpacity(0.5);
		rect.setDisable(true);
		rect.setVisible(false);
		
		/** die einzellnen stufen der Rainbow transition
		 * @CodeAutor: Alex */
	    FillTransition rb1 = new FillTransition(Duration.millis(duration), rect, Color.web("#f8bd55"), Color.web("#c0fe56"));
	    rb1.setAutoReverse(true);
	    
	    FillTransition rb2 = new FillTransition(Duration.millis(duration), rect, Color.web("#c0fe56"), Color.web("#5dfbc1"));
	    rb2.setAutoReverse(true);
	    
	    FillTransition rb3 = new FillTransition(Duration.millis(duration), rect, Color.web("#5dfbc1"), Color.web("#64c2f8"));
	    rb3.setAutoReverse(true);
	    
	    FillTransition rb4 = new FillTransition(Duration.millis(duration), rect, Color.web("#64c2f8"), Color.web("#be4af7"));
	    rb4.setAutoReverse(true);
	    
	    FillTransition rb5 = new FillTransition(Duration.millis(duration), rect, Color.web("#be4af7"), Color.web("#ed5fc2"));
	    rb5.setAutoReverse(true);
	    
	    FillTransition rb6 = new FillTransition(Duration.millis(duration), rect, Color.web("#ed5fc2"), Color.web("#ef504c"));
	    rb6.setAutoReverse(true);
	    
	    FillTransition rb7 = new FillTransition(Duration.millis(duration), rect, Color.web("#ef504c"), Color.web("#f2660f"));
	    rb7.setAutoReverse(true);
	    
	    FillTransition rb8 = new FillTransition(Duration.millis(duration), rect, Color.web("#f2660f"), Color.web("#f8bd55"));
	    rb8.setAutoReverse(true);

		/** Zusammen fügen des Rainbows in einer sequenz
		 * @CodeAutor: Alex */
	    sequentialTransition.getChildren().addAll(rb1, rb2, rb3, rb4, rb5, rb6, rb7, rb8);
	    sequentialTransition.setCycleCount(Timeline.INDEFINITE);
	    sequentialTransition.setAutoReverse(false);
	    sequentialTransition.play();

	    this.getChildren().add(rect);
	}
	
	/** getter für den Presenter
	 * @CodeAutor: Alex */
	public Rectangle getrect() {
		return rect;
	}
	
	public SequentialTransition getSequentialTransition() {
		return sequentialTransition;
	}
			
}
