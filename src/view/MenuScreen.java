package view;

import static model.Constants.CARD_WIDTH;
import static model.Constants.CELL_SIZE;
import static model.Constants.FIELDRATIO;
import static model.Constants.FIELD_OFFSET;
import static model.Constants.GOLDENRATIO;
import static model.Constants.HEIGHT;
import static model.Constants.STANDARTCSS;
import static model.Constants.WIDTH;
import static view.ImageLoader.getImageLoader;
import javafx.beans.binding.Bindings;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Menue
 * lblÜberschrift = Überschrift
 * Button
 * btnJoinGame = beitreten in Lobby
 * btnCreateServer = starten eines Servers
 * btnOptions = aufrufen der Optionen
 * btnExit = beenden des Spiels
 * 
 * 
 * @author Tim
 * @codeauthor Tim, An, Alex
 */

public class MenuScreen extends Scene {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(MenuScreen.class);
	
	private static StackPane root = new StackPane();
	private GridPane grid = new GridPane();
	
	private ImageView background = new ImageView(getImageLoader().getImage("background"));
	
	private Label lblUeberschrift = new Label();
	private Button btnJoinGame = new Button("Join  Server");
	private Button btnCreateServer = new Button("Host Server");
	private Button btnOptions = new Button("Options");
	private Button btnExit = new Button("Exit Game");
		
	//private Image flash = new Image(GameScreen.class.getResourceAsStream(
	//		"/resources/giphy.gif"));
	//private ImageView coolFlash = new ImageView(flash);

	public MenuScreen(){
		super(root);
		this.getStylesheets().add(STANDARTCSS);
		
		/**
		 * Hintergrund wird erzeugt und dynamisch an Hintergrundfüllend aber
		 * Proportional sclariert
		 * 
		 * @CodeAutor: Alex
		 */
		root.getStyleClass().add("screen");
		double bgratio = background.getImage().getWidth()/background.getImage().getHeight();
		background.scaleXProperty().bind(Bindings
				.when(this.widthProperty().greaterThan(this.heightProperty().multiply(bgratio)))
				.then(this.widthProperty())
				.otherwise(this.heightProperty().multiply(bgratio))				
				.divide(background.getImage().getWidth()));
		background.scaleYProperty().bind(Bindings
				.when(this.heightProperty().greaterThan(this.widthProperty().divide(bgratio)))
				.then(this.heightProperty())
				.otherwise(this.widthProperty().divide(bgratio))				
				.divide(background.getImage().getHeight() ));	
		root.getChildren().add(background);
	
		
		/**
		 * GridPane wird initialisiert und in die StackPane Root eingefügt,
		 * damit sich die Scaliere nicht in quere kommen
		 * 
		 * @CodeAutor: Alex
		 */
		root.getChildren().add(grid);
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(CARD_WIDTH * 0.1);
		grid.setVgap(CARD_WIDTH * 0.1);
        grid.setPadding(new Insets(0, 0, 0, 0));
        
        //Hilfslinien		
        //grid.setGridLinesVisible(true);
        
        /**
		 * Alle elemente werde zentriert 
		 * 
		 * @CodeAutor: Alex
		 */
        GridPane.setHalignment(lblUeberschrift, HPos.CENTER);
        GridPane.setHalignment(btnJoinGame, HPos.CENTER);
        GridPane.setHalignment(btnCreateServer, HPos.CENTER);
        GridPane.setHalignment(btnOptions, HPos.CENTER);
        GridPane.setHalignment(btnExit, HPos.CENTER);
        
        btnJoinGame.setPickOnBounds(false);
        btnCreateServer.setPickOnBounds(false);
        btnOptions.setPickOnBounds(false);
        btnExit.setPickOnBounds(false);

		/**
		 * Dynmaisches Scaleiren wie Field proportional zur Bidlschirmgrösse damit die schriff überall gleich ist
		 * @CodeAutor: Alex
		 */
        grid.scaleXProperty().bind(
				Bindings.when(this.widthProperty().lessThan(this.heightProperty().multiply(FIELDRATIO)))
						.then(this.widthProperty())
						.otherwise(this.heightProperty().multiply(FIELDRATIO))
						.divide(CELL_SIZE*(WIDTH +FIELD_OFFSET +FIELD_OFFSET)));
		grid.scaleYProperty().bind(
				Bindings.when(
						this.heightProperty().lessThan(this.widthProperty().divide(FIELDRATIO)))
						.then(this.heightProperty())
						.otherwise(this.widthProperty().divide(FIELDRATIO))
						.divide(CELL_SIZE*(HEIGHT +FIELD_OFFSET +FIELD_OFFSET)));
		
		/**
		 * Bewegt das menu zur entsprechend der Goldenen Regel
		 * @CodeAutor: Alex
		 */
		grid.translateXProperty().bind(this.widthProperty().multiply(1-GOLDENRATIO-0.5));
		
	    grid.add(lblUeberschrift, 0, 0);
        grid.add(btnJoinGame, 0, 1);
       	grid.add(btnCreateServer, 0, 2);
       	grid.add(btnOptions, 0, 3);
        grid.add(btnExit, 0, 4);

               
        lblUeberschrift.setText("Cluedo");
        lblUeberschrift.setStyle("-fx-font-size: 98px; -fx-text-fill: #CFCFCF;");

	}
	public GridPane getGridPane(){
		return grid;
		}
	
	public Button getJoinGame(){
		if(btnJoinGame == null)
			if (logger.isDebugEnabled()) {
				logger.debug("getJoinGame() - moinbsen"); //$NON-NLS-1$
			}
		return btnJoinGame;
	}
	
	public Button getCreateServer(){
		return btnCreateServer;
		}
	
	public Button getOptions(){
		return btnOptions;
		}
	
	public Button getExit(){
		return btnExit;
		}

}
