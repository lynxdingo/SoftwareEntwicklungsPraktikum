package view;

import static model.Constants.CELL_SIZE;
import static model.Constants.FIELDRATIO;
import static model.Constants.FIELD_OFFSET;
import static model.Constants.GOLDENRATIO;
import static model.Constants.HEIGHT;
import static model.Constants.STANDARTCSS;
import static model.Constants.WIDTH;
import static view.ImageLoader.getImageLoader;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javafx.beans.binding.Bindings;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import model.ClientModel;
import model.Server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Der Loginscreen, wo man einen Server beitreten kann
 * @codeauthor Tim, An
 * @author anikiNT
 *
 */
public class LoginScreen extends Scene {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(LoginScreen.class);
	
	private StackPane root;
	private GridPane grid = new GridPane();
	
	private ImageView background = new ImageView(getImageLoader().getImage("background"));
	
	private Label lblGameID	= new Label("IP:");
	private Label lblName = new Label("Name:");
	private Label lblPort = new Label("Port:");
	private TextField nameField = new TextField();
	private TextField ipField = new TextField();
	private TextField portField = new TextField();
	private Button btnBack = new Button("BACK");
	private Button btnJoinLobby = new Button("LOGIN");
	
	
	private Label lblServer = new Label("available server:");
	private ListView<Server> listServer;
	
	private ClientModel model;
	
	
	
	public LoginScreen (ClientModel model){

		super(new StackPane());
		this.root = (StackPane)this.getRoot();
		this.getStylesheets().add(STANDARTCSS);
		this.model = model;
		
		listServer = new ListView<Server>(this.model.getServers());
		
		listServer.setCellFactory(c->new ServerCell());
		listServer.getSelectionModel().selectedItemProperty().addListener(k->SelectionChanged());
		
		
		root.getStyleClass().add("screen");
		
		/**
		 * Hintergrund wird erzeugt und dynamisch an Hintergrundfüllend aber
		 * Proportional sclariert
		 * 
		 * @CodeAutor: Alex
		 */
		double bgratio = background.getImage().getWidth()/background.getImage().getHeight();
		background.setOpacity(0.5);
		background.scaleXProperty().bind(Bindings
				.when(this.widthProperty().greaterThan(this.heightProperty().multiply(bgratio)))
				.then(this.widthProperty())
				.otherwise(this.heightProperty().multiply(bgratio))				
				.divide(background.getImage().getWidth()));
		background.scaleYProperty().bind(Bindings
				.when(this.heightProperty().greaterThan(this.widthProperty().divide(bgratio)))
				.then(this.heightProperty())
				.otherwise(this.widthProperty().divide(bgratio))				
				.divide(background.getImage().getHeight() ));	
		root.getChildren().add(background);
		
		root.getChildren().addAll(grid, listServer);
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(20);
		grid.setVgap(5);
		grid.setPadding(new Insets(0, 0, 0, 0));
        //Hilfslinien		
//        grid.setGridLinesVisible(true);
        
        btnBack.setPickOnBounds(false);
        btnJoinLobby.setPickOnBounds(false);
        ipField.setFocusTraversable(false);
        portField.setFocusTraversable(false);
        nameField.setFocusTraversable(false);
        ipField.getStyleClass().add("notestext");
        portField.getStyleClass().add("notestext");
        nameField.getStyleClass().add("notestext");
        
        for (int i = 0; i < 7; i++) {
            ColumnConstraints column = new ColumnConstraints(CELL_SIZE*2);
            grid.getColumnConstraints().add(column);}
        
        grid.add(lblName, 0, 1);
        grid.add(nameField, 1, 1, 2, 1);
        grid.add(lblGameID, 0, 2);
        grid.add(ipField, 1, 2, 2, 1);
        grid.add(lblPort, 0, 3);
        grid.add(portField, 1, 3);
        grid.add(btnBack, 0, 9);
        grid.add(btnJoinLobby, 2, 9);
        grid.add(lblServer, 4, 1, 3, 1);
        grid.add(listServer, 4, 2, 3, 8);
        
        GridPane.setHalignment(btnJoinLobby, HPos.RIGHT); 
        GridPane.setHalignment(nameField, HPos.LEFT); 
        GridPane.setHalignment(portField, HPos.LEFT);         
        GridPane.setHalignment(ipField, HPos.LEFT); 
        
		/**
		 * Dynmaisches Scaleiren wie Field proportional zur Bidlschirmgrösse damit die schriff überall gleich ist
		 * @CodeAutor: Alex
		 */
        grid.scaleXProperty().bind(
				Bindings.when(this.widthProperty().lessThan(this.heightProperty().multiply(FIELDRATIO)))
						.then(this.widthProperty())
						.otherwise(this.heightProperty().multiply(FIELDRATIO))
						.divide(CELL_SIZE*(WIDTH +FIELD_OFFSET +FIELD_OFFSET)));
		grid.scaleYProperty().bind(
				Bindings.when(
						this.heightProperty().lessThan(this.widthProperty().divide(FIELDRATIO)))
						.then(this.heightProperty())
						.otherwise(this.widthProperty().divide(FIELDRATIO))
						.divide(CELL_SIZE*(HEIGHT +FIELD_OFFSET +FIELD_OFFSET)));
		
		grid.translateXProperty().bind(this.widthProperty().multiply(1-GOLDENRATIO-0.5));
		/* Pseudo-Tooltips*/
		nameField.setTooltip(new Tooltip("Doschte"));
		nameField.setPromptText("Enter your name");
		ipField.setPromptText("Enter Server-IP");
		portField.setPromptText("Enter Port");
		

		
	    nameField.setText("Maicus");
	    ipField.setText("vanuabalavu.pms.ifi.lmu.de");
	    portField.setText("30305");
	}

	
	private void SelectionChanged() {
		Server server = listServer.getSelectionModel().getSelectedItem();
		if(server == null)
			return;
		ipField.setText(server.getAddress().getHostAddress());
		portField.setText(""+server.getPort());
	}


	public String getIP(){
		return ipField.getText();
	}
	public int getPort(){
		return Integer.parseInt(portField.getText());
	}
	public String getName(){
		return nameField.getText();
	}
	public Button getJoinLobby(){
		return btnJoinLobby;
	}
	public Button getBackToMenu(){
		return btnBack;
	}
	
	static class ServerCell extends ListCell<Server>{
		final Tooltip tooltip = new Tooltip();
		@Override
		public void updateItem(Server server, boolean empty){
			
			super.updateItem(server, empty);
			if(server != null){
				setText(server.getGroup());
				tooltip.setText("IP: " + server.getAddress().getHostAddress() + " Port: " + server.getPort());
				this.setTooltip(tooltip);
			}
		}
	}

	public Server getServer() throws UnknownHostException {
		Server s = listServer.getSelectionModel().getSelectedItem();
		String group = "";
		if(s != null){
			group = s.getGroup();
		}
		return new Server(InetAddress.getByName(ipField.getText()), Integer.parseInt(portField.getText()), group);
	}
}
