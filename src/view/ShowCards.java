package view;

import static model.Constants.CARD_HEIGHT;
import static model.Constants.CARD_WIDTH;
import static view.ImageLoader.getImageLoader;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import server.model.Card;
import server.model.Room;
import server.model.Suspicion;
import server.model.Weapons;

/**
 * I dont know. Teil des Ask-Screens?
 * @codeauthor ALex, Markus
 * @author anikiNT
 *
 */
public class ShowCards extends StackPane{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(ShowCards.class);

	/** Elemente für showcards
	 * @CodeAutor: Alex, Markus		 */
	private GridPane grid = new GridPane();
	private HBox cardBox = new HBox(CARD_WIDTH * 0.1);
	private Label headline = new Label("Headline");
	private ImageView background = new ImageView(getImageLoader().getImage("background"));
	private Button contButton = new Button("continue");
	
	public ShowCards () {
		
		GaussianBlur gaussBlur = new GaussianBlur();
		gaussBlur.setRadius(50.0);
		
		/** Effecte für den snapshot hintergrund
		 * @CodeAutor: Alex		 */
		background.setEffect(gaussBlur);
		background.setOpacity(0.5);
		
		/** Styled die gridpane
		 * @CodeAutor: Alex */
		this.getChildren().addAll(background, grid);
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(CARD_WIDTH * 0.1);
		grid.setVgap(CARD_WIDTH * 0.1);
		grid.setPadding(new Insets(0, 0, 0, 0));
		fillGrid();
     	   	               
	}
	private void fillGrid() {
		//Add Suspicion Label
		grid.getChildren().clear();
		grid.add(headline, 0, 0);
				
		//suspicionBox.getChildren().addAll(suspectRoom, suspectColor, suspectWeapon);
		grid.add(cardBox, 0, 1, 3, 1);
		
 		contButton.setPickOnBounds(true);
 		grid.add(contButton, 2, 2);
 		GridPane.setHalignment(contButton, HPos.RIGHT);
				
	}
	public void setHeadline(String headline){
		this.headline.setText(headline);
	}
	public void setCards(List<Card> cards){
		cardBox.getChildren().clear();
		for(Card c : cards){
			ImageView img;
			if(c instanceof Room){
				img = new ImageView(getImageLoader().getImage("roomCards"));
			}else if(c instanceof Weapons){
				img = new ImageView(getImageLoader().getImage("weaponCards"));
			}else {
				img = new ImageView(getImageLoader().getImage("murdererCards"));
			}
			
			img.setViewport(new Rectangle2D(CARD_WIDTH * c.getPosition(), 0, CARD_WIDTH, CARD_HEIGHT));
			img.getStyleClass().add(c.toString());
			img.setDisable(true);
			cardBox.getChildren().add(img);
		}
	}
	public Button getCntBtn(){
		return contButton;
	}
	public ImageView getbackground() {
		return background;	
	}
	public void setSuspicion(Suspicion suspicion) {
		List<Card> cards = new ArrayList<Card>();
		cards.add(suspicion.getColor());
		cards.add(suspicion.getWeapon());
		cards.add(suspicion.getRoom());
		setCards(cards);
		
	}
	public GridPane getGrid() {
		return grid;
	}
}
