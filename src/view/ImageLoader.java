package view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import javafx.scene.image.Image;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Ladet Bilder(wieso?)
 * @codeauthor ?
 * @author anikiNT
 *
 */
public class ImageLoader {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(ImageLoader.class);

	private Map<String, Image> images = new HashMap<String, Image>();
	private Map<String, String> pathes = new HashMap<String, String>();
	
	private static ImageLoader instance;
	private ImageLoader(){
		loadPathes();
	}
	private void loadPathes() {
		String json = "";
		String line = "";
		try {
			InputStreamReader stream = new InputStreamReader(this.getClass().getResourceAsStream("/resources/images.json"));
			
			BufferedReader br = new BufferedReader(stream);
			while((line = br.readLine())!=null){
				json = json + line;
	
			}
			stream.close();
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("loadPathes()", e); //$NON-NLS-1$
		}
		JSONObject obj = new JSONObject(json);
		JSONArray arr = obj.getJSONArray("Images");
		for(int i = 0; !arr.isNull(i); i++){
			JSONObject path = arr.getJSONObject(i);
			pathes.put(path.getString("name"), path.getString("path"));
		}
		
	}
	public static ImageLoader getImageLoader(){
		if(ImageLoader.instance == null){
			ImageLoader.instance = new ImageLoader();
		}
		return ImageLoader.instance;
	}
	public Image getImage(String name){
		Image ret = images.get(name);
		if(ret == null){
			ret = loadImage(name);
		}
		return ret;
	}
	private Image loadImage(String name){
		if (logger.isDebugEnabled()) {
			logger.debug("loadImage(String) - Bild: " + name + " wird geladen."); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if(!pathes.containsKey(name)){
			return null;
		}
		Image image = null;
		//InputStream stream = ImageLoader.class.getResourceAsStream(pathes.get(name));
		image = new Image(pathes.get(name));
		//stream.close();
		if(image.isError()){
			logger.error("loadImage(String) - This image does not exist"); //$NON-NLS-1$
			return getImage("noTexture");
		}
		images.put(name, image);
		return image;
	}
}
