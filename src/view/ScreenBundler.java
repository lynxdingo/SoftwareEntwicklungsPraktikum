package view;

import model.ClientModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Verwaltet alle Screens, die es gibt.
 * @author anikiNT
 *
 */
public class ScreenBundler {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(ScreenBundler.class);

	private MenuScreen menuScreen = new MenuScreen();
	private ColorChoseScreen lobbyScreen = new ColorChoseScreen();
	private ProgressScreen progressScreen = new ProgressScreen();
	private GameScreen gameScreen;
	private EndScreen endScreen = new EndScreen();
	private OptionsScreen optionsScreen = new OptionsScreen();
	private LoginScreen loginScreen;
	private ErrorScreen errorScreen = new ErrorScreen();
	private DeveloperScreen developerScreen = new DeveloperScreen();
	public ScreenBundler(ClientModel model){
		gameScreen = new GameScreen(model);
		loginScreen = new LoginScreen(model);
	}
	public DeveloperScreen getDeveloperScreen(){
		return developerScreen;
	}
	public LoginScreen getLoginScreen(){
		return loginScreen;
	}
	public MenuScreen getMenuScreen() {
		return menuScreen;
	}
	public ColorChoseScreen getLobbyScreen() {
		return lobbyScreen;
	}
	public ProgressScreen getProgressScreen() {
		return progressScreen;
	}
	public GameScreen getGameScreen() {
		return gameScreen;
	}
	public EndScreen getEndScreen() {
		return endScreen;
	}
	public OptionsScreen getOptionsScreen() {
		return optionsScreen;
	}
	public ErrorScreen getErrorScreen(){
		return errorScreen;
	}
}
