package view;

import static model.Constants.CELL_SIZE;
import static model.Constants.FIELD_OFFSET;
import static model.Constants.HEIGHT;
import static model.Constants.WIDTH;

import java.util.ArrayList;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.util.Callback;
import model.ClientModel;
import model.GameStub;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Der LobbyScreen, wo man Spiel beitreten/erstellen/beobachten kann.
 * @codeauthor Alex
 * @author anikiNT
 *
 */
public class LobbyScreen extends GridPane{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(LobbyScreen.class);
	
	/** erzeugt alle elemente für die lobby
	 * @CodeAutor: Alex */
	private Button btnJoinGame;
	private Button btnWatchGame;
	private Button btnCreateGame;
	private Button btnStartGame;
	private Button btnErrorTest;
	private TableView <GameStub> table;
	private ObservableList<GameStub> items;
	
	private ClientModel model;
	
	public LobbyScreen(ClientModel model){
		this.model = model;
		initialize();
	}
	
	/** Getter für elemente der Lobby
	 * @CodeAutor: Alex */
	public Button getJoinButton(){
		return btnJoinGame;
	}
	public Button getCreateButton(){
		return btnCreateGame;
	}
	public Button getWatchButton(){
		return btnWatchGame;
	}

	public int getSelectedGameID(){
		if(table.getSelectionModel().getSelectedItem()==null)
			return -1;
		return table.getSelectionModel().getSelectedItem().getGameID();  
	}
	public Button getStartButton(){
		return btnStartGame;
	}
	public Button getErrorButton(){
		return btnErrorTest;
	}

	private void initialize(){
		/**Table werden listen zugeordnet 
		 * @CodeAutor: Markus */
		TableColumn<GameStub, Integer> columnID = new TableColumn<>("GameID");
		columnID.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GameStub, Integer>, ObservableValue<Integer>>() {

            @Override
            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<GameStub, Integer> p) {
                return new SimpleIntegerProperty(p.getValue().getGameID()).asObject();
            }
        });
		
		TableColumn<GameStub, String> columnState = new TableColumn<>("GameState");
		columnState.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GameStub, String> , ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<GameStub, String> p) {
                return new SimpleStringProperty(p.getValue().getGamestate().toString());
            }
        });
		
		TableColumn<GameStub, Integer> columnCount = new TableColumn<>("Player Count");
		columnCount.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GameStub, Integer>, ObservableValue<Integer>>() {

            @Override
            public ObservableValue<Integer> call(TableColumn.CellDataFeatures<GameStub, Integer> p) {
                return new SimpleIntegerProperty(p.getValue().getPlayers().size()).asObject();
            }
        });
		
		/**Gridpane wird eingestellt
		 * @CodeAutor: Alex */
		//wichtig weill der aus irgendnem becheeuerten grund wenn er 0.8 procent der bildschirmgrösser erreicht immer grösser wird
		this.setMaxSize((CELL_SIZE*(WIDTH +FIELD_OFFSET +FIELD_OFFSET)), (CELL_SIZE*(HEIGHT +FIELD_OFFSET +FIELD_OFFSET)));
		this.setMinSize((CELL_SIZE*(WIDTH +FIELD_OFFSET +FIELD_OFFSET)), (CELL_SIZE*(HEIGHT +FIELD_OFFSET +FIELD_OFFSET)));
		this.setAlignment(Pos.CENTER_LEFT);
		this.setHgap(0);
		this.setVgap(0);
		this.setPadding(new Insets(0, 0, 0, CELL_SIZE*0.1));   
		items = FXCollections.observableList(new ArrayList<GameStub>());
		
		/**Buttones werden eingestellt 
		 * @CodeAutor: Alex */
		btnJoinGame = new Button("Join Game");
		btnJoinGame.setPickOnBounds(false);
        GridPane.setHalignment(btnJoinGame, HPos.CENTER);
		
		btnWatchGame = new Button("Watch Game");
		btnWatchGame.setPickOnBounds(false);
        GridPane.setHalignment(btnWatchGame, HPos.CENTER);
		
		btnStartGame = new Button("Start Game");
		btnStartGame.setPickOnBounds(false);
        GridPane.setHalignment(btnStartGame, HPos.CENTER);
		
		btnCreateGame = new Button("Create Game");
		btnCreateGame.setPickOnBounds(false);
        GridPane.setHalignment(btnCreateGame, HPos.CENTER);
		
		btnErrorTest = new Button("Error Test");
		btnErrorTest.setPickOnBounds(false);
        GridPane.setHalignment(btnErrorTest, HPos.CENTER);
        GridPane.setValignment(btnErrorTest, VPos.TOP);
		
    	/**Columns werden eingestellt 
    	 * @CodeAutor: Alex */
        columnID.setResizable(false);
        columnID.impl_setReorderable(false);
        columnID.prefWidthProperty().set(CELL_SIZE*14);
        
        columnState.setResizable(false);
        columnState.impl_setReorderable(false);
        columnState.prefWidthProperty().set(CELL_SIZE*4.4);
        
        columnCount.setResizable(false);
        columnCount.impl_setReorderable(false);
        columnCount.prefWidthProperty().set(CELL_SIZE*3);        
        
    	/**Table wird eingestellt 
    	 * @CodeAutor: Alex */
		table = new TableView<GameStub>(items);
		table.setPadding(new Insets(0, 0, 0, 0));
		table.getColumns().setAll(columnID, columnState, columnCount);
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		table.setPickOnBounds(false);
			
		//Linien zum bessern erkennen der Grids sollte im Spiel ausgeschaltet sein
		//this.setGridLinesVisible(true);
		
		/** die Gridpane rows und Columns größe wird festgelegt 
		 * @CodeAutor: Alex */
        ColumnConstraints col1 = new ColumnConstraints((CELL_SIZE*(WIDTH +FIELD_OFFSET +FIELD_OFFSET-3-0.1)));
        ColumnConstraints col2 = new ColumnConstraints((CELL_SIZE*3));
        this.getColumnConstraints().addAll(col1, col2);
		

		for (int i = 0; i < 25; i++) {
			RowConstraints row = new RowConstraints(CELL_SIZE);
			this.getRowConstraints().addAll(row); 	}
		RowConstraints row1 = new RowConstraints(CELL_SIZE*(FIELD_OFFSET +FIELD_OFFSET));
		this.getRowConstraints().addAll(row1); 
		
		/** Alle elemente werden in die gridpane eingefügt 
		 * @CodeAutor: Alex */
		//nicht vergesseh die höhe wieder auf 25 setzten 
		this.add(table, 0, 0, 1, 25);
		this.add(btnJoinGame, 1, 1);
		this.add(btnWatchGame, 1, 2);
		this.add(btnStartGame, 1, 3);
		this.add(btnCreateGame, 1, 4);
		//this.add(btnErrorTest, 1, 5);

		/** Listner für neue Spiele im sever
		 * @CodeAutor: Markus */
		model.getGames().addListener(new MapChangeListener<Integer, GameStub>() {
			@Override
				
				public void onChanged(MapChangeListener.Change change) {
				items.clear();
				items.addAll(model.getGames().values());
		      	}
		});
		table.setPlaceholder(new Label("No Games are currently on the Server!"));
	}
		
}
