package view;

import static model.Constants.CARD_WIDTH;
import static model.Constants.CELL_SIZE;
import static model.Constants.FIELDRATIO;
import static model.Constants.FIELD_OFFSET;
import static model.Constants.HEIGHT;
import static model.Constants.STANDARTCSS;
import static model.Constants.WIDTH;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**  Wird angezeigt bei Fehlermeldung
 * @CodeAutor: Alex, Markus */
public class ErrorScreen extends Scene{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(ErrorScreen.class);
	
	/** Elementer für scene
	 * @CodeAutor: Alex */
	private GridPane pane = new GridPane();
	
	private StackPane root;
	private Label text = new Label("Toller Text, sag ich mal");
	private Button ok = new Button("OK");
	
	private ImageView background = new ImageView();
	
	private DoubleProperty ratio = new SimpleDoubleProperty(1);
	
	public ErrorScreen (){
		super(new StackPane());
		this.root = (StackPane)this.getRoot();
		this.getStylesheets().add(STANDARTCSS);
		
		/** Effecte für snapshot background
		 * @CodeAutor: Alex */
		GaussianBlur gaussBlur = new GaussianBlur();
		gaussBlur.setRadius(15.0);
		
		background.setEffect(gaussBlur);
		background.setOpacity(0.5);
		
		/** Scalieret für Background
		 * @CodeAutor: Alex */
		background.fitWidthProperty().bind(Bindings
				.when(this.widthProperty().lessThan(this.heightProperty().multiply(ratio)))
				.then(this.widthProperty())
				.otherwise(this.heightProperty().multiply(ratio))	);
		background.fitHeightProperty().bind(Bindings
				.when(this.heightProperty().lessThan(this.widthProperty().divide(ratio)))
				.then(this.heightProperty())
				.otherwise(this.widthProperty().divide(ratio))	);
	
		//hintergrund und Vordergrund werden seperat skaliert also müssen beide in eine stackpane die dann erst der scene gegeben wird
		root.getStyleClass().add("screen");
		root.getChildren().addAll(background, pane);
		
		/** einstellung für Gridpane und ihre elemente 
		 * @CodeAutor: Alex */
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(CARD_WIDTH * 0.1);
        pane.setVgap(CARD_WIDTH * 0.1);
        pane.setPadding(new Insets(0, 0, 0, 0));
        
        text.getStyleClass().add("red");
        
        text.setDisable(true);
        ok.setPickOnBounds(true);
        
        pane.add(text, 0, 0);
        pane.add(ok, 0, 1);

        GridPane.setHalignment(ok, HPos.RIGHT);
		
		/**
		 * Dynmaisches Scaleiren wie Field proportional zur Bidlschirmgrösse damit die schriff überall gleich ist
		 * @CodeAutor: Alex
		 */
        pane.scaleXProperty().bind(
				Bindings.when(this.widthProperty().lessThan(this.heightProperty().multiply(FIELDRATIO)))
						.then(this.widthProperty())
						.otherwise(this.heightProperty().multiply(FIELDRATIO))
						.divide(CELL_SIZE*(WIDTH +FIELD_OFFSET +FIELD_OFFSET)));
		pane.scaleYProperty().bind(
				Bindings.when(
						this.heightProperty().lessThan(this.widthProperty().divide(FIELDRATIO)))
						.then(this.heightProperty())
						.otherwise(this.widthProperty().divide(FIELDRATIO))
						.divide(CELL_SIZE*(HEIGHT +FIELD_OFFSET +FIELD_OFFSET)));
        
	}
	
	public Button getOKButton(){
		return ok;
	}
	public void setText(String error){
		text.setText(error);
	}
	
	/** set für das Background Image
	 * @CodeAutor: Markus */
	public void setBackground(WritableImage snapshot) {
		background.setImage(snapshot);
		ratio.set(snapshot.getWidth()/snapshot.getHeight());
	}
}
