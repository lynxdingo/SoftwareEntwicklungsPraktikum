package view;

import static model.Constants.CELL_SIZE;
import static model.Constants.FIELDRATIO;
import static model.Constants.FIELD_OFFSET;
import static model.Constants.GOLDENRATIO;
import static model.Constants.HEIGHT;
import static model.Constants.STANDARTCSS;
import static model.Constants.WIDTH;
import static view.ImageLoader.getImageLoader;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.StackPane;
import model.ClientModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/** Gamescreen ist Knotenpunkt für Field Chat Info Options History und fügt diese in eine Scene ein
 * @CodeAutor: Alex */
public class GameScreen extends Scene{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(GameScreen.class);
	
	/** Aufrufen der Elemente und erzeigen einer Pane als Skelett
	 * @CodeAutor: Alex */
	private StackPane root;
	private LobbyScreen lobby;
	private GameStage field;
	private TabMenu tp;
	private Ask ask;
	private ShowCards showCards;
	private Rainbow rainbow;
	
	/**
	 * Geladene Texturen
	 * @CodeAutor: Alex
	 */
	private ImageView background = new ImageView(getImageLoader().getImage("background"));
	
	private DoubleProperty askratio = new SimpleDoubleProperty(FIELDRATIO);
	private DoubleProperty notesratio = new SimpleDoubleProperty(FIELDRATIO);
	
	public GameScreen(ClientModel model) {
		super(new StackPane());
		this.root = (StackPane)this.getRoot();
		lobby = new LobbyScreen(model);
		field = new GameStage(model);
		tp = new TabMenu(model);
		ask = new Ask(model);
		showCards = new ShowCards();
		rainbow = new Rainbow();
		this.getStylesheets().add(STANDARTCSS);

        
		/**
		 * Hintergrund wird erzeugt und dynamisch an Hintergrundfüllend aber
		 * Proportional sclariert
		 * @CodeAutor: Alex
		 */
		// hintergrund wird textur geladen und 50% opacity gesetzt hintergrund
		// ist #191919
		root.getStyleClass().add("screen");
		background.setOpacity(0.5);
		double bgratio = background.getImage().getWidth()/background.getImage().getHeight();
		background.scaleXProperty().bind(Bindings
				.when(this.widthProperty().greaterThan(this.heightProperty().multiply(bgratio)))
				.then(this.widthProperty())
				.otherwise(this.heightProperty().multiply(bgratio))				
				.divide(background.getImage().getWidth()));
		background.scaleYProperty().bind(Bindings
				.when(this.heightProperty().greaterThan(this.widthProperty().divide(bgratio)))
				.then(this.heightProperty())
				.otherwise(this.widthProperty().divide(bgratio))				
				.divide(background.getImage().getHeight() ));	
		
		root.getChildren().addAll(background, lobby, tp);
		
		/** gibt den spielfeld im gesammten einen Schatten der auf hover reagiert
		 * @CodeAutor: Alex */
		field.getStyleClass().add("shadow");
                                
        /** Field skalliert sich Proportional und Dynamisch an die Bildschirmgrösse dabei lässt es immer minimal 39% (Goldene Regel)
         * Platz für die Info Chat und so weiter
         * @CodeAutor: Alex */
		field.scaleXProperty().bind(Bindings
				.when(this.widthProperty().multiply(GOLDENRATIO).lessThan(this.heightProperty().multiply(FIELDRATIO)))
				.then(this.widthProperty().multiply(GOLDENRATIO))
				.otherwise(this.heightProperty().multiply(FIELDRATIO))				
				.divide(CELL_SIZE*(WIDTH +FIELD_OFFSET +FIELD_OFFSET)));
		field.scaleYProperty().bind(Bindings
				.when(this.heightProperty().lessThan(this.widthProperty().divide(FIELDRATIO).multiply(GOLDENRATIO)))
				.then(this.heightProperty())
				.otherwise(this.widthProperty().divide(FIELDRATIO).multiply(GOLDENRATIO))				
				.divide(CELL_SIZE*(HEIGHT  +FIELD_OFFSET +FIELD_OFFSET)));	
		
		/** hält das Spielfeld oben links in der Ecke
		 * @CodeAutor: Alex */
		field.translateXProperty().bind(Bindings	
				.when(this.widthProperty().multiply(GOLDENRATIO).lessThan(this.heightProperty().multiply(FIELDRATIO)))
				.then(this.widthProperty().multiply(GOLDENRATIO))
				.otherwise(this.heightProperty().multiply(FIELDRATIO))
				.subtract(CELL_SIZE*(WIDTH +FIELD_OFFSET +FIELD_OFFSET)).divide(2)	);
		/** Zerntriert das Spielfeld in der mitte in Y richtung
		 * @CodeAutor: Alex */
		field.translateYProperty().bind(
        		this.heightProperty().subtract(CELL_SIZE*(HEIGHT  +FIELD_OFFSET +FIELD_OFFSET))
        		.divide(2)	);
				
		/** scallier den screencap von ask genauso wie field
		 * @CodeAutor: Alex */
		ask.getbackground().fitWidthProperty().bind(Bindings
				.when(this.widthProperty().multiply(GOLDENRATIO).lessThan(this.heightProperty().multiply(askratio)))
				.then(this.widthProperty().multiply(GOLDENRATIO))
				.otherwise(this.heightProperty().multiply(askratio))	);
		ask.getbackground().fitHeightProperty().bind(Bindings
				.when(this.heightProperty().lessThan(this.widthProperty().multiply(GOLDENRATIO).divide(askratio)))
				.then(this.heightProperty())
				.otherwise(this.widthProperty().multiply(GOLDENRATIO).divide(askratio))	);
		
		/** Scaliert das grid von ask dass die icons diesele grösse haben wie die icons im field
		 * @CodeAutor: Alex */
		ask.getVBox().scaleXProperty().bind(field.scaleXProperty());
		ask.getVBox().scaleYProperty().bind(field.scaleYProperty());
		
		/** Positioniert Ask 
		 * @CodeAutor: Alex */
		ask.translateXProperty().bind(this.widthProperty().subtract(ask.getbackground().fitWidthProperty()).divide(-2));
		ask.maxWidthProperty().bind(ask.getbackground().fitWidthProperty());
		
		
		/** scallier den screencap von show genauso wie field
		 * @CodeAutor: Alex */
		showCards.getbackground().fitWidthProperty().bind(Bindings
				.when(this.widthProperty().multiply(GOLDENRATIO).lessThan(this.heightProperty().multiply(askratio)))
				.then(this.widthProperty().multiply(GOLDENRATIO))
				.otherwise(this.heightProperty().multiply(askratio))	);
		showCards.getbackground().fitHeightProperty().bind(Bindings
				.when(this.heightProperty().lessThan(this.widthProperty().multiply(GOLDENRATIO).divide(askratio)))
				.then(this.heightProperty())
				.otherwise(this.widthProperty().multiply(GOLDENRATIO).divide(askratio))	);
		
		/** Scaliert das grid von showCards dass die icons diesele grösse haben wie die icons im field
		 * @CodeAutor: Alex */
		showCards.getGrid().scaleXProperty().bind(field.scaleXProperty());
		showCards.getGrid().scaleYProperty().bind(field.scaleYProperty());
		
		/** Positioniert showCards 
		 * @CodeAutor: Alex */
		showCards.translateXProperty().bind(this.widthProperty().subtract(showCards.getbackground().fitWidthProperty()).divide(-2));
		showCards.maxWidthProperty().bind(showCards.getbackground().fitWidthProperty());
		
		/** Positioniert lobby nach oben rechts ist dann einfacher zum repositioniern und scalieren
		 * @CodeAutor: Alex */
        StackPane.setAlignment(lobby, Pos.TOP_LEFT);
        StackPane.setMargin(lobby, new Insets(0,0,0,0));
        
		/** lobby ist an der stelle vom spielfeld und ist genauso gross kann also genauso scaliert werden 
		 * @CodeAutor: Alex */
		//root.setAlignment(lobby, Pos.TOP_CENTER);
		lobby.scaleXProperty().bind(field.scaleXProperty());
		lobby.scaleYProperty().bind(field.scaleYProperty());	
		
		//lobby.translateXProperty().bind(this.widthProperty().subtract(arg0).divide(-2));
		lobby.translateXProperty().bind(field.translateXProperty());
		lobby.translateYProperty().bind(field.translateYProperty());
		
		/** Beschränkt die grösee der Tappane. aus den platz neben dem field. muss gemacht werden da alles außerhalb abgeschnitten wird 
		 * @CodeAutor: Alex */
		DoubleProperty test = new SimpleDoubleProperty(CELL_SIZE* (HEIGHT  +FIELD_OFFSET +FIELD_OFFSET));
		tp.maxHeightProperty().bind(test);
		tp.minHeightProperty().bind(test);
		tp.maxWidthProperty().bind(Bindings
				.when( this.widthProperty().multiply(GOLDENRATIO).lessThan(this.heightProperty().multiply(FIELDRATIO)) )
				.then( this.widthProperty().multiply(1-GOLDENRATIO).divide(tp.scaleXProperty()))
				.otherwise( this.widthProperty().subtract(this.heightProperty().multiply(FIELDRATIO)).divide(tp.scaleXProperty()) )		);
		tp.minWidthProperty().bind(Bindings
				.when( this.widthProperty().multiply(GOLDENRATIO).lessThan(this.heightProperty().multiply(FIELDRATIO)) )
				.then( this.widthProperty().multiply(1-GOLDENRATIO).divide(tp.scaleXProperty()))
				.otherwise( this.widthProperty().subtract(this.heightProperty().multiply(FIELDRATIO)).divide(tp.scaleXProperty()) )		);
		
    	/** Bewegt die TP Block rechts neben das Field
		 * @CodeAutor: Alex */
		StackPane.setAlignment(tp, Pos.TOP_LEFT);
        tp.translateXProperty().bind(Bindings
				.when(this.widthProperty().multiply(GOLDENRATIO).lessThan(this.heightProperty().multiply(FIELDRATIO)))
				.then(this.widthProperty().multiply(GOLDENRATIO).add(tp.widthProperty().subtract(tp.widthProperty().multiply(tp.scaleXProperty())).divide(-2) 	))
				.otherwise(this.heightProperty().multiply(FIELDRATIO).add(tp.widthProperty().subtract(tp.widthProperty().multiply(tp.scaleXProperty())).divide(-2) 	))	);
        tp.translateYProperty().bind(tp.heightProperty().subtract(tp.heightProperty().multiply(tp.scaleYProperty())).divide(-2) 	);
        
		/** scaliert die Tappane 
		 * @CodeAutor: Alex */
        tp.scaleYProperty().bind(this.heightProperty().divide(CELL_SIZE*(HEIGHT  +FIELD_OFFSET +FIELD_OFFSET)));
        tp.scaleXProperty().bind(tp.scaleYProperty());
        
		/** sorgt dafür das der chat nicht scaliert wird und damit lesbar bleibt bis Tim ihn fertig styled (er hat angegeben, dass es  bis zum 20.07 fertig ist). sollte dann aber ausgeschaltet werden
		 * @CodeAutor: Alex */
//        tp.getgridChat().scaleXProperty().bind(Bindings.divide(1, tp.scaleXProperty()));
//        tp.getgridChat().scaleYProperty().bind(Bindings.divide(1, tp.scaleXProperty()));
//        tp.getgridChat().translateXProperty().bind(tp.getgridChat().widthProperty().multiply(tp.getgridChat().scaleXProperty()).subtract(tp.getgridChat().widthProperty()).divide(2));
//        tp.getgridChat().translateYProperty().bind(tp.getgridChat().heightProperty().multiply(tp.getgridChat().scaleXProperty()).subtract(tp.getgridChat().heightProperty()).divide(2));
        
        rainbow.getrect().widthProperty().bind(this.widthProperty());
        rainbow.getrect().heightProperty().bind(this.heightProperty());
        
	}
	
	/** Getter für die insets 
	 * @CodeAutor: Alex */
	public LobbyScreen getLobby(){
		return lobby;
	}
	public GameStage getStage(){
		return field;
	}
	public TabMenu getTabMenu(){
		return tp;
	}
		
	public Ask getask() {
		return ask;
	}
	
	public GameStage getfield() {
		return field;
	}
	
	public Ask getAsk() {
		return ask;
	}
	
	public ShowCards getShowCards() {
		return showCards;
	}
	
	public Rainbow getRainbow() {
		return rainbow;
	}
	/** Läst einen zwischen den einzellnen insets der gamescreen wechslen alo Game Lobby und Ask
	 * @CodeAutor: Alex, Markus */
	public void toGame(){
		if(root.getChildren().contains(lobby)) {
			tp.getTabs().add(0, tp.getNotes());
			tp.getSelectionModel().select(tp.getNotes());
			//.add doesnt work so with apsolut values until you have more than one switchable element its no problem
			tp.getTabcounter().set(3);
			//System.out.println(tp.getTabcounter().getValue());
		}
		root.getChildren().clear();
		root.getChildren().addAll(background, tp, field, rainbow);
	}
	public void toLobby(){
		root.getChildren().clear();
		tp.getTabs().remove(tp.getNotes());
		//.add doesnt work so with apsolut values until you have more than one switchable element its no problem
		tp.getTabcounter().set(2);
				
		root.getChildren().addAll(background,lobby, tp);
	}
	
	public void toAsk(){
		root.getChildren().clear();
		root.getChildren().addAll(background, ask, tp, rainbow);
	}
	
	public void toShow() {
		root.getChildren().clear();
		root.getChildren().addAll(background, tp, showCards, rainbow);	
		
	}
	
	/** erzeugt Snapshot für Ask und Showkards hintergründe
	 * @CodeAutor: Alex, Markus */
	public void setBackground(WritableImage snapshot) {
		ask.getbackground().setImage(snapshot);
		ask.getbackground().setViewport(new Rectangle2D(0 ,0 , super.getWidth()*GOLDENRATIO,super.getHeight()));	

		showCards.getbackground().setImage(snapshot);
		showCards.getbackground().setViewport(new Rectangle2D(0 ,0 , super.getWidth()*GOLDENRATIO,super.getHeight()));
	}

	
		
}
