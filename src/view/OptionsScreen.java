package view;

import static model.Constants.CARD_HEIGHT;
import static model.Constants.CARD_WIDTH;
import static model.Constants.CELL_SIZE;
import static model.Constants.FIELDRATIO;
import static model.Constants.FIELD_OFFSET;
import static model.Constants.GOLDENRATIO;
import static model.Constants.HEIGHT;
import static model.Constants.STANDARTCSS;
import static model.Constants.WIDTH;
import static view.ImageLoader.getImageLoader;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Optionsmenu, von Menuscreen erreichbar. Sound kann man ändern
 * @codeauthor Tim, An
 * @author anikiNT
 *
 */
public class OptionsScreen extends Scene {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(OptionsScreen.class);

	private StackPane root;
	private GridPane grid = new GridPane();
	private ImageView background = new ImageView(getImageLoader().getImage("background"));
	private CheckBox cbSound = new CheckBox();
	private Label lblSound = new Label("Sound:");
	private Label lblVolume = new Label("Volume:");
	private Slider slider = new Slider();
	private Button btnBack = new Button("Back");
	private Label volumeValue = new Label();
	public ToggleButton btnSound = new ToggleButton();
	private Rectangle2D SoundOn;
	private Rectangle2D SoundOff;
	private Rectangle2D SoundOnWhite;
	private Rectangle2D SoundOffWhite;
	
	public OptionsScreen() {
		super(new StackPane(), Color.BLACK);
		this.root = (StackPane)this.getRoot();
		this.getStylesheets().add(STANDARTCSS);
	
		
		
		/** Hintergrund wird erzeugt und dynamisch an Hintergrundfüllend aber Proportional sclariert 
    	 * @CodeAutor: Alex */
		//hintergrund wird textur geladen und 50% opacity gesetzt hintergrund ist #191919
		root.getStyleClass().add("screen");
		double bgratio = background.getImage().getWidth()/background.getImage().getHeight();
		background.scaleXProperty().bind(Bindings
				.when(this.widthProperty().greaterThan(this.heightProperty().multiply(bgratio)))
				.then(this.widthProperty())
				.otherwise(this.heightProperty().multiply(bgratio))				
				.divide(background.getImage().getWidth()));
		background.scaleYProperty().bind(Bindings
				.when(this.heightProperty().greaterThan(this.widthProperty().divide(bgratio)))
				.then(this.heightProperty())
				.otherwise(this.widthProperty().divide(bgratio))				
				.divide(background.getImage().getHeight() ));	
		root.getChildren().addAll(background);
		
		root.getChildren().add(grid);
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(CARD_WIDTH * 0.1);
		grid.setVgap(CARD_WIDTH * 0.1);
        grid.setPadding(new Insets(0, 0, 0, 0));
        
        SoundOnWhite = new Rectangle2D(0 ,0, CARD_WIDTH*1.315, CARD_HEIGHT*0.742);
        
       	SoundOn = new Rectangle2D(CARD_WIDTH*1.3155, 0, CARD_WIDTH*1.42, CARD_HEIGHT*0.742);
       	
        SoundOffWhite = new Rectangle2D(0, CARD_HEIGHT*1.48, CARD_WIDTH*1.31, CARD_HEIGHT*0.742);
       
       	SoundOff = new Rectangle2D(CARD_WIDTH*1.3155, CARD_HEIGHT*1.48, CARD_WIDTH*1.42, CARD_HEIGHT*0.742);
       	
        ImageView Sound = new ImageView(getImageLoader().getImage("sound"));
        
        
        btnSound.setGraphic(Sound);
        
 
        Sound.viewportProperty().bind(Bindings
        	      .when(btnSound.selectedProperty())
        	        .then(SoundOn)
        	        .otherwise(SoundOff)
        	    );
        
        Sound.scaleXProperty().bind(btnSound.widthProperty().divide(1250));
        Sound.scaleYProperty().bind(btnSound.heightProperty().divide(1250));
               
        slider.setMin(0);
        slider.setMax(100);
        slider.setValue(50);
        slider.setShowTickMarks(true);
        slider.setMinorTickCount(5);
        slider.setBlockIncrement(30);
        slider.valueProperty().addListener(new ChangeListener<Number>() {
            
			@Override
			public void changed(ObservableValue<? extends Number> arg0,
					Number arg1, Number arg2) {
            volumeValue.setText(String.format("%.2f", arg2));
				
			}
        });
        
        volumeValue.setText(String.valueOf(slider.getValue()));
        
        
        btnBack.setPickOnBounds(false);
        cbSound.setPickOnBounds(false);
        
        for (int i = 0; i < 8; i++) {
            ColumnConstraints column = new ColumnConstraints(CELL_SIZE*1.5);
            grid.getColumnConstraints().add(column);}
        
        grid.add(lblSound, 1, 0);
        grid.add(btnSound, 3, 0);
        grid.add(lblVolume, 1, 1);
        grid.add(slider, 2, 1, 3,1);
        grid.add(volumeValue, 5, 1,2,1);
        grid.add(btnBack, 1, 5);
        GridPane.setHalignment(btnSound, HPos.CENTER);

		/**
		 * Dynmaisches Scaleiren wie Field proportional zur Bidlschirmgrösse damit die schriff überall gleich ist
		 * @CodeAutor: Alex
		 */
        grid.scaleXProperty().bind(
				Bindings.when(this.widthProperty().lessThan(this.heightProperty().multiply(FIELDRATIO)))
						.then(this.widthProperty())
						.otherwise(this.heightProperty().multiply(FIELDRATIO))
						.divide(CELL_SIZE*(WIDTH +FIELD_OFFSET +FIELD_OFFSET)));
		grid.scaleYProperty().bind(
				Bindings.when(
						this.heightProperty().lessThan(this.widthProperty().divide(FIELDRATIO)))
						.then(this.heightProperty())
						.otherwise(this.widthProperty().divide(FIELDRATIO))
						.divide(CELL_SIZE*(HEIGHT +FIELD_OFFSET +FIELD_OFFSET)));
		
		grid.translateXProperty().bind(this.widthProperty().multiply(1-GOLDENRATIO-0.5));

	}
	public ToggleButton getSoundButton(){
		return btnSound;
	}
	public Button getBackToMenu(){
		return btnBack;
		}
}
