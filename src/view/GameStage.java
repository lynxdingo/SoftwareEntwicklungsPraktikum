package view;

import static model.Constants.CELL_SIZE;
import static model.Constants.DIE_SIZE;
import static model.Constants.FIELD_OFFSET;
import static view.ImageLoader.getImageLoader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.shape.Rectangle;
import model.ClientModel;
import model.Person;
import model.Position;
import model.RoomPos;
import model.Weapon;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import server.model.Card;
import server.model.Color;
import server.model.Field;
import server.model.Key;
import server.model.Room;
import server.model.Weapons;

/**
 * Spielfeld für Cluedo
 * @Codeauthor Marcus, An, Alex
 * @author anikiNT
 *
 */
public class GameStage extends Group{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(GameStage.class);
	
	/** Graficen werden erzeugt
	 * @CodeAutor: Alex, Markus	 */
	private ClientModel model;
	private Map<Card, ImageView> objects = new HashMap<Card, ImageView>();
	private Map<Key, Rectangle> highlightFields = new HashMap<Key, Rectangle>();
	private Map<Room, ImageView> roomHighlights = new HashMap<Room, ImageView>();
	private Map<Room, RoomPos> roomPositions = new HashMap<Room, RoomPos>();
	
	public GameStage (ClientModel model){
		this.model = model;
		initialize();
	}
	public Map<Key, Rectangle> getFields(){
		return highlightFields;
	}
	public Map<Room, ImageView> getRooms(){
		return roomHighlights;
	}
	private void initialize(){
		
		//Background hinzufügen
		ImageView background = new ImageView(getImageLoader().getImage("map"));
		this.setManaged(false);
		this.autoSizeChildrenProperty().set(false);
		this.getChildren().add(background);
		//Initialize Highlight Fields
		for(Entry<Key, server.model.Field> entry : model.getGameField().getFields().entrySet()){
			//Doors should not be highlighted
			if(entry.getValue().getRoom()!=null)
				continue;
			Rectangle r = new Rectangle(CELL_SIZE, CELL_SIZE);
			r.setX((entry.getKey().getX()+FIELD_OFFSET)*CELL_SIZE);
			r.setY((entry.getKey().getY()+FIELD_OFFSET)*CELL_SIZE);

			r.getStyleClass().add("light");
			r.setBlendMode(BlendMode.OVERLAY);
			
			//wird von maus ignoriert
			//r.setDisable(true);
			
			highlightFields.put(entry.getKey(), r);
			this.getChildren().add(r);
		}

		//Room Highlights
		String json = "";
		String line = "";
		try {
			InputStreamReader stream = new InputStreamReader(this.getClass().getResourceAsStream("/resources/Rooms.json"));
			
			BufferedReader br = new BufferedReader(stream);
			while((line = br.readLine())!=null){
				json = json + line;
	
			}
			br.close();
			stream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("initialize()", e); //$NON-NLS-1$
		}
		JSONObject rooms = new JSONObject(json);
		JSONArray arr = rooms.getJSONArray("Rooms");
		for(int i = 0; !arr.isNull(i); i++){
			JSONObject roomObj = arr.getJSONObject(i);
			Room room = Room.getRoom(roomObj.getString("name"));
			RoomPos roomPos = new RoomPos();
			roomPos.setRoom(room);
			for(int j = 1; !roomObj.isNull("px"+j); j++){
				Position<Color> pos = new Position<Color>(roomObj.getDouble("px"+j), roomObj.getDouble("py"+j));
				roomPos.getPersonPositions().add(pos);
				/*Rectangle rec = new Rectangle(CELL_SIZE, CELL_SIZE);
				rec.setX(CELL_SIZE*(roomObj.getDouble("px"+j)+FIELD_OFFSET));
				rec.setY(CELL_SIZE*(roomObj.getDouble("py"+j)+FIELD_OFFSET));
				rec.setFill(javafx.scene.paint.Color.GREEN);
				this.getChildren().add(rec);*/
			}
			
			for(int j = 1; !roomObj.isNull("wx"+j); j++){
				Position<Weapons> pos = new Position<Weapons>(roomObj.getDouble("wx"+j), roomObj.getDouble("wy"+j));
				roomPos.getWeaponPositions().add(pos);
				/*Rectangle rec = new Rectangle(CELL_SIZE, CELL_SIZE);
				rec.setX(CELL_SIZE*(roomObj.getDouble("wx"+j)+FIELD_OFFSET));
				rec.setY(CELL_SIZE*(roomObj.getDouble("wy"+j)+FIELD_OFFSET));
				rec.setFill(javafx.scene.paint.Color.RED);
				this.getChildren().add(rec);*/
				
			}
			roomPositions.put(room, roomPos);
			ImageView view = new ImageView(getImageLoader().getImage("roomHighlight"));
			view.getStyleClass().add("room");
			view.setViewport(new Rectangle2D(CELL_SIZE*(roomObj.getInt("x")), CELL_SIZE*(roomObj.getInt("y")), CELL_SIZE*roomObj.getDouble("w"), CELL_SIZE*roomObj.getDouble("h")));
			view.setBlendMode(BlendMode.OVERLAY);
			view.setX(CELL_SIZE*roomObj.getInt("x"));
			view.setY(CELL_SIZE*roomObj.getInt("y"));
			setTooltip(view, room.toString().toUpperCase());

			roomHighlights.put(room, view);
			this.getChildren().add(view);	
		}
		
		/**Die einzelnen layer des spielfeldes werden geladen und eingestellt 
		 * @CodeAutor: Alex, Markus	 */
		ImageView walls = new ImageView(getImageLoader().getImage("walls"));
		this.getChildren().add(walls);
		
		//Initialize Persons
		for(Color c : Color.values()){
			ImageView object = new ImageView(getImageLoader().getImage("playerIcons"));
			object.setViewport(new Rectangle2D(0, c.getPosition()*CELL_SIZE, CELL_SIZE, CELL_SIZE));
			object.xProperty().bind(model.getGame().getPersons().get(c).xProperty().add(FIELD_OFFSET).multiply(CELL_SIZE));
			object.yProperty().bind(model.getGame().getPersons().get(c).yProperty().add(FIELD_OFFSET).multiply(CELL_SIZE));
			object.getStyleClass().add("shadow");
			model.getGame().getPersons().get(c).keyProperty().addListener(k->positionChanged(c));
			setTooltip(object, c.toString().toLowerCase());
			objects.put(c, object);
			this.getChildren().add(object);		}
		
		//Initialize Weapons
		for(Weapons w : Weapons.values()){
			ImageView object = new ImageView(getImageLoader().getImage("weaponIcons"));
			object.setViewport(new Rectangle2D(0, w.getPosition()*CELL_SIZE, CELL_SIZE, CELL_SIZE));
			object.xProperty().bind(model.getGame().getWeapons().get(w).xProperty().add(FIELD_OFFSET).multiply(CELL_SIZE));
			object.yProperty().bind(model.getGame().getWeapons().get(w).yProperty().add(FIELD_OFFSET).multiply(CELL_SIZE));
			object.getStyleClass().add("shadow");
			model.getGame().getWeapons().get(w).keyProperty().addListener(k->positionChanged(w));
			model.getGame().getWeapons().get(w).setPosition(new Key(11, 8));
			setTooltip(object, w.getText().toLowerCase());
			objects.put(w, object);
			this.getChildren().add(object); 	
			}
		
		//Erste effect ebene für Field
		ImageView lightwindow = new ImageView(getImageLoader().getImage("light1"));
		lightwindow.setBlendMode(BlendMode.HARD_LIGHT);
		//sorgt dafür das mouseeventes diese element ignorieren
		lightwindow.setDisable(true);
		this.getChildren().add(lightwindow);
				
		//zweite effect ebene für field
		ImageView light = new ImageView(getImageLoader().getImage("light2"));
		light.setBlendMode(BlendMode.HARD_LIGHT);
		light.setDisable(true);  
		this.getChildren().add(light);
		
		//_______________MiddButton teil soll gelöst werden______________________________________		
		
		
		//TODO setDice(false);
		
		//solter nacher durch was einfacheres erschtzt werden
		GridPane pane = new GridPane();
        pane.setAlignment(Pos.TOP_LEFT);
        pane.setPadding(new Insets(0, 0, 0, 0));
        ColumnConstraints column = new ColumnConstraints(CELL_SIZE*2);
        pane.getColumnConstraints().add(column);        
        RowConstraints Row = new RowConstraints(CELL_SIZE);
        pane.getRowConstraints().add(Row);
		// Linien zum bessern erkennen der Grids sollte im Spiel ausgeschaltet sein
		//pane.setGridLinesVisible(true);
		
		pane.setTranslateX(CELL_SIZE*10.75);
		pane.setTranslateY(CELL_SIZE*8);
        //_______________MiddButton enden______________________________________	
        
        
        this.getChildren().add(pane);
		
		
		
		
	}
	public void setSuspectButton(boolean active){
		if(active){
			//Glow on Figures and Weapons
			for(ImageView view : objects.values()){
				view.getStyleClass().clear();
				view.getStyleClass().add("objectClick");
			}
		}
		
		if(!active){
			for(ImageView view : objects.values()){
				view.getStyleClass().clear();
				view.getStyleClass().add("shadow");
			}
		}
		
	}
	
	
	private void positionChanged(Weapons w) {
		if (logger.isDebugEnabled()) {
			logger.debug("positionChanged(Weapons) - Weaponpos changed"); //$NON-NLS-1$
		}
		Weapon weapon = model.getGame().getWeapons().get(w);
		Room room = model.getGameField().getFields().get(weapon.getPosition()).getRoom();
		if(weapon.getCurrentRoom()!=null){
			weapon.getCurrentRoom().remove(w);
			weapon.setCurrentRoom(null);
		}
		if(room == null){
			//TODO DeFuck
			weapon.setPosition(weapon.getPosition().getX()*1.0, weapon.getPosition().getY()*1.0);
		}
		int i = 0;
		RoomPos roomPos = roomPositions.get(room);
		Weapon insert = weapon;
		Weapon next = null;
		boolean save = false;
		do{
			if(roomPos.getWeaponPositions().size()<=i){
				if(save)
					break;
				roomPos = roomPositions.get(Room.POOL);
				i = 0;
				save = true;
			}
			Position<Weapons> pos = roomPos.getWeaponPositions().get(i);
			next = null;
			if(!pos.isFree()){
				next = model.getGame().getWeapons().get(pos.getObject());
			}
			pos.setObject(insert.getWeapon());
			insert.setPosition(pos.getX(), pos.getY());
			insert.setCurrentRoom(roomPos);
			if (logger.isDebugEnabled()) {
				logger.debug("positionChanged(Weapons) - " + insert.getWeapon().toString() + " is now in the " + roomPos.getRoom() + " at Position " + i); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			insert = next;
			i++;
		}while(next!=null);
		
	}
	private void positionChanged(Color c) {
		if (logger.isDebugEnabled()) {
			logger.debug("positionChanged(Color) - Position geändert"); //$NON-NLS-1$
		}
		Person person = model.getGame().getPersons().get(c);
		Room room = model.getGameField().getFields().get(person.getPosition()).getRoom();
		if(person.getCurrentRoom()!=null){
			person.getCurrentRoom().remove(c);
			person.setCurrentRoom(null);
		}
		if(room == null){
			person.setPosition(person.getPosition().getX() * 1.0, person.getPosition().getY() * 1.0);
			return;
		}
		int i = 0;
		RoomPos roomPos = roomPositions.get(room);
		Person insert = person;
		Person next = null;
		boolean save = false;
		do{
			if(roomPos.getPersonPositions().size()<=i){
				if(save)
					break;
				roomPos = roomPositions.get(Room.POOL);
				i = 0;
				save = true;
			}
			Position<Color> pos = roomPos.getPersonPositions().get(i);
			next = null;
			if(!pos.isFree()){
				next = model.getGame().getPersons().get(pos.getObject());
			}
			pos.setObject(insert.getColor());
			insert.setPosition(pos.getX(), pos.getY());
			insert.setCurrentRoom(roomPos);
			insert = next;
			i++;
		}while(next!=null);
		
	}
	public Map<Card, ImageView> getObjects(){
		return objects;
	}
	
	/*public void setShow(boolean active){
		die1.setDisable(!active);
		die1.setDisable(!active);
	}*/
	public void setFields(Set<Field> fieldsForRoll) {
		for(Field field : fieldsForRoll){
			if(field.getRoom()==null){
				highlightFields.get(field.getKey()).getStyleClass().remove("light");
				highlightFields.get(field.getKey()).getStyleClass().add("field");
			}else{
				roomHighlights.get(field.getRoom()).getStyleClass().remove("field");
				roomHighlights.get(field.getRoom()).getStyleClass().add("roomaccesible");
			}
		}
		
	}
	public void removeHighlights() {
		for(Field field : model.getGame().getAccesibleFields()){
			if(field.getRoom()==null){
				highlightFields.get(field.getKey()).getStyleClass().remove("field");
				highlightFields.get(field.getKey()).getStyleClass().add("light");
			}
		}
		for(ImageView room : roomHighlights.values()){
			room.getStyleClass().clear();
			room.getStyleClass().add("room");
		}
		
	}
	public void setSecret(Room secret) {
		roomHighlights.get(secret).getStyleClass().clear();
		roomHighlights.get(secret).getStyleClass().add("roomaccesible");
		
	}
	
	
	//Erstellt ein Tooltip für ein ImageView, mit festem Font
	public void setTooltip(ImageView iv, String text) {
		
		Tooltip t = new Tooltip(text);
		Tooltip.install(iv, t);
	}
	

	
}
