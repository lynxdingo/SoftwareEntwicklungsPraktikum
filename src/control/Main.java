package control;

import static model.Constants.FONT;
import static model.Constants.ICON;

import java.text.SimpleDateFormat;
import java.util.Date;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.ClientModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import view.ScreenBundler;
import code.Client;
import code.JSONHandler;

/** Main Startet das Spiel erstellt Model View und Severconect
 * @CodeAutor: Alex, An */

public class Main extends Application {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Main.class);
	
  @Override
  public void start(Stage primaryStage) {
	Font.loadFont(getClass().getResourceAsStream(FONT), 12);
	
	ClientModel model = new ClientModel();
	JSONHandler handler = new JSONHandler(model);
	Client client = new Client(handler, model);
	Thread t = new Thread(client);
	t.setDaemon(true);
	model.setClient(client);
	model.setHandler(handler);
	Presenter presenter = new Presenter(new ScreenBundler(model),primaryStage, model);
	model.setPresenter(presenter);
	t.start();


	/** Fenster startet im maximierten modus wenn man es minimiert wird es 1/4 so groß wie der Bildschirm und bewegt sich in de mitte des Bildschirms
	 * @CodeAutor: Alex */
    primaryStage.setMaximized(true);
    
    Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
    primaryStage.setWidth(primScreenBounds.getWidth()/2);
    primaryStage.setHeight(primScreenBounds.getWidth()/(3.122));
    primaryStage.setX(primScreenBounds.getWidth()/2 - primaryStage.getWidth()/2);
    primaryStage.setY(primScreenBounds.getHeight()/2 - primaryStage.getHeight()/2);
    
	/** Setzt das Icon und den Namen für das Fenster und zeigt es
	 * @CodeAutor: Alex */
    primaryStage.getIcons().add(new Image(ICON));
    primaryStage.setTitle("Offene Onthologen CLUEDO");
    primaryStage.show();
    
    primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
        @Override
        public void handle(WindowEvent t) {
        	if(model.getClient().isConnected()){ 
	        	JSONObject disco = new JSONObject();
	        	disco.put("type", "disconnect");
	        	model.getClient().sendJSON(disco);
        	}
        	
            Platform.exit();
        }
    });

  }

	static{
	    SimpleDateFormat dateFormat = new SimpleDateFormat("D-HH-mm-ss");
	    System.setProperty("logFilename", "Client_"+dateFormat.format(new Date()));
	}
  public static void main(String[] args) {
	org.apache.logging.log4j.core.LoggerContext ctx =
			    (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
			ctx.reconfigure();
    launch(args);
  }
  
}
