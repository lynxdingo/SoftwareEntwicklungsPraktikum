package control;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

import model.ClientModel;
import model.GameStub;
import model.Nick;
import model.chat.Receive;
import model.chat.Send;
import model.chat.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import server.model.Color;
import view.TabMenu;
import code.JSONHelper;

/**
 * Logik für den Chat!
 * @Codeauthor: Angela
 * @author anikiNT
 *
 */
public class ChatPresenter {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(ChatPresenter.class);
	
	  private JSONHelper jhelper;
	  private TabMenu gui;
	  private ClientModel model;
	  private String errorMessage = "";
	  private String sender = "";
	  private String textToSend = "";
	  private int gameid = -1;
	  private String toUser = "";
	  private String startOfError = "Not send because of::  ";
	  private String[] chat_key = new String[]{"type","message","timestamp","sender","gameID","nick"};
	  private String[] comboBoxItems = new String[]{"Send to all!", "Send to game:", "Send to user: ","Send to your game."};
	  private String message = "";
	  private LocalDateTime timestamp;
	  private String user = "" ;
	  private JSONObject lastJObj = new JSONObject();

		
		public ChatPresenter(TabMenu g, ClientModel m, JSONHelper json) {
			
			gui = g;
			model = m;
			jhelper = json;
			setupLists();
			activateEvent();
				
		}
		
		/**
		 * put items in the combobox
		 */
		public void setupLists()
		{
				gui.getDropDownMenu().getItems().addAll(comboBoxItems );
		}
		
		/**
		 * handle events that are fired by the user
		 */
		public void activateEvent() {
			gui.getBtnSend().setOnAction(e -> btnSend_Click());
			gui.getDropDownMenu().setOnAction(e -> updateListView());
		}
		
		/**
		 * get the current time as a LocalDateTime
		 * @return the time as a string
		 */
		public String getTime()
		{
			return LocalDateTime.now().toString();
		}
		
		/**
		 * set the color in which the received message will be displayed
		 * by finding the color associated with the player or watcher
		 * @param s
		 * @return a string that contains the name of the color
		 */
		public String setColor(String s)
		{
			//Color colorOfWatcher = Color.BLACK;
			String color = "black";
			//System.out.println("Ist GamesList leer? " +  model.getGames().isEmpty());
			for ( Map.Entry<Integer, GameStub> entry : model.getGames().entrySet()) 
			{
			    Integer key = entry.getKey();
			    GameStub value = entry.getValue();
			     Color col =  value.getColorFromNickname(s);
			     if (col != null )
			     {
			    	 color = col.toString();
			     }
			}
			return color;
		}
		
		/**
		 * Check whether user entered a text for sending. 
		 * If entered message is greater than zero set
		 * the required parameters for a chat message
		 * otherwise set an error message
		 * @return true if the user is logged in and length of message is greater than zero
		 */
		public boolean prepMessage()
		{
			boolean isReady = false;
			textToSend = gui.getSendText().getText();
			int lengthOfText = textToSend.length();
			
			switch (lengthOfText) {
			case 0:
				errorMessage = "Your message is a required field.";
				break;
			default:
				if (!model.getNickname().isEmpty())
				{ // message can be sent
					 model.getSend().setSendTXT(textToSend);
					 sender = model.getNickname() ;
			         isReady = true;
				}
				else {
					errorMessage = "Probably not connected at all.";
				}
				break;
			}
			return isReady;
		}
		
		/**
		 * tell the send method where to send the message
		 * or not to send the entered text at all
		 * @return an int that is used by the send method
		 */
		public int getSendKey()
		{
			int code = -1;
			int index = -1;
			boolean isReady = prepMessage();
			int gameidOfThisGame = model.getGame().getGameID();
			String comboBoxItem = "";
			boolean isEmptyUserList = gui.getChatUsers().getSelectionModel().isEmpty();
			boolean isDropDownSelectedNull = isNullDropDownMenu();
			boolean isGamesEmpty = model.getGames().isEmpty();
			boolean isNicksEmpty = model.getNicks().isEmpty();
			
			if (!isDropDownSelectedNull) {
				comboBoxItem =  gui.getDropDownMenu().getValue().toString() ;
				index = getIndexOfComboBox(comboBoxItem);
			}
			
			switch(index)
			{
			case -1:
				// if user did not select an item in dropdown menu
				errorMessage = "Please select an item in dropdown menu.";
				break;
				
			case 0:
				//user selected: "send to all"
				if (isReady) {
					code = 0;
				}
				break;
				
			case 1:
				//user selected: "send to game:"
				if ( isReady && isGamesEmpty) {
					errorMessage = startOfError + "No games are taking place.";
				}
				if (isReady  && !isGamesEmpty)
				{
					boolean hasSelected = IsSelected();
					// send to gameid
					if (hasSelected ) {	
							gameid = Integer.valueOf(gui.getChatUsers().getSelectionModel().getSelectedItem().toString());
								if (gameid > -1) {	
								code = 1;
							}
					}
					if ( ! hasSelected ) {
						errorMessage = startOfError + "Please select a game id in the list.";
					}
				}
				break;
				
			case 2:
				//user selected: "send to user:"	
				if (isReady  && isNicksEmpty) {
					errorMessage = startOfError + "No users are logged in.";
				}
				if (isReady  && !isNicksEmpty)
				{
					//boolean selectedItemIsEmpty = gui.getChatUsers().getSelectionModel().getSelectedItem().toString().isEmpty();
					boolean hasSelected = IsSelected();
					// send to user
					if ( hasSelected) {
						code = 2;
					}
					if ( ! hasSelected ) {
						errorMessage = startOfError + "Please select a user to whom you want to send your message.";
					}
				}
				break;
				
			case 3:
				//user selected: "Send to your game."
				if (gameidOfThisGame == -1 && isReady) {
					errorMessage = startOfError + "You aren't currently involved in a game";
				}
				// when everything is ok:
				if (isReady && gameidOfThisGame > -1 ) {	
					//send to players of your game
					code = 3;
				}
				break;
			} // end switch
			return code;
		}
		
		
		/**
		 * send entered message or print out error message
		 */
		public void btnSend_Click()
		{	
			int key = getSendKey();
			switch (key)
			{
				case -1:
					// print out error message
					model.getSend().setSendMessage(errorMessage);
					errorMessage = "";
					break;
				case 0:
					//send to all
					model.getClient().sendJSON(jhelper.sendMessage(textToSend, getTime(), sender));
					//model.getSend().setSendMessage("Sending to all...");
					break;
				case 1:
					// send to gameid
					model.getClient().sendJSON(jhelper.sendMessage(textToSend ,getTime(), sender, gameid));
					//model.getSend().setSendMessage("Sending to all players of the game " + gameid + " ...");
					break;
				case 2:
					// send to user
					toUser = gui.getChatUsers().getSelectionModel().getSelectedItem().toString();
					model.getClient().sendJSON(jhelper.sendMessage(textToSend ,getTime(), sender, toUser));
					//model.getSend().setSendMessage("Sending to "+toUser+"...");
					break;
				case 3:
					//send to players of your game
					gameid = model.getGame().getGameID();
					model.getClient().sendJSON(jhelper.sendMessage(textToSend ,getTime(), sender, gameid));
					//model.getSend().setSendMessage("Sending to your game...");
					break;
			}
			
		}
		
		/**
		 * save objects of the received JSONObject and
		 * call the displayMessage()
		 * @param jObj
		 */
		public void prepDisplay(JSONObject jObj)
		{	
			if (jObj.toString().equals(lastJObj.toString())  ) {
				message ="";
			}
			else {
			if ( jObj.has(chat_key[1])) {
				message = jObj.getString(chat_key[1]);
			}
			if ( jObj.has(chat_key[2])) {
				timestamp = LocalDateTime.parse(jObj.getString(chat_key[2]));
				
				if (timestamp == null) {
					timestamp = LocalDateTime.parse(getTime());
				}
			}
			if ( jObj.has(chat_key[3])) {
				user = jObj.getString(chat_key[3]);
				
				if (user.isEmpty()) {
					user = "unknown";
				}
				
			}
			lastJObj = jObj;
			}
			
			displayMessage(message.length());
		}
		
		/**
		 * display received message 
		 * @param jObj
		 */
		public void displayMessage(int i) 
		{
			int lengthOfMessage = i;
			
			switch (lengthOfMessage)
			{
				case 0:
					// do not display message if length of message is zero
					break;
				default:
					// display message
					model.getRCV().setRcvTXT(timestamp.getHour()+":"+timestamp.getMinute()+ " :: "+ user +" :: " ,message, setColor(user));
					gui.getWebEngine().loadContent(model.getRCV().getRcvTXT());
					break;
			}
		}
		
		/**
		 * 
		 * @return true if user selected an item in the list
		 */
		public boolean IsSelected()
		{
			return gui.getChatUsers().getSelectionModel().getSelectedItem() != null;
		}
		
	
	/**
	 * 	
	 * @return true if no item in combobox was selected 
	 */
	public boolean isNullDropDownMenu()
	{
			return (gui.getDropDownMenu().getValue() == null );
	}
	
	/**
	 * 
	 * @param item
	 * @return index of selected combobox item
	 */
	public int getIndexOfComboBox(String item)
	{	
		for (int i = 0;i < comboBoxItems.length; i++ )
		{
			if (item.equals(comboBoxItems[i])) {
				return i;
			}
		}
		return -1;
	}
	
	
	/**
	 * update listview or print out notification when selecting an item
	 * in the combobox
	 */
	public void updateListView()
	{		
			boolean isEmptyUserList = gui.getChatUsers().getSelectionModel().isEmpty();
			boolean isGamesEmpty = model.getGames().isEmpty();
			boolean isNicksEmpty = model.getNicks().isEmpty();
			String selectedItem = gui.getDropDownMenu().getValue().toString();
			int index = getIndexOfComboBox(selectedItem);

			switch(index) {
			
			case 0:
				// clear label for notification
				model.getSend().setSendMessage("");
				break;
			
			case 1:
				// print out notification if games list is empty
				if (isGamesEmpty) {
					gui.getChatUsers().getItems().clear();
					model.getSend().setSendMessage("List is empty.");
				}
				// print out games list if not empty
				if (!isGamesEmpty) {
					gui.getChatUsers().getItems().clear();
					for ( Map.Entry<Integer, GameStub> entry : model.getGames().entrySet()) 
					{
					    Integer key = entry.getKey();
					    GameStub value = entry.getValue();
					    int gameid = value.getGameID();
					    if (gameid > -1) {
					    	String game = Integer.toString(gameid);
						    gui.getChatUsers().getItems().add(game);
					    }
					}
					model.getSend().setSendMessage("");
				}
				break;
			
			case 2:
				// print out notification if user list is empty
				if (isNicksEmpty) {
					gui.getChatUsers().getItems().clear();
					model.getSend().setSendMessage("List is empty.");
				}
				// print out user list if not empty
				if (!isNicksEmpty) {
					//String emptyNick = "\"\"";
					gui.getChatUsers().getItems().clear();
					//gui.getChatUsers().getItems().addAll(model.getNicks());
					for ( Map.Entry<String, Nick> entry : model.getNicks().entrySet()) 
					{
					    String key = entry.getKey();
					    Nick value = entry.getValue();
					    String nick = value.getNick();
					    //if (nick != null && !nick.equals(emptyNick) &&nick.length() > 2 )
					    if (nick != null  &&nick.length() > 2 )
					    {
						    gui.getChatUsers().getItems().add(nick);
					    }
					}
					model.getSend().setSendMessage("");
				}
				break;
			
			case 3:
				// clear label for notification
				model.getSend().setSendMessage("");
				break;
				
			default:
				model.getSend().setSendMessage("Probably no item selected?!");
				break;
				
			}
		}
}
