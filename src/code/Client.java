package code;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket; 
import java.net.UnknownHostException;

import model.ClientModel;
import model.Server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 * @Codeauthor: Marcus
 * @author anikiNT
 *
 */
public class Client implements Runnable {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Client.class);

	private JSONHandler handler;
	private Socket socket;
	private ClientModel model;
	private OutputStreamWriter out;
	private BufferedReader in;
	private Thread thread;
	public Client(JSONHandler handler, ClientModel model ){
		this.handler = handler;
		this.model = model;
		
	}
	
	

	@Override
	public void run() {
		
		//Handshake aufbauen
		UDPHandshakeClient hand = new UDPHandshakeClient(model);
		Thread t = new Thread(hand);
		t.setDaemon(true);
		t.start();
		
		socket = new Socket();
		
	}
	
	
	/**
	 * Nimmt einen Server(IP, Port) als Parameter.
	 * Falls der Socket des Servers noch offen ist, verbindet sich der Client mit dem Server, indem er
	 * eine vollduplexe Verbindung öffnet. Client erstellt zu dem einen Receiver, der den Input übersetzen tut.
	 *
	 * @param s
	 */
	public void connectToServer(Server s){

		if (logger.isDebugEnabled()) {
			logger.debug("connectToServer(Server) - Connect to Server: " + s.getAddress().getHostAddress() + ":" + s.getPort() + " from: " + s.getGroup()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		try {
			if(socket.isClosed()){
				socket.close();
				in.close();
				out.close();
			}
			socket = new Socket(s.getAddress(),s.getPort());
			out = new OutputStreamWriter(socket.getOutputStream(),"UTF-8" );
			in = new BufferedReader (new InputStreamReader(socket.getInputStream(), "UTF-8"));
			Receiver recv = new Receiver(handler, in);
			thread = new Thread(recv);
			thread.setDaemon(true);
			thread.start();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("connectToServer(Server)", e); //$NON-NLS-1$
		}
	}
	
	/**
	 * Falls Socket offen ist, wird ein JSON-Object in den Output-Stream hinzugefügt.
	 * Dieser wird anschließend geflushed.
	 * @param obj
	 */
	public void sendJSON(JSONObject obj){
		if(socket.isClosed())
			return;
		try {
			out.write(obj.toString() + "\n");
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("sendJSON(JSONObject)", e); //$NON-NLS-1$
		}
	}
	
	/**
	 * Ruft die Methode connectToServer(Server) auf, fuer mehr Infos, lesen sie
	 * die Kommentare zu dieser Methode.
	 * @param ip
	 * @param port
	 * @param group
	 */
	public void connectToServer(String ip, int port, String group){
		try {
			connectToServer(new Server(InetAddress.getByName(ip),port, group));
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.error("connectToServer(String, int, String)", e); //$NON-NLS-1$
		}
	}
	
	/**
	 * Gibt zurück, ob der Socket offen oder geschlossen ist.
	 * @return
	 */
	public boolean isConnected(){
		return socket.isConnected();
	}


}
