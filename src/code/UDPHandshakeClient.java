package code;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;

import model.ClientModel;
import model.Server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 * Führt ein "Verbindungsaufbau" zwischen Server und Client aus über UDP.
 * Ist das nicht eine TCP-Verbindung?
 * @Codeauthor: Marcus
 * @author anikiNT
 *
 */
public class UDPHandshakeClient implements Runnable{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(UDPHandshakeClient.class);

	ClientModel model;
	UDPHandshakeClient(ClientModel model){
		if (logger.isDebugEnabled()) {
			logger.debug("UDPHandshakeClient(ClientModel) - Hola"); //$NON-NLS-1$
		}
		this.model = model;
		model.getClass();
		this.model.getClass();
	}
	@Override
	public void run() {
		JSONObject broad = new JSONObject();
		broad.put("type", "udp client");
		broad.put("group", "offene onthologen");
		byte[] raw = broad.toString().getBytes();
		try(DatagramSocket udpSocket = new DatagramSocket(null)){;
			udpSocket.setReuseAddress(true);
			udpSocket.bind(new InetSocketAddress(30303));
			
			InetAddress ia = InetAddress.getByName("255.255.255.255");
			DatagramPacket packet = new DatagramPacket(raw, raw.length, ia, 30303);
			udpSocket.setBroadcast(true);
			udpSocket.send(packet);
			//Receive Request
			udpSocket.setBroadcast(false);
			while(true){
				packet = new DatagramPacket(new byte[1024], 1024);
				if (logger.isDebugEnabled()) {
					logger.debug("run() - Warten"); //$NON-NLS-1$
				}
				udpSocket.receive(packet);
				//System.out.println("Mail");
				
				InetAddress serverAddress = packet.getAddress();
				//int port = packet.getPort();	//Redundant, should be 30303, to be secure
				byte[] data = packet.getData();
				
				JSONObject recv = new JSONObject(new String(data));
				if (logger.isDebugEnabled()) {
					logger.debug("run() - Mail: " + recv); //$NON-NLS-1$
				}
				if(recv.isNull("type")||recv.isNull("group")||recv.isNull("tcp port")){
					continue;
				}
				
				if(!recv.getString("type").equalsIgnoreCase("udp server")){
					continue;
				}
				int port = recv.getInt("tcp port");
				//Message is valid
				//New Server was found
				for(Server server : model.getServers()){
					if(server.getAddress().getHostAddress().equals(serverAddress.getHostAddress())&&server.getPort()==port){
						if (logger.isDebugEnabled()) {
							logger.debug("run() - This server is already in the list"); //$NON-NLS-1$
						}
						return;
					}
				}
				model.addServer(new Server(serverAddress, port, recv.getString("group")));
				if (logger.isDebugEnabled()) {
					logger.debug("run() - Nice a new Server appeared"); //$NON-NLS-1$
				}
				data = null;
					
				
			}
		} catch (IOException e) {
			logger.error("run()", e); //$NON-NLS-1$
			if (logger.isDebugEnabled()) {
				logger.debug("run() - The UDP Port is already in use"); //$NON-NLS-1$
			}
		}
		
	}

}
