package code;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import server.model.Card;
import server.model.Color;
import server.model.Key;
import server.model.Suspicion;


/**
 * Diese Klasse unterstützt den Presenter JSON-Pakete zu verpacken,
 * hilft gegen redundanten Code im Presenter.
 * @Codeauthor: An, Marcus
 * @author anikiNT
 *
 */
public class JSONHelper {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(JSONHelper.class);
	
	/**
	 * Verpackt ein joinGame Paket.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject joinGame(int id, Color color) {
		JSONObject ret = new JSONObject();
		ret.put("type", "join game");
		ret.put("gameID", id);
		ret.put("color", color.toString());
		return ret;
	}
	
	/**
	 * Verpackt ein createGame Paket.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject createGame(Color color){
		JSONObject ret = new JSONObject();
		ret.put("type", "create game");
		ret.put("color", color.toString());
		return ret;
	}
	
	/**
	 * Verpackt ein joinServer Paket.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject joinServer(String nick, String group, String version, String[] expansions){
		JSONObject ret = new JSONObject();
		ret.put("type", "login");
		ret.put("nick", nick);
		ret.put("group", group);
		ret.put("version", version);
		JSONArray exp = new JSONArray();
		for(String ex : expansions){
			exp.put(ex);
		}
		ret.put("expansions", exp);
		return ret;
	}
	
	
	/**
	 * Verpackt ein sendMessage Paket für die Lobby.
	 * 
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject sendMessage(String message,String timestamp,  String sender) {
		JSONObject ret = new JSONObject();
		ret.put("type", "chat");
		ret.put("message", message);
		ret.put("timestamp", timestamp);
		ret.put("sender", sender);
		return ret;
	}
	
	/**
	 * Verpackt ein sendMessage Paket für InGame.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject sendMessage(String message,String timestamp, String sender, int gameID) {
		JSONObject ret = new JSONObject();
		ret.put("type", "chat");
		ret.put("message", message);
		ret.put("timestamp", timestamp);
		ret.put("sender", sender);
		ret.put("gameID", gameID);
		return ret;
	}
	
	/**
	 * Verpackt ein sendMessage Paket an einem bestimmten User.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject sendMessage(String message,String timestamp, String sender, String user) {
		JSONObject ret = new JSONObject();
		ret.put("type", "chat");
		ret.put("message", message);
		ret.put("timestamp", timestamp);
		ret.put("sender", sender);
		ret.put("nick", user);
		return ret;
	}
	//_________
	/**
	 * Verpackt ein watchGame Paket.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject watchGame(int gameID) {
		JSONObject ret = new JSONObject();
		ret.put("type", "watch game");
		ret.put("gameID", gameID);
		return ret;
	}
	
	/**
	 * Verpackt ein leaveGame Paket.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject leaveGame(int gameID) {
		JSONObject ret = new JSONObject();
		ret.put("type", "leave game");
		ret.put("gameID", gameID);
		return ret;
	}
	
	/**
	 * Verpackt ein move Paket.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject move(int gameID, Key key){
		JSONObject ret = new JSONObject();
		ret.put("type", "move");
		ret.put("gameID", gameID);
		JSONObject field = new JSONObject();
		field.put("x", key.getX());
		field.put("y", key.getY());
		ret.put("field", field);
		return ret;
	}
	
	
	/**
	 * Verpackt ein startGame Paket.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject startGame(int gameID) {
		JSONObject ret = new JSONObject();
		ret.put("type", "start game");
		ret.put("gameID", gameID);
		return ret;
	}
	/**
	 * Verpackt ein rollDice Paket.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject rollDice(int gameID) {
		JSONObject ret = new JSONObject();
		ret.put("type", "roll dice");
		ret.put("gameID", gameID);
		return ret;
	}
	
	/**
	 * Verpackt ein useSecretpassage Paket.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject useSecretpassage(int gameID) {
		JSONObject ret = new JSONObject();
		ret.put("type", "secret passage");
		ret.put("gameID", gameID);
		return ret;
	}
	
	/**
	 * Verpackt ein suspect Paket.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject suspect(int gameID, Suspicion suspicion) {
		JSONObject ret = new JSONObject();
		ret.put("type", "suspect");
		ret.put("gameID", gameID);
		ret.put("statement", suspicion.getJSON());
		return ret;
	}
	
	/**
	 * Verpackt ein disapprove Paket.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject disapprove(int gameID, Card card) {
		JSONObject ret = new JSONObject();
		ret.put("type", "disprove");
		ret.put("gameID", gameID);
		ret.put("card", card.toString());
		return ret;
	}
	
	/**
	 * Verpackt ein endTurn Paket.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject endTurn(int gameID) {
		JSONObject ret = new JSONObject();
		ret.put("type", "end turn");
		ret.put("gameID", gameID);
		return ret;
	}
	
	/**
	 * Verpackt ein accuse Paket.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject accuse(int gameID, Suspicion sus) {
		JSONObject ret = new JSONObject();
		ret.put("type", "accuse");
		ret.put("gameID", gameID);
		ret.put("statement", sus.getJSON());
		return ret;
	}

	/**
	 * Verpackt ein disapprove Paket.
	 * @param id
	 * @param color
	 * @return
	 */
	public JSONObject disapprove(int gameID) {
		JSONObject ret = new JSONObject();
		ret.put("type", "disprove");
		ret.put("gameID", gameID);
		return ret;
	}
	
	/**
	 * Extension SuperPower
	 */
	public JSONObject useSuperPower(int gameID, Color color) {
		JSONObject ret = new JSONObject();
		ret.put("type", "superpower");
		ret.put("gameID", gameID);
		ret.put("color", color.toString());
		return ret;
	}

}
