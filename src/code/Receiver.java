package code;

import java.io.BufferedReader;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Omnomnomnomnom Server-Pakete
 * Hört den Kanal ab, wo Server Pakete reinschickt.
 * @Codeauthor: Marcus
 * @author anikiNT
 *
 */
public class Receiver implements Runnable {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(Receiver.class);

	private JSONHandler handler;
	private BufferedReader in; 
	
	public Receiver (JSONHandler handler, BufferedReader reader){
		this.handler = handler;
		this.in = reader;

	}
	@Override
	public void run() {
		try {
			
			String line;
			while((line=in.readLine())!=null)
			{
				JSONObject obj = new JSONObject(line);
				if (logger.isDebugEnabled()) {
					logger.debug("run() - Received: " + obj); //$NON-NLS-1$
				}
				handler.processJSON(obj);
				
			}
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			logger.error("run()", e); //$NON-NLS-1$
		}
		
	}
	
	
}
