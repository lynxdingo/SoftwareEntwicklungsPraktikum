package code;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javafx.util.Pair;
import model.ClientModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import server.model.Card; import server.model.Game;
import server.model.Color;
import server.model.Field;
import server.model.Key;
import server.model.Playerstate;
import server.model.Room;
import server.model.Suspicion;
import server.model.Weapons;

/**
 * @Codeauthor: Franjo, Marcus
 * @author anikiNT
 *
 */
public class AI {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(AI.class);

	private ClientModel model;
	private JSONHelper json = new JSONHelper();
	private boolean accuse = false;
	private Random random = new Random();
	
	/*
	 * holds all the information known to the player
	 * -1:=Accused Card
	 *  0:=No Information known
	 *  1:=Player does not have the card, once 1 cannot be changed, this is a 100% true statement
	 *  2:=Player might have the card
	 *  3:=Player has the card, once 3 cannot be changed, this is a 100% true statement
	 *  
	 *  Size:= amountPlayers x 21(max amount of cards)
	 */
	private int[][] playerSheet;
	
	private int amountPlayers;
	
	private int[] thisTurnSusCards;//Cards suspected this turn
	
	private int disprover;//The one who disproved(color) the suspicion
	private int suspector;//The one who suspected(color) the suspicion
	
	private int[] playerOrder;//The order of the players in the game
	
	private int myColor;//one's color
	private int myOrder;//one's order
	
	private Card disCard;//the card one chooses to disprove the suspicion
	
	/*
	 * holds the information in which columns are the 3's
	 * 0:=Column doesn't hold a 3
	 * 1:=Column holds a 3
	 */
	private int[] threeLoc;
	
	/*
	 * Important if 18%amountPlayers!=0, because in this case, one should enter the pool just in case your acquisition is wrong 
	 */
	private boolean visitPool = false;
	
	private int[] accCards = new int[3];//Cards one should accuse
	
	private Card[] allCards = {Color.RED,Color.YELLOW,Color.WHITE,Color.GREEN,Color.BLUE,Color.PURPLE,
			Weapons.DAGGER,Weapons.CANDLESTICK,Weapons.REVOLVER,Weapons.ROPE,Weapons.PIPE,Weapons.SPANNER,
			Room.HALL,Room.LOUNGE,Room.DININGROOM,Room.KITCHEN,Room.BALLROOM,Room.CONSERVATORY,Room.BILLIARD,Room.LIBRARY};
	
	public AI(ClientModel model){
		this.model = model;	
	}
	public void update(){
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error("update()", e); //$NON-NLS-1$
		}
		
		List<Playerstate> states = model.getGame().getPlayerstate();
		Key pos = model.getGame().getPersons().get(model.getGame().getColor()).getPosition();
		
		if(states.contains(Playerstate.USE_SECRET)){
			Key currKey = model.getGame().getPersons().get(model.getGame().getColor()).getPosition();
			Room currRoom = model.getGameField().getFields().get(currKey).getRoom();
			Room secret = null;
			switch(currRoom){
			case STUDY: 	secret = Room.KITCHEN;
							break;
			case KITCHEN:	secret = Room.STUDY;
							break;
			case LOUNGE:	secret = Room.CONSERVATORY;
							break;
			case CONSERVATORY:	secret = Room.LOUNGE;
								break;
			default:		break;
			}
			int secRoom = convertRoomToInt(secret)+12;
			if(threeLoc[secRoom] != 1){
				model.getClient().sendJSON(json.useSecretpassage(model.getGame().getGameID()));
				return;
			}
		}
		if(states.contains(Playerstate.ROLL_DICE)){
			model.getClient().sendJSON(json.rollDice(model.getGame().getGameID()));
		}else if(states.contains(Playerstate.MOVE)){

			Set<Field> fields = model.getGameField().getFieldsForRoll(pos, model.getGame().getLastRoll(), model.getGame().getPositions());
			if(fields.isEmpty()){
				if (logger.isDebugEnabled()) {
					logger.debug("update() - sollte doch eigentlich nich passieren"); //$NON-NLS-1$
				}
				return;
			}
			Set<Pair<Key,Room>> rooms = new HashSet<Pair<Key,Room>>();
			Set<Pair<Key,Room>> roomPool = new HashSet<Pair<Key,Room>>();
			Set<Pair<Key,Room>> roomsBackUp = new HashSet<Pair<Key,Room>>();
			for(Field field : fields){
				if(field.getRoom()!=null){
					if(threeLoc[convertRoomToInt(field.getRoom())+12]!=1){//optimal choice of rooms
						rooms.add(new Pair<Key, Room>(field.getKey(),field.getRoom()));
					}else if(!visitPool&&18%amountPlayers!=0&&field.getRoom()==Room.POOL){
						roomPool.add(new Pair<Key, Room>(field.getKey(),field.getRoom()));
						break;
					}
					roomsBackUp.add(new Pair<Key, Room>(field.getKey(),field.getRoom()));
				}
			}			
			if(roomsBackUp.isEmpty()&&roomPool.isEmpty()){
				int max = 0;
				Field destiny = null;
				for(Field field: fields){
					int disx = Math.abs(pos.getX()-field.getX());
					int disy = Math.abs(pos.getY()-field.getY());
					int distance = disx * disy;
					if(max < distance){
						destiny = field;
						max = distance;
					}
				}
				model.getClient().sendJSON(json.move(model.getGame().getGameID(), destiny.getKey()));
				return;
			}else if(!roomPool.isEmpty()){
				Pair<Key, Room> destiny=roomPool.stream().findFirst().get();//which is the pool room
				model.getClient().sendJSON(json.move(model.getGame().getGameID(), destiny.getKey()));
				List<Card> poolCards = model.getGame().getPool();
				for(int i =0,j=0;i<poolCards.size();j++){
					if(poolCards.get(i)==allCards[j]){
						playerSheet[0][j]=3;
						threeLoc[j]=1;//should be the same as the line before
						i++;
					}
				}
				return;
			}
			
			if(rooms.isEmpty()){
				Pair<Key, Room> destiny=roomsBackUp.stream().findFirst().get();
				model.getClient().sendJSON(json.move(model.getGame().getGameID(), destiny.getKey()));
			}else{
				Pair<Key, Room> destiny=rooms.stream().findFirst().get();
				model.getClient().sendJSON(json.move(model.getGame().getGameID(), destiny.getKey()));
			}
		}else if(states.contains(Playerstate.DISPROVE)){
			
			if(model.getGame().getDisproveCards().isEmpty()){
				model.getClient().sendJSON(json.disapprove(model.getGame().getGameID()));
			}else{
				this.disCard = model.getGame().getDisproveCards().get(0);
				model.getClient().sendJSON(json.disapprove(model.getGame().getGameID(), disCard));
				
			}
			return;
		}else if(states.contains(Playerstate.SUSPECT)){
			Key key = model.getGame().getPersons().get(model.getGame().getColor()).getPosition();
			Room room = model.getGameField().getFields().get(key).getRoom();
			if(room == null){
				return;
			}
			
			int[] susCards = calculateSuspicion();
			Color c = convertIntToColor(susCards[0]);
			Weapons w = convertIntToWeapons(susCards[1]);
			
			Suspicion sus = new Suspicion(room,w,c);
			model.getClient().sendJSON(json.suspect(model.getGame().getGameID(), sus));
		}else if(states.contains(Playerstate.ACCUSE)){
			if(accuse){
				Suspicion sus = new Suspicion(convertIntToRoom(accCards[2]),convertIntToWeapons(accCards[1]),convertIntToColor(accCards[0]));
				model.getClient().sendJSON(json.accuse(model.getGame().getGameID(), sus));
			}
		}
		
		if(states.contains(Playerstate.END_TURN)){
			model.getClient().sendJSON(json.endTurn(model.getGame().getGameID()));
		}
	}
//--------------------------------------INITIALIZE---------------------------------------------
	/**
	 * Initializes the suspected cards for this turn
	 * @param c:	suspected Color
	 * @param w:	suspected Weapon
	 * @param r:	suspected Room
	 */
	public void suspectedCards(Color c, Weapons w, Room r){
		//cards suspected this turn
		this.thisTurnSusCards = new int[3];
		thisTurnSusCards[0] = convertColorToInt(c);
		thisTurnSusCards[1] = convertWeaponToInt(w);
		thisTurnSusCards[2] = convertRoomToInt(r);
	}
	/**
	 * Initializes disprover's location
	 * @param disprover:	The one who disproved the suspicion
	 */
	public void theDisprover(Color disprover){
		this.disprover = convertColorToInt(disprover);//holds the color as int
		for(int i=0;i<playerOrder.length;i++){
			if(playerOrder[i]==this.disprover){
				this.disprover = i;//holds the location of the color
				break;
			}
		}
	}
	/**
	 * Initializes suspector's location
	 * @param suspector: The one who suspected the suspicion
	 */
	public void theSuspector(Color suspector){
		this.suspector = convertColorToInt(suspector);//holds the color as int
		for(int i=0;i<playerOrder.length;i++){
			if(playerOrder[i]==this.suspector){
				this.suspector = i;//holds the location of the color
				break;
			}
		}
	}
	
	/**
	 * Initializes one's playerSheet and all other matrices and arrays that are based off the players
	 * @param players: the amount of players in the game
	 */
	private void initPlayerSheet(int players){
		this.playerSheet = new int[players][21];//create a matrix will (amountPlayer x maxCards)
		this.playerOrder = new int[players];//holds the order of players
		this.threeLoc = new int[21];//holds the locations of 3's
	}
//--------------------------------------HANDEL PLAYERSHEET--------------------------------------
	/**
	 * Creates a matrix which holds all the information the player knows
	 * @param amountPlayers: the amount of players in the game
	 */
	public void createPlayerSheet(int amountPlayers){
		this.amountPlayers=amountPlayers;
		initPlayerSheet(amountPlayers);//initializes most important matrices and arrays
		
		int[] myCards = addPersonalCards();		
		
		List<Color> order = model.getGame().getOrder();
		
		this.myColor = convertColorToInt(model.getGame().getColor());//is color, but not location in the order
		this.myOrder=0;//saves one's order; 0 is initial value
		
		//Sets the orders of the players based on their orders
		for(int i=0;i<playerOrder.length;i++){
			playerOrder[i]=convertColorToInt(order.get(i));
			if(playerOrder[i]==myColor){//finds one's order
				myOrder = i;
			}
		}
		
		//inputs the cards one hold
		for(int i=0;i<playerSheet[0].length;i++){
			if(myCards[i]==3){
				playerSheet[myOrder][i]=3;
			}else{//Cards one doesn't have
				playerSheet[myOrder][i]=1;
			}
		}
		outputPlayerSheet();//for debugging purposes only
		
	}
	/**
	 * Adds all the information that is known to the public about the game.
	 * Remark:	If 1 or 3 are entered into the table, these values cannot be changed!
	 * @param plaStart:		The player who suspected something
	 * @param plaEnd:		The player that gave him a card, if no one gave anything 
	 * 						then accuse automatically afterwards! And set plaEnd to 0
	 * @param susCard:		Cards that were suspected
	 * @param amountPlayer:	Amount of players in the game
	 */
	public void addToPlayerSheet(){
		int[] susCardLoc = {thisTurnSusCards[0],thisTurnSusCards[1]+6,thisTurnSusCards[2]+12};//holds the location of the susCard
		Card[] allCards = {Color.RED,Color.YELLOW,Color.WHITE,Color.GREEN,Color.BLUE,Color.PURPLE,
				Weapons.DAGGER,Weapons.CANDLESTICK,Weapons.REVOLVER,Weapons.ROPE,Weapons.PIPE,Weapons.SPANNER,
				Room.HALL,Room.LOUNGE,Room.DININGROOM,Room.KITCHEN,Room.BALLROOM,Room.CONSERVATORY,Room.BILLIARD,Room.LIBRARY,Room.STUDY
		};
		
		if(disprover==myOrder){//if one is the disprover
			for(int i=0;i<allCards.length;i++){
				if(allCards[i]==disCard){//i is the location of the disCard in allCards
					if(i==susCardLoc[0]){//if disCard=susCard[0] then suspector might have susCard[1] and susCard[2]
						playerSheet[disprover][susCardLoc[0]] = 3;//just in case somewhere the value of this has been changed
						playerSheet[suspector][susCardLoc[1]] = 2;
						playerSheet[suspector][susCardLoc[2]] = 2;
						break;
					}else if(i==susCardLoc[1]){//if disCard=susCard[1] then suspector might have susCard[0] and susCard[2]
						playerSheet[suspector][susCardLoc[0]] = 2;
						playerSheet[disprover][susCardLoc[1]] = 3;//just in case somewhere the value of this has been changed
						playerSheet[suspector][susCardLoc[2]] = 2;
						break;
					}else{//else disCard=susCard[2] then suspector might have susCard[0] and susCard[1]
						playerSheet[suspector][susCardLoc[0]] = 2;
						playerSheet[suspector][susCardLoc[1]] = 2;
						playerSheet[disprover][susCardLoc[2]] = 3;//just in case somewhere the value of this has been changed
						break;
					}
				}
			}
		}else if(suspector==myOrder){//if one is the suspector
			this.disCard = model.getGame().getLastDisproveCard();
			for(int i=0;i<allCards.length;i++){
				if(allCards[i]==disCard){//i is the location of the disCard in allCards
					//by the following; sets the received card from player to 3
					if(i==susCardLoc[0]){
						playerSheet[disprover][susCardLoc[0]] = 3;
						playerSheet[disprover][susCardLoc[1]] = 2;
						playerSheet[disprover][susCardLoc[2]] = 2;
						threeLoc[susCardLoc[0]]=1;//adds location of 3
						break;
					}else if(i==susCardLoc[1]){
						playerSheet[disprover][susCardLoc[0]] = 2;
						playerSheet[disprover][susCardLoc[1]] = 3;
						playerSheet[disprover][susCardLoc[2]] = 2;
						threeLoc[susCardLoc[1]]=1;//adds location of 3
						break;
					}else{
						playerSheet[disprover][susCardLoc[0]] = 2;
						playerSheet[disprover][susCardLoc[1]] = 2;
						playerSheet[disprover][susCardLoc[2]] = 3;
						threeLoc[susCardLoc[2]]=1;//adds location of 3
						break;
					}
				}
			}
		}
		
		for(int i=suspector+1;i!=disprover+1&&i!=suspector;i++){//i!=suspector needed otherwise infty-loop
			if(i>=amountPlayers){i=0;}
			if(i!=disprover){//does not have
				playerSheet[i][susCardLoc[0]]=1;
				playerSheet[i][susCardLoc[1]]=1;
				playerSheet[i][susCardLoc[2]]=1;
				}else{//might have
					if(playerSheet[i][susCardLoc[0]]!=1&&playerSheet[i][susCardLoc[0]]!=3){
						playerSheet[i][susCardLoc[0]]=2;
					}
					if(playerSheet[i][susCardLoc[1]]!=1&&playerSheet[i][susCardLoc[1]]!=3){
						playerSheet[i][susCardLoc[1]]=2;
					}
					if(playerSheet[i][susCardLoc[2]]!=1&&playerSheet[i][susCardLoc[2]]!=3){
						playerSheet[i][susCardLoc[2]]=2;
					}

				}
			if (i==amountPlayers&&i!=disprover){i=-1;}
		}
		outputPlayerSheet();
		
		/*
		 * The following part is for an automatic Acquisition.
		 * @variable possibleAccuseLoc:	Saves the location of all the cards that no one has. 
		 * @variable accCards:			These are the accuse cards. Will be first accused if all spots in the array are filled.
		 */
		int[] possibleAccuseLoc = equalColumnValuesToOne();
		if(possibleAccuseLoc.length==3&&possibleAccuseLoc[0]!=0&&possibleAccuseLoc[1]!=0&&possibleAccuseLoc[2]!=0){
			accCards[0]=possibleAccuseLoc[0];
			accCards[1]=possibleAccuseLoc[1]-6;
			accCards[2]=possibleAccuseLoc[2]-12;
			accuse = true;
		}
		
		
		
	}
	
	/**
	 * Adds the cards one holds into one's playerSheet with a 3 at its location
	 * @return:	An array with all the cards one holds(as 3) 
	 */
	private int[] addPersonalCards(){
		List<Card> onesCards = model.getGame().getCards();
		int[] onesIntCards = new int[playerSheet[0].length];
		
		Color[] aColor = {Color.RED,Color.YELLOW,Color.WHITE,Color.GREEN,Color.BLUE,Color.PURPLE};
		Room[] aRoom = {Room.HALL,Room.LOUNGE,Room.DININGROOM,Room.KITCHEN,Room.BALLROOM,Room.CONSERVATORY,Room.BILLIARD,Room.LIBRARY,Room.STUDY,Room.POOL};
		Weapons[] aWeapon = {Weapons.DAGGER,Weapons.CANDLESTICK,Weapons.REVOLVER,Weapons.ROPE,Weapons.PIPE,Weapons.SPANNER};
		
		//number 3 indicates player has card
		for(int i=0;i<onesCards.size();i++){
			for(int j=0;j<aColor.length;j++){
				if(onesCards.get(i)==aColor[j]){
					onesIntCards[j]=3;
					threeLoc[j]=1;//adds location of 3
				}
			}
			for(int j=0;j<aWeapon.length;j++){
				if(onesCards.get(i)==aWeapon[j]){
					onesIntCards[j+6]=3;
					threeLoc[j+6]=1;//adds location of 3
				}
			}
			for(int j=0;j<aRoom.length;j++){
				if(onesCards.get(i)==aRoom[j]){
					onesIntCards[j+12]=3;
					threeLoc[j+12]=1;//adds location of 3
				}
			}
		}
		return onesIntCards;
	}
	
	/**
	 * Checks if the cell value is equal to three, if true then make all other cell value in the column to 1.
	 */
	private void checkCellValueThree(){
		
		for(int i=0;i<threeLoc.length;i++){
			if(threeLoc[i]==1){//checks if cell value=3
				//if true make all other cell values within this column to 1
				for(int k=0;k<playerSheet.length;k++){
					if(playerSheet[k][i]!=3){
						playerSheet[k][i]=1;
					}
				}
				
			}
		}
	}
	
	/**
	 * Outputs the information known to the player.
	 * @param myColor:		Ones color as an int
	 * @param amountPlayers:The amount of players in the game
	 */
	private void outputPlayerSheet(){
		
		checkCellValueThree();
		
		Color myColor = model.getGame().getColor();
		
		char[] rowInfoGen = new char[87];
		for(int i=0;i<rowInfoGen.length;i++){//sets each character to space
			rowInfoGen[i]=' ';
		}
		for(int i=2;i<rowInfoGen.length;i=i+4){
			rowInfoGen[i]='|';
		}
		
		char[][] matrixInfo = new char[amountPlayers][rowInfoGen.length];
		for(int i=0;i<amountPlayers;i++){
			matrixInfo[i][0]=Integer.toString(i+1).charAt(0);//outputs order
			for(int j=4,k=0;j<rowInfoGen.length;j=j+4){//creates general table without any info entered
				try{
					matrixInfo[i][j-2]=rowInfoGen[j-2];//inputs '|'
					matrixInfo[i][j]=Integer.toString(playerSheet[i][k]).charAt(0);
					k++;
				}catch(ArrayIndexOutOfBoundsException e){
					logger.error("k greater equal 21", e);
				}
			}
			matrixInfo[i][rowInfoGen.length-1]='|';
		}
		
		//outputs ones playerSheet
		if (logger.isDebugEnabled()) {
			logger.info(" Player " + myColor); //$NON-NLS-1$
			logger.info("<<<<<<<<<PERSON>>>>>>>>>>>|<<<<<<<<<WEAPON>>>>>>>>|<<<<<<<<<<<<<<<<ROOM>>>>>>>>>>>>>>>>"); //$NON-NLS-1$
			logger.info("X | 1 | 2 | 3 | 4 | 5 | 6 | 1 | 2 | 3 | 4 | 5 | 6 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |"); //$NON-NLS-1$
		
		for(int i=0;i<amountPlayers;i++){
			logger.info(new String(matrixInfo[i]));
			}
		}
	}
//--------------------------------------MATRICE FUNCTIONS--------------------------------------	
	/**
	 * Finds the first known room. Should be used with the function findColumnsWithThree.
	 * @return location of first known room or if -1, then no room known.
	 */
	private int findFirstRoomWithThree(){
		int firstRoom = 12;//initial starting postion for rooms
		int[] trueThreeLoc = findColumnsWithThree();
		
		for(int i = 0; i<=trueThreeLoc.length;i++){
			if(trueThreeLoc[i]>=firstRoom){
				return i;//location of the first room
			}
		}
		return -1;//supposely no room known
	}
	
	/**
	 * Finds the actual location of the 3's within this.threeLoc
	 * @return	A smaller array with the actual location of the 3's from smallest to largest
	 */
	private int[] findColumnsWithThree(){
		int[] threeLoc = new int[playerSheet[0].length];
		
		//Finds the actual location of the 3's in the threeLoc
		for(int i=0,j=0;i<this.threeLoc.length;i++){
			if(this.threeLoc[i]==1){
				threeLoc[j]=i;
				j++;
			}
		}
		
		int trueLength = nonNullLength(threeLoc);
		int[] trueThreeLoc = new int[trueLength];
		
		//Saves the actual location of the 3's in a smaller array, so that it is faster
		for(int i=0;i<trueLength;i++){
			trueThreeLoc[i]=threeLoc[i];
		}
		
		return trueThreeLoc;
	}
	
	/**
	 * Finds the location of columns where each cell-value within the column = 1.
	 * Remark: Takes into calculation if cards in pool or not!
	 * 
	 * @return:	the location of columns where each cell in the column = 1
	 * 			-1:=means the values within the columns were not equal
	 */
	private int[] equalColumnValuesToOne(){
		int rest = 18%amountPlayers;//the amount of cards in the pool
		int[] zeroLoc;
		
		if(visitPool){//has one been in the pool to see cards?
			zeroLoc = new int[amountPlayers];
		}else{
			zeroLoc = new int[amountPlayers+rest];
		}
		
		boolean allEquals=true;
		
		/*
		 * The value we want to compare to is ALWAYS 1. Since it is implemented that if one doesn't have a card the cell-value = 1
		 */
		int value = 1;
		
		/*
		 * Will exit once k=zerooLoc.length which in general will happen before the other condition
		 */
		for(int j=0,k=0;j<playerSheet[0].length&&k<zeroLoc.length;j++){
			
			if(threeLoc[j]==1){//if a column has a 3 in it go to next column
				j++;
				if(j==21){//suppose the last column has a 3
					break;
				}
			}
			
			//compares each value to the one before
			for(int i=1;i<amountPlayers;i++){
				if(playerSheet[i][j]!=value){allEquals=false;break;}//makes sure the cell = 1
			}
			//checks if all values are equal to 1, if true save location
			if(allEquals){zeroLoc[k]=j;k++;}
			else{allEquals=true;}
		}
		return zeroLoc;
	}	
	
	/**
	 * Counts the amount of 0,1, and 2 within a column. 3's do not matter
	 * @return:			a matrix with the save information
	 */
	private int[][] countNumbersInColumn(){
		int[][] numberCount = new int[3][playerSheet[0].length];
		
		for(int i=0,j=0;j<playerSheet[0].length;i++){
			if(playerSheet[i][j]==0){
				numberCount[0][j]++;}
			else if(playerSheet[i][j]==1){
				numberCount[1][j]++;}
			else if(playerSheet[i][j]==2){
				numberCount[2][j]++;}
			
			if(i==playerSheet.length-1){
				j++;
				i=-1;
			}
		}
		return numberCount;
	}
	/**
	 * Finds the maximum value within a column
	 * @param matrix:	A [i][j]-Matrix
	 * @return:			The maximum value of column j
	 */
	private int[] findMaxInColumn(int[][] matrix){
		int[] maxInColumn = new int[matrix[0].length];
		int[] trueThreeLoc = findColumnsWithThree();
		int max;
		
		for(int j=0,k=0;j<matrix[0].length;j++){
			max = matrix[0][j];
			for(int i=0;i<matrix.length;i++){
				if(trueThreeLoc[k]==j){
					max=0;//if a 3's is located in the column, this column will be disregarded
					k++;
					/*
					 * Once k reaches full length k will be set 0.
					 * Since we already checked trueThreeLoc[k]==j @ k=0
					 * it will never go into this if-loop again.
					 */
					if(k==trueThreeLoc.length){
						k=0;
					}
					break;
				}else if(matrix[i][j]>max){
					max = matrix[i][j];
				}
			}
			maxInColumn[j]=max;
		}
		//length is equal to the length of of matrix[0].length	
		return maxInColumn;
	}
	
	/**
	 * Finds the location of the elements with the maximum value.
	 * @param arrayRow:	Array that should be checked
	 * @param start:	The starting position. Important for the different cards.
	 * @param end:		The ending position. Important for the different cards.
	 * @return:			An array with the locations of the max-values. Length = [1,end-start+2]
	 */
	private int[] findMaxInRow(int[] arrayRow, int start, int end){
		int[] maxLoc = new int[end-start+1];//length is larger than wanted
		int maxValue = arrayRow[start];
		/*
		 * in case an error has occurred at calculating the nonNullLength, one returns an array where the first element is the max
		 */
		int[] errorArray ={0};
		
		//finds the max value in the array
		for(int i=start+1;i<=end;i++){
			if(arrayRow[i]>maxValue){
				maxValue = arrayRow[i];
			}
			if(maxValue==amountPlayers){
				break;
			}
		}
		
		//finds the location of the maxValue's
		for(int i=start,j=0;i<=end;i++){
			if(arrayRow[i]==maxValue){
				maxLoc[j]=i;
				j++;
			}
		}
		
		try{
			int trueLength = nonNullLength(maxLoc);//locates the first 0-value
			int[] trueMaxLoc = new  int[trueLength];
			
			for(int i=0;i<trueLength;i++){
				trueMaxLoc[i] = maxLoc[i];
			}
			
			return trueMaxLoc;//the length is <= than maxLoc.length
			
		}catch(NegativeArraySizeException e){
			if(logger.isDebugEnabled()){
				logger.error("Error has conquered at calculated the nonNullLength!", e);
			}
			return errorArray;
		}
	}
	
	/**
	 * Finds the first 0-element within an array, which is the same as the length of the array without the 0's
	 * @param array:	array
	 * @return:			returns the location of the first 0-element
	 * 					if no 0-element found, then return array length
	 */
	private int nonNullLength(int[] array){
		//returns the location of the first 0-element
		for(int i=1;i<array.length;i++){//has to start at 1 since the 0th-element is usually 0 and therefore wrong calculations happen
			if(array[0]==0&&array[1]==0){//if the first two elements are actually 0
				break;
			}
			else if(array[i]==0){return i;}//returns actually length of array
			else if(i==array.length-1){
				return array.length;
				}
		}
		return -1;//array consists of only zeroes
	}
	
	/**
	 * Randomly finds a card that one does not own
	 * @param exclusionCards:	Are the cards(array) that one possesses
	 * @param trueLength:		The length of an array without the null values
	 * @param max:				Max number for the randInt function
	 * @return:					A random card that one does not own
	 */
	private int randCardArray(int[] exclusionCards, int trueLength, int max){
		boolean inArray = false;
		int test = random.nextInt(max);
		//finds a card that one does not own
		do{
			for(int i=0;i<trueLength;i++){
				if(exclusionCards[i]==test){
					inArray=true;
					test = random.nextInt(max);
				}
			}
		}while(inArray);
		
		return test;
	}
//--------------------------------------CALCULATION FUNCTIONS--------------------------------------		
	/**
	 * Calculates the best random suspect cards based on the playerSheet
	 * @return: the best random suspected color and weapon
	 */
	private int[] calculateSuspicion(){
		int[] susCards = new int[2];//0: For color/person; 1:Weapon; For the room a different function will be used.
		
		/*
		 * Returns the locations of the columns with the max number of 0's or 1's.
		 * 2's and 3's aren't important in this step, because one wants to get more new info.
		 * The array will always have at least one elements, since that element will be the one, one should accuse.
		 */
		int[] maxLocColor = findMaxInRow(findMaxInColumn(countNumbersInColumn()), 0, 5);
		int[] maxLocWeapon = findMaxInRow(findMaxInColumn(countNumbersInColumn()), 6, 11);
		
		susCards[0] = maxLocColor[random.nextInt(maxLocColor.length)];
		susCards[1] = maxLocWeapon[random.nextInt(maxLocWeapon.length)]-6;
		
		return susCards;
	}
	/**
	 * Calculates the amount of cards in the pool, usually 0.
	 * @return pool cards
	 */
	private int calculatePoolCards(){
		int greaterSix = 0;
		int cards = 18;//the accused cards have been excluded
		if(amountPlayers>6){
			greaterSix = amountPlayers%6;
			cards = cards + 2*greaterSix;//Additional cards, if more than 6 players
		}
		return cards%amountPlayers;
	}
//--------------------------------------CONVERSION FUNCTIONS--------------------------------------	
	/*
	 * The following three functions are important for converting the known cards into ints.
	 * if return=-1 failed to convert.
	 */
	private int convertColorToInt(Color c){
		Color[] array = {Color.RED,Color.YELLOW,Color.WHITE,Color.GREEN,Color.BLUE,Color.PURPLE};
		
		for(int i=0;i<array.length;i++){
			if(c==array[i]){
				return i;
			}
		}
		if(logger.isDebugEnabled()){
			logger.error("Failed to convert from Color to int");
		}
		return -1;
	}
	
	private int convertRoomToInt(Room r){
		Room[] array = {Room.HALL,Room.LOUNGE,Room.DININGROOM,Room.KITCHEN,Room.BALLROOM,Room.CONSERVATORY,Room.BILLIARD,Room.LIBRARY,Room.STUDY};
		
		for(int i=0;i<array.length;i++){
			if(r==array[i]){
				return i;
			}
		}
		if(logger.isDebugEnabled()){
			logger.error("Failed to convert from Room to int");
		}
		return -1;
	}
	
	private int convertWeaponToInt(Weapons w){
		Weapons[] array = {Weapons.DAGGER,Weapons.CANDLESTICK,Weapons.REVOLVER,Weapons.ROPE,Weapons.PIPE,Weapons.SPANNER};
		
		for(int i=0;i<array.length;i++){
			if(w==array[i]){
				return i;
			}
		}
		if(logger.isDebugEnabled()){
			logger.error("Failed to convert from Weapon to int");
		}
		return -1;
	}
	/*
	 * The following three functions are for converting the ints back into the specific cards.
	 */
	private Color convertIntToColor(int i){
		Color[] array = {Color.RED,Color.YELLOW,Color.WHITE,Color.GREEN,Color.BLUE,Color.PURPLE};
		
		try{
			return array[i];
		}catch(ArrayIndexOutOfBoundsException e){
			if(logger.isDebugEnabled()){
				logger.error("Happend most likely by Acquisition, then by Suspicion.",e);
			}
			return array[i%6];//when error happens there will still be some random
		}		
	}
	
	private Room convertIntToRoom(int i){
		Room[] array = {Room.HALL,Room.LOUNGE,Room.DININGROOM,Room.KITCHEN,Room.BALLROOM,Room.CONSERVATORY,Room.BILLIARD,Room.LIBRARY,Room.POOL};
		
		try{
			return array[i];
		}catch(ArrayIndexOutOfBoundsException e){
			if(logger.isDebugEnabled()){
				logger.error("Happend most likely by Acquisition, then by Suspicion.",e);
			}
			return array[i%9];//when error happens there will still be some random
		}
	}
	
	private Weapons convertIntToWeapons(int i){
		Weapons[] array = {Weapons.DAGGER,Weapons.CANDLESTICK,Weapons.REVOLVER,Weapons.ROPE,Weapons.PIPE,Weapons.SPANNER};
		
		try{
			return array[i];
		}catch(ArrayIndexOutOfBoundsException e){
			if(logger.isDebugEnabled()){
				logger.error("Happend most likely by Acquisition, then by Suspicion.",e);
			}
			return array[i%6];//when error happens there will still be some random
		}		
	}
}
