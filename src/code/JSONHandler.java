package code;


import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;
import javafx.scene.media.AudioClip;
import model.ClientModel;
import model.Constants;
import model.Game;
import model.GameStub;
import model.Nick;
import model.Player;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;



import server.model.Card;
import server.model.Color;
import server.model.Gamestate;
import server.model.Key;
import server.model.Playerstate;
import server.model.Suspicion;
import server.model.Weapons;
import control.Presenter;

/**
 * Der JSONHandler bekommt JSON-Pakete von einem Server, die er anschließend auspackt und
 * in die lokale Syntax übersetzt. 
 * 
 * @Codeauthor: Marcus
 * @author anikiNT
 *
 */
public class JSONHandler{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LogManager.getLogger(JSONHandler.class);

	ClientModel model;
	Presenter presenter;
	public JSONHandler (ClientModel model){
		this.model = model;
	}
	
	/**
	 * Zerlegt ALLE eintreffenden JSON-Pakete in die lokale Syntax.
	 * 
	 * @param obj
	 */
	public void processJSON(JSONObject obj){
		if(obj.isNull("type"))
			return;
		switch(obj.getString("type")){
		case "login successful":	HandleLoginSuccessful(obj);
									break;
		case "user added":			HandleUserAdded(obj);
									break;
		case "user left":			HandleUserLeft(obj);
									break;
		case "chat":				HandleChat(obj);
									break;
		case "game created":		HandleGameCreated(obj);
									break;
		case "player added":		HandlePlayerAdded(obj);
									break;
		case "watcher added":		HandleWatcherAdded(obj);
									break;
		case "gameinfo":			HandleGameinfo(obj);
									break;
		case "left game":			HandleLeftGame(obj);
									break;
		case "game ended":			HandleGameEnded(obj);
									break;
		case "game deleted":		HandleGameDeleted(obj);
									break;
		case "player_cards":		HandlePlayerCards(obj);
									break;
		case "game started":		HandleGameStarted(obj);
									break;
		case "stateupdate":			HandleStateupdate(obj);
									break;
		case "dice result":			HandleDiceResult(obj);
									break;
		case "moved":				HandleMoved(obj);
									break;
		case "poolcards":			HandlePoolcards(obj);
									break;
		case "suspicion":			HandleSuspicion(obj);
									break;
		case "disproved":			HandleDisproved(obj);
									break;
		case "no disprove":			HandleNoDisprove(obj);
									break;
		case "wrong accusation":	HandleWrongAccusation(obj);
									break;
		case "disconnected":		HandleDisconnected(obj);
									break;
		case "error":				HandleError(obj);
									break;
		case "superpower":			HandleSuperpower(obj);
									break;
		case "superpowerPurple":	HandleSuperpowerPurple(obj);	//music
									break;
		
		}
	}
	
	

	/**
	 * Diese Methode öffnet ein Fehlerbildschirm in der Szene.
	 * @param obj
	 */
	private void HandleError(JSONObject obj) {
		Platform.runLater(new Runnable(){
			@Override public void run(){
				presenter.HandleError(obj.getString("message"));
			}
		});
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um ein Disconnect zu handeln.
	 * @param obj
	 */
	private void HandleDisconnected(JSONObject obj) {
		if(obj.isNull("message")){
			logger.error("HandleDisconnected(JSONObject) - Just why????");
			return;
		}
		if (logger.isDebugEnabled()) {
			logger.debug("HandleDisconnected(JSONObject) - " + obj.get("message"));
		}
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um ein falsche Accusation zu handeln.
	 * 
	 * @param obj
	 */
	private void HandleWrongAccusation(JSONObject obj) {
		if(obj.isNull("gameID") || obj.isNull("statement")){
			logger.error("HandleWrongAccusation(JSONObject) - JSON not complete: " + obj); //$NON-NLS-1$
			return;
		}
		int gameID = obj.getInt("gameID");
		//Am I in the Game?
		if(model.getGame().getGameID()!=gameID){
			logger.error("HandleWrongAccusation(JSONObject) - But im not in the game!"); //$NON-NLS-1$
			return;
		}
		
		JSONObject sus = obj.getJSONObject("statement");
		Suspicion suspicion = new Suspicion(sus.getString("room"),sus.getString("weapon"), sus.getString("person"));
		if(suspicion.isIllegal()){
			logger.error("HandleWrongAccusation(JSONObject) - Thats no suspicion at all! AN: hier sollte ACCUSATION STEHEN"); //$NON-NLS-1$
			return;
		}
		Platform.runLater(()->presenter.WrongAccusation(suspicion));
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt, falls keine Disprove-Phase(Karten zeigen) aufgetreten ist, zu handeln.
	 * Der Ask-Screen wird bei validen Paket geschlossen.
	 * @param obj
	 */
	private void HandleNoDisprove(JSONObject obj) {
		if(obj.isNull("gameID")){
			logger.error("HandleNoDisprove(JSONObject) - JSON not complete: " + obj); //$NON-NLS-1$
			return;
		}
		int gameID = obj.getInt("gameID");
		//Am I in the Game?
		if(model.getGame().getGameID()!=gameID){
			logger.error("HandleNoDisprove(JSONObject) - But im not in the game!"); //$NON-NLS-1$
			return;
		}
		Platform.runLater(()->presenter.noDisprove());
		
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um ein Disprove(Karten zeigen) zu handeln.
	 * Bei einem validen Paket wird die Karte, die die Suspicion wiederlegt ans Modell weitergegeben.
	 * Bzw. Ki.
	 * @param obj
	 */
	private void HandleDisproved(JSONObject obj) {
		if(obj.isNull("gameID") || obj.isNull("nick")){
			logger.error("HandleDisproved(JSONObject) - JSON not complete: " + obj); //$NON-NLS-1$
			return;
		}
		int gameID = obj.getInt("gameID");
		//Am I in the Game?
		if(model.getGame().getGameID()!=gameID){
			logger.error("HandleDisproved(JSONObject) - But im not in the game!"); //$NON-NLS-1$
			return;
		}
		Game game = model.getGame();
		Color disprover = game.getColorFromNickname(obj.getString("nick"));
		if(disprover == null){
			logger.error("HandleDisproved(JSONObject) - Den hab isch garnisch"); //$NON-NLS-1$
		}
		Card card = null;
		if(obj.has("card")){
			card = Card.getCard(obj.getString("card"));
		}
		model.getGame().setLastDisproveCard(card);
		model.getGame().setLastDisprover(disprover);
		Platform.runLater(()->presenter.disproved());
		
		//sends the disprover to AI
		if(model.getGame().isAI()){
			model.getGame().getAI().theDisprover(disprover);
			model.getGame().getAI().addToPlayerSheet();//adds the info to playerSheet
		}
	}
	
	/**
	 * Wird von processJSON() ausgeführt um eine Suspicion zu handeln.
	 * Falls eine valides Paket ankommt, wird der Suspicion-Screen angezeigt.
	 * @param obj
	 */
	private void HandleSuspicion(JSONObject obj) {
		if(obj.isNull("gameID") || obj.isNull("statement")){
			logger.error("HandleSuspicion(JSONObject) - JSON not complete: " + obj); //$NON-NLS-1$
			return;
		}
		int gameID = obj.getInt("gameID");
		//Am I in the Game?
		if(model.getGame().getGameID()!=gameID){
			logger.error("HandleSuspicion(JSONObject) - But im not in the game!"); //$NON-NLS-1$
			return;
		}
		Game game = model.getGame();
		JSONObject sus = obj.getJSONObject("statement");
		Suspicion suspicion = new Suspicion(sus.getString("room"),sus.getString("weapon"), sus.getString("person"));
		if(suspicion.isIllegal()){
			logger.error("HandleSuspicion(JSONObject) - Thats no suspicion at all!"); //$NON-NLS-1$
			return;
		}
		//Get Key for Room
		Key key = model.getGameField().getFieldForRoom(suspicion.getRoom()); 
		//Move suspect to Room
		game.getPersons().get(suspicion.getColor()).setPosition(key);
		//Move Weapon to Room
		game.getWeapons().get(suspicion.getWeapon()).setPosition(key);
		
		game.setSuspicion(suspicion);
		Platform.runLater(()->presenter.showask());
		
		//sends the suspected cards to AI
		if(model.getGame().isAI()){
			model.getGame().getAI().suspectedCards(suspicion.getColor(),suspicion.getWeapon(),suspicion.getRoom());
			model.getGame().getAI().theSuspector(game.getSuspector());
			
		}
		
		
	}
	/**
	 * Wird von processJSON() ausgeführt um die Poolkarts zu generieren.
	 * Erzeugt alle übergebenen Karten(Server) und fügt sie dem Schwimmbad hinzu.
	 * 
	 * @param obj
	 */
	private void HandlePoolcards(JSONObject obj) {
		if(obj.isNull("cards")){
			logger.error("HandlePoolcards(JSONObject) - JSON not complete: " + obj); //$NON-NLS-1$
			return;
		}
		JSONArray cards = obj.getJSONArray("cards");
		for(int i = 0; !cards.isNull(i); i++){
			Card card = Card.getCard(cards.getString(i));
			if(card == null){
				logger.error("HandlePoolcards(JSONObject) - Dis is no card: " + cards.getString(i)); //$NON-NLS-1$
				continue;
			}
			model.getGame().addToPool(card);
		}
		Platform.runLater(()->presenter.Poolcards());
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um eine Bewegung zu handeln.
	 * Falls valides Paket, wird die Farbe bzw. Person im Paket zu den Koordination im Paket bewegt.
	 * @param obj
	 */
	private void HandleMoved(JSONObject obj) {
		if(obj.isNull("gameID") || obj.isNull("person position")){
			logger.error("HandleMoved(JSONObject) - JSON not complete: " + obj); //$NON-NLS-1$
			return;
		}
		int gameID = obj.getInt("gameID");
		//Am I in the Game?
		if(model.getGame().getGameID()!=gameID){
			logger.error("HandleMoved(JSONObject) - But im not in the game!"); //$NON-NLS-1$
			return;
		}
		Game game = model.getGame();
		
		JSONObject position = obj.getJSONObject("person position");
		if(position.isNull("person") || position.isNull("field")){
			logger.error("HandleMoved(JSONObject) - The Person Position JSON was not complete"); //$NON-NLS-1$
			return;
		}
		
		Color color = Color.getColor(position.getString("person"));
		if(color == null){
			logger.error("HandleMoved(JSONObject) - This Color does not exist!"); //$NON-NLS-1$
			return;
		}
		
		
		JSONObject field = position.getJSONObject("field");
		if(field.isNull("x") || field.isNull("y")){
			logger.error("HandleMoved(JSONObject) - The field JSON is not complete"); //$NON-NLS-1$
			return;
		}
		
		int x = field.getInt("x");
		int y = field.getInt("y");
		Key key = new Key(x, y);
		game.setPosition(color, key);
		
		if(color == game.getColor()){
			model.getGame().setMovedKey(key);
		}
		
		Platform.runLater(()->presenter.DisableHighlight());
		
		
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um ein Würfelergebnis vom Server zu handeln.
	 * Würfelergebnisse wird in einem roll Objekt gespeichert.
	 * @param obj
	 */
	private void HandleDiceResult(JSONObject obj) {
		if(obj.isNull("gameID") || obj.isNull("result")){
			logger.error("HandleDiceResult(JSONObject) - JSON not complete: " + obj); //$NON-NLS-1$
			return;
		}
		int gameID = obj.getInt("gameID");
		//Am I in the Game?
		if(model.getGame().getGameID()!=gameID){
			logger.error("HandleDiceResult(JSONObject) - But im not in the game!"); //$NON-NLS-1$
			return;
		}
		Game game = model.getGame();
		JSONArray roll = obj.getJSONArray("result");
		Platform.runLater(new Runnable(){
			@Override public void run(){
				presenter.AnimateDice(roll.getInt(0), roll.getInt(1));
			}
		});

		
		game.setLastRoll(roll.getInt(0), roll.getInt(1));
		
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um ein Statusänderung (eines Spielers) zu handeln.
	 * END_TURN, schliesst End-Turn-Button, versteckt Würfel.
	 * USE_SECRET, speichert das Secret im GameScreen ab -> Hightlight.
	 * ROLL_DICE, zeigt Würfel an.
	 * SUSPECT, zeigt Suspect-Button an.
	 * DISPROVE, zeigt ASK-Screen an.
	 * sonst DO_NOTHING state
	 * @param obj
	 */
	private void HandleStateupdate(JSONObject obj) {
		if(obj.isNull("gameID") || obj.isNull("player")){
			logger.error("HandleStateupdate(JSONObject) - JSON not complete: " + obj + "/n(Statusupdate)"); //$NON-NLS-1$
			return;
		}
		int gameID = obj.getInt("gameID");
		//Am I in the Game?
		if(model.getGame().getGameID()!=gameID){
			logger.error("HandleStateupdate(JSONObject) - But im not in the game!"); //$NON-NLS-1$
			return;
		}
		Game game = model.getGame();
		Player info = getPlayerInfo(obj.getJSONObject("player"));
		Player player = game.getPlayers().get(info.getColor());
		if(player == null){
			logger.error("HandleStateupdate(JSONObject) - Does this Player really exist?"); //$NON-NLS-1$
			return;
		}
		player.getPlayerstate().clear();
		player.getPlayerstate().addAll(info.getPlayerstate());
		if(info.getPlayerstate().contains(Playerstate.SUSPECT)){
			model.getGame().setSuspecter(player.getColor());
		}
		if(info.getColor() == model.getGame().getColor()){
			model.getGame().setPlayerstate(info.getPlayerstate());
			if(info.getPlayerstate().contains(Playerstate.END_TURN)){
				Platform.runLater(()->presenter.EndTurn());
			}else if(info.getPlayerstate().contains(Playerstate.SUSPECT)){
				Platform.runLater(()->presenter.SuspectTurn());
			}
			if(info.getPlayerstate().contains(Playerstate.USE_SECRET)){
				Platform.runLater(()->presenter.Secret());
			}
			if(info.getPlayerstate().contains(Playerstate.ROLL_DICE)){
				
				Platform.runLater(()->presenter.ShowDice());
			}
			if(model.getGame().isAI()){
				model.getGame().getAI().update();
			}
		}else{
			model.getGame().getPlayerstate().clear(); 
			model.getGame().getPlayerstate().add(Playerstate.DO_NOTHING);
			model.getScreens().getGameScreen().getTabMenu().getSuperpowerBtn().setVisible(false);
			
		}
		
		if(info.getPlayerstate().contains(Playerstate.DISPROVE)){
			model.getGame().setActiveDisprover(player.getColor());
			Platform.runLater(()->presenter.disproveTurn());
		}
		if(info.getPlayerstate().contains(Playerstate.TELEPORT)){
			
			
		}
		
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um ein SpielStart zu handeln.
	 * Rechnet die Spielreihenfolge aus und setzt das Spiel auf gestarted, falls es
	 * sich um ein valides Paket handelt.
	 * 
	 * @param obj
	 */
	private void HandleGameStarted(JSONObject obj) {
		if(obj.isNull("gameID") || obj.isNull("order")){
			logger.error("HandleGameStarted(JSONObject) - JSON not complete: " + obj); //$NON-NLS-1$
			return;
		}
		
		int gameID = obj.getInt("gameID");
		//Does the game exist?
		GameStub stub = model.getGames().get(gameID);
		if(stub == null){
			logger.error("HandleGameStarted(JSONObject) - This game does not exist how can it possibly start"); //$NON-NLS-1$
			return;
		}
		stub.getOrder().clear();
		//Make Players new to represent the correct order
		JSONArray players = obj.getJSONArray("order");
		for(int i = 0; !players.isNull(i); i++){
			Color color = stub.getColorFromNickname(players.getString(i));
			if(color == null){
				logger.error("HandleGameStarted(JSONObject) - I dont have this color :("); //$NON-NLS-1$
				continue;
			}
			stub.addToOrder(color);
		}
		
		stub.setGamestate(Gamestate.STARTED);
		
		if(gameID == model.getGame().getGameID()){
			Platform.runLater(new Runnable(){
				@Override public void run(){
					presenter.showGameField();
				}
			});
		}
		
		if(model.getGame().isAI()){
			//players.length() returns the amount of players in game
			model.getGame().getAI().createPlayerSheet(players.length());
		}

	}
	
	/**
	 * Wird von processJSON() ausgeführt um ein falsche Accusation zu handeln.
	 * Handelt die das Paket mit den überreichten Karten des Servers.
	 * Fügt es bei valider Eingabe dem Model.Game.Cards-liste hinzu.
	 * @param obj
	 */
	private void HandlePlayerCards(JSONObject obj) {
		if(obj.isNull("gameID") || obj.isNull("cards")){
			logger.error("HandlePlayerCards(JSONObject) - JSON not complete: " + obj); //$NON-NLS-1$
			return;
		}
		int gameID = obj.getInt("gameID");
		//Am I in the Game
		if(model.getGame().getGameID() != gameID){
			logger.error("HandlePlayerCards(JSONObject) - Im not in the Game"); //$NON-NLS-1$
			return;
		}
		//Am I just a watcher
		if(model.getGame().getColor() == null){
			logger.error("HandlePlayerCards(JSONObject) - Im just watching"); //$NON-NLS-1$
			return;
		}
		JSONArray cards = obj.getJSONArray("cards");
		for(int i = 0; !cards.isNull(i); i++){
			Card card = Card.getCard(cards.getString(i));
			if(card == null){
				logger.error("HandlePlayerCards(JSONObject) - Is dis even a card?: " + cards.getString(i)); //$NON-NLS-1$
				continue;
			}
			model.getGame().getCards().add(card);
		}
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um ein gelöschtes Spiel zu handeln.
	 * Falls es existiert, wird das Spiel (bzw. ihre ID) gelöscht.
	 * 
	 * @param obj
	 */
	private void HandleGameDeleted(JSONObject obj) {
		if(obj.isNull("gameID")){
			logger.error("HandleGameDeleted(JSONObject) - JSON not complete: " + obj); //$NON-NLS-1$
			return;
		}
		int gameID = obj.getInt("gameID");
		model.getGames().remove(gameID);
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um beendetes Spiel zu handeln.
	 * Bei valider Eingabe setzt sich das Gamestate auf ENDED.
	 * Anschließend wird die Lösung auf der Szene angezeigt und das Spiel auf Anfangszustand resettet.
	 * @param obj
	 */
	private void HandleGameEnded(JSONObject obj) {
		if(obj.isNull("gameID")){
			logger.error("HandleGameEnded(JSONObject) - JSON not complete: " + obj); //$NON-NLS-1$
			return;
		}
		int gameID = obj.getInt("gameID");
		GameStub game = model.getGames().get(gameID);
		if(game == null){
			logger.error("HandleGameEnded(JSONObject) - A Game with this ID does not exist."); //$NON-NLS-1$
			return;
		}
		
		game.setGamestate(Gamestate.ENDED);
		
		if(game.getGameID() == model.getGame().getGameID()){
			if(obj.isNull("statement")){
				logger.error("HandleGameEnded(JSONObject) - No statement attached"); //$NON-NLS-1$
				//Possible Left Of Player
				Platform.runLater(()->presenter.showLobby());
				return;
				
			}
			JSONObject sus = obj.getJSONObject("statement");
			Suspicion suspicion = new Suspicion(sus.getString("room"),sus.getString("weapon"), sus.getString("person"));
			if(suspicion.isIllegal()){
				logger.error("HandleGameEnded(JSONObject) - This is no suspicion"); //$NON-NLS-1$
				return;
			}
			model.getGame().setSuspicion(suspicion);
			if(obj.has("nick")){
				Color player = model.getGame().getColorFromNickname(obj.getString("nick"));
				if(player == null){
					logger.error("HandleGameEnded(JSONObject) - There is not such a player"); //$NON-NLS-1$
				}
				model.getGame().setAccuser(player);
			}
			
			Platform.runLater(()->presenter.EndGame());
			
		}
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um ein Player-Leave zu handeln.
	 * Bei valider Eingabe, wird der Nick von der Spielerliste entfernt.
	 * @param obj
	 */
	private void HandleLeftGame(JSONObject obj) {
		if(obj.isNull("gameID") || obj.isNull("nick")){
			logger.error("HandleLeftGame(JSONObject) - JSON not complete: " + obj); //$NON-NLS-1$
			return;
		}
		int gameID = obj.getInt("gameID");
		Nick nick = model.getNick(obj.getString("nick"));
		//Does such a Nick exist
		if(nick == null){
			logger.error("HandleLeftGame(JSONObject) - This Nick does not exist"); //$NON-NLS-1$
			return;
		}
		//Does Game exist
		GameStub game = model.getGames().get(gameID);
		if(game == null){
			logger.error("HandleLeftGame(JSONObject) - A Game with this ID does not exist!"); //$NON-NLS-1$
			return;
		}
		game.remove(nick);
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um das Spielfeld zu aktualisieren.
	 * Diese Methode ist für Zuschauer gedacht.
	 * @param obj
	 */
	private void HandleGameinfo(JSONObject obj) {
		//Is it complete
		if(obj.isNull("game")){
			logger.error("HandleGameinfo(JSONObject) - The JSONObject 'game' has to be there."); //$NON-NLS-1$
			return;
		}
		GameStub stub = getGameStub(obj.getJSONObject("game"));
		//Is it the game I'm currently in
		if(stub.getGameID() != model.getGame().getGameID()){
			logger.error("HandleGameinfo(JSONObject) - Im not even watching that game!"); //$NON-NLS-1$
			return;
		}
		//Refresh the GameStub
		model.getGames().put(stub.getGameID(), stub);
		model.getGame().setGameStub(stub);
		Platform.runLater(()->presenter.showGameField());
		
		
	}
	/**
	 * Wird von processJSON() ausgeführt um ein neuen Zuschauer zu handeln.
	 * Wird zu Watcher-Liste hinzugefügt.
	 * @param obj
	 */
	private void HandleWatcherAdded(JSONObject obj) {
		if(obj.isNull("gameID") || obj.isNull("nick")){
			logger.error("HandleWatcherAdded(JSONObject) - JSON not complete: " + obj); //$NON-NLS-1$
			return;
		}
		int gameID = obj.getInt("gameID");
		Nick nick = model.getNick(obj.getString("nick"));
		//Does such a Nick exist
		if(nick == null){
			logger.error("HandleWatcherAdded(JSONObject) - This Nick does not exist"); //$NON-NLS-1$
			return;
		}
		//Does Game exist
		GameStub game = model.getGames().get(gameID);
		if(game == null){
			logger.error("HandleWatcherAdded(JSONObject) - A Game with this ID does not exist!"); //$NON-NLS-1$
			return;
		}
		
		game.addWatcher(nick);
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um ein neuen Spieler(Join) zu handeln.
	 * Bei valider Eingabe wird er zur Spielerliste hinzugefügt im Modell und sein Name 
	 * aufgenommen.
	 * @param obj
	 */
	private void HandlePlayerAdded(JSONObject obj) {
		if(obj.isNull("gameID") || obj.isNull("player")){
			logger.error("HandlePlayerAdded(JSONObject) - JSON not complete: " + obj); //$NON-NLS-1$
			return;
		}
		int gameID = obj.getInt("gameID");
		
		Player player = getPlayerInfo(obj.getJSONObject("player"));
		if(player == null){
			logger.error("HandlePlayerAdded(JSONObject) - No game was created!"); //$NON-NLS-1$
			return;
		}
		//Does Game exist
		GameStub game = model.getGames().get(gameID);
		if(game == null){
			logger.error("HandlePlayerAdded(JSONObject) - A Game with this ID does not exist!"); //$NON-NLS-1$
			return;
		}
		if(player.getNick().getNick().equals(model.getNickname())){
			model.getGame().setColor(player.getColor());
		}
		game.addPlayer(player);
		//Workaround to update the table
		model.getGames().put(-1, new GameStub(-1, Gamestate.ENDED));
		model.getGames().remove(-1);
		
		//Is it in my current game
		if(gameID == model.getGame().getGameID()){
			model.getGame().getPlayers().get(player.getColor()).setNick(player.getNick());
		}
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um ein Spielerstellen zu handeln.
	 * Ein neues Spiel wird erstellt mit dem Status NOT_STARTED. Der Spieler der das Spiel erstellt hat
	 * wird der Spielerliste hinzugefügt.
	 * Das Spiel wird anschließend der Spielliste im ClientModell hinzugefügt.
	 * @param obj
	 */
	private void HandleGameCreated(JSONObject obj) {
		if(obj.isNull("gameID") || obj.isNull("player")){
			logger.error("HandleGameCreated(JSONObject) - JSON not complete: " + obj); //$NON-NLS-1$
			return;
		}
		int gameID = obj.getInt("gameID");
		
		Player player = getPlayerInfo(obj.getJSONObject("player"));
		if(player == null){
			logger.error("HandleGameCreated(JSONObject) - No game was created!"); //$NON-NLS-1$
			return;
		}
		GameStub game = new GameStub(gameID, Gamestate.NOT_STARTED);
		game.addPlayer(player);
		model.addGameStub(game); 
		
		
		if(player.getNick().getNick().equals(model.getNickname())){
			model.getGame().setGameStub(game);
			model.getGame().setColor(player.getColor());
		}
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um den Chat zu handeln.
	 * Falls es was zum anzeigen gibt, wird das nun angezeigt.
	 * @param obj
	 */
	private void HandleChat(JSONObject obj) {
		 Platform.runLater(() -> presenter.getchatPresenter().prepDisplay(obj));
		
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um ein Nick zu löschen, der
	 * früher im Spiel war.
	 * 
	 * @param obj
	 */
	private void HandleUserLeft(JSONObject obj) {
		if(obj.isNull("nick")){
			logger.error("HandleUserLeft(JSONObject) - Who exactly left???"); //$NON-NLS-1$
			return;
		}
		model.deleteNick(obj.getString("nick"));
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um einen neuen Nick zu handeln.
	 * Dieser Nick wird im Modell gespeichert.
	 * @param obj
	 */
	private void HandleUserAdded(JSONObject obj) {
		if(obj.isNull("nick")){
			logger.error("HandleUserAdded(JSONObject) - You have to give me a nickname to add him!"); //$NON-NLS-1$
			return;
		}
		model.addNick(new Nick(obj.getString("nick")));
		
	}
	
	/**
	 * Wird von processJSON() ausgeführt um ein erfolgreichen Login(?) zu handeln.
	 * Hier werden alle übergebenen Nicks, Expansions und Games in das Modell übernommen.
	 * @param obj
	 */
	private void HandleLoginSuccessful(JSONObject obj) {
		Platform.runLater(new Runnable(){
			@Override public void run(){
				presenter.loginSuccessful();
			}
		});
		
		//Check if it is complete
		if(obj.isNull("nick array")||obj.isNull("game array")){
			logger.error("HandleLoginSuccessful(JSONObject) - Login Successful is not complete"); //$NON-NLS-1$
			return;
		}
		//Write all compatible Expansions to the model
		if(obj.has("expansions")){
			JSONArray expansions = obj.getJSONArray("expansions");
			for(int i = 0; !expansions.isNull(i); i++){
				model.addCompatibleExpansions(expansions.getString(i));
			}
		}
		//Add Players
		JSONArray nicks = obj.getJSONArray("nick array");
		for(int i = 0; !nicks.isNull(i); i++){
			model.addNick(new Nick(nicks.getString(i)));
		}
		//Add Gamestubs
		JSONArray games = obj.getJSONArray("game array");
		for(int i = 0; !games.isNull(i); i++){

			GameStub stub = getGameStub(games.getJSONObject(i));
			if(stub == null){
				logger.error("HandleLoginSuccessful(JSONObject) - That was no real game."); //$NON-NLS-1$
				continue;
			}
			model.addGameStub(stub);
		}
	
		
	}
	
	/**
	 * Übergibt die Information eines Spielers, die vom Server angefragt wurde zurück,
	 * in Form von Nick, Color und Playerstate.
	 * @param playerObj
	 * @return
	 */
	private Player getPlayerInfo(JSONObject playerObj){
		if(playerObj.isNull("nick")||playerObj.isNull("color")){
			logger.error("getPlayerInfo(JSONObject) - The Playerinformation is not complete vgl.: 6.7"); //$NON-NLS-1$
			return null;
		}
		Nick nick = model.getNick(playerObj.getString("nick"));
		Color color = Color.getColor(playerObj.getString("color"));
		
		List<Playerstate> playerstate = new ArrayList<Playerstate>();
		if(playerObj.has("playerstate")){
			JSONArray arr = playerObj.getJSONArray("playerstate");
		
			for(int i = 0; !arr.isNull(i); i++){
				playerstate.add(Playerstate.getPlayerstate(arr.getString(i)));
			}
		}else{
			playerstate.add(Playerstate.DO_NOTHING);
		}
		if(nick == null || color == null){
			logger.error("getPlayerInfo(JSONObject) - This Nickname/ Color does not exist"); //$NON-NLS-1$
			return null;
		}
		
		return new Player(nick, color, playerstate);
	}
	
	/**
	 * Falls der Server dieses Paket(gameOBJ) übergibt, mit der Absicht ein Spiel zu starten, wird diese Methode ausgeführt.
	 * Es werden alle Spectators, Personen, Waffen hinzugefugt und auf ihre Startpositionen gesetzt.
	 * und die SpielReihenfolge erstellt.
	 * @param gameObj
	 * @return
	 */
	private GameStub getGameStub (JSONObject gameObj){
		if(gameObj.isNull("gameID")||gameObj.isNull("gamestate")||gameObj.isNull("players")||gameObj.isNull("watchers")||gameObj.isNull("person positions")||gameObj.isNull("weapon positions")){
			logger.error("getGameStub(JSONObject) - Gamestub was not complete"); //$NON-NLS-1$
			return null;
		}
		Gamestate state = Gamestate.getGamestate(gameObj.getString("gamestate"));
		if(state == null){
			logger.error("getGameStub(JSONObject) - This Client does not know the Gamestate: " + gameObj.getString("gamestate")); //$NON-NLS-1$ //$NON-NLS-2$
			return null;
		}
		GameStub game = new GameStub(gameObj.getInt("gameID"),state);
		
		//Add Players
		JSONArray players = gameObj.getJSONArray("players");
		game.getOrder().clear();
		for(int j = 0; !players.isNull(j); j++){
			Player player = getPlayerInfo(players.getJSONObject(j));
			if(player == null)
				continue;
			
			game.addPlayer(player);
			if(state == Gamestate.STARTED){
				game.addToOrder(player.getColor());
			}
			
		}
		
		//Add watchers
		JSONArray watchers = gameObj.getJSONArray("watchers");
		for(int j = 0; !watchers.isNull(j); j++){
			String watcher = watchers.getString(j);
			Nick nick = model.getNick(watcher);
			if(nick == null){
				logger.error("getGameStub(JSONObject) - This Nickname does not exist!"); //$NON-NLS-1$
				continue;
			}
			game.addWatcher(nick);
		}
		
		//Add Person Position
		JSONArray personPositions = gameObj.getJSONArray("person positions");
		for(int j = 0; !personPositions.isNull(j); j++){
			JSONObject position = personPositions.getJSONObject(j);
			if(position.isNull("person") || position.isNull("field")){
				logger.error("getGameStub(JSONObject) - The Person Position JSON was not complete"); //$NON-NLS-1$
				continue;
			}
			
			Color color = Color.getColor(position.getString("person"));
			if(color == null){
				logger.error("getGameStub(JSONObject) - This Color does not exist!"); //$NON-NLS-1$
				continue;
			}
			
			
			JSONObject field = position.getJSONObject("field");
			if(field.isNull("x") || field.isNull("y")){
				logger.error("getGameStub(JSONObject) - The field JSON is not complete"); //$NON-NLS-1$
				continue;
			}
			
			int x = field.getInt("x");
			int y = field.getInt("y");
			game.addPerson(color, new Key(x, y));
			
		}
		
		//Add Weapon Position
		JSONArray weaponPositions = gameObj.getJSONArray("weapon positions");
		for(int j = 0; !weaponPositions.isNull(j); j++){
			JSONObject weaponObj = weaponPositions.getJSONObject(j);
			if(weaponObj.isNull("weapon") || weaponObj.isNull("field")){
				logger.error("getGameStub(JSONObject) - The WeaponPosition JSON was not complete"); //$NON-NLS-1$
				continue;
			}
			Weapons weapon = Weapons.getWeapon(weaponObj.getString("weapon"));
			if(weapon == null){
				logger.error("getGameStub(JSONObject) - This Weapon does not exist: " + weaponObj.getString("weapon")); //$NON-NLS-1$ //$NON-NLS-2$
				continue;
			}
			JSONObject field = weaponObj.getJSONObject("field");
			if(field.isNull("x") || field.isNull("y")){
				logger.error("getGameStub(JSONObject) - The field JSON is not complete"); //$NON-NLS-1$
				continue;
			}
			
			int x = field.getInt("x");
			int y = field.getInt("y");
			game.addWeapon(weapon, new Key(x, y));
			
			
		}
		return game;
	
	}
	
	/**
	 * Der momentaner Presenter wird durch den paramter uberschrieben.
	 * @param presenter
	 */
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
		
	}
	
	/**
	 * GameHandler(Server) -> hier -> dothis()
	 * @param playerObj
	 */
	public void HandleSuperpower(JSONObject playerObj){
		if(playerObj.isNull("gameID")||playerObj.isNull("type")){
			logger.error("Fehlerhaftes JSON -> Verstecke würfel wieder"); //$NON-NLS-1$
			model.getScreens().getGameScreen().getTabMenu().setDice(false);
		
		} else {
			logger.info("superpower return arrived succesfully!");
		}
		boolean checkUsed = playerObj.getBoolean("used");
		if(!checkUsed) {
			model.getScreens().getGameScreen().getTabMenu().getNotesButton2().setVisible(false);
		}
	}
	
	private void HandleSuperpowerPurple(JSONObject obj) {
		if(obj.isNull("gameID")||obj.isNull("type")){
			logger.error("Fehlerhaftes JSON -> Verstecke würfel wieder"); //$NON-NLS-1$
			model.getScreens().getGameScreen().getTabMenu().setDice(false);
		
		} else {
			logger.info("superpower return arrived succesfully!");
		}
		
		new AudioClip(Constants.PUMUSIC).play();
		
	}
	

}











