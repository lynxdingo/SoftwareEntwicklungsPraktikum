Cluedo, the game
================

Within this application one can play *Cluedo*. Where one has the option to try 
to beat the AI or play with friends on a server. In both cases one needs to 
create a server to add players to it.

To start a server or an user interface, there are two different execution files.

Once a player is added one can choose if that player will be human or an AI. 
The player also has the option to choose their color and name.

###### Contribitors
Marcus Janik, Alexander Kenkenberg, Franjo Lukezic, Angela Minosi, An Ngo Tien, and Tim Spang